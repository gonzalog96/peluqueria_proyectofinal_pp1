package controllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;
import javax.swing.JButton;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.EstadoProducto;
import dto.ProductoDTO;
import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.administrador.producto.AddProductoController;
import presentacion.controlador.administrador.producto.EditProductoController;
import presentacion.controlador.administrador.producto.ProductoController;
import presentacion.controlador.administrador.profesional.AdministradorController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoControllerTest {
	private static ProductoController prContr;
	private static AddProductoController addPrContr;
	private static EditProductoController editPrContr;
	private static Peluqueria peluqueria;
	private static AdministradorController admin;

	@BeforeClass
	public static void init() {
		System.out.println("Iniciando ProductoControllerTest!");
		peluqueria = new Peluqueria(new DAOSQLFactory());
		admin = AdministradorController.getInstance(peluqueria);
		prContr = ProductoController.getInstance(peluqueria, admin);
		addPrContr = AddProductoController.getInstance(peluqueria, prContr);
		editPrContr = EditProductoController.getInstance(peluqueria, prContr);
	}
	
	@Test
	public void seleccionarTestTrue(){
		int seleccionado = prContr.seleccionar();
		assertEquals(seleccionado, -1);
	}

	@Test
	public void seleccionarTestFalse() {
		int seleccionado = prContr.seleccionar();
		assertFalse(seleccionado == 1);
	}
	
	@Test
	public void deleteProductoTestTrue() {
		JButton delete = prContr.getProductosVista().getBtnRemoveProducto();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		EstadoProducto estadoAntiguo = prContr.getProductos().get(0).getEstado();
		prContr.deleteProducto(evento, 0);
		EstadoProducto estadoNuevo = prContr.getProductos().get(0).getEstado();
		assertTrue(!estadoAntiguo.equals(estadoNuevo));
	}
	
	@Test
	public void deleteProductoTestFalse() {
		JButton delete = prContr.getProductosVista().getBtnRemoveProducto();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		
		assertFalse(prContr.deleteProducto(evento, 100));
	}
	
	@Test
	public void crearProductoTestTrue() {
		addPrContr.getAddProductoVista().getTxtNombre().setText("TEST");
		addPrContr.getAddProductoVista().getTxtPrecio().setText("10");
		addPrContr.getAddProductoVista().getTxtRubro().setText("TEST");
		addPrContr.getAddProductoVista().getComboEstado().setSelectedIndex(0);
		JButton confirmar = addPrContr.getAddProductoVista().getBtnConfirmar();
		ActionEvent evento = new ActionEvent(confirmar, 1001, "");
		addPrContr.crearProducto(evento);
		assertTrue(prContr.getProductos().size() != 0);
	}
	
	@Test
	public void validadorDeCamposTestTrue() {
		String nombre = "TEST";
		String precio = "100";
		String rubro = "TEST";
		assertTrue(addPrContr.validarCampos(nombre, precio, rubro));
		assertTrue(editPrContr.validarCampos(nombre, precio, rubro));
	}
	
	@Test
	public void validadorDeCamposTestNombreIncorrecto() {
		String nombre = "111";
		String precio = "100";
		String rubro = "TEST";
		assertFalse(addPrContr.validarCampos(nombre, precio, rubro));
		assertFalse(editPrContr.validarCampos(nombre, precio, rubro));
	}
	
	@Test
	public void validadorDeCamposTestPrecioIncorrecto() {
		String nombre = "TEST";
		String precio = "TEST";
		String rubro = "TEST";
		assertFalse(addPrContr.validarCampos(nombre, precio, rubro));
		assertFalse(editPrContr.validarCampos(nombre, precio, rubro));
	}
	@Test
	public void validadorDeCamposTestRubroIncorrecto() {
		String nombre = "TEST";
		String precio = "100";
		String rubro = "100";
		assertFalse(addPrContr.validarCampos(nombre, precio, rubro));
		assertFalse(editPrContr.validarCampos(nombre, precio, rubro));
	}
	
	@Test 
	public void mostrarAddServiciosVistaTest() {
		addPrContr.mostrarAddServicioVista();
	}
	
	@Test
	public void mostarEditServiciosVistaTest() {
		editPrContr.mostrarEditServicioVista();
	}
	
	@Test
	public void editarProductoTestTrue() {
		ProductoDTO prod = prContr.getProductos().get(0);
		editPrContr.setEditar(prod);
		JButton edit = editPrContr.getEditProductoVista().getBtnConfirmar();
		editPrContr.getEditProductoVista().getTxtNombre().setText("TEST EDIT");
		editPrContr.getEditProductoVista().getTxtPrecio().setText("1");
		editPrContr.getEditProductoVista().getTxtRubro().setText("TEST EDIT");
		
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertTrue(editPrContr.editarProducto(evento));
	}
	
	@Test
	public void editarProductoTestFalse() {
		ProductoDTO prod = prContr.getProductos().get(0);
		editPrContr.setEditar(prod);
		JButton edit = editPrContr.getEditProductoVista().getBtnConfirmar();
		editPrContr.getEditProductoVista().getTxtNombre().setText("11");
		editPrContr.getEditProductoVista().getTxtPrecio().setText("1");
		editPrContr.getEditProductoVista().getTxtRubro().setText("TEST EDIT");
		
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertFalse(editPrContr.editarProducto(evento));
	}
	
	
	
	
	
}



































