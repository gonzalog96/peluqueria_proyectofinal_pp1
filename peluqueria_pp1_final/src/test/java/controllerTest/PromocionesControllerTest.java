package controllerTest;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.controlador.administrador.promociones.PromocionesController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PromocionesControllerTest {
	private static Peluqueria peluqueria;
	private static PromocionesController promContr;
	private static AdministradorController admin;
	

	@BeforeClass
	public static void init() {
		System.out.println("Iniciando ProfesionalControllerTest!");
		peluqueria =  new Peluqueria(new DAOSQLFactory());
		admin = AdministradorController.getInstance(peluqueria);
		promContr = PromocionesController.getInstance(peluqueria, admin);
	}
	
	@Test
	public void activateInfoDePromocionTestTrue() {
		JButton buttonInfo = promContr.getPromocionesVista().getBtnInfo();
		ActionEvent evento = new ActionEvent(buttonInfo, 1001, "");
		
		assertTrue(promContr.activateInforDePromocion(evento, 0));
	}
	
	@Test
	public void activateInfoDePromocionTestFalse() {
		JButton buttonInfo = promContr.getPromocionesVista().getBtnInfo();
		ActionEvent evento = new ActionEvent(buttonInfo, 1001, "");
		
		assertFalse(promContr.activateInforDePromocion(evento, -1));
	}
	
//	@Test
//	public void deletePromTestTrue() {
//		JButton delete = promContr.getPromocionesVista().getBtnRemovePromocion();
//		ActionEvent evento = new ActionEvent(delete, 1001, "");
//		
//		assertTrue(promContr.deleteProm(evento, 0));
//	}
//	
	
	




	
	
	
	
}



































