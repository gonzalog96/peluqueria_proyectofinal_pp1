package controllerTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.ClienteDTO;
import dto.EstadoCliente;
import dto.EstadoUsuario;
import dto.RolUsuario;
import dto.SucursalDTO;
import dto.UsuarioDTO;
import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.administrativo.AdministrativoController;
import presentacion.controlador.administrativo.cliente.AddClientesController;
import presentacion.controlador.administrativo.cliente.ClientesController;
import presentacion.controlador.administrativo.cliente.EditClienteController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClienteControllerTest {
	private static ClientesController clienteContr;
	private static AddClientesController addContr;
	private static EditClienteController editContr;
	private static AdministrativoController admin;
	private static Peluqueria peluqueria;
	private static UsuarioDTO usuario;

	@BeforeClass
	public static void init() {
		System.out.println("Iniciando ProductoControllerTest!");
		peluqueria = new Peluqueria(new DAOSQLFactory());
		SucursalDTO sucursal = new SucursalDTO(0, "TEST", "test", "test", 123);
		usuario = new UsuarioDTO(0, "user", "test", "test", "123", "test@test.com", "test", RolUsuario.ADMINISTRATIVO, sucursal, EstadoUsuario.INACTIVO);
		admin = AdministrativoController.getInstance(peluqueria, usuario);
		clienteContr = ClientesController.getInstance(peluqueria, admin);
		addContr = AddClientesController.getInstance(peluqueria, clienteContr);
		editContr = EditClienteController.getInstance(peluqueria, clienteContr);
	}
	
	@Test 
	public void deleteTestTrue() {
		JButton delete = clienteContr.getClientesVista().getBtnCambiarEstadoCliente();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		EstadoCliente estadoAntiguo = clienteContr.getClientes().get(0).getEstado();
		clienteContr.deleteCliente(evento, 0);
		EstadoCliente estadoNuevo = clienteContr.getClientes().get(0).getEstado();
		assertTrue(!estadoAntiguo.equals(estadoNuevo));
	}
	
	@Test
	public void deleteTestFalse() {
		JButton delete = clienteContr.getClientesVista().getBtnCambiarEstadoCliente();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		assertFalse(clienteContr.deleteCliente(evento, 600));
	}
	
	@Test
	public void crearClienteTestTrue() {
		JButton conf = addContr.getAddClienteVista().getBtnAgregarProfesional();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addContr.getAddClienteVista().getTxtNombre().setText("TEST");
		addContr.getAddClienteVista().getTxtApellido().setText("TEST");
		addContr.getAddClienteVista().getTxtDni().setText("123");
		addContr.getAddClienteVista().getTxtEmail().setText("TEST@test.com");
		addContr.getAddClienteVista().getTxtTelefono().setText("123");
		
		assertTrue(addContr.crearCliente(evento));
	}
	
	@Test
	public void crearClienteTestFalse() {
		JButton conf = addContr.getAddClienteVista().getBtnAgregarProfesional();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addContr.getAddClienteVista().getTxtNombre().setText("12");
		addContr.getAddClienteVista().getTxtApellido().setText("12");
		addContr.getAddClienteVista().getTxtDni().setText("as");
		addContr.getAddClienteVista().getTxtEmail().setText("est.com");
		addContr.getAddClienteVista().getTxtTelefono().setText("as");
		
		assertFalse(addContr.crearCliente(evento));
	}
	
	@Test
	public void editarProfesionalTestTrue() {
		ClienteDTO prof = clienteContr.getClientes().get(0);
		editContr.setEditar(prof);
		JButton edit = editContr.getEditProfVista().getBtnConfirmar();
		
		editContr.getEditProfVista().getTxtNombre().setText("TEST EDIT");
		editContr.getEditProfVista().getTxtApellido().setText("TEST EDIT");
		editContr.getEditProfVista().getTxtEmail().setText("test@edit.com");
		editContr.getEditProfVista().getTxtTelefono().setText("123");
		editContr.getEditProfVista().getTxtDni().setText("123");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertTrue(editContr.editarCliente(evento));
	}
	
	@Test
	public void editarProfesionalTestTrueFalse() {
		ClienteDTO prof = clienteContr.getClientes().get(0);
		editContr.setEditar(prof);
		JButton edit = editContr.getEditProfVista().getBtnConfirmar();
		
		editContr.getEditProfVista().getTxtNombre().setText("T12");
		editContr.getEditProfVista().getTxtApellido().setText("TEST 12EDIT");
		editContr.getEditProfVista().getTxtEmail().setText("tesedit.com");
		editContr.getEditProfVista().getTxtTelefono().setText("123as");
		editContr.getEditProfVista().getTxtDni().setText("12as3");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertFalse(editContr.editarCliente(evento));
	}
}



































