package controllerTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;
import javax.swing.JButton;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.EstadoProfesional;
import dto.ProductoDTO;
import dto.ProfesionalDTO;
import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.administrador.profesional.AddProfesionalController;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.controlador.administrador.profesional.EditProfesionalController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProfesionalControllerTest {
	private static AdministradorController admin;
	private static Peluqueria peluqueria;
	private static EditProfesionalController editPrfContr;
	private static AddProfesionalController addPrfContr;
	

	@BeforeClass
	public static void init() {
		System.out.println("Iniciando ProfesionalControllerTest!");
		peluqueria =  new Peluqueria(new DAOSQLFactory());
		admin = AdministradorController.getInstance(peluqueria);
		editPrfContr = EditProfesionalController.getInstance(peluqueria, admin);
		addPrfContr = AddProfesionalController.getInstance(peluqueria, admin);
	}
	
	@Test
	public void agregarServicioAProfesionalTestTrue(){
		JButton addServicio = admin.getAdministradorVista().getBtnAddServicioProfesional();
		ActionEvent evento = new ActionEvent(addServicio, 1001, "");
		
		assertTrue(admin.agregarServicioAprofesional(evento, 1));
	}
	
	@Test
	public void agregarServicioAProfesionalTestFalse(){
		JButton addServicio = admin.getAdministradorVista().getBtnAddServicioProfesional();
		ActionEvent evento = new ActionEvent(addServicio, 1001, "");
		
		assertFalse(admin.agregarServicioAprofesional(evento, 600));
	}
	
	@Test
	public void activateVistasTest(){
		JButton buttonBBDD = admin.getAdministradorVista().getBtnBaseDeDatos();
		JButton buttonProd = admin.getAdministradorVista().getBtnProductos();
		JButton buttonServ = admin.getAdministradorVista().getBtnServicios();
		JButton buttonEdit = admin.getAdministradorVista().getBtnEditProfesional();
		JButton buttonAdd = admin.getAdministradorVista().getBtnAddProfesional();
		
		ActionEvent z = new ActionEvent(buttonBBDD, 1001, "");
		ActionEvent xz = new ActionEvent(buttonProd, 1001, "");
		ActionEvent x = new ActionEvent(buttonServ, 1001, "");
		ActionEvent p = new ActionEvent(buttonEdit, 1001, "");
		ActionEvent e = new ActionEvent(buttonAdd, 1001, "");
		
		admin.activateBBDDVista(z);
		admin.activateProductosVista(xz);
		admin.activatePromocionesVista();
		admin.activateServiciosVista(x);
		admin.actualizarTablaProfesionales();
		admin.activateEditProfesionalVista(p);
		admin.activateAddProfesionalVista(e);
		
	}
	
	@Test
	public void activateVistasEditTest() {
		editPrfContr.mostrarEditProfesionalVista();
		editPrfContr.actualizarComboSucursal();
	}
	
	@Test
	public void seleccionarTest() {
		assertTrue(admin.seleccionar() == -1);
	}

	@Test
	public void deleteProfTestTrue() {
		JButton delete = admin.getAdministradorVista().getBtnRemoveProfesional();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		EstadoProfesional estadoAntiguo = admin.getProfesionalesEnTabla().get(0).getEstado();
		admin.deleteProf(evento, 0);
		EstadoProfesional estadoNuevo = admin.getProfesionalesEnTabla().get(0).getEstado();
		assertTrue(!estadoAntiguo.equals(estadoNuevo));
	}
	
	@Test
	public void deleteProfTestFalse() {
		JButton delete = admin.getAdministradorVista().getBtnRemoveProfesional();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		assertFalse(admin.deleteProf(evento, 600));
	}
	
	@Test
	public void editarProfesionalTestTrue() {
		ProfesionalDTO prof = admin.getProfesionalesEnTabla().get(0);
		editPrfContr.setEditar(prof);
		JButton edit = editPrfContr.getEditProfVista().getBtnConfirmar();
		
		editPrfContr.getEditProfVista().getTxtNombre().setText("TEST EDIT");
		editPrfContr.getEditProfVista().getTxtApellido().setText("TEST EDIT");
		editPrfContr.getEditProfVista().getTxtEmail().setText("test@edit.com");
		editPrfContr.getEditProfVista().getTxtTelefono().setText("123");
		editPrfContr.getEditProfVista().getTxtDni().setText("123");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertTrue(editPrfContr.editarProfesional(evento));
	}
	
	@Test
	public void editarProfesionalTestFalse() {
		ProfesionalDTO prof = admin.getProfesionalesEnTabla().get(0);
		editPrfContr.setEditar(prof);
		JButton edit = editPrfContr.getEditProfVista().getBtnConfirmar();
		
		editPrfContr.getEditProfVista().getTxtNombre().setText("11");
		editPrfContr.getEditProfVista().getTxtApellido().setText("TEST EDIT");
		editPrfContr.getEditProfVista().getTxtEmail().setText("test@edit.com");
		editPrfContr.getEditProfVista().getTxtTelefono().setText("123");
		editPrfContr.getEditProfVista().getTxtDni().setText("123");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertFalse(editPrfContr.editarProfesional(evento));
	}
	
	@Test
	public void validarProfesionalTestTrue() {
		ProfesionalDTO profesional = new ProfesionalDTO("TEST", "TEST", "TEST@TEST.COM", "11221", "12", null, EstadoProfesional.ACTIVO);
		assertTrue(editPrfContr.validarCampos(profesional));
	}
	
	@Test
	public void validarProfesionalTestFalse() {
		ProfesionalDTO profesional = new ProfesionalDTO("11", "11", "TESTOM", "asd", "asd", null, EstadoProfesional.ACTIVO);
		assertFalse(editPrfContr.validarCampos(profesional));
	}

	@Test
	public void crearProfesionalTestTrue() {
		JButton conf = addPrfContr.getAddProfVista().getBtnAgregarProfesional();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addPrfContr.getAddProfVista().getTxtNombre().setText("TEST");
		addPrfContr.getAddProfVista().getTxtApellido().setText("TEST");
		addPrfContr.getAddProfVista().getTxtDni().setText("123");
		addPrfContr.getAddProfVista().getTxtEmail().setText("TEST@test.com");
		addPrfContr.getAddProfVista().getTxtTelefono().setText("123");
		
		assertTrue(addPrfContr.crearProfesional(evento));
	}
	
	@Test
	public void crearProfesionalTestFalse() {
		JButton conf = addPrfContr.getAddProfVista().getBtnAgregarProfesional();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addPrfContr.getAddProfVista().getTxtNombre().setText("12");
		addPrfContr.getAddProfVista().getTxtApellido().setText("12");
		addPrfContr.getAddProfVista().getTxtDni().setText("as");
		addPrfContr.getAddProfVista().getTxtEmail().setText("est.com");
		addPrfContr.getAddProfVista().getTxtTelefono().setText("as");
		
		assertFalse(addPrfContr.crearProfesional(evento));
	}
	
	
	
	
	
	
	
	
	
	




	
	
	
	
}



































