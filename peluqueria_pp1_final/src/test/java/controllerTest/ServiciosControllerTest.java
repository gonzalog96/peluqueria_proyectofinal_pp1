package controllerTest;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.ProfesionalDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.controlador.administrador.promociones.PromocionesController;
import presentacion.controlador.administrador.servicios.AddServicioController;
import presentacion.controlador.administrador.servicios.EditServicioController;
import presentacion.controlador.administrador.servicios.ServiciosController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServiciosControllerTest {
	private static Peluqueria peluqueria;
	private static AdministradorController admin;
	private static ServiciosController serContr;
	private static EditServicioController editServContr;
	private static AddServicioController addServContr;

	@BeforeClass
	public static void init() {
		System.out.println("Iniciando ServicioControllerTest!");
		peluqueria =  new Peluqueria(new DAOSQLFactory());
		admin = AdministradorController.getInstance(peluqueria);
		serContr = ServiciosController.getInstance(peluqueria, admin);
		editServContr = EditServicioController.getInstance(peluqueria, serContr);
		addServContr = AddServicioController.getInstance(peluqueria, serContr);
	}

	@Test
	public void deleteServTestTrue() {
		JButton delete = serContr.getServiciosVista().getBtnRemoveServicio();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		
		assertTrue(serContr.deleteServ(evento, 1));
	}
	
	@Test
	public void deleteServTestFalse() {
		JButton delete = serContr.getServiciosVista().getBtnRemoveServicio();
		ActionEvent evento = new ActionEvent(delete, 1001, "");
		
		assertFalse(serContr.deleteServ(evento, -1));
	}
	
	@Test
	public void editServTestTrue() {
		ServicioDTO serv = serContr.getServicios().get(0);
		editServContr.setEditar(serv);
		JButton edit = editServContr.getEditProfVista().getBtnConfirmar();
		
		editServContr.getEditProfVista().getTxtNombre().setText("TEST EDIT");
		editServContr.getEditProfVista().getTxtDuracion().setText("10");
		editServContr.getEditProfVista().getTxtPrecio().setText("10");
		editServContr.getEditProfVista().getTxtPuntos().setText("123");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertTrue(editServContr.editarServicio(evento));
	}
	
	@Test
	public void editServTestFalse() {
		ServicioDTO serv = serContr.getServicios().get(0);
		editServContr.setEditar(serv);
		JButton edit = editServContr.getEditProfVista().getBtnConfirmar();
		
		editServContr.getEditProfVista().getTxtNombre().setText("12");
		editServContr.getEditProfVista().getTxtDuracion().setText("as10");
		editServContr.getEditProfVista().getTxtPrecio().setText("10as");
		editServContr.getEditProfVista().getTxtPuntos().setText("123as");
		
		ActionEvent evento = new ActionEvent(edit, 1001, "");

		assertFalse(editServContr.editarServicio(evento));
	}
	
	
	@Test
	public void activateEditServicioVistaTestTrue() {
		JButton edit = serContr.getServiciosVista().getBtnEditServicio();
		ActionEvent evento = new ActionEvent(edit, 1001, "");
		assertFalse(serContr.activateEditServicioVista(evento, -1));
		serContr.mostrarServiciosVista();
	}
	
	@Test
	public void crearServicioTestTrue() {
		JButton conf = addServContr.getAddEditProfVista().getBtnConfirmar();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addServContr.getAddEditProfVista().getTxtNombre().setText("TEST");
		addServContr.getAddEditProfVista().getTxtDuracion().setText("100");
		addServContr.getAddEditProfVista().getTxtPrecio().setText("123");
		addServContr.getAddEditProfVista().getTxtPuntos().setText("10");
		
		assertTrue(addServContr.crearServicio(evento));
	}
	
	@Test
	public void crearServicioTestFalse() {
		JButton conf = addServContr.getAddEditProfVista().getBtnConfirmar();
		ActionEvent evento = new ActionEvent(conf, 1001, "");
		
		addServContr.getAddEditProfVista().getTxtNombre().setText("12");
		addServContr.getAddEditProfVista().getTxtDuracion().setText("as");
		addServContr.getAddEditProfVista().getTxtPrecio().setText("12a3");
		addServContr.getAddEditProfVista().getTxtPuntos().setText("1s0");
		
		assertFalse(addServContr.crearServicio(evento));
	}
	
	
	
	




	
	
	
	
}



































