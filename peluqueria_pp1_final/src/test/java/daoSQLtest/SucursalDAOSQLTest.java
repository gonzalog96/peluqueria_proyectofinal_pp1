package daoSQLtest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.SucursalDTO;
import persistencia.dao.mysql.SucursalDAOSQL;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SucursalDAOSQLTest {
	SucursalDTO sucursal;
	SucursalDAOSQL daosql;
	
	public void variables() {
		daosql = new SucursalDAOSQL();
		sucursal = new SucursalDTO(1, "pepepelo", "espaniol", "velazquez", 1635);
	}
	@Test
	public void test1ReadAll() {
		variables();
		List<SucursalDTO> lista = daosql.readAll();
		assertTrue(lista.size()>0);
	}
	@Test
	public void test2ObtenerDesdeId() {
		variables();
		SucursalDTO test = daosql.obtenerDesdeID(1);
		assertTrue(test.equals(sucursal));
	}
	
}
