package daoSQLtest;

import static org.junit.Assert.assertTrue;

import java.sql.Time;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.ClienteDTO;
import dto.DetalleTurnoDTO;
import dto.EstadoCliente;
import dto.EstadoProfesional;
import dto.EstadoTurno;
import dto.ProfesionalDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import dto.SucursalDTO;
import dto.TurnoDTO;
import persistencia.dao.mysql.TurnoDAOSQL;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TurnoDAOSQLtest {
	TurnoDAOSQL daosql;
	EstadoTurno estado;
	TurnoDTO prueba;
	ClienteDTO cliente;
	List<DetalleTurnoDTO> detalles;
	PromocionDTO promo;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

	String date = "28/01/2020";

	// convert String to LocalDate
	
	LocalDate localDate = LocalDate.parse(date, formatter);

	@SuppressWarnings("deprecation")
	public void variables() {
		daosql = new TurnoDAOSQL();
		detalles = new ArrayList<DetalleTurnoDTO>();
		Time horaInicio = new Time(18,0, 0);
		Time horaFin = new Time(18,30, 0);
		Time horaFin2 = new Time(19,00, 0);
		
		ServicioDTO servicio1 = new ServicioDTO(1, "corte", 1000, 30, 300);
		SucursalDTO sucursal = new SucursalDTO(1, "pepepelo", "espaniol", "velazquez", 1635);
		ProfesionalDTO prof = new ProfesionalDTO(1, "pedro", "sanchez", "pedrito@gmail.com", "252525", "382348871", sucursal, EstadoProfesional.ACTIVO);
		promo = new PromocionDTO(1, "sin promocion", null, null, 0, null);
		//insert into cliente(nombre,apellido,dni,email,telefono,estado_cliente,ultima_visita) values("julian","rodriguez","38888871","julian@gmail.com",46673857,"ACTIVO","2019-10-27");
		String date = "27/10/2019";
		estado = EstadoTurno.OCUPADO;
		// convert String to LocalDate
		LocalDate fechaCliente = LocalDate.parse(date, formatter);
		cliente = new ClienteDTO(1, "julian", "rodriguez", "julian@gmail.com", "46673857", "38888857", EstadoCliente.ACTIVO, fechaCliente,100);
		
		DetalleTurnoDTO detalle = new DetalleTurnoDTO(0, horaInicio, horaFin, prof, servicio1); 
		DetalleTurnoDTO detalle2 = new DetalleTurnoDTO(0, horaFin, horaFin2, prof, servicio1); 
		detalles.add(detalle);
		detalles.add(detalle2);
		
		prueba = new TurnoDTO(0, localDate, horaInicio, 0, 0, 1, cliente, detalles, sucursal, estado, promo);
}
	@Test
	public void test1Insert() {
		variables();
		assertTrue(daosql.insert(prueba));
	}
	@Test
	public void test2ReadAll() {
		variables();
		List<TurnoDTO> turnos = daosql.readAll();
		assertTrue(turnos.size()>0);
	}
	@Test
	public void test3ReadAll() {
		variables();
		List<TurnoDTO> turnos = daosql.readAll(1);
		assertTrue(turnos.get(1).getSucursal().getId()==1);
	}
	@Test
	public void test4ReadAll() {
		variables();
		String date = "16/11/2019";
		LocalDate localDate = LocalDate.parse(date, formatter);
		List<TurnoDTO> turnos = daosql.readAll(1,LocalDate.now());
		
		assertTrue(turnos.size()>0);
	}
	@Test
	public void test5ReadAll() {
		variables();
		
		List<TurnoDTO> turnos = daosql.getTurnosDelDia(LocalDate.now());
		assertTrue(turnos.size()>=0);
	}
	@Test
	public void test6ObtenerDesdeId() {
		variables();
		int id = daosql.ultimoId();
		TurnoDTO test = daosql.obtenerDesdeID(id);
		assertTrue(!test.equals(null));
	}
	@Test 
	public void test7CancelarTurno() {
		variables();
		int id = daosql.ultimoId();
		daosql.cancelarTurno(id);
		TurnoDTO test = daosql.obtenerDesdeID(id);
		assertTrue(test.getEstado_turno().equals(EstadoTurno.CANCELADO));
	}
	@Test 
	public void test8Update() {
		variables();
		int id = daosql.ultimoId();
		TurnoDTO test = daosql.obtenerDesdeID(id);
		test.setEstado_turno(EstadoTurno.CERRADO);
		daosql.update(test);
		test = daosql.obtenerDesdeID(id);
		assertTrue(test.getEstado_turno().equals(EstadoTurno.CERRADO));
	}
	@Test
	public void test9Delete() {
		variables();
		List<TurnoDTO> turnos = daosql.readAll();
		daosql.delete(turnos.get(turnos.size()-1));
		List<TurnoDTO> test = daosql.readAll();
		assertTrue(turnos.size()>test.size());
	}
	@Test
	public void test91deuda() {
		variables();
		cliente.setId(7);
		List<TurnoDTO> lista = daosql.getTurnosDeuda(cliente);
		assertTrue(lista.size()>0);
	}
	
	@Test 
	public void test92avisos() {
		variables();
		List<TurnoDTO> turnos = daosql.getAvisos();
		assertTrue(turnos.size()>=0);
	}
//	@Test
//	public void test93avisos() {
//		variables();
//		List<TurnoDTO> turnos = daosql.getAvisos();
//		System.out.println("ID"+turnos.get(0).getId());
//		int length = turnos.size();
//		daosql.updateAviso(turnos.get(0).getId());
//		turnos = daosql.getAvisos();
//		assertTrue(turnos.size()>=0);
//	}
	@Test
	public void test94() {
		variables();
		TurnoDTO turno = daosql.obtenerDesdeID(9);
	}

}
