//package daoSQLtest;
//
//import static org.junit.Assert.*;
//
//import java.util.List;
//
//import org.junit.FixMethodOrder;
//import org.junit.Test;
//import org.junit.runners.MethodSorters;
//
//import dto.EstadoProducto;
//import dto.ProductoDTO;
//import dto.StockDTO;
//import persistencia.dao.mysql.ProductoDAOSQL;
//
//
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//public class ProductoDAOSQLTest {
//	private ProductoDAOSQL daosql;
//	private ProductoDTO producto;
//	
//	private void variables() {
//		daosql = new ProductoDAOSQL();
//		producto = new ProductoDTO(1, "shampoo sedal", "shampoo", 100, EstadoProducto.ACTIVO);
//	}
//	@Test
//	public void test1Insert() {
//		variables();
//		assertTrue(daosql.insert(producto));
//	}
//	
//	@Test
//	public void test2Readall() {
//		variables();
//		List<ProductoDTO> lista = daosql.readAll();
//		assertTrue(lista.size()>0);
//	}
//	@Test
//	public void test3Update() {
//		variables();
//		producto.setId(daosql.getUltimoId());
//		daosql.update(producto);
//		List<ProductoDTO> lista = daosql.readAll();
//		assertTrue(lista.get(lista.size()-1).equals(producto));
//	}
//	@Test
//	public void test4ObtenerStock() {
//		variables();
//		int stock = daosql.obtenerStock(producto, 1);
//		assertTrue(stock>=0);
//	}
//	
//	
//	@Test
//	public void test5obtenerStock() {
//		variables();
//		List<StockDTO> lista = daosql.readAllStock();
//		System.out.println(lista.get(0).toString());
//		assertTrue(lista.size()>0);
//	}
//	
//	@Test
//	public void test6obtenerStock() {
//		variables();
//		List<StockDTO> lista = daosql.readAllStock(1);
//		assertTrue(lista.size()>0);
//	}
//	
//	@Test
//	public void insertStockTestTrue() {
//		variables();
//		daosql.deleteStock(producto, 2);
//		assertTrue(daosql.insertStock(producto, 2, 3));
//	}
//	
//	@Test
//	public void insertStockTestFalse() {
//		variables();
//		assertFalse(daosql.insertStock(producto, 2, 3));
//	}
//	
//	@Test
//	public void updateStockTestTrue() {
//		variables();
//		assertTrue(daosql.updateStock(producto, 2, 6));
//	}
//
//}
