//package daoSQLtest;
//
//import static org.junit.Assert.assertTrue;
//
//import org.junit.Test;
//
//import dto.EstadoUsuario;
//import dto.RolUsuario;
//import dto.SucursalDTO;
//import dto.UsuarioDTO;
//import persistencia.dao.mysql.UsuarioDAOSQL;
//
//public class UsuarioDAOSQLtest {
//	UsuarioDAOSQL daosql;
//	UsuarioDTO user;
//
//	public void variables() {
//		daosql = new UsuarioDAOSQL();
//		SucursalDTO sucursal = new SucursalDTO(1, "pepepelo", "espaniol", "velazquez", 1635);
//		user = new UsuarioDTO(1, "test", "test", "test", "1", "test", "test", RolUsuario.ADMINISTRADOR, sucursal,
//				EstadoUsuario.ACTIVO);
//	}
//	
//	@Test
//	public void insert() {
//		variables();
//		assertTrue(daosql.insert(user));
//	}
//	@Test
//	public void getUserTest() {
//		variables();
//		user = daosql.getUser("churrok", "root");
//		assertTrue(!user.equals(null));
//	}
//}
