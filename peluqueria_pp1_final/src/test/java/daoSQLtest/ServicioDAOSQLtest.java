package daoSQLtest;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.ServicioDTO;
import persistencia.dao.mysql.ServicioDAOSQL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServicioDAOSQLtest {

	ServicioDAOSQL daosql;
	ServicioDTO servicio;

	public void variables() {
		daosql = new ServicioDAOSQL();
		servicio = new ServicioDTO(0, "test", 1, 1, 1);
	}

	@Test
	public void test1Insert() {
		variables();
		assertTrue(daosql.insert(servicio));
	}
	@Test
	public void test2ReadAllTest() {
		variables();
		List<ServicioDTO> lista = daosql.readAll();
		assertTrue(lista.get(lista.size()-1).equals(servicio));
	}
	@Test
	public void test3ObtenerDesdeId() {
		variables();
		ServicioDTO test = daosql.obtenerDesdeID(daosql.ultimoId());
		assertTrue(servicio.equals(test));
	}
	@Test
	public void test4Update() {
		variables();
		servicio.setId(daosql.ultimoId());
		servicio.setNombre("test2");
		System.out.println(servicio.toString());
		daosql.update(servicio);
		ServicioDTO test = daosql.obtenerDesdeID(daosql.ultimoId());
		System.out.println(test.toString());
		assertTrue(test.equals(servicio));
		
	}

	@Test
	public void obtenerDesdeProfesionalTest() {
		variables();
		assertTrue(daosql.obtenerDesdeProfesional(1).size() >= 0);
	}
	
	@Test 
	public void test8Delete() {
		variables();
		List<ServicioDTO> lista = daosql.readAll();
		daosql.delete(lista.get(lista.size()-1));
		List<ServicioDTO> test = daosql.readAll();
		assertTrue(lista.size()>test.size());
	}
}
