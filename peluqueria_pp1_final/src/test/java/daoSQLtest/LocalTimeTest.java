//package daoSQLtest;
//
//import static org.junit.Assert.*;
//
//import java.sql.Time;
//import java.time.LocalDate;
//import java.time.LocalTime;
//import java.time.format.DateTimeFormatter;
//import java.util.List;
//
//import org.junit.Test;
//
//import dto.DetalleTurnoDTO;
//import dto.EstadoTurno;
//import dto.ProfesionalDTO;
//import dto.TurnoDTO;
//import modelo.Peluqueria;
//import persistencia.dao.interfaz.DAOAbstractFactory;
//import persistencia.dao.mysql.DAOSQLFactory;
//import persistencia.dao.mysql.TurnoDAOSQL;
//
//public class LocalTimeTest {
//	DAOAbstractFactory dao;
//	TurnoDAOSQL dao2;
//	Peluqueria peluqueria;
//	
//	
//	public boolean validarHorarioProfesional(ProfesionalDTO profesional,Time horaInicio, Time horaFin, LocalDate fecha) {
//		variables();
//		List<TurnoDTO> turnos = peluqueria.obtenerTurnosDia(1, fecha);
//		boolean libre = true;
//		for (TurnoDTO turnoDTO : turnos) {
//			if(turnoDTO.getEstado_turno().equals(EstadoTurno.OCUPADO)) {
//				for (DetalleTurnoDTO detalle : turnoDTO.getDetalles()) {
//					if(detalle.getProfesional().getId() == profesional.getId()) {
//						
//						if(horaInicio.compareTo(detalle.getHoraInicio())>=0 && horaInicio.compareTo(detalle.getHoraFin())<=0) {
//							System.out.println("hora inicio entre el detalle" + detalle.getHoraInicio());
//							return false;
//						}
//						if(horaFin.compareTo(detalle.getHoraFin())>=0 && horaFin.compareTo(detalle.getHoraFin())<=0) {
//							System.out.println("hora fin entre del detalle" + detalle.getHoraFin());
//							return false;
//						}
//					}
//				}
//			}
//		}
//		return libre;
//	}
//	public void variables(){
//		dao = new DAOSQLFactory();
//		dao2 = new TurnoDAOSQL();
//		peluqueria = new Peluqueria(dao);
//	}
//	
//	@Test
//	public void test() {
//		variables();
//		ProfesionalDTO prof = peluqueria.getProfesionalDesdeID(1);
//		Time horaInicio = new Time(18, 30, 0);
//		Time horaFin = new Time(19, 00, 0);
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
//
//		String date = "28/11/2019";
//
//		// convert String to LocalDate
//		
//		LocalDate localDate = LocalDate.parse(date, formatter);
//		System.out.println(validarHorarioProfesional(prof,horaInicio,horaFin,localDate));
//	}
//	@Test
//	public void test2() {
//		variables();
//		List<TurnoDTO> lista = dao2.getAvisos();
//		System.out.println(lista.get(0).toString());
//		assertTrue(lista.size()>0);
//	}
//	@Test
//	public void test3() {
//		variables();
//		dao2.cancelarTurnoAviso(2);
//		List<TurnoDTO> lista = dao2.getAvisos();
//		System.out.println(lista.get(1).toString());
//		assertTrue(lista.size()>1);
//	}
//	@Test
//	public void test4() {
//		System.out.println((long) 3.6e+5);
//	}
//
//}
