package daoSQLtest;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.EstadoProfesional;
import dto.ProfesionalDTO;
import dto.ServicioDTO;
import dto.SucursalDTO;
import persistencia.dao.mysql.ProfesionalDAOSQL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProfesionalDAOSQLtest {
	ProfesionalDAOSQL daosql;
	EstadoProfesional estado;
	ProfesionalDTO profesional;
	ServicioDTO servicio;
	SucursalDTO sucu;

	public void variables() {
		//insert into sucursal(nombre,idioma,calle,altura) values("pepepelo","espaniol","velazquez",1635);
		
		daosql = new ProfesionalDAOSQL();
		servicio = new ServicioDTO(1, "corte" , 1000, 30,300);
		estado = EstadoProfesional.ACTIVO;
		sucu = new SucursalDTO(1, "pepepelo", "espaniol", "velazquez", 1635);
		profesional = new ProfesionalDTO(0, "test", "test", "pedrito@gmail.com", "252525", "38888871", sucu,
				EstadoProfesional.ACTIVO);
	}
	@Test
	public void test1Insert() {
		variables();
		assertTrue(daosql.insert(profesional));
	}
	
	
	@Test
	public void test2ObtenerDesdeIDTest() {
		variables();
		ProfesionalDTO prof = daosql.obtenerDesdeID(daosql.ultimoId());
		assertTrue(prof.equals(profesional));
	}
	
	@Test
	public void test3ReadAllTest() {
		variables();
		List<ProfesionalDTO> lista = daosql.readAll();
		assertTrue(lista.size()>0);

	}
	@Test
	public void test4Update() {
		variables();
		profesional = daosql.obtenerDesdeID(daosql.ultimoId());
		System.out.println(profesional.toString());
		profesional.setNombre("xxx");
		daosql.update(profesional);
		ProfesionalDTO prof = daosql.obtenerDesdeID(daosql.ultimoId());
		System.out.println(prof.toString());
		assertTrue(prof.equals(profesional));
	}
//	@Test
//	public void test5AtarProfesionalServicio() {
//		variables();
//		prueba = daosql.obtenerDesdeID(daosql.ultimoId());
//		daosql.atarProfesionaServicio(servicio, prueba);
//		assertTrue(daosql.tieneHabilidad(servicio.getId(),prueba.getId()));
//	}
//	@Test
//	public void test6Find() {
//		variables();
//		prueba = daosql.find("xxx");
//		assertTrue(!prueba.equals(null));
//	}
	@Test 
	public void test7Find() {
		variables();
		profesional = daosql.find("xxx",1);
		assertTrue(!profesional.equals(null));
	}
//	@Test
//	public void test8Find() {
//		variables();
//		prueba = daosql.find("xxx",1,1);
//		assertTrue(!prueba.equals(null));
//	}
	@Test
	public void test9Delete() {
		variables();
		List<ProfesionalDTO> lista = daosql.readAll();
		daosql.delete(lista.get(lista.size()-1));
		List<ProfesionalDTO> test = daosql.readAll();
		assertTrue(lista.size()>test.size());
	}
	
	@Test
	public void readAllXsucursalTest() {
		variables();
		assertTrue(daosql.readAll(2).size() >= 0);
	}
	
	
	
}

























