package daoSQLtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.CajaDTO;
import persistencia.dao.mysql.CajaDAOSQL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CajaDAOSQLTest
{
	private static CajaDAOSQL cajaDao;
	private CajaDTO pago;
	
	@BeforeClass
	public static void init() {
		cajaDao = new CajaDAOSQL();
	}
	
	@Test
	public void a_insertPagoTest() {
		pago = new CajaDTO(9999, LocalDate.of(2019, 11, 03), "ingresos", "efectivo", 1, 2, 1, "ninguno", new BigDecimal(100));
		assertTrue(cajaDao.insert(pago));
	}
	
	@Test
	public void c_getMontoTotalIngresosPorSucursalTest() {
		int sucursalID = 1;
		assertEquals(cajaDao.getTotalesPorSucursalID(sucursalID, "INGRESOS"), new BigDecimal(1200));
	}
	
	@Test
	public void d_getMontoTotalEgresosPorSucursalTest() {
		int sucursalID = 1;
		assertEquals(cajaDao.getTotalesPorSucursalID(sucursalID, "EGRESOS"), new BigDecimal(600));
	}
	
	@Test
	public void f_getMontoTotalIngresosEgresosGlobalesTest() {
		assertEquals(cajaDao.getSaldosDeSucursales("INGRESOS"), new BigDecimal(1200));
		assertEquals(cajaDao.getSaldosDeSucursales("EGRESOS"), new BigDecimal(623));
	}
	
	@Test
	public void g_getDetalleParticularIngresosPorSucursalTest() {
		assertEquals(cajaDao.getDetalleIngresosPorSucursalID(1).size(), 2);
	}
	
	@Test
	public void h_getDetalleParticularEgresosPorSucursalTest() {
		assertEquals(cajaDao.getDetalleEgresosPorSucursalID(1).size(), 1);
	}
	
	@Test
	public void i_getDetalleGlobalDeIngresosTest() {
		assertEquals(cajaDao.getDetalleGlobalDeIngresos().size(), 2);
	}
	
	@Test
	public void j_getDetalleGlobalDeEgresosTest() {
		assertEquals(cajaDao.getDetalleGlobalDeEgresos().size(), 2);
	}
}
