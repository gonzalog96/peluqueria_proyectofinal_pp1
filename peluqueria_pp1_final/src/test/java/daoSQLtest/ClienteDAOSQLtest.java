package daoSQLtest;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.ClienteDTO;
import dto.EstadoCliente;
import persistencia.dao.mysql.ClienteDAOSQL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClienteDAOSQLtest {
	ClienteDAOSQL daosql;
	EstadoCliente estado;
	ClienteDTO prueba;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");

	public void variables() {
		daosql = new ClienteDAOSQL();
		estado = EstadoCliente.ACTIVO;
		String date = "11/11/1111";

		// convert String to LocalDate
		LocalDate localDate = LocalDate.parse(date, formatter);
		System.out.println(localDate.toString());
		prueba = new ClienteDTO(0, "test", "test", "test", "1111", "1111", EstadoCliente.ACTIVO, localDate,100);
	}
	
	@Test
	public void test1Insert() {
		variables();
		assertTrue(daosql.insert(prueba));
	}
	@Test
	public void test2ReadAllTest() {
		variables();
		List<ClienteDTO> lista = daosql.readAll();
		assertTrue(lista.size()>0);
	}
	@Test
	public void test3ObtenerDesdeId() {
		variables();
		int id = daosql.obtenerUltimoID();
		ClienteDTO test = daosql.obtenerDesdeID(id);
		prueba.setId(id);
		prueba.setUltimaVisita(LocalDate.now());
		System.out.println(prueba.toString());
		System.out.println(test.toString());
		assertTrue(prueba.equals(test));
	}
	@Test
	public void test4Update() {
		variables();
		prueba.setId(daosql.obtenerUltimoID());
		prueba.setNombre("test2");
		daosql.update(prueba);
		ClienteDTO test = daosql.obtenerDesdeID(daosql.obtenerUltimoID());
		assertTrue(test.equals(prueba));
		
	}
	@Test
	public void test5Find() {
		variables();
		prueba.setId(daosql.obtenerUltimoID());
		prueba.setNombre("xxx");
		daosql.update(prueba);
		ClienteDTO test = daosql.find("xxx");
		assertTrue(test.equals(prueba));
		
	}
	@Test
	public void test6Delete() {
		variables();
		List<ClienteDTO> lista = daosql.readAll();
		daosql.delete(lista.get(lista.size()-1));
		List<ClienteDTO> test = daosql.readAll();
		assertTrue(lista.size()>test.size());
	}
}
