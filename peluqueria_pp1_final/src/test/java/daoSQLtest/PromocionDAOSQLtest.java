package daoSQLtest;



import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dto.EstadoPromocion;
import dto.PromocionDTO;
import dto.ServicioDTO;
import persistencia.dao.mysql.PromocionDAOSQL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PromocionDAOSQLtest {
	PromocionDTO prueba;
	ServicioDTO servicio1 ;
	ServicioDTO servicio2 ;
	ServicioDTO servicio3 ;
	List<ServicioDTO> servicios = new ArrayList<ServicioDTO>();
	PromocionDAOSQL daosql;
	
	public void variables() {
		servicio1 = new ServicioDTO(1, "corte", 1000, 30, 300);
		servicio2 = new ServicioDTO(2, "unias", 1000, 30, 300);
		servicio3 = new ServicioDTO(3, "masaje", 1000, 30, 300);
		servicios.add(servicio1);
		servicios.add(servicio2);
		servicios.add(servicio3);
		BigDecimal precio = new BigDecimal("1000");
		prueba = new PromocionDTO(2, "test", EstadoPromocion.ACTIVO, precio, 3, servicios);
		daosql = new PromocionDAOSQL();
	}
	@Test
	public void test1Insert() {
		variables();
		assertTrue(daosql.insert(prueba));
	}
	@Test
	public void test2ReadallTest() {
		variables();
		List<PromocionDTO> lista = daosql.readall();
		assertTrue(lista.size()>0);
	}
	@Test
	public void test3updateTest() {
		variables();
		List<PromocionDTO> lista = daosql.readall();
		PromocionDTO promo = lista.get(lista.size()-1);
		promo.setMultiplicacion(5);
		assertTrue(daosql.update(promo));
	}
}
