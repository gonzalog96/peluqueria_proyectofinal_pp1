package presentacionTest;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Test;

import dto.TurnoDTO;
import modelo.Peluqueria;
import persistencia.dao.mysql.DAOSQLFactory;
import persistencia.dao.mysql.TurnoDAOSQL;
import presentacion.controlador.Demonio;

public class DemonioTest {
	Demonio demonio;
	TurnoDAOSQL daosql;
	
	
	private void variables(){
	
		daosql = new TurnoDAOSQL();
	}
	
	
	@Test
	public void test() {
		variables();
		TurnoDTO turno = daosql.obtenerDesdeID(4);
		LocalTime tiempo = LocalTime.parse("18:45");
		System.out.println(tiempo.toString());
		Demonio.analizarTurno(turno, tiempo);
		System.out.println(turno.getEstado_turno());
	}
	

}
