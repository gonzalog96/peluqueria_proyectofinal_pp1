package presentacion.vista.contador;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.Cursor;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import dto.EgresoCaja;
import dto.IngresoCaja;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.util.List;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

public class ContadorVista extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel panelPrincipal;
	private JPanel subPanelInferior;
	private JPanel subPanelDerecho;
	private JPanel panelFiltros;
	private JPanel panelLogoPeluqueria;
	private JLabel lblLogo;
	private JPanel subPanelSuperior;
	private JLabel lblFechaActual;
	private JLabel lblUsuario;
	
	private JPanel subPanelCentral;
	private JPanel subPanelIzquierdo;

	private JTable tablaIngresos;
	private DefaultTableModel modelInfoIngresos;
	private String[] nombreColumnasTablaIngresos;
	private JTableHeader tableHeaderIngresos;
	
	private JTable tablaEgresos;
	private DefaultTableModel modelInfoEgresos;
	private String[] nombreColumnasTablaEgresos;
	private JTableHeader tableHeaderEgresos;
	
	private static ContadorVista INSTANCE;
	private JPanel panelTotalIngresos;
	private JLabel lblTotalIngresos;
	private JPanel panelTablaIngresos;
	private JPanel panelTotalEgresos;
	private JLabel lblTotalEgresos;
	private JTextField montoIngresos;
	private JTextField montoEgresos;
	private JPanel panelTablaEgresos;
	
	private JLabel sucursal;
	private JLabel lblModoContador;
	private JComboBox<Object> comboBoxSucursales;
	private JButton btnIngresoEgreso;

	public static ContadorVista getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ContadorVista();
		return INSTANCE;
	}

	private ContadorVista() {

		setPropiedades();

		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelDerecho();
		setSubPanelInferior();

		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1073, 816);
		setMinimumSize(new Dimension(920, 650));
		getContentPane().setLayout(new BorderLayout(0, 0));
		setTitle("Contabilidad de la empresa");
		controlDeCierre();
	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 30));
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setPreferredSize(new Dimension(270, 10));
		subPanelDerecho.setBorder(null);
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
		subPanelDerecho.setLayout(new GridLayout(2, 0, 0, 0));

		panelFiltros = new JPanel();
		panelFiltros.setOpaque(false);
		subPanelDerecho.add(panelFiltros);
		panelFiltros.setLayout(null);

		sucursal = new JLabel("Sucursal:");
		sucursal.setHorizontalAlignment(SwingConstants.CENTER);
		sucursal.setForeground(Color.WHITE);
		sucursal.setFont(new Font("Verdana", Font.PLAIN, 18));
		sucursal.setBounds(10, 11, 98, 23);
		panelFiltros.add(sucursal);
		
		comboBoxSucursales = new JComboBox<Object>();
		comboBoxSucursales.setBounds(108, 14, 137, 24);
		panelFiltros.add(comboBoxSucursales);

		panelLogoPeluqueria = new JPanel();
		panelLogoPeluqueria.setOpaque(false);
		subPanelDerecho.add(panelLogoPeluqueria);
				panelLogoPeluqueria.setLayout(new BorderLayout(0, 0));
		
				lblLogo = new JLabel("");
				lblLogo.setHorizontalTextPosition(SwingConstants.CENTER);
				lblLogo.setAlignmentY(Component.TOP_ALIGNMENT);
				lblLogo.setIcon(new ImageIcon(
						ContadorVista.class.getResource("/presentacion/vista/img/logo-peluqueria2_blanca_chica.png")));
				lblLogo.setHorizontalAlignment(SwingConstants.LEFT);
				lblLogo.setForeground(Color.WHITE);
				lblLogo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelLogoPeluqueria.add(lblLogo);

	}

	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setForeground(Color.WHITE);
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		
		panelTotalIngresos = new JPanel();
		panelTotalIngresos.setBackground(UIManager.getColor("Button.darkShadow"));
		
		panelTablaIngresos = new JPanel();
		panelTotalEgresos = new JPanel();
		panelTotalEgresos.setBackground(SystemColor.controlDkShadow);
		
		panelTablaEgresos = new JPanel();
		GroupLayout gl_subPanelCentral = new GroupLayout(subPanelCentral);
		gl_subPanelCentral.setHorizontalGroup(
			gl_subPanelCentral.createParallelGroup(Alignment.LEADING)
				.addComponent(panelTablaIngresos, GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
				.addComponent(panelTotalIngresos, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
				.addComponent(panelTablaEgresos, GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_subPanelCentral.createSequentialGroup()
					.addGap(71)
					.addComponent(panelTotalEgresos, GroupLayout.DEFAULT_SIZE, 687, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_subPanelCentral.setVerticalGroup(
			gl_subPanelCentral.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_subPanelCentral.createSequentialGroup()
					.addComponent(panelTotalIngresos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(19)
					.addComponent(panelTablaIngresos, GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
					.addGap(30)
					.addComponent(panelTotalEgresos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelTablaEgresos, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
					.addGap(39))
		);
		
		lblTotalEgresos = new JLabel("Total de egresos: ");
		lblTotalEgresos.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalEgresos.setForeground(Color.WHITE);
		lblTotalEgresos.setFont(new Font("Tahoma", Font.BOLD, 16));
		panelTotalEgresos.add(lblTotalEgresos);
		
		montoEgresos = new JTextField();
		montoEgresos.setFont(new Font("Tahoma", Font.BOLD, 14));
		montoEgresos.setHorizontalAlignment(SwingConstants.CENTER);
		montoEgresos.setEnabled(false);
		montoEgresos.setEditable(false);
		panelTotalEgresos.add(montoEgresos);
		montoEgresos.setColumns(20);
		
		lblTotalIngresos = new JLabel("Total de ingresos: ");
		lblTotalIngresos.setForeground(Color.WHITE);
		lblTotalIngresos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTotalIngresos.setHorizontalAlignment(SwingConstants.CENTER);
		panelTotalIngresos.add(lblTotalIngresos);
		
		montoIngresos = new JTextField();
		montoIngresos.setFont(new Font("Tahoma", Font.BOLD, 14));
		montoIngresos.setHorizontalAlignment(SwingConstants.CENTER);
		montoIngresos.setEditable(false);
		montoIngresos.setEnabled(false);
		panelTotalIngresos.add(montoIngresos);
		montoIngresos.setColumns(20);
		subPanelCentral.setLayout(gl_subPanelCentral);


		JScrollPane spTablaIngresos = new JScrollPane();
		spTablaIngresos.setBackground(Color.WHITE);
		spTablaIngresos.setPreferredSize(new Dimension(0, 0));
		spTablaIngresos.setBorder(null);
		spTablaIngresos.setViewportBorder(null);
		spTablaIngresos.getViewport().setBackground(new Color(64, 64, 64));
		panelTablaIngresos.setLayout(new BorderLayout(0, 0));
		spTablaIngresos.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaIngresos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelTablaIngresos.add(spTablaIngresos);

		nombreColumnasTablaIngresos = new String[] { "Fecha", "Sucursal", "Empleado", "Cliente", "Importe"};
		modelInfoIngresos = new DefaultTableModel(null, nombreColumnasTablaIngresos);
		tablaEgresos = new JTable(modelInfoIngresos);
		tablaEgresos.setShowGrid(false);
		tableHeaderIngresos = tablaEgresos.getTableHeader();
		tableHeaderIngresos.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeaderIngresos.setBackground(new Color(255, 255, 255));
		tableHeaderIngresos.setForeground(Color.black);

		tablaEgresos.setBackground(new Color(64, 64, 64));
		tablaEgresos.setForeground(Color.white);
		tablaEgresos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaEgresos.setRowHeight(30);
		
		spTablaIngresos.setViewportView(tablaEgresos);
		
		JScrollPane spTablaEgresos = new JScrollPane();
		spTablaEgresos.setBackground(Color.WHITE);
		spTablaEgresos.setPreferredSize(new Dimension(0, 0));
		spTablaEgresos.setBorder(null);
		spTablaEgresos.setViewportBorder(null);
		spTablaEgresos.getViewport().setBackground(new Color(64, 64, 64));
		panelTablaEgresos.setLayout(new BorderLayout(0, 0));
		spTablaEgresos.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaEgresos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelTablaEgresos.add(spTablaEgresos);

		nombreColumnasTablaEgresos = new String[] { "Fecha", "Sucursal", "Empleado", "Importe", "Concepto" };
		modelInfoEgresos = new DefaultTableModel(null, nombreColumnasTablaEgresos);
		tablaEgresos = new JTable(modelInfoEgresos);
		tableHeaderEgresos = tablaEgresos.getTableHeader();
		tableHeaderEgresos.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeaderEgresos.setBackground(new Color(255, 255, 255));
		tableHeaderEgresos.setForeground(Color.black);

		tablaEgresos.setBackground(new Color(64, 64, 64));
		tablaEgresos.setForeground(Color.white);
		tablaEgresos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaEgresos.setRowHeight(30);
		tablaEgresos.setShowGrid(false);

		spTablaEgresos.setViewportView(tablaEgresos);
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setPreferredSize(new Dimension(100, 10));
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
		subPanelIzquierdo.setSize(new Dimension(300, 50));
		
		btnIngresoEgreso = new JButton("Ingreso / egreso");
		subPanelIzquierdo.add(btnIngresoEgreso);
	}

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 10));

		lblFechaActual = new JLabel("28/10/2019");
		lblFechaActual.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaActual.setForeground(Color.WHITE);
		lblFechaActual.setFont(new Font("Verdana", Font.PLAIN, 18));
		lblFechaActual.setSize(new Dimension(300, 100));
		subPanelSuperior.add(lblFechaActual);

		lblUsuario = new JLabel("Panel contable");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblUsuario);
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

	public JPanel getSubPanelInferior()
	{
		return subPanelInferior;
	}

	public JPanel getSubPanelDerecho()
	{
		return subPanelDerecho;
	}

	public JLabel getLblLogo()
	{
		return lblLogo;
	}

	public JPanel getSubPanelSuperior()
	{
		return subPanelSuperior;
	}

	public JLabel getLblFechaActual()
	{
		return lblFechaActual;
	}

	public JLabel getLblUsuario()
	{
		return lblUsuario;
	}

	public JPanel getSubPanelCentral()
	{
		return subPanelCentral;
	}

	public JPanel getSubPanelIzquierdo()
	{
		return subPanelIzquierdo;
	}

	public JTable getTablaIngresos()
	{
		return tablaIngresos;
	}

	public DefaultTableModel getModelInfoIngresos()
	{
		return modelInfoIngresos;
	}

	public String[] getNombreColumnasTablaIngresos()
	{
		return nombreColumnasTablaIngresos;
	}

	public JTableHeader getTableHeaderIngresos()
	{
		return tableHeaderIngresos;
	}

	public JTable getTablaEgresos()
	{
		return tablaEgresos;
	}

	public DefaultTableModel getModelInfoEgresos()
	{
		return modelInfoEgresos;
	}

	public String[] getNombreColumnasTablaEgresos()
	{
		return nombreColumnasTablaEgresos;
	}

	public JTableHeader getTableHeaderEgresos()
	{
		return tableHeaderEgresos;
	}

	public static ContadorVista getINSTANCE()
	{
		return INSTANCE;
	}

	public JPanel getPanelTotalIngresos()
	{
		return panelTotalIngresos;
	}

	public JLabel getLblTotalIngresos()
	{
		return lblTotalIngresos;
	}

	public JPanel getPanelTablaIngresos()
	{
		return panelTablaIngresos;
	}

	public JPanel getPanelTotalEgresos()
	{
		return panelTotalEgresos;
	}

	public JLabel getLblTotalEgresos()
	{
		return lblTotalEgresos;
	}

	public JTextField getMontoIngresos()
	{
		return montoIngresos;
	}

	public JTextField getMontoEgresos()
	{
		return montoEgresos;
	}

	public JPanel getPanelTablaEgresos()
	{
		return panelTablaEgresos;
	}

	public JLabel getSucursal()
	{
		return sucursal;
	}
	
	public void limpiarMovimientos() {
		this.getMontoIngresos().setText("");
		this.getMontoEgresos().setText("");
		
		this.getModelInfoIngresos().setRowCount(0); // Para vaciar la tabla
		this.getModelInfoIngresos().setColumnCount(0);
		
		this.getModelInfoEgresos().setRowCount(0); // Para vaciar la tabla
		this.getModelInfoEgresos().setColumnCount(0);
	}

	public JComboBox<Object> getComboBoxSucursales()
	{
		return comboBoxSucursales;
	}

	public void setSubPanelInferior(JPanel subPanelInferior)
	{
		this.subPanelInferior = subPanelInferior;
	}

	public void setSubPanelDerecho(JPanel subPanelDerecho)
	{
		this.subPanelDerecho = subPanelDerecho;
	}

	public void setLblLogo(JLabel lblLogo)
	{
		this.lblLogo = lblLogo;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior)
	{
		this.subPanelSuperior = subPanelSuperior;
	}

	public void setLblFechaActual(JLabel lblFechaActual)
	{
		this.lblFechaActual = lblFechaActual;
	}

	public void setLblUsuario(JLabel lblUsuario)
	{
		this.lblUsuario = lblUsuario;
	}

	public void setSubPanelCentral(JPanel subPanelCentral)
	{
		this.subPanelCentral = subPanelCentral;
	}

	public void setSubPanelIzquierdo(JPanel subPanelIzquierdo)
	{
		this.subPanelIzquierdo = subPanelIzquierdo;
	}

	public void setTablaIngresos(JTable tablaIngresos)
	{
		this.tablaIngresos = tablaIngresos;
	}

	public void setModelInfoIngresos(DefaultTableModel modelInfoIngresos)
	{
		this.modelInfoIngresos = modelInfoIngresos;
	}

	public void setNombreColumnasTablaIngresos(String[] nombreColumnasTablaIngresos)
	{
		this.nombreColumnasTablaIngresos = nombreColumnasTablaIngresos;
	}

	public void setTableHeaderIngresos(JTableHeader tableHeaderIngresos)
	{
		this.tableHeaderIngresos = tableHeaderIngresos;
	}

	public void setTablaEgresos(JTable tablaEgresos)
	{
		this.tablaEgresos = tablaEgresos;
	}

	public void setModelInfoEgresos(DefaultTableModel modelInfoEgresos)
	{
		this.modelInfoEgresos = modelInfoEgresos;
	}

	public void setNombreColumnasTablaEgresos(String[] nombreColumnasTablaEgresos)
	{
		this.nombreColumnasTablaEgresos = nombreColumnasTablaEgresos;
	}

	public void setTableHeaderEgresos(JTableHeader tableHeaderEgresos)
	{
		this.tableHeaderEgresos = tableHeaderEgresos;
	}

	public static void setINSTANCE(ContadorVista iNSTANCE)
	{
		INSTANCE = iNSTANCE;
	}

	public void setPanelTotalIngresos(JPanel panelTotalIngresos)
	{
		this.panelTotalIngresos = panelTotalIngresos;
	}

	public void setLblTotalIngresos(JLabel lblTotalIngresos)
	{
		this.lblTotalIngresos = lblTotalIngresos;
	}

	public void setPanelTablaIngresos(JPanel panelTablaIngresos)
	{
		this.panelTablaIngresos = panelTablaIngresos;
	}

	public void setPanelTotalEgresos(JPanel panelTotalEgresos)
	{
		this.panelTotalEgresos = panelTotalEgresos;
	}

	public void setLblTotalEgresos(JLabel lblTotalEgresos)
	{
		this.lblTotalEgresos = lblTotalEgresos;
	}

	public void setMontoIngresos(JTextField montoIngresos)
	{
		this.montoIngresos = montoIngresos;
	}

	public void setMontoEgresos(JTextField montoEgresos)
	{
		this.montoEgresos = montoEgresos;
	}

	public void setPanelTablaEgresos(JPanel panelTablaEgresos)
	{
		this.panelTablaEgresos = panelTablaEgresos;
	}

	public void setSucursal(JLabel sucursal)
	{
		this.sucursal = sucursal;
	}

	public void setComboBoxSucursales(JComboBox<Object> comboBoxSucursales)
	{
		this.comboBoxSucursales = comboBoxSucursales;
	}

	public void llenarTablaIngresos(List<IngresoCaja> ingresos) {
		this.getModelInfoIngresos().setRowCount(0); // Para vaciar la tabla
		this.getModelInfoIngresos().setColumnCount(0);
		this.getModelInfoIngresos().setColumnIdentifiers(this.getNombreColumnasTablaIngresos());

		for (int i = 0; i < ingresos.size(); i++) {
			IngresoCaja ingreso = ingresos.get(i);
			String fecha = ingreso.getFechaIngreso();
			String sucursal = ingreso.getNombreSucursal();
			String empleado = ingreso.getNombreApellidoEmpleado();
			String cliente = ingreso.getNombreApellido();
			String importe = ingreso.getImporteTotal();

			Object[] fila = { fecha, sucursal, empleado, cliente, importe };
			this.getModelInfoIngresos().addRow(fila);
		}
	}
	
	public void llenarTablaEgresos(List<EgresoCaja> egresos) {
		this.getModelInfoEgresos().setRowCount(0); // Para vaciar la tabla
		this.getModelInfoEgresos().setColumnCount(0);
		this.getModelInfoEgresos().setColumnIdentifiers(this.getNombreColumnasTablaEgresos());

		for (int i = 0; i < egresos.size(); i++) {
			EgresoCaja egreso = egresos.get(i);
			String fecha = egreso.getFecha_egreso();
			String sucursal = egreso.getNombreSucursal();
			String empleado = egreso.getNombreApellidoEmpleado();
			String importe = egreso.getImporte();
			String concepto = egreso.getConcepto();

			Object[] fila = { fecha, sucursal, empleado, importe, concepto };
			this.getModelInfoEgresos().addRow(fila);
		}
	}

	public void mostrar() {
		this.setVisible(true);
	}
	
	public void ocultar() {
		this.setVisible(false);
	}
	
	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				// guardar();
				close();
			}
		});
	}

	public JButton getBtnIngresoEgreso() {
		return btnIngresoEgreso;
	}

	public void setBtnIngresoEgreso(JButton btnIngresoEgreso) {
		this.btnIngresoEgreso = btnIngresoEgreso;
	}
	
	
}