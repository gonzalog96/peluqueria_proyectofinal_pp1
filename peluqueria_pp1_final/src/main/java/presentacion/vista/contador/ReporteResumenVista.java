package presentacion.vista.contador;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import dto.SucursalDTO;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

public class ReporteResumenVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private static ReporteResumenVista INSTANCE;
	private JDateChooser dateDesde;
	private JComboBox<SucursalDTO> comboSucursal;
	private JDateChooser dateHasta;
	private JCheckBox chckbxTodasLasSucursales;

	public static ReporteResumenVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ReporteResumenVista();
			return new ReporteResumenVista();
		} else
			return INSTANCE;
	}

	private ReporteResumenVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 324, 211);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, -12, 288, 184);
		contentPane.add(panel);
		panel.setLayout(null);


		btnConfirmar = new JButton("Generar");
		btnConfirmar.setBounds(84, 150, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblCrearProducto = new JLabel("Reporte Ingreso / Egreso");
		lblCrearProducto.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCrearProducto.setBounds(31, 11, 523, 40);
		panel.add(lblCrearProducto);


		dateDesde = new JDateChooser();
		dateDesde.setPreferredSize(new Dimension(150, 22));
		dateDesde.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dateDesde.setBounds(10, 62, 116, 22);
		panel.add(dateDesde);
		
		JLabel lblDesde = new JLabel("Desde");
		lblDesde.setBounds(10, 49, 46, 14);
		panel.add(lblDesde);
		
		dateHasta = new JDateChooser();
		dateHasta.setPreferredSize(new Dimension(150, 22));
		dateHasta.setFont(new Font("Tahoma", Font.PLAIN, 16));
		dateHasta.setBounds(166, 62, 116, 22);
		panel.add(dateHasta);
		
		JLabel lblHasta = new JLabel("Hasta");
		lblHasta.setBounds(167, 49, 46, 14);
		panel.add(lblHasta);
		
		comboSucursal = new JComboBox<SucursalDTO>();
		comboSucursal.setBounds(10, 113, 116, 20);
		panel.add(comboSucursal);
		
		chckbxTodasLasSucursales = new JCheckBox("Todas las sucursales");
		chckbxTodasLasSucursales.setBounds(154, 112, 148, 23);
		panel.add(chckbxTodasLasSucursales);
		
		JLabel lblSucursal = new JLabel("Sucursal");
		lblSucursal.setBounds(10, 101, 46, 14);
		panel.add(lblSucursal);
		
		
		
		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

//	public void cerrar() {
//		this.txtNombre.setText(null);
//		this.txtRubro.setText(null);
//		this.txtPrecio.setText(null);
//		this.dispose();
//	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}

	public JDateChooser getDateDesde() {
		return dateDesde;
	}

	public void setDateDesde(JDateChooser dateDesde) {
		this.dateDesde = dateDesde;
	}

	public JComboBox<SucursalDTO> getComboSucursal() {
		return comboSucursal;
	}

	public void setComboSucursal(JComboBox<SucursalDTO> comboSucursal) {
		this.comboSucursal = comboSucursal;
	}

	public JDateChooser getDateHasta() {
		return dateHasta;
	}

	public void setDateHasta(JDateChooser dateHasta) {
		this.dateHasta = dateHasta;
	}

	public JCheckBox getChckbxTodasLasSucursales() {
		return chckbxTodasLasSucursales;
	}

	public void setChckbxTodasLasSucursales(JCheckBox chckbxTodasLasSucursales) {
		this.chckbxTodasLasSucursales = chckbxTodasLasSucursales;
	}
	
	
}












