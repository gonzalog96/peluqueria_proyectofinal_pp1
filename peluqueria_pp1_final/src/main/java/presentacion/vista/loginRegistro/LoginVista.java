package presentacion.vista.loginRegistro;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import presentacion.vista.administrativo.AdministrativoVista;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

public class LoginVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private static LoginVista INSTANCE;
	private JPanel panelPrincipal;
	private JTextField jtxt_usuario;
	private JPasswordField passwordField;
	private JLabel lblUsuario;
	private JLabel lblContrasea;
	private JButton btnLogin;
	
	public static LoginVista getInstance() {
		if(INSTANCE == null) INSTANCE = new LoginVista();
		return INSTANCE;
	}

	private LoginVista() {
		setPropiedades();
		setPanelPrincipal();
		setCampoUsuario();
		setCampoPass();
		setPropiedadesDelBotonLogin();
		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 313, 445);
		setTitle("Login");
		//setResizable(false);
	}
	
	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panelPrincipal.setBackground(Color.DARK_GRAY);
		panelPrincipal.setBorder(new LineBorder(Color.WHITE, 3));
		panelPrincipal.setLayout(null);
		setContentPane(panelPrincipal);
	}
	
	private void setCampoUsuario() {
		lblUsuario = new JLabel("Usuario");
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblUsuario.setBounds(25, 44, 88, 16);
		panelPrincipal.add(lblUsuario);
		
		jtxt_usuario = new JTextField();
		jtxt_usuario.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jtxt_usuario.setBounds(25, 77, 243, 37);
		panelPrincipal.add(jtxt_usuario);
	}
	
	private void setCampoPass() {
		lblContrasea = new JLabel("Contraseña");
		lblContrasea.setForeground(Color.WHITE);
		lblContrasea.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblContrasea.setBounds(25, 141, 116, 16);
		panelPrincipal.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(23, 170, 243, 37);
		panelPrincipal.add(passwordField);
	}
	
	private void setBotonLogin() {
		btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnLogin.setBounds(313, 196, 97, 25);
		panelPrincipal.add(btnLogin);
	}
	
	private void setPropiedadesDelBotonLogin() {

		// se crean los iconos que se van a usar
		ImageIcon iconoLogin = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/login.png"));
		ImageIcon iconoLogin_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/login_rollover.png"));
		ImageIcon iconoLogin_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/login_pressed.png"));

		btnLogin = new JButton(iconoLogin);
		btnLogin.setBounds(180, 220, 88, 73);
		btnLogin.setFocusable(false);
		btnLogin.setRolloverIcon(iconoLogin_rollover);
		btnLogin.setPressedIcon(iconoLogin_pressed);
		btnLogin.setOpaque(false);// se hace transparente todo el boton default JButton
		btnLogin.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnLogin.setBorderPainted(false);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnLogin.getWidth();
		int alto = btnLogin.getHeight();
		iconoLogin.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoLogin_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoLogin_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Loguearse</font> con sus credenciales</h3>"
				+ "<font size=3 color=red><p>========================================================================</p></font>"
				+ "<font size=3><p><b>En caso de olvidar usuario y/o contraseña, comunicarse con el administrador</b></p></font>"
				+ "<font size=3 color=red><p>========================================================================</p></font>"
				+ "</body></html>";

		btnLogin.setToolTipText(html);
		panelPrincipal.add(btnLogin);
		
		JLabel lblImgPrincipal = new JLabel("");
		lblImgPrincipal.setIcon(new ImageIcon(LoginVista.class.getResource("/presentacion/vista/img/logo-peluqueria2_blanca_chica.png")));
		lblImgPrincipal.setBounds(0, 275, 181, 137);
		panelPrincipal.add(lblImgPrincipal);
	}
	
	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JTextField getJtxt_usuario() {
		return jtxt_usuario;
	}

	public void setJtxt_usuario(JTextField jtxt_usuario) {
		this.jtxt_usuario = jtxt_usuario;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}

	public JLabel getLblContrasea() {
		return lblContrasea;
	}

	public void setLblContrasea(JLabel lblContrasea) {
		this.lblContrasea = lblContrasea;
	}

	public JButton getBtnLogin() {
		return btnLogin;
	}

	public void setBtnLogin(JButton btnLogin) {
		this.btnLogin = btnLogin;
	}

	public void mostrar() {
		this.setVisible(true);
	}
	
	public void ocultar() {
		this.setVisible(false);
	}
}
