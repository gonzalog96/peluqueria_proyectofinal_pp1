package presentacion.vista.administrativo.cliente;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.EstadoCliente;
import dto.ServicioDTO;

import java.awt.Font;
import java.util.List;

import javax.swing.JComboBox;

public class EditClienteVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;
	private JTextField txtEmail;
	private JButton btnConfirmar;
	private static EditClienteVista INSTANCE;
	private JTextField txtTelefono;
	private JComboBox<EstadoCliente> comboEstado;
	private DefaultTableModel modelServicios;
	private String[] nombreColumnas = { "Servicios" };
	private JLabel lblAddEditProfesional;

	public static EditClienteVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EditClienteVista();
			return new EditClienteVista();
		} else
			return INSTANCE;
	}

	private EditClienteVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 383, 358);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 369, 320);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(22, 90, 113, 14);
		panel.add(lblApellido);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 62, 113, 14);
		panel.add(lblNombre);

		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(22, 174, 113, 14);
		panel.add(lblTelfono);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(22, 146, 113, 14);
		panel.add(lblEmail);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 59, 226, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setBounds(133, 87, 226, 20);
		panel.add(txtApellido);
		txtApellido.setColumns(10);
		txtDni = new JTextField();
		txtDni.setBounds(133, 115, 226, 20);
		panel.add(txtDni);
		txtDni.setColumns(10);
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 143, 226, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		lblAddEditProfesional = new JLabel("Editar Cliente");
		lblAddEditProfesional.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAddEditProfesional.setBounds(100, 18, 226, 33);
		panel.add(lblAddEditProfesional);

		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(22, 118, 113, 14);
		panel.add(lblDni);

		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(133, 171, 226, 20);
		panel.add(txtTelefono);

		comboEstado = new JComboBox<EstadoCliente>();
		comboEstado.setBounds(22, 226, 160, 20);
		comboEstado.addItem(EstadoCliente.ACTIVO);
		comboEstado.addItem(EstadoCliente.INACTIVO);

		panel.add(comboEstado);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(147, 281, 89, 23);
		panel.add(btnConfirmar);

		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(22, 212, 46, 14);
		panel.add(lblEstado);

		modelServicios = new DefaultTableModel(null, nombreColumnas);
		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}

	public JTextField getTxtDni() {
		return txtDni;
	}

	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnAgregarProfesional) {
		this.btnConfirmar = btnAgregarProfesional;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}

	public JComboBox<EstadoCliente> getComboEstado() {
		return comboEstado;
	}

	public void setComboEstado(JComboBox<EstadoCliente> comboEstado) {
		this.comboEstado = comboEstado;
	}

	public JLabel getLblAddEditProfesional() {
		return lblAddEditProfesional;
	}

	public void setLblAddEditProfesional(JLabel lblAddEditProfesional) {
		this.lblAddEditProfesional = lblAddEditProfesional;
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.txtDni.setText(null);
		this.txtEmail.setText(null);
		this.dispose();
	}

	public void llenarTablaServicios(List<ServicioDTO> servicios) {
		modelServicios.setRowCount(0); // Para vaciar la tabla
		modelServicios.setColumnCount(0);
		modelServicios.setColumnIdentifiers(nombreColumnas);

		for (ServicioDTO s : servicios) {
			String nombre = s.getNombre();
			Object[] fila = { nombre };
			modelServicios.addRow(fila);
		}
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
}
