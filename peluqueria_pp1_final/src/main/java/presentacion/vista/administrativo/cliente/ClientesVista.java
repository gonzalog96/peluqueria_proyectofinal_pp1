package presentacion.vista.administrativo.cliente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.time.LocalDate;
import java.util.List;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.Cursor;

import dto.ClienteDTO;
import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

public class ClientesVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel subPanelInferior;
	private JPanel subPanelDerecho;
	private JPanel subPanelCentral;
	private JPanel panelSuperior_tabla;
	private JPanel subPanelIzquierdo;
	private JLabel lblLogo;
	private JPanel subPanelSuperior;
	private JLabel lblFechaActual;
	private JLabel lblUsuario;
	private JLabel lblSucursal;
	private static ClientesVista INSTANCE;
	private JPanel panel_1;
	private JPanel panel_3;
	private JPanel panelInferior_tabla;
	private JPanel panelCentral_tabla;
	private JTable tablaClientes;
	private DefaultTableModel modelInformacion;
	private String[] nombreColumnasTabla;
	private JScrollPane spTablaInformacion;
	private JTableHeader tableHeader;
	private JButton btnCrearClientes;
	private JButton btnEditarCliente;
	private JButton btnCambiarEstadoCliente;
	private JPanel subPanelNorth_Panel_3;
	private JButton btnClientes;
	private JLabel lblClientes;
	private JButton btnTurnos;

	public static ClientesVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ClientesVista();
			return new ClientesVista();
		}else {
			return INSTANCE;
		}
	}
	

	private ClientesVista() {

		setPropiedades();

		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelDerecho();
		setSubPanelInferior();

		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1073, 816);
		setMinimumSize(new Dimension(920, 650));
		getContentPane().setLayout(new BorderLayout(0, 0));
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("Administrativo");
		controlDeCierre();
	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 30));

		setPropiedadesDelBotonCrearTurno();
		setPropiedadesDelBotonEditarTurno();
		setPropiedadesDelBotonCancelarTurno();
	}
	private void setPropiedadesDelBotonCrearTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCrearTurno = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/agregarTurno.png"));
		ImageIcon iconoCrearTurno_rollover = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/agregarTurno_rollover.png"));
		ImageIcon iiconoCrearTurno_pressed = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/agregarTurno_pressed.png"));

		btnCrearClientes = new JButton(iconoCrearTurno);
		btnCrearClientes.setFocusable(false);
		btnCrearClientes.setBorderPainted(false);
		btnCrearClientes.setRolloverIcon(iconoCrearTurno_rollover);
		btnCrearClientes.setPressedIcon(iiconoCrearTurno_pressed);
		btnCrearClientes.setOpaque(false);// se hace transparente todo el boton default JButton
		btnCrearClientes.setContentAreaFilled(false);
		btnCrearClientes.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnCrearClientes.getWidth();
		int alto = btnCrearClientes.getHeight();
		iconoCrearTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCrearTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iiconoCrearTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Crear</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes crear un turno completando los datos solicitados</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		btnCrearClientes.setToolTipText(html);
		subPanelInferior.add(btnCrearClientes);
	}

	private void setPropiedadesDelBotonEditarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoEditarTurno = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/editarTurno.png"));
		ImageIcon iconoEditarTurno_rollover = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/editarTurno_rollover.png"));
		ImageIcon iiconoEditarTurno_pressed = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/editarTurno_pressed.png"));

		btnEditarCliente = new JButton(iconoEditarTurno);
		btnEditarCliente.setFocusable(false);
		btnEditarCliente.setRolloverIcon(iconoEditarTurno_rollover);
		btnEditarCliente.setPressedIcon(iiconoEditarTurno_pressed);
		btnEditarCliente.setOpaque(false);// se hace transparente todo el boton default JButton
		btnEditarCliente.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnEditarCliente.setBorderPainted(false);// borra el borde default del JButton
		btnEditarCliente.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnEditarCliente.getWidth();
		int alto = btnEditarCliente.getHeight();
		iconoEditarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoEditarTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iiconoEditarTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Editar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>====================================================</p></font>"
				+ "<font size=3><p><b>Puedes editar un seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>====================================================</p></font>"
				+ "</body></html>";
		btnEditarCliente.setToolTipText(html);
		subPanelInferior.add(btnEditarCliente);
	}

	private void setPropiedadesDelBotonCancelarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCancelarTurno = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/cancelarTurno.png"));
		ImageIcon iconoCancelarTurno_rollover = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/cancelarTurno_rollover.png"));
		ImageIcon iiconoCancelarTurno_pressed = new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/cancelarTurno_pressed.png"));

		btnCambiarEstadoCliente = new JButton(iconoCancelarTurno);
		btnCambiarEstadoCliente.setFocusable(false);
		btnCambiarEstadoCliente.setRolloverIcon(iconoCancelarTurno_rollover);
		btnCambiarEstadoCliente.setPressedIcon(iiconoCancelarTurno_pressed);
		btnCambiarEstadoCliente.setOpaque(false);// se hace transparente todo el boton default JButton
		btnCambiarEstadoCliente.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnCambiarEstadoCliente.setBorderPainted(false);// borra el borde default del JButton
		btnCambiarEstadoCliente.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnCambiarEstadoCliente.getWidth();
		int alto = btnCambiarEstadoCliente.getHeight();
		iconoCancelarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCancelarTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iiconoCancelarTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Cancelar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes cancelar un turno seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		btnCambiarEstadoCliente.setToolTipText(html);
		subPanelInferior.add(btnCambiarEstadoCliente);
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setPreferredSize(new Dimension(270, 10));
		subPanelDerecho.setBorder(null);
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
		subPanelDerecho.setLayout(new GridLayout(2, 0, 0, 0));

		panel_1 = new JPanel();
		panel_1.setOpaque(false);
		subPanelDerecho.add(panel_1);
		panel_1.setLayout(null);

		panel_3 = new JPanel();
		panel_3.setOpaque(false);
		subPanelDerecho.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		lblLogo = new JLabel("");
		lblLogo.setHorizontalTextPosition(SwingConstants.CENTER);
		panel_3.add(lblLogo, BorderLayout.SOUTH);
		lblLogo.setAlignmentY(Component.TOP_ALIGNMENT);
		lblLogo.setIcon(new ImageIcon(
				ClientesVista.class.getResource("/presentacion/vista/img/logo-peluqueria2_blanca_chica.png")));
		lblLogo.setHorizontalAlignment(SwingConstants.LEFT);
		lblLogo.setForeground(Color.WHITE);
		lblLogo.setFont(new Font("Tahoma", Font.PLAIN, 18));

		subPanelNorth_Panel_3 = new JPanel();
		subPanelNorth_Panel_3.setOpaque(false);
		subPanelNorth_Panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel_3.add(subPanelNorth_Panel_3, BorderLayout.NORTH);

	}

	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(new BorderLayout(0, 0));

		panelSuperior_tabla = new JPanel();
		panelSuperior_tabla.setPreferredSize(new Dimension(10, 50));
		panelSuperior_tabla.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSuperior_tabla.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSuperior_tabla.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelSuperior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelSuperior_tabla, BorderLayout.NORTH);
		panelSuperior_tabla.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		lblClientes = new JLabel("Clientes");
		lblClientes.setFont(new Font("Tahoma", Font.BOLD, 27));
		panelSuperior_tabla.add(lblClientes);

		panelInferior_tabla = new JPanel();
		panelInferior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelInferior_tabla, BorderLayout.SOUTH);

		panelCentral_tabla = new JPanel();
		panelCentral_tabla.setBackground(Color.WHITE);
		subPanelCentral.add(panelCentral_tabla, BorderLayout.CENTER);
		panelCentral_tabla.setLayout(new BoxLayout(panelCentral_tabla, BoxLayout.Y_AXIS));

		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(0, 0, 976, 547);
		panelCentral_tabla.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Nombre", "Apellido", "DNI", "Email", "Telefono", "Estado", "Puntos","Ultima visita"};
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla);
		tablaClientes = new JTable(modelInformacion);
		tableHeader = tablaClientes.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaClientes.setBackground(new Color(64, 64, 64));
		tablaClientes.setForeground(Color.white);
		tablaClientes.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaClientes.setRowHeight(30);
		tablaClientes.setShowGrid(false);

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaClientes);
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setPreferredSize(new Dimension(100, 10));
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
		subPanelIzquierdo.setSize(new Dimension(300, 50));
		btnTurnos = new JButton("Turnos");
		subPanelIzquierdo.add(btnTurnos);
	}

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 10));

		lblFechaActual = new JLabel("28/10/2019");
		lblFechaActual.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaActual.setForeground(Color.WHITE);
		lblFechaActual.setFont(new Font("Verdana", Font.PLAIN, 18));
		lblFechaActual.setSize(new Dimension(300, 100));
		subPanelSuperior.add(lblFechaActual);

		lblUsuario = new JLabel("Usuario de prueba");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblUsuario);

		lblSucursal = new JLabel("Sucursal");
		lblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
		lblSucursal.setForeground(Color.WHITE);
		lblSucursal.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblSucursal);
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JPanel getSubPanelInferior() {
		return subPanelInferior;
	}

	public void setSubPanelInferior(JPanel subPanelInferior) {
		this.subPanelInferior = subPanelInferior;
	}

	public JPanel getSubPanelDerecho() {
		return subPanelDerecho;
	}

	public void setSubPanelDerecho(JPanel subPanelDerecho) {
		this.subPanelDerecho = subPanelDerecho;
	}

	public JPanel getSubPanelCentral() {
		return subPanelCentral;
	}

	public void setSubPanelCentral(JPanel subPanelCentral) {
		this.subPanelCentral = subPanelCentral;
	}

	public JPanel getPanelSuperior_tabla() {
		return panelSuperior_tabla;
	}

	public void setPanelSuperior_tabla(JPanel panelSuperior_tabla) {
		this.panelSuperior_tabla = panelSuperior_tabla;
	}

	public JPanel getSubPanelIzquierdo() {
		return subPanelIzquierdo;
	}

	public void setSubPanelIzquierdo(JPanel subPanelIzquierdo) {
		this.subPanelIzquierdo = subPanelIzquierdo;
	}

	public JLabel getLblLogo() {
		return lblLogo;
	}

	public void setLblLogo(JLabel lblLogo) {
		this.lblLogo = lblLogo;
	}

	public JPanel getSubPanelSuperior() {
		return subPanelSuperior;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior) {
		this.subPanelSuperior = subPanelSuperior;
	}

	public JLabel getLblFechaActual() {
		return lblFechaActual;
	}

	public void setLblFechaActual(JLabel lblFechaActual) {
		this.lblFechaActual = lblFechaActual;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}

	public JLabel getLblSucursal() {
		return lblSucursal;
	}

	public void setLblSucursal(JLabel lblSucursal) {
		this.lblSucursal = lblSucursal;
	}

	public JPanel getPanel_1() {
		return panel_1;
	}

	public void setPanel_1(JPanel panel_1) {
		this.panel_1 = panel_1;
	}

	public JPanel getPanel_3() {
		return panel_3;
	}

	public void setPanel_3(JPanel panel_3) {
		this.panel_3 = panel_3;
	}

	public JPanel getPanelInferior_tabla() {
		return panelInferior_tabla;
	}

	public void setPanelInferior_tabla(JPanel panelInferior_tabla) {
		this.panelInferior_tabla = panelInferior_tabla;
	}

	public JPanel getPanelCentral_tabla() {
		return panelCentral_tabla;
	}

	public void setPanelCentral_tabla(JPanel panelCentral_tabla) {
		this.panelCentral_tabla = panelCentral_tabla;
	}

	public JTable getTablaClientes() {
		return tablaClientes;
	}

	public void setTablaClientes(JTable tablaTurnos) {
		this.tablaClientes = tablaTurnos;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

	public JButton getBtnCrearClientes() {
		return btnCrearClientes;
	}

	public void setBtnCrearClientes(JButton btnCrearClientes) {
		this.btnCrearClientes = btnCrearClientes;
	}

	public JButton getBtnEditarCliente() {
		return btnEditarCliente;
	}

	public void setBtnEditarCliente(JButton btnEditarCliente) {
		this.btnEditarCliente = btnEditarCliente;
	}

	public JButton getBtnCambiarEstadoCliente() {
		return btnCambiarEstadoCliente;
	}

	public void setBtnCambiarEstadoCliente(JButton btnCambiarEstadoCliente) {
		this.btnCambiarEstadoCliente = btnCambiarEstadoCliente;
	}

	public JPanel getSubPanelNorth_Panel_3() {
		return subPanelNorth_Panel_3;
	}

	public void setSubPanelNorth_Panel_3(JPanel subPanelNorth_Panel_3) {
		this.subPanelNorth_Panel_3 = subPanelNorth_Panel_3;
	}

	public JButton getBtnClientes() {
		return btnClientes;
	}

	public void setBtnClientes(JButton btnClientes) {
		this.btnClientes = btnClientes;
	}

	public JLabel getLblClientes() {
		return lblClientes;
	}

	public void setLblClientes(JLabel lblClientes) {
		this.lblClientes = lblClientes;
	}

	public JButton getBtnTurnos() {
		return btnTurnos;
	}

	public void setBtnTurnos(JButton btnTurnos) {
		this.btnTurnos = btnTurnos;
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	public void llenarTabla(List<ClienteDTO> clientes) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (ClienteDTO t : clientes) {
			String nombre = t.getNombre().toString();
			String apellido = t.getApellido().toString();
			String dni = t.getDni().toString();
			String email = t.getEmail().toString();
			String telefono = t.getTelefono().toString();
			String estado = t.getEstado().toString();
			int puntos = t.getPuntos();
			LocalDate ultima = t.getUltimaVisita();
			
			
			Object[] fila = { nombre, apellido, dni, email, telefono, estado, puntos, ultima};
			this.getModelInformacion().addRow(fila);
		}
	}
}
