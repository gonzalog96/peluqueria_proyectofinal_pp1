package presentacion.vista.administrativo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import dto.ProfesionalDTO;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

public class PromocionVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private static PromocionVista INSTANCE;
	private JPanel panelPrincipal;
	private JPanel subPanelSuperior;
	private JPanel subPanelInferior;
	private JPanel subPanelIzquierdo;
	private JPanel subPanelDerecho;
	private JPanel subPanelCentral;
	private JLabel lblServicio1;
	private JComboBox<String> boxProfesionalServicio1;
	private JButton btnCancelar;
	private JButton btnReservar;
	private JTextField txtServicio1;
	private JLabel lblReservarDePromocin;
	private JLabel lblServicio2;
	private JTextField txtServicio2;
	private JComboBox<String> boxProfesionalServicio2;
	private JLabel lblServicio3;
	private JTextField txtServicio3;
	private JComboBox<String> boxProfesionalServicio3;
	private JLabel lblServicio4;
	private JTextField txtServicio4;
	private JComboBox<String> boxProfesionalServicio4;
	private JLabel lblServicio5;
	private JTextField txtServicio5;
	private JComboBox<String> boxProfesionalServicio5;
	private JSeparator separator;
	private JLabel lblSeleccioneElProfesional;
	private JLabel lblDetalleDeLa;
	private JLabel lblNombrePromocion;
	private JTextField txtNombrePromocion;
	private JTextField txtPrecioPromocion;
	private JLabel lblPrecioPromocion;
	private JLabel lblHorariosPromocion;
	private JComboBox<String> boxHorariosPromocion;
	private JTextField txtFechaElegida;
	private JLabel lblFechaElegida;

	public static PromocionVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PromocionVista();
			return new PromocionVista();
		}else {
			return INSTANCE;
		}
	}

	private PromocionVista() {
		setPropiedades();
		setPanelPrincipal();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 670, 770);
		controlDeCierre();
		ocultar();
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelPrincipal.setLayout(new BorderLayout(0, 0));
		setContentPane(panelPrincipal);

		setSubPanelSuperior();

		setSubPanelInferior();

		setSubPanelIzquierdo();

		setSubPanelDerecho();

		setSubPanelCentral();
	}

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		{
			lblReservarDePromocin = new JLabel("Reservar de Promoción");
			lblReservarDePromocin.setForeground(Color.WHITE);
			lblReservarDePromocin.setFont(new Font("Tahoma", Font.PLAIN, 20));
			subPanelSuperior.add(lblReservarDePromocin);
		}
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
	}

	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.GRAY);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(null);
		{
			lblServicio1 = new JLabel("Servicio 1");
			lblServicio1.setForeground(Color.WHITE);
			lblServicio1.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblServicio1.setBounds(12, 93, 81, 22);
			subPanelCentral.add(lblServicio1);
		}
		{
			boxProfesionalServicio1 = new JComboBox<String>();
			boxProfesionalServicio1.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxProfesionalServicio1.setEditable(false);
			boxProfesionalServicio1.setBounds(357, 88, 263, 31);
			subPanelCentral.add(boxProfesionalServicio1);
		}
		{
			txtServicio1 = new JTextField();
			txtServicio1.setEditable(false);
			txtServicio1.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtServicio1.setHorizontalAlignment(SwingConstants.CENTER);
			txtServicio1.setBounds(101, 88, 244, 31);
			subPanelCentral.add(txtServicio1);
			txtServicio1.setColumns(10);
		}
		{
			lblServicio2 = new JLabel("Servicio 2");
			lblServicio2.setForeground(Color.WHITE);
			lblServicio2.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblServicio2.setBounds(12, 133, 81, 22);
			subPanelCentral.add(lblServicio2);
		}
		{
			txtServicio2 = new JTextField();
			txtServicio2.setEditable(false);
			txtServicio2.setHorizontalAlignment(SwingConstants.CENTER);
			txtServicio2.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtServicio2.setColumns(10);
			txtServicio2.setBounds(101, 128, 244, 31);
			subPanelCentral.add(txtServicio2);
		}
		{
			boxProfesionalServicio2 = new JComboBox<String>();
			boxProfesionalServicio2.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxProfesionalServicio2.setEditable(false);
			boxProfesionalServicio2.setBounds(357, 128, 263, 31);
			subPanelCentral.add(boxProfesionalServicio2);
		}
		{
			lblServicio3 = new JLabel("Servicio 3");
			lblServicio3.setForeground(Color.WHITE);
			lblServicio3.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblServicio3.setBounds(12, 173, 81, 22);
			subPanelCentral.add(lblServicio3);
		}
		{
			txtServicio3 = new JTextField();
			txtServicio3.setEditable(false);
			txtServicio3.setHorizontalAlignment(SwingConstants.CENTER);
			txtServicio3.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtServicio3.setColumns(10);
			txtServicio3.setBounds(101, 168, 244, 31);
			subPanelCentral.add(txtServicio3);
		}
		{
			boxProfesionalServicio3 = new JComboBox<String>();
			boxProfesionalServicio3.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxProfesionalServicio3.setEditable(false);
			boxProfesionalServicio3.setBounds(357, 168, 263, 31);
			subPanelCentral.add(boxProfesionalServicio3);
		}
		{
			lblServicio4 = new JLabel("Servicio 4");
			lblServicio4.setForeground(Color.WHITE);
			lblServicio4.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblServicio4.setBounds(12, 213, 81, 22);
			subPanelCentral.add(lblServicio4);
		}
		{
			txtServicio4 = new JTextField();
			txtServicio4.setEditable(false);
			txtServicio4.setHorizontalAlignment(SwingConstants.CENTER);
			txtServicio4.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtServicio4.setColumns(10);
			txtServicio4.setBounds(101, 208, 244, 31);
			subPanelCentral.add(txtServicio4);
		}
		{
			boxProfesionalServicio4 = new JComboBox<String>();
			boxProfesionalServicio4.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxProfesionalServicio4.setEditable(false);
			boxProfesionalServicio4.setBounds(357, 208, 263, 31);
			subPanelCentral.add(boxProfesionalServicio4);
		}
		{
			lblServicio5 = new JLabel("Servicio 5");
			lblServicio5.setForeground(Color.WHITE);
			lblServicio5.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblServicio5.setBounds(12, 253, 81, 22);
			subPanelCentral.add(lblServicio5);
		}
		{
			txtServicio5 = new JTextField();
			txtServicio5.setEditable(false);
			txtServicio5.setHorizontalAlignment(SwingConstants.CENTER);
			txtServicio5.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtServicio5.setColumns(10);
			txtServicio5.setBounds(101, 248, 244, 31);
			subPanelCentral.add(txtServicio5);
		}
		{
			boxProfesionalServicio5 = new JComboBox<String>();
			boxProfesionalServicio5.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxProfesionalServicio5.setEditable(false);
			boxProfesionalServicio5.setBounds(357, 248, 263, 31);
			subPanelCentral.add(boxProfesionalServicio5);
		}
		{
			separator = new JSeparator();
			separator.setBounds(12, 336, 608, 10);
			subPanelCentral.add(separator);
		}
		{
			lblSeleccioneElProfesional = new JLabel("Seleccione el profesional de cada servicio");
			lblSeleccioneElProfesional.setForeground(Color.WHITE);
			lblSeleccioneElProfesional.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblSeleccioneElProfesional.setBounds(161, 33, 380, 25);
			subPanelCentral.add(lblSeleccioneElProfesional);
		}
		{
			lblDetalleDeLa = new JLabel("detalle de la promoción");
			lblDetalleDeLa.setForeground(Color.WHITE);
			lblDetalleDeLa.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblDetalleDeLa.setBounds(231, 370, 222, 25);
			subPanelCentral.add(lblDetalleDeLa);
		}
		{
			lblNombrePromocion = new JLabel("Nombre");
			lblNombrePromocion.setForeground(Color.WHITE);
			lblNombrePromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblNombrePromocion.setBounds(129, 427, 81, 22);
			subPanelCentral.add(lblNombrePromocion);
		}
		{
			txtNombrePromocion = new JTextField();
			txtNombrePromocion.setHorizontalAlignment(SwingConstants.CENTER);
			txtNombrePromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtNombrePromocion.setEditable(false);
			txtNombrePromocion.setColumns(10);
			txtNombrePromocion.setBounds(218, 423, 244, 31);
			subPanelCentral.add(txtNombrePromocion);
		}
		{
			txtPrecioPromocion = new JTextField();
			txtPrecioPromocion.setHorizontalAlignment(SwingConstants.CENTER);
			txtPrecioPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			txtPrecioPromocion.setEditable(false);
			txtPrecioPromocion.setColumns(10);
			txtPrecioPromocion.setBounds(218, 463, 244, 31);
			subPanelCentral.add(txtPrecioPromocion);
		}
		{
			lblPrecioPromocion = new JLabel("Precio");
			lblPrecioPromocion.setForeground(Color.WHITE);
			lblPrecioPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblPrecioPromocion.setBounds(125, 467, 81, 22);
			subPanelCentral.add(lblPrecioPromocion);
		}
		{
			lblHorariosPromocion = new JLabel("Horarios");
			lblHorariosPromocion.setForeground(Color.WHITE);
			lblHorariosPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblHorariosPromocion.setBounds(125, 546, 81, 22);
			subPanelCentral.add(lblHorariosPromocion);
		}
		{
			boxHorariosPromocion = new JComboBox<String>();
			boxHorariosPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
			boxHorariosPromocion.setEditable(false);
			boxHorariosPromocion.setBounds(218, 542, 244, 31);
			subPanelCentral.add(boxHorariosPromocion);
		}
		
		lblFechaElegida = new JLabel("Fecha");
		lblFechaElegida.setForeground(Color.WHITE);
		lblFechaElegida.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFechaElegida.setBounds(129, 507, 81, 22);
		subPanelCentral.add(lblFechaElegida);
		
		txtFechaElegida = new JTextField();
		txtFechaElegida.setHorizontalAlignment(SwingConstants.CENTER);
		txtFechaElegida.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtFechaElegida.setEditable(false);
		txtFechaElegida.setColumns(10);
		txtFechaElegida.setBounds(218, 502, 244, 31);
		subPanelCentral.add(txtFechaElegida);
	}

	public void llenarComboProfesional(List<ProfesionalDTO> profesionales, int nroBox) {
		List<String> lista = new ArrayList<String>();
		boxProfesionalServicio1.removeAllItems();
		boxProfesionalServicio2.removeAllItems();
		boxProfesionalServicio3.removeAllItems();
		boxProfesionalServicio4.removeAllItems();
		boxProfesionalServicio5.removeAllItems();

		if (profesionales != null) {
			if (profesionales.size() > 0) {
				for (ProfesionalDTO p : profesionales) {
					lista.add(p.getNombre() + " " + p.getApellido() + ", " + p.getDni());
				}

				switch (nroBox) {
				case 0:
					for (String s : lista)
						boxProfesionalServicio1.addItem(s);
					boxProfesionalServicio1.setEditable(true);
					boxProfesionalServicio1.setSelectedIndex(0);
					AutoCompleteDecorator.decorate(boxProfesionalServicio1);
					break;
				case 1:
					for (String s : lista)
						boxProfesionalServicio2.addItem(s);
					boxProfesionalServicio2.setEditable(true);
					boxProfesionalServicio2.setSelectedIndex(0);
					AutoCompleteDecorator.decorate(boxProfesionalServicio2);
					break;
				case 2:
					for (String s : lista)
						boxProfesionalServicio3.addItem(s);
					boxProfesionalServicio3.setEditable(true);
					boxProfesionalServicio3.setSelectedIndex(0);
					AutoCompleteDecorator.decorate(boxProfesionalServicio3);
					break;
				case 3:
					for (String s : lista)
						boxProfesionalServicio4.addItem(s);
					boxProfesionalServicio4.setEditable(true);
					boxProfesionalServicio4.setSelectedIndex(0);
					AutoCompleteDecorator.decorate(boxProfesionalServicio4);
					break;
				case 4:
					for (String s : lista)
						boxProfesionalServicio5.addItem(s);
					boxProfesionalServicio5.setEditable(true);
					boxProfesionalServicio5.setSelectedIndex(0);
					AutoCompleteDecorator.decorate(boxProfesionalServicio5);
					break;
				}

			} else {
				switch (nroBox) {
				case 0:
					boxProfesionalServicio1.addItem("Servicio sin profesionales");
					boxProfesionalServicio1.setSelectedIndex(0);
					boxProfesionalServicio1.setEditable(false);
					break;

				case 1:
					boxProfesionalServicio2.addItem("Servicio sin profesionales");
					boxProfesionalServicio2.setSelectedIndex(0);
					boxProfesionalServicio2.setEditable(false);
					break;

				case 2:
					boxProfesionalServicio3.addItem("Servicio sin profesionales");
					boxProfesionalServicio3.setSelectedIndex(0);
					boxProfesionalServicio3.setEditable(false);
					break;

				case 3:
					boxProfesionalServicio4.addItem("Servicio sin profesionales");
					boxProfesionalServicio4.setSelectedIndex(0);
					boxProfesionalServicio4.setEditable(false);
					break;

				case 4:
					boxProfesionalServicio5.addItem("Servicio sin profesionales");
					boxProfesionalServicio5.setSelectedIndex(0);
					boxProfesionalServicio5.setEditable(false);
					break;
				}

			}
		} else {
			switch (nroBox) {
			case 0:
				boxProfesionalServicio1.addItem("Servicio sin profesionales");
				boxProfesionalServicio1.setSelectedIndex(0);
				boxProfesionalServicio1.setEditable(false);
				break;

			case 1:
				boxProfesionalServicio2.addItem("Servicio sin profesionales");
				boxProfesionalServicio2.setSelectedIndex(0);
				boxProfesionalServicio2.setEditable(false);
				break;

			case 2:
				boxProfesionalServicio3.addItem("Servicio sin profesionales");
				boxProfesionalServicio3.setSelectedIndex(0);
				boxProfesionalServicio3.setEditable(false);
				break;

			case 3:
				boxProfesionalServicio4.addItem("Servicio sin profesionales");
				boxProfesionalServicio4.setSelectedIndex(0);
				boxProfesionalServicio4.setEditable(false);
				break;

			case 4:
				boxProfesionalServicio5.addItem("Servicio sin profesionales");
				boxProfesionalServicio5.setSelectedIndex(0);
				boxProfesionalServicio5.setEditable(false);
				break;
			}
		}
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		{
			btnCancelar = new JButton("Cancelar");
			subPanelInferior.add(btnCancelar);
			btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		}
		{
			btnReservar = new JButton("Reservar");
			btnReservar.setFont(new Font("Tahoma", Font.PLAIN, 18));
			subPanelInferior.add(btnReservar);
		}
	}

	public JComboBox<String> getBoxProfesionalServicio1() {
		return boxProfesionalServicio1;
	}

	public void setBoxProfesionalServicio1(JComboBox<String> boxProfesionalServicio1) {
		this.boxProfesionalServicio1 = boxProfesionalServicio1;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JButton getBtnReservar() {
		return btnReservar;
	}

	public void setBtnReservar(JButton btnReservar) {
		this.btnReservar = btnReservar;
	}

	public JTextField getTxtServicio1() {
		return txtServicio1;
	}

	public void setTxtServicio1(JTextField txtServicio1) {
		this.txtServicio1 = txtServicio1;
	}

	public JTextField getTxtServicio2() {
		return txtServicio2;
	}

	public void setTxtServicio2(JTextField txtServicio2) {
		this.txtServicio2 = txtServicio2;
	}

	public JComboBox<String> getBoxProfesionalServicio2() {
		return boxProfesionalServicio2;
	}

	public void setBoxProfesionalServicio2(JComboBox<String> boxProfesionalServicio2) {
		this.boxProfesionalServicio2 = boxProfesionalServicio2;
	}

	public JTextField getTxtServicio3() {
		return txtServicio3;
	}

	public void setTxtServicio3(JTextField txtServicio3) {
		this.txtServicio3 = txtServicio3;
	}

	public JComboBox<String> getBoxProfesionalServicio3() {
		return boxProfesionalServicio3;
	}

	public void setBoxProfesionalServicio3(JComboBox<String> boxProfesionalServicio3) {
		this.boxProfesionalServicio3 = boxProfesionalServicio3;
	}

	public JTextField getTxtServicio4() {
		return txtServicio4;
	}

	public void setTxtServicio4(JTextField txtServicio4) {
		this.txtServicio4 = txtServicio4;
	}

	public JComboBox<String> getBoxProfesionalServicio4() {
		return boxProfesionalServicio4;
	}

	public void setBoxProfesionalServicio4(JComboBox<String> boxProfesionalServicio4) {
		this.boxProfesionalServicio4 = boxProfesionalServicio4;
	}

	public JTextField getTxtServicio5() {
		return txtServicio5;
	}

	public void setTxtServicio5(JTextField txtServicio5) {
		this.txtServicio5 = txtServicio5;
	}

	public JComboBox<String> getBoxProfesionalServicio5() {
		return boxProfesionalServicio5;
	}

	public void setBoxProfesionalServicio5(JComboBox<String> boxProfesionalServicio5) {
		this.boxProfesionalServicio5 = boxProfesionalServicio5;
	}

	public JTextField getTxtNombrePromocion() {
		return txtNombrePromocion;
	}

	public void setTxtNombrePromocion(JTextField txtNombrePromocion) {
		this.txtNombrePromocion = txtNombrePromocion;
	}

	public JTextField getTxtPrecioPromocion() {
		return txtPrecioPromocion;
	}

	public void setTxtPrecioPromocion(JTextField txtPrecioPromocion) {
		this.txtPrecioPromocion = txtPrecioPromocion;
	}

	public JComboBox<String> getBoxHorariosPromocion() {
		return boxHorariosPromocion;
	}

	public void setBoxHorariosPromocion(JComboBox<String> boxHorariosPromocion) {
		this.boxHorariosPromocion = boxHorariosPromocion;
	}
	
	public JTextField getTxtFechaElegida() {
		return txtFechaElegida;
	}

	public void setTxtFechaElegida(JTextField txtFechaElegida) {
		this.txtFechaElegida = txtFechaElegida;
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(PromocionVista.DISPOSE_ON_CLOSE);
	}
}
