package presentacion.vista.administrativo;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.awt.FlowLayout;
import com.toedter.calendar.JDateChooser;
import javax.swing.*;
import dto.ClienteDTO;
import dto.ProfesionalDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import javax.swing.JTextField;

public class AddTurnoVista extends JDialog {

	private static final long serialVersionUID = 1L;
	private static AddTurnoVista INSTANCE;
	private JPanel panelPrincipal;
	private JTable tablaServicios;
	private JScrollPane spTablaInformacion;
	private String[] nombreColumnasTabla;
	private DefaultTableModel modelInformacion;
	private JTableHeader tableHeader;
	private JTextField txtTotal;
	private JTextField txtFechaElegida;
	private JTextField txtHoraInicio;
	private JPanel subPanelSuperior;
	private JLabel lblAgregarTurnoNuevo;
	private JPanel subPanelInferior;
	private JButton btnReiniciar;
	private JButton btnAgregarTurno;
	private JPanel subPanelIzquierdo;
	private JPanel subPanelDerecho;
	private JPanel subPanelCentral;
	private JDateChooser dateChooser;
	private JLabel lblFechaDelTurno;
	private JLabel lblServicio;
	private JComboBox<String> boxServicio;
	private JLabel lblProfesional;
	private JComboBox<String> boxProfesional;
	private JLabel lblHorario;
	private JComboBox<String> boxHorario;
	private JLabel lblPromocion;
	private JComboBox<String> boxPromocion;
	private JButton btnAgregarServicio;
	private JPanel subPanelTablaServicios;
	private JLabel lblCliente;
	private JComboBox<String> boxCliente;
	private JButton btnFijarFecha;
	private JPanel subPanelResumen;
	private JLabel lblFechaElegida;
	private JLabel lblInicio;
	private JLabel lblTotal;
	private JTextField txtHoraFin;
	private JButton btnBuscarHorarios;
	private JButton btnSeleccionarPromocion;
	private JButton btnSeleccionarServicio;

	public static AddTurnoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AddTurnoVista();
			return new AddTurnoVista();
		}else {
			return INSTANCE;
		}
	}

	public AddTurnoVista() {
		super();
		setPropiedades();
		ocultar();
	}

	private void setPropiedades() {
		setModal(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 670, 770);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setTitle("Agregar Turno");
		controlDeCierre();
		setPaneles();
	}

	private void setPaneles() {
		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelInferior();
		setSubPanelDerecho();
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		setTituloInformativo();
	}

	private void setTituloInformativo() {
		lblAgregarTurnoNuevo = new JLabel("Agregar Turno nuevo");
		lblAgregarTurnoNuevo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblAgregarTurnoNuevo.setForeground(Color.WHITE);
		subPanelSuperior.add(lblAgregarTurnoNuevo);
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
	}

	public void setSubPanelCentral() {
		setPropiedadesSubPanelCentral();
		setFormulario();
		setSubPanelTablaServicios();
	}

	private void setPropiedadesSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.GRAY);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(null);
	}

	private void setFormulario() {
		setCliente();
		setCalendario();
		setServicio();
		setProfesional();
		setHorario();
		setPromocion();
		setBotonVerPromocion();
		setBotonAgregar();
	}

	private void setCliente() {
		lblCliente = new JLabel("Cliente");
		lblCliente.setForeground(Color.WHITE);
		lblCliente.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCliente.setBounds(46, 31, 94, 16);
		subPanelCentral.add(lblCliente);

		boxCliente = new JComboBox<String>();
		boxCliente.setMaximumRowCount(5);
		boxCliente.setEditable(true);
		boxCliente.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxCliente.setBounds(152, 25, 447, 31);
		subPanelCentral.add(boxCliente);

	}

	public void llenarComboCliente(List<ClienteDTO> clientes) {
		List<String> lista = new ArrayList<String>();
		if (clientes != null) {
			if (clientes.size() != 0) {
				for (ClienteDTO c : clientes) {
					lista.add(c.getNombre() + " " + c.getApellido() + ", " + c.getDni());
				}
			}
		}

		boxCliente.setToolTipText("<html><body><h3><font color=red>Seleccione</font> un Cliente</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes seleccionarlo directo de la lista o realizar una búsqueda</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>");

		for (String s : lista) {
			boxCliente.addItem(s);
		}

		AutoCompleteDecorator.decorate(boxCliente);
	}

	private void setCalendario() {
		dateChooser = new JDateChooser();
		dateChooser.setFont(new Font("Tahoma", Font.PLAIN, 18));
		dateChooser.setBounds(152, 69, 307, 31);
		subPanelCentral.add(dateChooser);

		lblFechaDelTurno = new JLabel("Fecha");
		lblFechaDelTurno.setForeground(Color.WHITE);
		lblFechaDelTurno.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFechaDelTurno.setBounds(46, 75, 94, 16);
		subPanelCentral.add(lblFechaDelTurno);

		btnFijarFecha = new JButton("Fijar fecha");
		btnFijarFecha.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnFijarFecha.setBounds(471, 69, 128, 31);
		subPanelCentral.add(btnFijarFecha);
	}

	private void setServicio() {
		lblServicio = new JLabel("Servicio");
		lblServicio.setForeground(Color.WHITE);
		lblServicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblServicio.setBounds(46, 162, 94, 16);
		subPanelCentral.add(lblServicio);

		boxServicio = new JComboBox<String>();
		boxServicio.setEditable(true);
		boxServicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxServicio.setBounds(152, 157, 307, 31);
		subPanelCentral.add(boxServicio);
	}

	public void llenarComboServicio(List<ServicioDTO> servicios) {
		boxServicio.removeAllItems();
		List<String> lista = new ArrayList<String>();
		if (servicios != null) {
			if (servicios.size() != 0) {
				for (ServicioDTO s : servicios) {
					lista.add(s.getNombre() + " - " + s.getPrecio());
				}
			}
		}

		boxCliente.setToolTipText("<html><body><h3><font color=red>Seleccione</font> un servicio</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes seleccionarlo directo de la lista o realizar una búsqueda</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>");

		for (String s : lista) {
			boxServicio.addItem(s);
			boxServicio.setSelectedIndex(0);
		}

		AutoCompleteDecorator.decorate(boxServicio);
	}

	private void setProfesional() {
		lblProfesional = new JLabel("Profesional");
		lblProfesional.setForeground(Color.WHITE);
		lblProfesional.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblProfesional.setBounds(46, 206, 94, 16);
		subPanelCentral.add(lblProfesional);

		boxProfesional = new JComboBox<String>();
		boxProfesional.setEditable(true);
		boxProfesional.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxProfesional.setBounds(152, 201, 307, 31);
		subPanelCentral.add(boxProfesional);
	}

	public void llenarComboProfesional(List<ProfesionalDTO> profesionales) {
		List<String> lista = new ArrayList<String>();
		boxProfesional.removeAllItems();

		if (profesionales != null) {
			if (profesionales.size() > 0) {
				for (ProfesionalDTO p : profesionales) {
					lista.add(p.getNombre() + " " + p.getApellido() + ", " + p.getDni());
				}

				for (String s : lista) {
					boxProfesional.addItem(s);
				}
				
				boxProfesional.setEditable(true);
				boxProfesional.setSelectedIndex(0);
				AutoCompleteDecorator.decorate(boxProfesional);

			} else {
				boxProfesional.addItem("Servicio sin profesionales");
				boxProfesional.setSelectedIndex(0);
				boxProfesional.setEditable(false);
			}
		} else {
			boxProfesional.addItem("Servicio sin profesionales");
			boxProfesional.setSelectedIndex(0);
			boxProfesional.setEditable(false);
		}

		boxProfesional.setToolTipText("<html><body><h3><font color=red>Seleccione</font> un profesional</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes seleccionarlo directo de la lista o realizar una búsqueda</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>");
	}

	private void setHorario() {
		lblHorario = new JLabel("Horario");
		lblHorario.setForeground(Color.WHITE);
		lblHorario.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblHorario.setBounds(46, 250, 94, 16);
		subPanelCentral.add(lblHorario);

		boxHorario = new JComboBox<String>();
		boxHorario.setEditable(false);
		boxHorario.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxHorario.setBounds(152, 245, 307, 31);
		subPanelCentral.add(boxHorario);
	}

	public void llenarComboHorario(List<String> horarios) {
		boxHorario.removeAllItems();
		if (horarios != null) {
			if (horarios.size() > 0) {
				for (String h : horarios) {
					boxHorario.addItem(h);
				}
			}else {
				boxHorario.addItem("Sin disponibilidad");
				boxHorario.setSelectedIndex(0);
			}
		}else {
			boxHorario.addItem("Sin disponibilidad");
			boxHorario.setSelectedIndex(0);
		}

		boxHorario.setToolTipText("<html><body><h3><font color=red>Seleccione</font> un servicio</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes seleccionarlo directo de la lista o realizar una búsqueda</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>");

		//AutoCompleteDecorator.decorate(boxHorario);
	}

	private void setPromocion() {
		lblPromocion = new JLabel("Promoción");
		lblPromocion.setForeground(Color.WHITE);
		lblPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPromocion.setBounds(46, 118, 94, 16);
		subPanelCentral.add(lblPromocion);

		boxPromocion = new JComboBox<String>();
		boxPromocion.setEditable(true);
		boxPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxPromocion.setBounds(152, 113, 307, 31);
		subPanelCentral.add(boxPromocion);
	}

	public void llenarComboPromocion(List<PromocionDTO> promociones) {
		boxPromocion.removeAllItems();
		List<String> lista = new ArrayList<String>();
		if (promociones != null) {
			if (promociones.size() != 0) {
				for (PromocionDTO p : promociones) {
					lista.add(p.getNombre());
				}
			}
		}

		boxCliente.setToolTipText("<html><body><h3><font color=red>Seleccione</font> una promoción</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes seleccionarla directo de la lista o realizar una búsqueda</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>");

		for (String s : lista) {
			boxPromocion.addItem(s);
		}

		AutoCompleteDecorator.decorate(boxPromocion);
	}
	

	private void setBotonVerPromocion() {
	}

	private void setBotonAgregar() {
		btnAgregarServicio = new JButton("Agregar");
		btnAgregarServicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnAgregarServicio.setBounds(471, 243, 128, 31);
		subPanelCentral.add(btnAgregarServicio);
	}

	private void setSubPanelTablaServicios() {
		setPropiedadesSubPanelTablaServicios();
		setScrollDeTabla();
		setTablaServicios();
		setSubPanelResumen();
		setInfoFechaElegida();
		setInfoHoraElegida();
		setInfoTotal();
	}

	private void setPropiedadesSubPanelTablaServicios() {
		subPanelTablaServicios = new JPanel();
		subPanelTablaServicios.setBackground(Color.DARK_GRAY);
		subPanelTablaServicios.setBounds(0, 333, 644, 253);
		subPanelCentral.add(subPanelTablaServicios);
		subPanelTablaServicios.setLayout(null);
	}

	private void setScrollDeTabla() {
		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBounds(0, 0, 644, 264);
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		subPanelTablaServicios.add(spTablaInformacion);
	}

	private void setTablaServicios() {
		nombreColumnasTabla = new String[] { "Servicio", "Profesional", "Precio" };
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla);
		tablaServicios = new JTable(modelInformacion);

		tablaServicios.setBackground(new Color(64, 64, 64));
		tablaServicios.setForeground(Color.white);
		tablaServicios.setFont(new Font("Tahoma", Font.PLAIN, 25));
		tablaServicios.setRowHeight(30);
		tablaServicios.setShowGrid(false);

		tableHeader = tablaServicios.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaServicios);
	}

	private void setSubPanelResumen() {
		subPanelResumen = new JPanel();
		subPanelResumen.setBounds(0, 585, 632, 55);
		subPanelCentral.add(subPanelResumen);
		subPanelResumen.setBackground(Color.GRAY);
		
		btnBuscarHorarios = new JButton("Ver Horario");
		btnBuscarHorarios.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnBuscarHorarios.setBounds(471, 199, 128, 31);
		subPanelCentral.add(btnBuscarHorarios);
		
		btnSeleccionarPromocion = new JButton("Seleccionar");
		btnSeleccionarPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSeleccionarPromocion.setBounds(471, 113, 128, 31);
		subPanelCentral.add(btnSeleccionarPromocion);
		
		btnSeleccionarServicio = new JButton("Seleccionar");
		btnSeleccionarServicio.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSeleccionarServicio.setBounds(471, 155, 128, 31);
		subPanelCentral.add(btnSeleccionarServicio);
	}

	private void setInfoFechaElegida() {
		subPanelResumen.setLayout(null);
		lblFechaElegida = new JLabel("Fecha:");
		lblFechaElegida.setBounds(12, 20, 51, 22);
		subPanelResumen.add(lblFechaElegida);
		lblFechaElegida.setForeground(Color.WHITE);
		lblFechaElegida.setFont(new Font("Tahoma", Font.PLAIN, 18));

		txtFechaElegida = new JTextField();
		txtFechaElegida.setHorizontalAlignment(SwingConstants.CENTER);
		txtFechaElegida.setFont(new Font("Tahoma", Font.BOLD, 16));
		txtFechaElegida.setBounds(64, 21, 105, 22);
		subPanelResumen.add(txtFechaElegida);
		txtFechaElegida.setColumns(10);
	}

	private void setInfoHoraElegida() {
		lblInicio = new JLabel("Inicio:");
		lblInicio.setBounds(181, 20, 49, 22);
		subPanelResumen.add(lblInicio);
		lblInicio.setForeground(Color.WHITE);
		lblInicio.setFont(new Font("Tahoma", Font.PLAIN, 18));

		txtHoraInicio = new JTextField();
		txtHoraInicio.setHorizontalAlignment(SwingConstants.CENTER);
		txtHoraInicio.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtHoraInicio.setBounds(235, 20, 79, 22);
		subPanelResumen.add(txtHoraInicio);
		txtHoraInicio.setColumns(10);
	}

	private void setInfoTotal() {
		lblTotal = new JLabel("Total:");
		lblTotal.setBounds(448, 20, 46, 22);
		subPanelResumen.add(lblTotal);
		lblTotal.setForeground(Color.WHITE);
		lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 18));

		txtTotal = new JTextField();
		txtTotal.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotal.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtTotal.setBounds(501, 21, 131, 22);
		subPanelResumen.add(txtTotal);
		txtTotal.setColumns(10);
		
		JLabel lblFin = new JLabel("Fin:");
		lblFin.setForeground(Color.WHITE);
		lblFin.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFin.setBounds(326, 20, 32, 22);
		subPanelResumen.add(lblFin);
		
		txtHoraFin = new JTextField();
		txtHoraFin.setHorizontalAlignment(SwingConstants.CENTER);
		txtHoraFin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtHoraFin.setColumns(10);
		txtHoraFin.setBounds(357, 21, 79, 22);
		subPanelResumen.add(txtHoraFin);
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setPreferredSize(new Dimension(10, 60));
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 20));

		btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnReiniciar.setPreferredSize(new Dimension(150, 25));
		subPanelInferior.add(btnReiniciar);

		btnAgregarTurno = new JButton("Agregar turno");
		btnAgregarTurno.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnAgregarTurno.setPreferredSize(new Dimension(150, 25));
		subPanelInferior.add(btnAgregarTurno);
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JTable getTablaServicios() {
		return tablaServicios;
	}

	public void setTablaServicios(JTable tablaServicios) {
		this.tablaServicios = tablaServicios;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

	public JTextField getTxtTotal() {
		return txtTotal;
	}

	public void setTxtTotal(JTextField txtTotal) {
		this.txtTotal = txtTotal;
	}

	public JTextField getTxtFechaElegida() {
		return txtFechaElegida;
	}

	public void setTxtFechaElegida(JTextField txtFechaElegida) {
		this.txtFechaElegida = txtFechaElegida;
	}

	public JTextField getTxtHoraInicio() {
		return txtHoraInicio;
	}

	public void setTextField(JTextField txtHoraInicio) {
		this.txtHoraInicio = txtHoraInicio;
	}

	public JButton getBtnReiniciar() {
		return btnReiniciar;
	}

	public void setBtnReiniciar(JButton btnReiniciar) {
		this.btnReiniciar = btnReiniciar;
	}

	public JButton getBtnAgregarTurno() {
		return btnAgregarTurno;
	}

	public void setBtnAgregarTurno(JButton btnAgregarTurno) {
		this.btnAgregarTurno = btnAgregarTurno;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(JDateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public JComboBox<String> getBoxServicio() {
		return boxServicio;
	}

	public int getIndexServicio() {
		return this.boxServicio.getSelectedIndex();
	}

	public void setBoxServicio(JComboBox<String> boxServicio) {
		this.boxServicio = boxServicio;
	}

	public JComboBox<String> getBoxProfesional() {
		return boxProfesional;
	}

	public int getIndexProfesional() {
		return this.boxProfesional.getSelectedIndex();
	}

	public void setBoxProfesional(JComboBox<String> boxProfesional) {
		this.boxProfesional = boxProfesional;
	}

	public JComboBox<String> getBoxHorario() {
		return boxHorario;
	}

	public int getIndexHorario() {
		return this.boxHorario.getSelectedIndex();
	}

	public void setBoxHorario(JComboBox<String> boxHorario) {
		this.boxHorario = boxHorario;
	}

	public JComboBox<String> getBoxPromocion() {
		return boxPromocion;
	}

	public int getIndexPromocion() {
		return this.boxPromocion.getSelectedIndex();
	}

	public void setBoxPromocion(JComboBox<String> boxPromocion) {
		this.boxPromocion = boxPromocion;
	}

	public JButton getBtnAgregarServicio() {
		return btnAgregarServicio;
	}

	public void setBtnAgregarServicior(JButton btnAgregar) {
		this.btnAgregarServicio = btnAgregar;
	}

	public JComboBox<String> getBoxCliente() {
		return boxCliente;
	}

	public int getIndexCliente() {
		return this.boxCliente.getSelectedIndex();
	}

	public void setBoxCliente(JComboBox<String> boxCliente) {
		this.boxCliente = boxCliente;
	}

	public JButton getBtnFijarFecha() {
		return btnFijarFecha;
	}

	public void setBtnFijarFecha(JButton btnFijarFecha) {
		this.btnFijarFecha = btnFijarFecha;
	}
	
	public void setTxtHoraInicio(JTextField txtHoraInicio) {
		this.txtHoraInicio = txtHoraInicio;
	}
	
	public JTextField getTxtHoraFin() {
		return txtHoraFin;
	}

	public void setTxtHoraFin(JTextField txtHoraFin) {
		this.txtHoraFin = txtHoraFin;
	}

	public JButton getBtnBuscarHorarios() {
		return btnBuscarHorarios;
	}

	public void setBtnBuscarHorarios(JButton btnBuscarHorarios) {
		this.btnBuscarHorarios = btnBuscarHorarios;
	}

	public void setBtnReservarServicio(JButton btnReservarServicio) {
		this.btnAgregarServicio = btnReservarServicio;
	}

	public void setBtnAgregarServicio(JButton btnAgregarServicio) {
		this.btnAgregarServicio = btnAgregarServicio;
	}

	public JButton getBtnSeleccionarPromocion() {
		return btnSeleccionarPromocion;
	}

	public void setBtnSeleccionarPromocion(JButton btnSeleccionarPromocion) {
		this.btnSeleccionarPromocion = btnSeleccionarPromocion;
	}

	public JButton getBtnSeleccionarServicio() {
		return btnSeleccionarServicio;
	}

	public void setBtnSeleccionarServicio(JButton btnSeleccionarServicio) {
		this.btnSeleccionarServicio = btnSeleccionarServicio;
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(AddTurnoVista.DISPOSE_ON_CLOSE);
	}
	
	public void AgregarServicioALaTabla(ServicioDTO servicio, ProfesionalDTO profesional) {
			String nombreServicio = servicio.getNombre();
			String Nombreprofesional = profesional.getNombre();
			String precio = Float.toString(servicio.getPrecio());
		
			// {  "Servicio", "Profesional", "Precio" };
			Object[] fila = { nombreServicio, Nombreprofesional, precio };
			this.getModelInformacion().addRow(fila);
	}
	
	public void vaciarTabla() {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());
	}
}
