package presentacion.vista.administrativo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.sql.Time;
import java.util.List;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.Cursor;
import dto.TurnoDTO;

import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import com.toedter.calendar.JDateChooser;

import javax.swing.JComboBox;

public class AdministrativoVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel subPanelInferior;
	private JPanel subPanelDerecho;
	private JCheckBox boxPromocion;
	private JCheckBox boxPagoPendiente;
	private JCheckBox boxDemorado;
	private JLabel lblFiltros;
	private JPanel subPanelCentral;
	private JPanel panelSuperior_tabla;
	private JPanel subPanelIzquierdo;
	private JLabel lblLogo;
	private JPanel subPanelSuperior;
	private JLabel lblUsuario;
	private JLabel lblSucursal;
	private static AdministrativoVista INSTANCE;
	private JPanel panel_1;
	private JPanel panel_3;
	private JPanel panelInferior_tabla;
	private JPanel panelCentral_tabla;
	private JTable tablaTurnos;
	private DefaultTableModel modelInformacion;
	private String[] nombreColumnasTabla;
	private JScrollPane spTablaInformacion;
	private JTableHeader tableHeader;
	private JButton botonCrearTurno;
	private JButton botonCancelarTurno;
	private JButton botonPerspectivaTurno;
	private JButton botonPagarTurno;
	private JDateChooser dateChooser;
	private JComboBox<Object> comboBoxHoras;
	private JButton botonFiltrarTurnos;
	private JButton botonLimpiarFiltros;
	private JButton btnClientes;
	private JButton btnStock;
	private JButton btnAvisos;
	private JButton btnHelp;
	private JButton botonCerrarTurno;
	private JButton botonEgreso;
	private JLabel label_1;
	private JLabel label_2;
	private JPanel panelIconosFiltros;
	private JCheckBox boxCancelados;
	private JComboBox<Object> boxLang;
	private JLabel lblFecha;
	private JLabel lblHora;
	private JLabel lblModoAdministrativo;
	private JButton btnSetIdioma;
	private JLabel label_3;
	private JLabel label;

	public static AdministrativoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AdministrativoVista();
			return new AdministrativoVista();
		} else {
			return INSTANCE;
		}
	}

	private AdministrativoVista() {

		setPropiedades();

		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelDerecho();
		setSubPanelInferior();

		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 1073, 816);
		setMinimumSize(new Dimension(1024, 850));
		getContentPane().setLayout(new BorderLayout(0, 0));
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("Administrativo");
		controlDeCierre();
	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 30));

		setPropiedadesDelBotonEgresos();
		setPropiedadesDelBotonCrearTurno();
		setPropiedadesDelBotonPagarTurno();
		setPropiedadesDelBotonCerrarTurno();
		setPropiedadesDelBotonCancelarTurno();
	}

	private void setPropiedadesDelBotonEgresos() {
		// se crean los iconos que se van a usar
		ImageIcon iconoBotonEgreso = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/registrarEgreso.png"));
		ImageIcon iconoBotonEgreso_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/registrarEgreso_rollover.png"));
		ImageIcon iconoBotonEgreso_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/registrarEgreso_pressed.png"));

		botonEgreso = new JButton(iconoBotonEgreso);
		botonEgreso.setFocusable(false);
		botonEgreso.setBorderPainted(false);
		botonEgreso.setRolloverIcon(iconoBotonEgreso_rollover);
		botonEgreso.setPressedIcon(iconoBotonEgreso_pressed);
		botonEgreso.setOpaque(false);// se hace transparente todo el boton default JButton
		botonEgreso.setContentAreaFilled(false);
		botonEgreso.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonEgreso.getWidth();
		int alto = botonEgreso.getHeight();
		iconoBotonEgreso.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoBotonEgreso_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoBotonEgreso_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Clic para <font color=red>registrar un egreso.</font></h3>"
				+ "</body></html>";
		botonEgreso.setToolTipText(html);
		subPanelInferior.add(botonEgreso);
	}

	private void setPropiedadesDelBotonPagarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoPagarTurno = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno.png"));
		ImageIcon iconoPagarTurno_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno_rollover.png"));
		ImageIcon iconoPagarTurno_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno_pressed.png"));

		botonPagarTurno = new JButton(iconoPagarTurno);
		botonPagarTurno.setFocusable(false);
		botonPagarTurno.setBorderPainted(false);
		botonPagarTurno.setRolloverIcon(iconoPagarTurno_rollover);
		botonPagarTurno.setPressedIcon(iconoPagarTurno_pressed);
		botonPagarTurno.setOpaque(false);// se hace transparente todo el boton default JButton
		botonPagarTurno.setContentAreaFilled(false);
		botonPagarTurno.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonPagarTurno.getWidth();
		int alto = botonPagarTurno.getHeight();
		iconoPagarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoPagarTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoPagarTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Abonar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=======================================================</p></font>"
				+ "<font size=3><p><b>Puedes pagar de forma parcial o completa el turno seleccionado</b></p></font>"
				+ "<font size=3 color=red><p>=======================================================</p></font>"
				+ "</body></html>";
		botonPagarTurno.setToolTipText(html);
		subPanelInferior.add(botonPagarTurno);
	}

	private void setPropiedadesDelBotonCrearTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCrearTurno = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/agregarTurno.png"));
		ImageIcon iconoCrearTurno_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/agregarTurno_rollover.png"));
		ImageIcon iiconoCrearTurno_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/agregarTurno_pressed.png"));

		botonCrearTurno = new JButton(iconoCrearTurno);
		botonCrearTurno.setFocusable(false);
		botonCrearTurno.setBorderPainted(false);
		botonCrearTurno.setRolloverIcon(iconoCrearTurno_rollover);
		botonCrearTurno.setPressedIcon(iiconoCrearTurno_pressed);
		botonCrearTurno.setOpaque(false);// se hace transparente todo el boton default JButton
		botonCrearTurno.setContentAreaFilled(false);
		botonCrearTurno.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonCrearTurno.getWidth();
		int alto = botonCrearTurno.getHeight();
		iconoCrearTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCrearTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iiconoCrearTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Crear</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes crear un turno completando los datos solicitados</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		botonCrearTurno.setToolTipText(html);
		subPanelInferior.add(botonCrearTurno);
	}

	private void setPropiedadesDelBotonCancelarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCancelarTurno = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cancelarTurno.png"));
		ImageIcon iconoCancelarTurno_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cancelarTurno_rollover.png"));
		ImageIcon iconoCancelarTurno_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cancelarTurno_pressed.png"));

		botonCancelarTurno = new JButton(iconoCancelarTurno);
		botonCancelarTurno.setFocusable(false);
		botonCancelarTurno.setRolloverIcon(iconoCancelarTurno_rollover);
		botonCancelarTurno.setPressedIcon(iconoCancelarTurno_pressed);
		botonCancelarTurno.setOpaque(false);// se hace transparente todo el boton default JButton
		botonCancelarTurno.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		botonCancelarTurno.setBorderPainted(false);// borra el borde default del JButton
		botonCancelarTurno.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonCancelarTurno.getWidth();
		int alto = botonCancelarTurno.getHeight();
		iconoCancelarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCancelarTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCancelarTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Cancelar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes cancelar un turno seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		botonCancelarTurno.setToolTipText(html);
		subPanelInferior.add(botonCancelarTurno);
	}

	private void setPropiedadesDelBotonCerrarTurno() {
		// se crean los iconos que se van a usar
		ImageIcon iconoCerrarTurno = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cerrarTurno.png"));
		ImageIcon iconoCerrarTurno_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cerrarTurno_rollover.png"));
		ImageIcon iconoCerrarTurno_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/cerrarTurno_pressed.png"));

		botonCerrarTurno = new JButton(iconoCerrarTurno);
		botonCerrarTurno.setFocusable(false);
		botonCerrarTurno.setRolloverIcon(iconoCerrarTurno_rollover);
		botonCerrarTurno.setPressedIcon(iconoCerrarTurno_pressed);
		botonCerrarTurno.setOpaque(false);// se hace transparente todo el boton default JButton
		botonCerrarTurno.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		botonCerrarTurno.setBorderPainted(false);// borra el borde default del JButton
		botonCerrarTurno.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonCerrarTurno.getWidth();
		int alto = botonCerrarTurno.getHeight();
		iconoCerrarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCerrarTurno_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCerrarTurno_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Cerrar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes cerrar un turno seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		botonCerrarTurno.setToolTipText(html);
		subPanelInferior.add(botonCerrarTurno);
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setPreferredSize(new Dimension(300, 10));
		subPanelDerecho.setBorder(null);
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
		subPanelDerecho.setLayout(new GridLayout(2, 0, 0, 0));

		panel_1 = new JPanel();
		panel_1.setOpaque(false);
		subPanelDerecho.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 10));

		lblFiltros = new JLabel("Filtrar por:");
		panel_1.add(lblFiltros);
		lblFiltros.setHorizontalAlignment(SwingConstants.CENTER);
		lblFiltros.setForeground(Color.WHITE);
		lblFiltros.setFont(new Font("Verdana", Font.PLAIN, 20));

		boxPromocion = new JCheckBox("Turnos con promoción");
		boxPromocion.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(boxPromocion);
		boxPromocion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxPromocion.setForeground(Color.WHITE);
		boxPromocion.setOpaque(false);

		boxPagoPendiente = new JCheckBox("Pagos pendientes");
		boxPagoPendiente.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(boxPagoPendiente);
		boxPagoPendiente.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxPagoPendiente.setForeground(Color.WHITE);
		boxPagoPendiente.setOpaque(false);

		boxDemorado = new JCheckBox("Turnos demorados");
		boxDemorado.setHorizontalAlignment(SwingConstants.LEFT);
		panel_1.add(boxDemorado);
		boxDemorado.setFont(new Font("Tahoma", Font.PLAIN, 18));
		boxDemorado.setForeground(Color.WHITE);
		boxDemorado.setOpaque(false);

		boxCancelados = new JCheckBox("Turnos cancelados");
		boxCancelados.setOpaque(false);
		boxCancelados.setHorizontalAlignment(SwingConstants.LEFT);
		boxCancelados.setForeground(Color.WHITE);
		boxCancelados.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_1.add(boxCancelados);

		panel_3 = new JPanel();
		panel_3.setOpaque(false);
		subPanelDerecho.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		lblLogo = new JLabel("");
		lblLogo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblLogo.setAlignmentY(Component.TOP_ALIGNMENT);
		lblLogo.setIcon(new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/logo-peluqueria2_blanca_chica.png")));
		lblLogo.setHorizontalAlignment(SwingConstants.LEFT);
		lblLogo.setForeground(Color.WHITE);
		lblLogo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_3.add(lblLogo);

		setPropiedadesDelBotonfiltrarTurnos();
		setPropiedadesDelBotonLimpiarfiltros();
	}

	private void setPropiedadesDelBotonfiltrarTurnos() {

		// se crean los iconos que se van a usar
		ImageIcon iconoFiltrarTurnos = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/filtrarTurnos.png"));
		ImageIcon iconoiconoFiltrarTurnos_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/filtrarTurnos_rollover.png"));
		ImageIcon iconoiconoFiltrarTurnos_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/filtrarTurnos_pressed.png"));

		botonFiltrarTurnos = new JButton(iconoFiltrarTurnos);
		botonFiltrarTurnos.setFocusable(false);
		botonFiltrarTurnos.setRolloverIcon(iconoiconoFiltrarTurnos_rollover);
		botonFiltrarTurnos.setPressedIcon(iconoiconoFiltrarTurnos_pressed);
		botonFiltrarTurnos.setOpaque(false);// se hace transparente todo el boton default JButton
		botonFiltrarTurnos.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		botonFiltrarTurnos.setBorderPainted(false);
		botonFiltrarTurnos.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonFiltrarTurnos.getWidth();
		int alto = botonFiltrarTurnos.getHeight();
		iconoFiltrarTurnos.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoiconoFiltrarTurnos_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoiconoFiltrarTurnos_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Aplicar los filtros</font> de Turnos</h3>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "<font size=3><p><b>Puedes filtrar la información de la tabla estableciendo filtros</b></p></font>"
				+ "<font size=3 color=red><p>========================================================</p></font>"
				+ "</body></html>";

		panelIconosFiltros = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelIconosFiltros.getLayout();
		flowLayout.setVgap(15);
		flowLayout.setHgap(0);
		panelIconosFiltros.setOpaque(false);
		panel_1.add(panelIconosFiltros);

		botonFiltrarTurnos.setToolTipText(html);
		panelIconosFiltros.add(botonFiltrarTurnos);
	}

	private void setPropiedadesDelBotonLimpiarfiltros() {

		// se crean los iconos que se van a usar
		ImageIcon iconoLimpiarFiltros = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/limpiarFiltros.png"));
		ImageIcon iconoLimpiarFiltros_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/limpiarFiltros_rollover.png"));
		ImageIcon iconoLimpiarFiltros_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/limpiarFiltros_pressed.png"));

		botonLimpiarFiltros = new JButton(
				new ImageIcon(AdministrativoVista.class.getResource("/presentacion/vista/img/limpiarFiltros.png")));
		botonLimpiarFiltros.setFocusable(false);
		botonLimpiarFiltros.setRolloverIcon(iconoLimpiarFiltros_rollover);
		botonLimpiarFiltros.setPressedIcon(iconoLimpiarFiltros_pressed);
		botonLimpiarFiltros.setOpaque(false);// se hace transparente todo el boton default JButton
		botonLimpiarFiltros.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		botonLimpiarFiltros.setBorderPainted(false);
		botonLimpiarFiltros.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = botonLimpiarFiltros.getWidth();
		int alto = botonLimpiarFiltros.getHeight();
		iconoLimpiarFiltros.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoLimpiarFiltros_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoLimpiarFiltros_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Limpiar los filtros</font> de Turnos</h3>"
				+ "<font size=3 color=red><p>=======================================================</p></font>"
				+ "<font size=3><p><b>Puedes limpiar los filtros para ver todos los turnos del día</b></p></font>"
				+ "<font size=3 color=red><p>=======================================================</p></font>"
				+ "</body></html>";

		botonLimpiarFiltros.setToolTipText(html);
		panelIconosFiltros.add(botonLimpiarFiltros);
	}

	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(new BorderLayout(0, 0));

		panelSuperior_tabla = new JPanel();
		panelSuperior_tabla.setPreferredSize(new Dimension(10, 50));
		panelSuperior_tabla.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSuperior_tabla.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSuperior_tabla.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelSuperior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelSuperior_tabla, BorderLayout.NORTH);
		panelSuperior_tabla.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		panelInferior_tabla = new JPanel();
		panelInferior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelInferior_tabla, BorderLayout.SOUTH);

		panelCentral_tabla = new JPanel();
		panelCentral_tabla.setBackground(Color.WHITE);
		subPanelCentral.add(panelCentral_tabla, BorderLayout.CENTER);
		panelCentral_tabla.setLayout(new BoxLayout(panelCentral_tabla, BoxLayout.Y_AXIS));

		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(0, 0, 976, 547);
		panelCentral_tabla.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Fecha", "Inicio", "fin", "Cliente", "Precio", "Puntaje", "Pagado",
				"Estado" };
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla);
		tablaTurnos = new JTable(modelInformacion);
		tableHeader = tablaTurnos.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaTurnos.setBackground(new Color(64, 64, 64));
		tablaTurnos.setForeground(Color.white);
		tablaTurnos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaTurnos.setRowHeight(30);
		tablaTurnos.setShowGrid(false);

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaTurnos);
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setPreferredSize(new Dimension(120, 10));
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
		subPanelIzquierdo.setSize(new Dimension(300, 50));

		setPropiedadesDelBtnClientes();
		///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
		///////////////////////// PROPIEDADES//////////////////////////////////
		setPropiedadesDelBotonAvisos();
		///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
		///////////////////////// PROPIEDADES//////////////////////////////////
		
		///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
		///////////////////////// PROPIEDADES//////////////////////////////////
		setPropiedadesDelBotonReportes();
		///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
		///////////////////////// PROPIEDADES//////////////////////////////////
		setPropiedadesDelBtnStock();
		setPropiedadesDelBotonHelp();
	}

	private void setPropiedadesDelBtnStockborrar() {
		btnStock = new JButton("Stock");
		subPanelIzquierdo.add(btnStock);
	}
	
	private void setPropiedadesDelBtnStock() {
		
		btnStock = new JButton();
		btnStock.setVerticalAlignment(SwingConstants.BOTTOM);

		// se crean los iconos que se van a usar
		ImageIcon iconoStock = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/stock.png"));
		ImageIcon iconoStock_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/stock_rollover.png"));
		ImageIcon iconoStock_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/stock_pressed.png"));

		btnStock = new JButton(iconoStock);
		btnStock.setFocusable(false);
		btnStock.setRolloverIcon(iconoStock_rollover);
		btnStock.setPressedIcon(iconoStock_pressed);
		btnStock.setOpaque(false);// se hace transparente todo el boton default JButton
		btnStock.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnStock.setBorderPainted(false);// borra el borde default del JButton
		btnStock.setBounds(20, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnStock.getWidth();
		int alto = btnStock.getHeight();
		iconoStock.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoStock_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoStock_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para ver la vista de <font color=red>Stock</font></h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Permite ver el Stock de productos</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		subPanelIzquierdo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 30));
		btnStock.setToolTipText(html);
		subPanelIzquierdo.add(btnStock);
		
	}
	
	private void setPropiedadesDelBtnClientes() {
		
		// se crean los iconos que se van a usar
		ImageIcon iconoClientes = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/clientes.png"));
		ImageIcon iconoClientes_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/clientes_rollover.png"));
		ImageIcon iconoClientes_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/clientes_pressed.png"));

		btnClientes = new JButton(iconoClientes);
		btnClientes.setFocusable(false);
		btnClientes.setRolloverIcon(iconoClientes_rollover);
		btnClientes.setPressedIcon(iconoClientes_pressed);
		btnClientes.setOpaque(false);// se hace transparente todo el boton default JButton
		btnClientes.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnClientes.setBorderPainted(false);// borra el borde default del JButton
		btnClientes.setBounds(20, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnClientes.getWidth();
		int alto = btnClientes.getHeight();
		iconoClientes.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoClientes_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoClientes_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Abrir la vista</font> de Clientes</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Permite ver y editar clientes</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		
		//btnClientes.setVerticalAlignment(SwingConstants.BOTTOM);
		//subPanelIzquierdo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 30));
		btnClientes.setToolTipText(html);
		subPanelIzquierdo.add(btnClientes);
		
	}

	///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
	///////////////////////// PROPIEDADES//////////////////////////////////
	
	private void setPropiedadesDelBotonAvisos() {
		
		btnAvisos = new JButton();
		btnAvisos.setVerticalAlignment(SwingConstants.BOTTOM);

		// se crean los iconos que se van a usar
		ImageIcon iconoAvisos = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/avisos.png"));
		ImageIcon iconoAvisos_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/avisos_rollover.png"));
		ImageIcon iconoAvisos_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/avisos_pressed.png"));

		btnAvisos = new JButton(iconoAvisos);
		btnAvisos.setFocusable(false);
		btnAvisos.setRolloverIcon(iconoAvisos_rollover);
		btnAvisos.setPressedIcon(iconoAvisos_pressed);
		btnAvisos.setOpaque(false);// se hace transparente todo el boton default JButton
		btnAvisos.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnAvisos.setBorderPainted(false);// borra el borde default del JButton
		btnAvisos.setBounds(20, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnClientes.getWidth();
		int alto = btnClientes.getHeight();
		iconoAvisos.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoAvisos_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoAvisos_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Abrir la vista</font> de Avisos</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Permite ver y cerrar los recordatorios</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		subPanelIzquierdo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 30));
		btnAvisos.setToolTipText(html);
		subPanelIzquierdo.add(btnAvisos);
		
	}

	///////////////////////// SE AGREGAN EL BOTÓN AVISOS Y SUS
	///////////////////////// PROPIEDADES//////////////////////////////////
	private void setPropiedadesDelBotonHelp() {
		
		btnHelp = new JButton();
		btnHelp.setVerticalAlignment(SwingConstants.BOTTOM);

		// se crean los iconos que se van a usar
		ImageIcon iconoHelp = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/ayuda.png"));
		ImageIcon iconoHelp_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/ayuda_rollover.png"));
		ImageIcon iconoHelp_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/ayuda_pressed.png"));

		btnHelp = new JButton(iconoHelp);
		btnHelp.setFocusable(false);
		btnHelp.setRolloverIcon(iconoHelp_rollover);
		btnHelp.setPressedIcon(iconoHelp_pressed);
		btnHelp.setOpaque(false);// se hace transparente todo el boton default JButton
		btnHelp.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnHelp.setBorderPainted(false);// borra el borde default del JButton
		btnHelp.setBounds(20, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnHelp.getWidth();
		int alto = btnHelp.getHeight();
		iconoHelp.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoHelp_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoHelp_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Ayuda</font></h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Permite ver el manual de usuario</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		subPanelIzquierdo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 30));
		btnHelp.setToolTipText(html);
		subPanelIzquierdo.add(btnHelp);
		
	}

	///////////////////////// SE AGREGAN EL BOTÓN REPORTES Y SUS
	///////////////////////// PROPIEDADES//////////////////////////////////
	private void setPropiedadesDelBotonReportes() {
	}
	///////////////////////// SE AGREGAN EL BOTÓN REPORTES Y SUS
	///////////////////////// PROPIEDADES//////////////////////////////////

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setPreferredSize(new Dimension(10, 80));
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));

		boxLang = new JComboBox<Object>();
		boxLang.setMaximumRowCount(12);
		boxLang.setFont(new Font("Tahoma", Font.PLAIN, 18));
		subPanelSuperior.add(boxLang);

		setPropiedadesDelBtnIdioma();
		
		label = new JLabel("");
		label.setPreferredSize(new Dimension(30, 0));
		subPanelSuperior.add(label);

		lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblUsuario);
		
		label_3 = new JLabel("");
		label_3.setPreferredSize(new Dimension(30, 0));
		subPanelSuperior.add(label_3);

		lblSucursal = new JLabel("Sucursal: ");
		lblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
		lblSucursal.setForeground(Color.WHITE);
		lblSucursal.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblSucursal);
				
				label_1 = new JLabel("");
				label_1.setPreferredSize(new Dimension(30, 0));
				subPanelSuperior.add(label_1);
		
				lblFecha = new JLabel("Fecha");
				subPanelSuperior.add(lblFecha);
				lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
				lblFecha.setForeground(Color.WHITE);
				lblFecha.setFont(new Font("Verdana", Font.PLAIN, 18));
				
						dateChooser = new JDateChooser();
						subPanelSuperior.add(dateChooser);
						dateChooser.setFont(new Font("Tahoma", Font.PLAIN, 16));
						dateChooser.setPreferredSize(new Dimension(150, 22));

								
								label_2 = new JLabel("");
								label_2.setPreferredSize(new Dimension(30, 0));
								subPanelSuperior.add(label_2);
						
								lblHora = new JLabel("Hora");
								subPanelSuperior.add(lblHora);
								lblHora.setHorizontalAlignment(SwingConstants.CENTER);
								lblHora.setForeground(Color.WHITE);
								lblHora.setFont(new Font("Verdana", Font.PLAIN, 18));
								
										comboBoxHoras = new JComboBox<Object>();
										subPanelSuperior.add(comboBoxHoras);
										comboBoxHoras.setMaximumRowCount(12);
										comboBoxHoras.setFont(new Font("Tahoma", Font.PLAIN, 18));
	}
	
	private void setPropiedadesDelBtnIdioma() {
		
		btnSetIdioma = new JButton();

		// se crean los iconos que se van a usar
		ImageIcon iconoIdioma = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/idioma.png"));
		ImageIcon iconoIdioma_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/idioma_rollover.png"));
		ImageIcon iconoIdioma_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/idioma_pressed.png"));

		btnSetIdioma = new JButton(iconoIdioma);
		btnSetIdioma.setFocusable(false);
		btnSetIdioma.setRolloverIcon(iconoIdioma_rollover);
		btnSetIdioma.setPressedIcon(iconoIdioma_pressed);
		btnSetIdioma.setOpaque(false);// se hace transparente todo el boton default JButton
		btnSetIdioma.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnSetIdioma.setBorderPainted(false);// borra el borde default del JButton
		btnSetIdioma.setBounds(20, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnSetIdioma.getWidth();
		int alto = btnSetIdioma.getHeight();
		iconoIdioma.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoIdioma_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoIdioma_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para seleccionar el <font color=red>Idioma</font></h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Permite Cambiar el idioma de la aplicación</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		btnSetIdioma.setToolTipText(html);
		subPanelSuperior.add(btnSetIdioma);
		
	}
	
	

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JPanel getSubPanelInferior() {
		return subPanelInferior;
	}

	public void setSubPanelInferior(JPanel subPanelInferior) {
		this.subPanelInferior = subPanelInferior;
	}

	public JPanel getSubPanelDerecho() {
		return subPanelDerecho;
	}

	public void setSubPanelDerecho(JPanel subPanelDerecho) {
		this.subPanelDerecho = subPanelDerecho;
	}

	public JCheckBox getBoxPromocion() {
		return boxPromocion;
	}

	public void setBoxPromocion(JCheckBox boxPromocion) {
		this.boxPromocion = boxPromocion;
	}

	public JCheckBox getBoxPagoPendiente() {
		return boxPagoPendiente;
	}

	public void setBoxPagoPendiente(JCheckBox boxPagoPendiente) {
		this.boxPagoPendiente = boxPagoPendiente;
	}

	public JCheckBox getBoxDemorado() {
		return boxDemorado;
	}

	public void setBoxDemorado(JCheckBox boxDemorado) {
		this.boxDemorado = boxDemorado;
	}

	public JLabel getLblFiltros() {
		return lblFiltros;
	}

	public void setLblFiltros(JLabel lblFiltros) {
		this.lblFiltros = lblFiltros;
	}

	public JPanel getSubPanelCentral() {
		return subPanelCentral;
	}

	public void setSubPanelCentral(JPanel subPanelCentral) {
		this.subPanelCentral = subPanelCentral;
	}

	public JPanel getPanelSuperior_tabla() {
		return panelSuperior_tabla;
	}

	public void setPanelSuperior_tabla(JPanel panelSuperior_tabla) {
		this.panelSuperior_tabla = panelSuperior_tabla;
	}

	public JPanel getSubPanelIzquierdo() {
		return subPanelIzquierdo;
	}

	public void setSubPanelIzquierdo(JPanel subPanelIzquierdo) {
		this.subPanelIzquierdo = subPanelIzquierdo;
	}

	public JLabel getLblLogo() {
		return lblLogo;
	}

	public void setLblLogo(JLabel lblLogo) {
		this.lblLogo = lblLogo;
	}

	public JPanel getSubPanelSuperior() {
		return subPanelSuperior;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior) {
		this.subPanelSuperior = subPanelSuperior;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}

	public JLabel getLblSucursal() {
		return lblSucursal;
	}

	public void setLblSucursal(JLabel lblSucursal) {
		this.lblSucursal = lblSucursal;
	}

	public JPanel getPanel_1() {
		return panel_1;
	}

	public void setPanel_1(JPanel panel_1) {
		this.panel_1 = panel_1;
	}

	public JPanel getPanel_3() {
		return panel_3;
	}

	public void setPanel_3(JPanel panel_3) {
		this.panel_3 = panel_3;
	}

	public JPanel getPanelInferior_tabla() {
		return panelInferior_tabla;
	}

	public void setPanelInferior_tabla(JPanel panelInferior_tabla) {
		this.panelInferior_tabla = panelInferior_tabla;
	}

	public JPanel getPanelCentral_tabla() {
		return panelCentral_tabla;
	}

	public void setPanelCentral_tabla(JPanel panelCentral_tabla) {
		this.panelCentral_tabla = panelCentral_tabla;
	}

	public JTable getTablaTurnos() {
		return tablaTurnos;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

//	public JButton getBtnClientes() {
//		return btnClientes;
//	}
//
//	public void setBtnClientes(JButton btnClientes) {
//		this.btnClientes = btnClientes;
//	}

	public JButton getBotonCrearTurno() {
		return botonCrearTurno;
	}

	public void setBotonCrearTurno(JButton botonCrearTurno) {
		this.botonCrearTurno = botonCrearTurno;
	}

	public JButton getBotonCancelarTurno() {
		return botonCancelarTurno;
	}

	public void setBotonCancelarTurno(JButton botonCancelarTurno) {
		this.botonCancelarTurno = botonCancelarTurno;
	}

	public JButton getBotonPerspectivaTurno() {
		return botonPerspectivaTurno;
	}

	public void setBotonPerspectivaTurno(JButton botonPerspectivaTurno) {
		this.botonPerspectivaTurno = botonPerspectivaTurno;
	}

	public JButton getBotonPagarTurno() {
		return botonPagarTurno;
	}

	public void setBotonPagarTurno(JButton botonPagarTurno) {
		this.botonPagarTurno = botonPagarTurno;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(JDateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public JComboBox<Object> getComboBoxHoras() {
		return comboBoxHoras;
	}

	public void setComboBoxHoras(JComboBox<Object> comboBoxHoras) {
		this.comboBoxHoras = comboBoxHoras;
	}

	public JButton getBotonFiltrarTurnos() {
		return botonFiltrarTurnos;
	}

	public void setBotonFiltrarTurnos(JButton botonFiltrarTurnos) {
		this.botonFiltrarTurnos = botonFiltrarTurnos;
	}

	public JButton getBotonLimpiarFiltros() {
		return botonLimpiarFiltros;
	}

	public void setBotonLimpiarFiltros(JButton botonLimpiarFiltros) {
		this.botonLimpiarFiltros = botonLimpiarFiltros;
	}

	public JButton getBtnClientes() {
		return btnClientes;
	}

	public void setBtnClientes(JButton btnClientes) {
		this.btnClientes = btnClientes;
	}

	public JButton getBtnStock() {
		return btnStock;
	}

	public void setBtnStock(JButton btnStock) {
		this.btnStock = btnStock;
	}

	public JButton getBtnAvisos() {
		return btnAvisos;
	}

	public void setBtnAvisos(JButton btnAvisos) {
		this.btnAvisos = btnAvisos;
	}
	
	public JButton getBtnHelp() {
		return btnHelp;
	}

	public void setBtnHelp(JButton btnHelp) {
		this.btnHelp = btnHelp;
	}	
	
	public JButton getBotonCerrarTurno() {
		return botonCerrarTurno;
	}

	public void setBotonCerrarTurno(JButton botonCerrarTurno) {
		this.botonCerrarTurno = botonCerrarTurno;
	}

	public JCheckBox getBoxCancelados() {
		return boxCancelados;
	}

	public void setBoxCancelados(JCheckBox boxCancelados) {
		this.boxCancelados = boxCancelados;
	}

	public JButton getBotonEgreso() {
		return botonEgreso;

	}

	public JComboBox<Object> getBoxLang() {
		return boxLang;
	}

	public void setBoxLang(JComboBox<Object> boxLang) {
		this.boxLang = boxLang;
	}

	public void setBotonEgreso(JButton botonEgreso) {
		this.botonEgreso = botonEgreso;
	}

	public JLabel getLblFecha() {
		return lblFecha;
	}

	public void setLblFecha(JLabel lblFecha) {
		this.lblFecha = lblFecha;
	}

	public JLabel getLblHora() {
		return lblHora;
	}

	public void setLblHora(JLabel lblHora) {
		this.lblHora = lblHora;
	}

	public JButton getBtnSetIdioma() {
		return btnSetIdioma;
	}

	public void setBtnSetIdioma(JButton btnSetIdioma) {
		this.btnSetIdioma = btnSetIdioma;
	}
	
	public int mensajeClienteCasual() {
		return JOptionPane.showConfirmDialog(this, "No se ha seleccionado un turno. ¿Registrar un ingreso casual?");
	}
	
	public void turnoCerradoConExito() {
		JOptionPane.showMessageDialog(null, "El turno se cerró con éxito. Gracias.", "Cierre de turnos", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void turnoCanceladoConExito() {
		JOptionPane.showMessageDialog(null, "El turno se canceló con éxito. Gracias.", "Cierre de turnos", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void errorTurnoPago() {
		JOptionPane.showMessageDialog(null, "Error: el turno ya está pago.", "Pago de turnos", JOptionPane.ERROR_MESSAGE);
	}
	
	public void errorTurnoCancelado() {
		JOptionPane.showMessageDialog(null, "Error: el turno se encuentra cancelado.", "Pago de turnos", JOptionPane.ERROR_MESSAGE);
	}
	
	public void errorTurnoNoSeleccionado() {
		JOptionPane.showMessageDialog(null, "Error: no se ha seleccionado un turno.", "Pago de turnos", JOptionPane.ERROR_MESSAGE);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	public void llenarTabla(List<TurnoDTO> turnos) {
		
		System.out.println("\n\nCantidad de turnos que me pasan desde addTurno: "+turnos.size());
		
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (TurnoDTO t : turnos) {
			// { "Fecha", "Inicio", "fin", "Cliente", "Precio", "Puntaje", "Pagado",
			// "Estado"};
			String fecha = t.getFecha().toString();
			Time inicio = t.getHora_inicio();

			System.out.println("turno:" + t.getId() + "tiene:" + t.getDetalles().size());
			Time fin = t.getDetalles().get(t.getDetalles().size() - 1).getHoraFin();

			System.out.println(t.getId());

			if (t.getDetalles().size() > 0) {
				fin = t.getDetalles().get(t.getDetalles().size() - 1).getHoraFin();
			}

			String cliente = t.getCliente().getNombre() + " " + t.getCliente().getApellido();
			Float precio = t.getPrecio();
			int puntaje = t.getPuntos();
			Float pagado = t.getMontoPagado();
			String estado = t.getEstado_turno().toString();

			Object[] fila = { fecha, inicio, fin, cliente, precio, puntaje, pagado, estado };
			this.getModelInformacion().addRow(fila);
		}
	}

	public void llenarTabla(List<TurnoDTO> turnos, String fechaTurnos) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (TurnoDTO t : turnos) {
			// { "Fecha", "Inicio", "fin", "Cliente", "Precio", "Puntaje", "Pagado",
			// "Estado"};
			String fecha = t.getFecha().toString();
			Time inicio = t.getHora_inicio();
			Time fin = t.getDetalles().get(t.getDetalles().size() - 1).getHoraFin();
			String cliente = t.getCliente().getNombre() + " " + t.getCliente().getApellido();
			Float precio = t.getPrecio();
			int puntaje = t.getPuntos();
			Float pagado = t.getMontoPagado();
			String estado = t.getEstado_turno().toString();

			Object[] fila = { fecha, inicio, fin, cliente, precio, puntaje, pagado, estado };

			this.getModelInformacion().addRow(fila);
		}
	}
}