package presentacion.vista.administrativo.avisos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.Time;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import dto.TurnoDTO;

import javax.swing.JButton;

public class AvisosVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private static AvisosVista INSTANCE;

	public static AvisosVista getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new AvisosVista();
			return new AvisosVista();
		} else {
			return INSTANCE;
		}

	}

	private JPanel panelPrincipal;
	private JPanel subPanelSuperior;
	private JPanel subPanelIzquierdo;
	private JPanel subPanelCentral;
	private JPanel subPanelDerecho;
	private JPanel subPanelInferior;
	private JPanel panelSuperior_tabla;
	private JPanel panelCentral_tabla;
	private JScrollPane spTablaInformacion;
	private String[] nombreColumnasTabla;
	private DefaultTableModel modelInformacion;
	private JTable tablaTurnos;
	private JTableHeader tableHeader;
	private JButton btnInformado;

	private AvisosVista() {
		setPropiedades();
		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelDerecho();
		setSubPanelInferior();
		ocultar();
	}
	
	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 638, 697);
		setResizable(false);
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("Avisos");
		controlDeCierre();
	}
	
	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}
	
	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Se cerrará la ventana de avisos", "Salir de avisos",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			dispose();
		}
	}
	
	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}
	
	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 10));
	}
	
	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setPreferredSize(new Dimension(30, 10));
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
		subPanelIzquierdo.setSize(new Dimension(300, 50));
	}
	
	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(new BorderLayout(0, 0));
		
		panelSuperior_tabla = new JPanel();
		panelSuperior_tabla.setPreferredSize(new Dimension(10, 30));
		panelSuperior_tabla.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSuperior_tabla.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSuperior_tabla.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelSuperior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelSuperior_tabla, BorderLayout.NORTH);
		panelSuperior_tabla.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		panelCentral_tabla = new JPanel();
		panelCentral_tabla.setBackground(Color.WHITE);
		subPanelCentral.add(panelCentral_tabla, BorderLayout.CENTER);
		panelCentral_tabla.setLayout(new BoxLayout(panelCentral_tabla, BoxLayout.Y_AXIS));
		
		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(0, 0, 976, 547);
		panelCentral_tabla.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Fecha", "Cliente", "Telefono", "Email" };
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla);
		tablaTurnos = new JTable(modelInformacion);
		tableHeader = tablaTurnos.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaTurnos.setBackground(new Color(64, 64, 64));
		tablaTurnos.setForeground(Color.white);
		tablaTurnos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaTurnos.setRowHeight(30);
		tablaTurnos.setShowGrid(false);

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaTurnos);
	}
	
	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setPreferredSize(new Dimension(30, 10));
		subPanelDerecho.setBorder(null);
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
		subPanelDerecho.setLayout(new GridLayout(2, 0, 0, 0));
	}
	
	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 30));
		
		setPropiedadesDelBotonInformado();
	}
	
	private void setPropiedadesDelBotonInformado() {
		btnInformado = new JButton("Informado");
		subPanelInferior.add(btnInformado);
	}

	public static AvisosVista getINSTANCE() {
		return INSTANCE;
	}

	public static void setINSTANCE(AvisosVista iNSTANCE) {
		INSTANCE = iNSTANCE;
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JPanel getSubPanelSuperior() {
		return subPanelSuperior;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior) {
		this.subPanelSuperior = subPanelSuperior;
	}

	public JPanel getSubPanelIzquierdo() {
		return subPanelIzquierdo;
	}

	public void setSubPanelIzquierdo(JPanel subPanelIzquierdo) {
		this.subPanelIzquierdo = subPanelIzquierdo;
	}

	public JPanel getSubPanelCentral() {
		return subPanelCentral;
	}

	public void setSubPanelCentral(JPanel subPanelCentral) {
		this.subPanelCentral = subPanelCentral;
	}

	public JPanel getSubPanelDerecho() {
		return subPanelDerecho;
	}

	public void setSubPanelDerecho(JPanel subPanelDerecho) {
		this.subPanelDerecho = subPanelDerecho;
	}

	public JPanel getSubPanelInferior() {
		return subPanelInferior;
	}

	public void setSubPanelInferior(JPanel subPanelInferior) {
		this.subPanelInferior = subPanelInferior;
	}

	public JPanel getPanelSuperior_tabla() {
		return panelSuperior_tabla;
	}

	public void setPanelSuperior_tabla(JPanel panelSuperior_tabla) {
		this.panelSuperior_tabla = panelSuperior_tabla;
	}

	public JPanel getPanelCentral_tabla() {
		return panelCentral_tabla;
	}

	public void setPanelCentral_tabla(JPanel panelCentral_tabla) {
		this.panelCentral_tabla = panelCentral_tabla;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public JTable getTablaTurnos() {
		return tablaTurnos;
	}

	public void setTablaTurnos(JTable tablaTurnos) {
		this.tablaTurnos = tablaTurnos;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

	public JButton getBtnInformado() {
		return btnInformado;
	}

	public void setBtnInformado(JButton btnInformado) {
		this.btnInformado = btnInformado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void ocultar() {
		this.setVisible(false);
	}
	
	public void mostrar() {
		this.setVisible(true);
	}
	
	public void llenarTabla(List<TurnoDTO> turnos) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (TurnoDTO t : turnos) {
			//nombreColumnasTabla = new String[] { "Fecha", "Cliente", "Telefono", "Email" };
			String fecha = t.getFecha().toString();
			String cliente = t.getCliente().getNombre() + " " + t.getCliente().getApellido();
			String telefono = t.getCliente().getTelefono();
			String email = t.getCliente().getEmail();

			Object[] fila = { fecha, cliente, telefono, email };
			
			this.getModelInformacion().addRow(fila);
		}
	}

}
