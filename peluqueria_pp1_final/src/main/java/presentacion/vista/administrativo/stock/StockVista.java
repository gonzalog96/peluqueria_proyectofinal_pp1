package presentacion.vista.administrativo.stock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.List;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Component;
import java.awt.Cursor;

import dto.EstadoProducto;
import dto.StockDTO;
import presentacion.vista.administrativo.AdministrativoVista;

import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.awt.event.ActionEvent;

public class StockVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel subPanelInferior;
	private JPanel subPanelDerecho;
	private JLabel lblFiltros;
	private JPanel subPanelCentral;
	private JPanel panelSuperior_tabla;
	private JPanel subPanelIzquierdo;
	private JLabel lblLogo;
	private JPanel subPanelSuperior;
	private JLabel lblFechaActual;
	private JLabel lblUsuario;
	private JLabel lblSucursal;
	private static StockVista INSTANCE;
	private JPanel panel_1;
	private JPanel panel_3;
	private JPanel panelInferior_tabla;
	private JPanel panelCentral_tabla;
	private JTable tablaProductos;
	private DefaultTableModel modelInformacion;
	private String[] nombreColumnasTabla;
	private JScrollPane spTablaInformacion;
	private JTableHeader tableHeader;
	private JButton btnEgreso;
	private JButton btnAddProducto;
	private JButton btnEditProducto;
	private JButton btnRemoveProducto;
	private JLabel lblServicios;
	private JButton btnServicios;
	private JButton btnProfesionales;

	public static StockVista getInstance() {
		if (INSTANCE == null)
			INSTANCE = new StockVista();
		return INSTANCE;
	}

	private StockVista() {
		setPropiedades();

		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelIzquierdo();
		setSubPanelCentral();
		setSubPanelDerecho();
		setSubPanelInferior();

		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1073, 816);
		setMinimumSize(new Dimension(920, 650));
		getContentPane().setLayout(new BorderLayout(0, 0));
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("Administrador");
		controlDeCierre();
	}
	
	private void setPropiedadesDelBotonRegistrarEgresoProducto() {
		ImageIcon iconoEgreso = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno.png"));
		ImageIcon iconoEgreso_rollover = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno_rollover.png"));
		ImageIcon iconoEgreso_pressed = new ImageIcon(
				AdministrativoVista.class.getResource("/presentacion/vista/img/pagarTurno_pressed.png"));

		btnEgreso = new JButton(iconoEgreso);
		btnEgreso.setFocusable(false);
		btnEgreso.setBorderPainted(false);
		btnEgreso.setRolloverIcon(iconoEgreso_rollover);
		btnEgreso.setPressedIcon(iconoEgreso_pressed);
		btnEgreso.setOpaque(false);// se hace transparente todo el boton default JButton
		btnEgreso.setContentAreaFilled(false);
		btnEgreso.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnEgreso.getWidth();
		int alto = btnEgreso.getHeight();
		iconoEgreso.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoEgreso_rollover.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoEgreso_pressed.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Clic para <font color=red>Registrar un egreso</font></h3>"
				+ "<font size=3 color=red><p>=======================================================</p></font>"
				+ "</body></html>";
		btnEgreso.setToolTipText(html);
		subPanelInferior.add(btnEgreso);
	}

	private void setPropiedadesDelBotonCrearTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCrearTurno = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/agregarTurno.png"));
		ImageIcon iconoCrearTurno_rollover = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/agregarTurno_rollover.png"));
		ImageIcon iiconoCrearTurno_pressed = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/agregarTurno_pressed.png"));

		btnAddProducto = new JButton(iconoCrearTurno);
		btnAddProducto.setFocusable(false);
		btnAddProducto.setBorderPainted(false);
		btnAddProducto.setRolloverIcon(iconoCrearTurno_rollover);
		btnAddProducto.setPressedIcon(iiconoCrearTurno_pressed);
		btnAddProducto.setOpaque(false);// se hace transparente todo el boton default JButton
		btnAddProducto.setContentAreaFilled(false);
		btnAddProducto.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnAddProducto.getWidth();
		int alto = btnAddProducto.getHeight();
		iconoCrearTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCrearTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCrearTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Crear</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes crear un turno completando los datos solicitados</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		btnAddProducto.setToolTipText(html);
		subPanelInferior.add(btnAddProducto);
	}

	private void setPropiedadesDelBotonEditarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoEditarTurno = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/editarTurno.png"));
		ImageIcon iconoEditarTurno_rollover = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/editarTurno_rollover.png"));
		ImageIcon iiconoEditarTurno_pressed = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/editarTurno_pressed.png"));

		btnEditProducto = new JButton(iconoEditarTurno);
		btnEditProducto.setFocusable(false);
		btnEditProducto.setRolloverIcon(iconoEditarTurno_rollover);
		btnEditProducto.setPressedIcon(iiconoEditarTurno_pressed);
		btnEditProducto.setOpaque(false);// se hace transparente todo el boton default JButton
		btnEditProducto.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnEditProducto.setBorderPainted(false);// borra el borde default del JButton
		btnEditProducto.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnEditProducto.getWidth();
		int alto = btnEditProducto.getHeight();
		iconoEditarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoEditarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoEditarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Editar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>====================================================</p></font>"
				+ "<font size=3><p><b>Puedes editar un seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>====================================================</p></font>"
				+ "</body></html>";
		btnEditProducto.setToolTipText(html);
		subPanelInferior.add(btnEditProducto);
	}

	private void setPropiedadesDelBotonCancelarTurno() {

		// se crean los iconos que se van a usar
		ImageIcon iconoCancelarTurno = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/cancelarTurno.png"));
		ImageIcon iconoCancelarTurno_rollover = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/cancelarTurno_rollover.png"));
		ImageIcon iiconoCancelarTurno_pressed = new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/cancelarTurno_pressed.png"));

		btnRemoveProducto = new JButton(iconoCancelarTurno);
		btnRemoveProducto.setFocusable(false);
		btnRemoveProducto.setRolloverIcon(iconoCancelarTurno_rollover);
		btnRemoveProducto.setPressedIcon(iiconoCancelarTurno_pressed);
		btnRemoveProducto.setOpaque(false);// se hace transparente todo el boton default JButton
		btnRemoveProducto.setContentAreaFilled(false);// cuando se presiona no muestra el fondo default del JButton
		btnRemoveProducto.setBorderPainted(false);// borra el borde default del JButton
		btnRemoveProducto.setBounds(10, 20, 32, 32);

		// setea los tamanios de imagenes a partir de lo que ponga en setbounds
		int ancho = btnRemoveProducto.getWidth();
		int alto = btnRemoveProducto.getHeight();
		iconoCancelarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCancelarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		iconoCancelarTurno.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT);
		String html = "<html>" + "<body>" + "<h3>Click para <font color=red>Cancelar</font> un Turno</h3>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "<font size=3><p><b>Puedes cancelar un turno seleccionado de la tabla</b></p></font>"
				+ "<font size=3 color=red><p>=================================================</p></font>"
				+ "</body></html>";
		btnRemoveProducto.setToolTipText(html);
		subPanelInferior.add(btnRemoveProducto);
	}

	private void setSubPanelDerecho() {
		subPanelDerecho = new JPanel();
		subPanelDerecho.setPreferredSize(new Dimension(270, 10));
		subPanelDerecho.setBorder(null);
		subPanelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelDerecho, BorderLayout.EAST);
		subPanelDerecho.setLayout(new GridLayout(2, 0, 0, 0));

		panel_1 = new JPanel();
		panel_1.setOpaque(false);
		subPanelDerecho.add(panel_1);
		panel_1.setLayout(null);

		lblFiltros = new JLabel("Filtrar por:");
		lblFiltros.setBounds(22, 13, 123, 23);
		panel_1.add(lblFiltros);
		lblFiltros.setHorizontalAlignment(SwingConstants.CENTER);
		lblFiltros.setForeground(Color.WHITE);
		lblFiltros.setFont(new Font("Verdana", Font.PLAIN, 20));

		panel_3 = new JPanel();
		panel_3.setOpaque(false);
		subPanelDerecho.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		lblLogo = new JLabel("");
		lblLogo.setHorizontalTextPosition(SwingConstants.CENTER);
		panel_3.add(lblLogo, BorderLayout.SOUTH);
		lblLogo.setAlignmentY(Component.TOP_ALIGNMENT);
		lblLogo.setIcon(new ImageIcon(
				StockVista.class.getResource("/presentacion/vista/img/logo-peluqueria2_blanca_chica.png")));
		lblLogo.setHorizontalAlignment(SwingConstants.LEFT);
		lblLogo.setForeground(Color.WHITE);
		lblLogo.setFont(new Font("Tahoma", Font.PLAIN, 18));

	}

	private void setSubPanelInferior() {
		subPanelInferior = new JPanel();
		subPanelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelInferior, BorderLayout.SOUTH);
		subPanelInferior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 30));

		setPropiedadesDelBotonRegistrarEgresoProducto();
		setPropiedadesDelBotonCrearTurno();
		setPropiedadesDelBotonEditarTurno();
		setPropiedadesDelBotonCancelarTurno();
	}

	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(new BorderLayout(0, 0));

		panelSuperior_tabla = new JPanel();
		panelSuperior_tabla.setPreferredSize(new Dimension(10, 50));
		panelSuperior_tabla.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSuperior_tabla.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSuperior_tabla.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelSuperior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelSuperior_tabla, BorderLayout.NORTH);
		panelSuperior_tabla.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		lblServicios = new JLabel("Productos");
		lblServicios.setFont(new Font("Arial Black", Font.BOLD, 21));
		panelSuperior_tabla.add(lblServicios);

		panelInferior_tabla = new JPanel();
		panelInferior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelInferior_tabla, BorderLayout.SOUTH);

		panelCentral_tabla = new JPanel();
		panelCentral_tabla.setBackground(Color.WHITE);
		subPanelCentral.add(panelCentral_tabla, BorderLayout.CENTER);
		panelCentral_tabla.setLayout(new BoxLayout(panelCentral_tabla, BoxLayout.Y_AXIS));

		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(0, 0, 976, 547);
		panelCentral_tabla.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Nombre", "Rubro", "Precio x unidad", "Estado", "cantidad",
				"Fecha ult. modificacion" };
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla) {
			private static final long serialVersionUID = 9133156499672580767L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaProductos = new JTable(modelInformacion);
		tableHeader = tablaProductos.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaProductos.setBackground(new Color(64, 64, 64));
		tablaProductos.setForeground(Color.white);
		tablaProductos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaProductos.setRowHeight(30);
		tablaProductos.setShowGrid(false);

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaProductos);
	}

	private void setSubPanelIzquierdo() {
		subPanelIzquierdo = new JPanel();
		subPanelIzquierdo.setPreferredSize(new Dimension(100, 10));
		subPanelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelIzquierdo, BorderLayout.WEST);
		subPanelIzquierdo.setSize(new Dimension(300, 50));

		btnServicios = new JButton("Servicios");
		btnServicios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		btnProfesionales = new JButton("Profesionales");
		btnProfesionales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		subPanelIzquierdo.add(btnProfesionales);
		subPanelIzquierdo.add(btnServicios);
	}

	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 10));

		lblFechaActual = new JLabel("28/10/2019");
		lblFechaActual.setHorizontalAlignment(SwingConstants.CENTER);
		lblFechaActual.setForeground(Color.WHITE);
		lblFechaActual.setFont(new Font("Verdana", Font.PLAIN, 18));
		lblFechaActual.setSize(new Dimension(300, 100));
		subPanelSuperior.add(lblFechaActual);

		lblUsuario = new JLabel("Usuario de prueba");
		lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuario.setForeground(Color.WHITE);
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblUsuario);

		lblSucursal = new JLabel("Sucursal");
		lblSucursal.setHorizontalAlignment(SwingConstants.CENTER);
		lblSucursal.setForeground(Color.WHITE);
		lblSucursal.setFont(new Font("Verdana", Font.PLAIN, 18));
		subPanelSuperior.add(lblSucursal);
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JPanel getSubPanelInferior() {
		return subPanelInferior;
	}

	public void setSubPanelInferior(JPanel subPanelInferior) {
		this.subPanelInferior = subPanelInferior;
	}

	public JPanel getSubPanelDerecho() {
		return subPanelDerecho;
	}

	public void setSubPanelDerecho(JPanel subPanelDerecho) {
		this.subPanelDerecho = subPanelDerecho;
	}

	public JLabel getLblFiltros() {
		return lblFiltros;
	}

	public void setLblFiltros(JLabel lblFiltros) {
		this.lblFiltros = lblFiltros;
	}

	public JPanel getSubPanelCentral() {
		return subPanelCentral;
	}

	public void setSubPanelCentral(JPanel subPanelCentral) {
		this.subPanelCentral = subPanelCentral;
	}

	public JPanel getPanelSuperior_tabla() {
		return panelSuperior_tabla;
	}

	public void setPanelSuperior_tabla(JPanel panelSuperior_tabla) {
		this.panelSuperior_tabla = panelSuperior_tabla;
	}

	public JPanel getSubPanelIzquierdo() {
		return subPanelIzquierdo;
	}

	public void setSubPanelIzquierdo(JPanel subPanelIzquierdo) {
		this.subPanelIzquierdo = subPanelIzquierdo;
	}

	public JLabel getLblLogo() {
		return lblLogo;
	}

	public void setLblLogo(JLabel lblLogo) {
		this.lblLogo = lblLogo;
	}

	public JPanel getSubPanelSuperior() {
		return subPanelSuperior;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior) {
		this.subPanelSuperior = subPanelSuperior;
	}

	public JLabel getLblFechaActual() {
		return lblFechaActual;
	}

	public void setLblFechaActual(JLabel lblFechaActual) {
		this.lblFechaActual = lblFechaActual;
	}

	public JLabel getLblUsuario() {
		return lblUsuario;
	}

	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}

	public JLabel getLblSucursal() {
		return lblSucursal;
	}

	public void setLblSucursal(JLabel lblSucursal) {
		this.lblSucursal = lblSucursal;
	}

	public JPanel getPanel_1() {
		return panel_1;
	}

	public void setPanel_1(JPanel panel_1) {
		this.panel_1 = panel_1;
	}

	public JPanel getPanel_3() {
		return panel_3;
	}

	public void setPanel_3(JPanel panel_3) {
		this.panel_3 = panel_3;
	}

	public JPanel getPanelInferior_tabla() {
		return panelInferior_tabla;
	}

	public void setPanelInferior_tabla(JPanel panelInferior_tabla) {
		this.panelInferior_tabla = panelInferior_tabla;
	}

	public JPanel getPanelCentral_tabla() {
		return panelCentral_tabla;
	}

	public void setPanelCentral_tabla(JPanel panelCentral_tabla) {
		this.panelCentral_tabla = panelCentral_tabla;
	}

	public JTable getTablaProductos() {
		return tablaProductos;
	}

	public void setTablaProductos(JTable tablaProfesionales) {
		this.tablaProductos = tablaProfesionales;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

	public JButton getBtnAddProducto() {
		return btnAddProducto;
	}

	public void setBtnAddProducto(JButton btnAddProducto) {
		this.btnAddProducto = btnAddProducto;
	}

	public JButton getBtnEditProducto() {
		return btnEditProducto;
	}

	public void setBtnEditProducto(JButton btnEditServicio) {
		this.btnEditProducto = btnEditServicio;
	}

	public JButton getBtnRemoveProducto() {
		return btnRemoveProducto;
	}

	public void setBtnRemoveProducto(JButton btnRemoveProducto) {
		this.btnRemoveProducto = btnRemoveProducto;
	}

	public JLabel getLblServicios() {
		return lblServicios;
	}

	public void setLblServicios(JLabel lblServicios) {
		this.lblServicios = lblServicios;
	}

	public JButton getBtnServicios() {
		return btnServicios;
	}

	public void setBtnServicios(JButton btnServicios) {
		this.btnServicios = btnServicios;
	}
	
	public JButton getBtnEgreso()
	{
		return btnEgreso;
	}

	public JButton getBtnProfesionales() {
		return btnProfesionales;
	}

	public void setBtnProfesionales(JButton btnProfesionales) {
		this.btnProfesionales = btnProfesionales;
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public void ocultarPanelPrincipal() {
		panelPrincipal.setVisible(false);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				// guardar();
				close();
			}
		});
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	public void llenarTabla(List<StockDTO> stock) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (StockDTO s : stock) {
			String nombre = s.getProducto().getNombre();
			String rubro = s.getProducto().getRubro();
			float precio = s.getProducto().getPrecio();
			EstadoProducto estado = s.getProducto().getEstado();
			int cantidad = s.getCantidad();
			LocalDate ultimaMofificacion = s.getFechaUltimaModificacion();
			
			Object[] fila = { nombre, rubro, precio, estado, cantidad, ultimaMofificacion};
			this.getModelInformacion().addRow(fila);
		}
	}
}