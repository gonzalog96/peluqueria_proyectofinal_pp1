package presentacion.vista.administrativo.stock;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.EstadoProducto;
import dto.ProductoDTO;

import java.awt.Font;
import javax.swing.JCheckBox;

public class AddStockVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnConfirmar;
	private static AddStockVista INSTANCE;
	private JTextField txtCantidad;
	private JComboBox<ProductoDTO> comboProductos;

	public static AddStockVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AddStockVista();
			return new AddStockVista();
		} else
			return INSTANCE;
	}

	private AddStockVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 246, 151);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 230, 131);
		contentPane.add(panel);
		panel.setLayout(null);


		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(71, 95, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblCrearProducto = new JLabel("Agregar Stock");
		lblCrearProducto.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblCrearProducto.setBounds(10, 11, 257, 28);
		panel.add(lblCrearProducto);
		
		comboProductos = new JComboBox<ProductoDTO>();
		comboProductos.setBounds(10, 64, 114, 20);
		panel.add(comboProductos);
		
		txtCantidad = new JTextField();
		txtCantidad.setBounds(140, 64, 60, 20);
		panel.add(txtCantidad);
		txtCantidad.setColumns(10);
		
		JLabel lblProductos = new JLabel("Producto");
		lblProductos.setBounds(10, 50, 68, 14);
		panel.add(lblProductos);
		
		JLabel lblCantidad = new JLabel("Cantidad");
		lblCantidad.setBounds(145, 50, 68, 14);
		panel.add(lblCantidad);


		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public void cerrar() {
		this.dispose();
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}

	public JTextField getTxtCantidad() {
		return txtCantidad;
	}

	public void setTxtCantidad(JTextField txtCantidad) {
		this.txtCantidad = txtCantidad;
	}

	public JComboBox<ProductoDTO> getComboProducto() {
		return comboProductos;
	}

	public void setComboProducto(JComboBox<ProductoDTO> comboBox) {
		this.comboProductos = comboBox;
	}

	
	
	
}












