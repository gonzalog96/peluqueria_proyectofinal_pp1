package presentacion.vista.administrativo.pagos;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;

public class EgresoProductoVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private static EgresoProductoVista INSTANCE;
	
	private JTextField txtFechaEgresoProducto;
	private JTextField txtTotalNeto;
	private JButton btnRegistrarEgresoProducto;
	private JButton btnCancelarEgresoProducto;
	private JLabel lblRegistrarEgresoProducto;
	private JLabel lblCancelar;
	
	private JTextField txtProducto;
	private JComboBox<String> comboBoxCantidadProducto;

	public static EgresoProductoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EgresoProductoVista();
			return new EgresoProductoVista();
		} else
			return INSTANCE;
	}

	private EgresoProductoVista() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 363, 358);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		controlDeCierre();
		
		JLabel lblTitulo = new JLabel("Registro de egresos por PRODUCTO");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 0, 357, 26);
		contentPane.add(lblTitulo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 24, 337, 2);
		contentPane.add(separator);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setForeground(Color.WHITE);
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFecha.setBounds(10, 37, 86, 14);
		contentPane.add(lblFecha);
		
		txtFechaEgresoProducto = new JTextField();
		txtFechaEgresoProducto.setHorizontalAlignment(SwingConstants.CENTER);
		txtFechaEgresoProducto.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtFechaEgresoProducto.setEnabled(false);
		txtFechaEgresoProducto.setEditable(false);
		txtFechaEgresoProducto.setBounds(106, 37, 198, 20);
		contentPane.add(txtFechaEgresoProducto);
		txtFechaEgresoProducto.setColumns(10);
		
		JLabel lblServicios = new JLabel("Cantidad: ");
		lblServicios.setForeground(Color.WHITE);
		lblServicios.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblServicios.setBounds(10, 119, 86, 14);
		contentPane.add(lblServicios);
		
		JLabel lblTotalNeto = new JLabel("Total neto: ");
		lblTotalNeto.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalNeto.setForeground(Color.WHITE);
		lblTotalNeto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTotalNeto.setBounds(10, 190, 86, 14);
		contentPane.add(lblTotalNeto);
		
		txtTotalNeto = new JTextField();
		txtTotalNeto.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalNeto.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtTotalNeto.setEnabled(false);
		txtTotalNeto.setEditable(false);
		txtTotalNeto.setColumns(10);
		txtTotalNeto.setBounds(106, 187, 198, 20);
		contentPane.add(txtTotalNeto);
		
		btnRegistrarEgresoProducto = new JButton("");
		btnRegistrarEgresoProducto.setIcon(new ImageIcon(EgresoProductoVista.class.getResource("/presentacion/vista/img/guardarPago.png")));
		btnRegistrarEgresoProducto.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegistrarEgresoProducto.setBounds(65, 233, 86, 73);
		contentPane.add(btnRegistrarEgresoProducto);
		
		btnCancelarEgresoProducto = new JButton("");
		btnCancelarEgresoProducto.setIcon(new ImageIcon(EgresoProductoVista.class.getResource("/presentacion/vista/img/cancelarPago.png")));
		btnCancelarEgresoProducto.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnCancelarEgresoProducto.setBounds(195, 233, 98, 73);
		contentPane.add(btnCancelarEgresoProducto);
		
		lblRegistrarEgresoProducto = new JLabel("REGISTRAR EGRESO");
		lblRegistrarEgresoProducto.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrarEgresoProducto.setForeground(Color.WHITE);
		lblRegistrarEgresoProducto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRegistrarEgresoProducto.setBounds(45, 308, 125, 14);
		contentPane.add(lblRegistrarEgresoProducto);
		
		lblCancelar = new JLabel("CANCELAR");
		lblCancelar.setHorizontalAlignment(SwingConstants.CENTER);
		lblCancelar.setForeground(Color.WHITE);
		lblCancelar.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCancelar.setBounds(195, 308, 98, 14);
		contentPane.add(lblCancelar);
		
		JLabel lblProducto = new JLabel("Producto: ");
		lblProducto.setForeground(Color.WHITE);
		lblProducto.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblProducto.setBounds(10, 77, 86, 14);
		contentPane.add(lblProducto);
		
		comboBoxCantidadProducto = new JComboBox<String>();
		comboBoxCantidadProducto.setBounds(106, 117, 198, 20);
		contentPane.add(comboBoxCantidadProducto);
		
		txtProducto = new JTextField();
		txtProducto.setHorizontalAlignment(SwingConstants.CENTER);
		txtProducto.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtProducto.setEnabled(false);
		txtProducto.setEditable(false);
		txtProducto.setColumns(10);
		txtProducto.setBounds(106, 75, 198, 20);
		contentPane.add(txtProducto);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 162, 337, 2);
		contentPane.add(separator_1);
		this.setVisible(false);
	}

	public int mensajeConfirmarEgreso() {
		return JOptionPane.showConfirmDialog(this, "¿Estás seguro?", "Confirmación de operación", JOptionPane.YES_NO_OPTION);
	}
	
	public void egresoExitoso() {
		JOptionPane.showMessageDialog(null, "Egreso registrado con éxito.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
		ocultar();
	}

	public JTextField getTxtFechaEgresoProducto()
	{
		return txtFechaEgresoProducto;
	}

	public JTextField getTxtTotalNeto()
	{
		return txtTotalNeto;
	}

	public JButton getBtnRegistrarEgresoProducto()
	{
		return btnRegistrarEgresoProducto;
	}

	public JButton getBtnCancelarEgresoProducto()
	{
		return btnCancelarEgresoProducto;
	}

	public JTextField getTxtProducto()
	{
		return txtProducto;
	}

	public JComboBox<String> getComboBoxCantidadProducto()
	{
		return comboBoxCantidadProducto;
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		limpiar();
		this.setVisible(false);
	}
	
	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}
	
	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			limpiar();
			this.ocultar();
		}
	}
	
	private void limpiar() {
		this.comboBoxCantidadProducto.removeAllItems();
		this.txtFechaEgresoProducto.setText("");
		this.txtProducto.setText("");
		this.txtTotalNeto.setText("");
	}
}