package presentacion.vista.administrativo.pagos;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;

public class PagarTurnoVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private static PagarTurnoVista INSTANCE;
	
	private JTextField txtFechaHoraTurno;
	private JTextField txtNombreCliente;
	private JTextField txtTotalNeto;
	private JTextField txtImporteARegistrar;
	
	private JButton btnVerDetalleDeServicios;
	private JButton btnPagarTurno;
	private JButton btnCancelarTurno;

	private JSeparator separator_2;
	private JLabel lblTotalRestante;
	private JTextField txtTotalRestante;
	
	private JLabel lblMedioDePago;
	private JComboBox<Object> comboBoxMediosDePago;
	private JLabel lblPagar;
	private JLabel lblCancelar;
	private JLabel lblAgregarComentario;
	private JButton btnAgregarComentarios;
	
	private boolean campoComentarioActivado = false;
	private JTextField txtComentario;

	public static PagarTurnoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PagarTurnoVista();
			return new PagarTurnoVista();
		} else
			return INSTANCE;
	}

	private PagarTurnoVista() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 363, 718);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		controlDeCierre();
		
		JLabel lblTitulo = new JLabel("Registro de ingresos");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 10, 357, 26);
		contentPane.add(lblTitulo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 34, 337, 2);
		contentPane.add(separator);
		
		JLabel lblFechaHora = new JLabel("Fecha y hora: ");
		lblFechaHora.setForeground(Color.WHITE);
		lblFechaHora.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFechaHora.setBounds(10, 51, 86, 14);
		contentPane.add(lblFechaHora);
		
		txtFechaHoraTurno = new JTextField();
		txtFechaHoraTurno.setHorizontalAlignment(SwingConstants.CENTER);
		txtFechaHoraTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtFechaHoraTurno.setEnabled(false);
		txtFechaHoraTurno.setEditable(false);
		txtFechaHoraTurno.setBounds(106, 47, 198, 20);
		contentPane.add(txtFechaHoraTurno);
		txtFechaHoraTurno.setColumns(10);
		
		JLabel lblNombreCliente = new JLabel("Cliente: ");
		lblNombreCliente.setForeground(Color.WHITE);
		lblNombreCliente.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNombreCliente.setBounds(10, 86, 86, 14);
		contentPane.add(lblNombreCliente);
		
		txtNombreCliente = new JTextField();
		txtNombreCliente.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombreCliente.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtNombreCliente.setEnabled(false);
		txtNombreCliente.setEditable(false);
		txtNombreCliente.setColumns(10);
		txtNombreCliente.setBounds(106, 82, 198, 20);
		contentPane.add(txtNombreCliente);
		
		JLabel lblServicios = new JLabel("Servicios: ");
		lblServicios.setForeground(Color.WHITE);
		lblServicios.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblServicios.setBounds(10, 122, 86, 14);
		contentPane.add(lblServicios);
		
		btnVerDetalleDeServicios = new JButton("Ver detalle de servicios");
		btnVerDetalleDeServicios.setBounds(106, 119, 198, 23);
		contentPane.add(btnVerDetalleDeServicios);
		
		JLabel lblTotalNeto = new JLabel("Total neto: ");
		lblTotalNeto.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalNeto.setForeground(Color.WHITE);
		lblTotalNeto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTotalNeto.setBounds(10, 183, 86, 14);
		contentPane.add(lblTotalNeto);
		
		txtTotalNeto = new JTextField();
		txtTotalNeto.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalNeto.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtTotalNeto.setEnabled(false);
		txtTotalNeto.setEditable(false);
		txtTotalNeto.setColumns(10);
		txtTotalNeto.setBounds(106, 180, 198, 20);
		contentPane.add(txtTotalNeto);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 312, 337, 2);
		contentPane.add(separator_1);
		
		JLabel lblImporte = new JLabel("Importe a registrar:");
		lblImporte.setForeground(Color.WHITE);
		lblImporte.setHorizontalAlignment(SwingConstants.CENTER);
		lblImporte.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblImporte.setBounds(106, 327, 148, 20);
		contentPane.add(lblImporte);
		
		txtImporteARegistrar = new JTextField();
		txtImporteARegistrar.setHorizontalAlignment(SwingConstants.CENTER);
		txtImporteARegistrar.setFont(new Font("Tahoma", Font.BOLD, 16));
		txtImporteARegistrar.setColumns(10);
		txtImporteARegistrar.setBounds(80, 358, 198, 20);
		contentPane.add(txtImporteARegistrar);
		
		/*checkBoxUsarPuntos = new JCheckBox("Incluir bonificación por puntos acumulados.");
		checkBoxUsarPuntos.setEnabled(false);
		checkBoxUsarPuntos.setFont(new Font("Tahoma", Font.BOLD, 11));
		checkBoxUsarPuntos.setForeground(Color.WHITE);
		checkBoxUsarPuntos.setBackground(Color.BLACK);
		checkBoxUsarPuntos.setBounds(35, 282, 269, 23);
		contentPane.add(checkBoxUsarPuntos);*/
		
		btnPagarTurno = new JButton("");
		btnPagarTurno.setIcon(new ImageIcon(PagarTurnoVista.class.getResource("/presentacion/vista/img/guardarPago.png")));
		btnPagarTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnPagarTurno.setBounds(80, 589, 86, 73);
		contentPane.add(btnPagarTurno);
		
		btnCancelarTurno = new JButton("");
		btnCancelarTurno.setIcon(new ImageIcon(PagarTurnoVista.class.getResource("/presentacion/vista/img/cancelarPago.png")));
		btnCancelarTurno.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnCancelarTurno.setBounds(180, 589, 98, 73);
		contentPane.add(btnCancelarTurno);
		
		separator_2 = new JSeparator();
		separator_2.setBounds(10, 160, 337, 2);
		contentPane.add(separator_2);
		
		lblTotalRestante = new JLabel("Restante: ");
		lblTotalRestante.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalRestante.setForeground(Color.WHITE);
		lblTotalRestante.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTotalRestante.setBounds(10, 222, 86, 14);
		contentPane.add(lblTotalRestante);
		
		txtTotalRestante = new JTextField();
		txtTotalRestante.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalRestante.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtTotalRestante.setEnabled(false);
		txtTotalRestante.setEditable(false);
		txtTotalRestante.setColumns(10);
		txtTotalRestante.setBounds(106, 219, 198, 20);
		contentPane.add(txtTotalRestante);
		
		lblMedioDePago = new JLabel();
		lblMedioDePago.setHorizontalAlignment(SwingConstants.CENTER);
		lblMedioDePago.setForeground(Color.WHITE);
		lblMedioDePago.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMedioDePago.setBounds(10, 259, 86, 42);
		lblMedioDePago.setText("<HTML> <center>Modalidad de pago: </center></HTML>");
		lblMedioDePago.setOpaque(false);
		lblMedioDePago.setFocusable(false);
		
		contentPane.add(lblMedioDePago);
		
		comboBoxMediosDePago = new JComboBox<Object>();
		comboBoxMediosDePago.setBounds(106, 263, 198, 20);
		contentPane.add(comboBoxMediosDePago);
		
		lblPagar = new JLabel("PAGAR");
		lblPagar.setHorizontalAlignment(SwingConstants.CENTER);
		lblPagar.setForeground(Color.WHITE);
		lblPagar.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPagar.setBounds(80, 664, 90, 14);
		contentPane.add(lblPagar);
		
		lblCancelar = new JLabel("CANCELAR");
		lblCancelar.setHorizontalAlignment(SwingConstants.CENTER);
		lblCancelar.setForeground(Color.WHITE);
		lblCancelar.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCancelar.setBounds(180, 664, 98, 14);
		contentPane.add(lblCancelar);
		
		btnAgregarComentarios = new JButton();
		btnAgregarComentarios.setIcon(new ImageIcon(PagarTurnoVista.class.getResource("/presentacion/vista/img/agregarComentario.png")));
		btnAgregarComentarios.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAgregarComentarios.setBounds(141, 476, 80, 61);
		contentPane.add(btnAgregarComentarios);
		
		lblAgregarComentario = new JLabel("AGREGAR COMENTARIO");
		lblAgregarComentario.setHorizontalAlignment(SwingConstants.CENTER);
		lblAgregarComentario.setForeground(Color.WHITE);
		lblAgregarComentario.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAgregarComentario.setBounds(106, 541, 148, 14);
		contentPane.add(lblAgregarComentario);
		
		txtComentario = new JTextField();
		txtComentario.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtComentario.setText("SIN COMENTARIOS.");
		txtComentario.setHorizontalAlignment(SwingConstants.CENTER);
		txtComentario.setEnabled(false);
		txtComentario.setEditable(false);
		txtComentario.setBounds(80, 411, 198, 54);
		contentPane.add(txtComentario);
		txtComentario.setColumns(10);
		
		this.setVisible(false);
	}
	
	public void actualizarDatosParaFiado() {
		this.txtImporteARegistrar.setText("0");
		this.txtImporteARegistrar.setEnabled(false);
		this.txtImporteARegistrar.setEditable(false);
		
		JOptionPane.showMessageDialog(null, "Se ha seleccionado FIAR el turno.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void mensajePagoConPuntos() {
		JOptionPane.showMessageDialog(null, "Se ha seleccionado pagar el turno con puntos.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void limpiar() {
		this.getComboBoxMediosDePago().setSelectedIndex(0);
		
		this.txtComentario.setText("SIN COMENTARIOS.");
		this.txtImporteARegistrar.setText("");
		this.txtImporteARegistrar.setEnabled(true);
		this.txtImporteARegistrar.setEditable(true);
	}
	
	public void setCampoComentarioActivado(boolean campoComentarioActivado)
	{
		this.campoComentarioActivado = campoComentarioActivado;
	}

	public JTextField getTxtFechaHoraTurno()
	{
		return txtFechaHoraTurno;
	}

	public JTextField getTxtNombreCliente()
	{
		return txtNombreCliente;
	}

	public JTextField getTxtTotalNeto()
	{
		return txtTotalNeto;
	}

	public JTextField getTxtImporteARegistrar()
	{
		return txtImporteARegistrar;
	}

	public void setTxtFechaHoraTurno(JTextField txtFechaHoraTurno)
	{
		this.txtFechaHoraTurno = txtFechaHoraTurno;
	}

	public void setTxtNombreCliente(JTextField txtNombreCliente)
	{
		this.txtNombreCliente = txtNombreCliente;
	}
	
	public void setTxtTotalNeto(JTextField txtTotalNeto)
	{
		this.txtTotalNeto = txtTotalNeto;
	}

	public void setTxtImporteARegistrar(JTextField txtImporteARegistrar)
	{
		this.txtImporteARegistrar = txtImporteARegistrar;
	}

	public JButton getBtnVerDetalleDeServicios()
	{
		return btnVerDetalleDeServicios;
	}

	public JButton getBtnPagarTurno()
	{
		return btnPagarTurno;
	}

	public JButton getBtnCancelarTurno()
	{
		return btnCancelarTurno;
	}

/*	public JCheckBox getCheckBoxUsarPuntos()
	{
		return checkBoxUsarPuntos;
	}*/

	public void setBtnVerDetalleDeServicios(JButton btnVerDetalleDeServicios)
	{
		this.btnVerDetalleDeServicios = btnVerDetalleDeServicios;
	}

	public void setBtnPagarTurno(JButton btnPagarTurno)
	{
		this.btnPagarTurno = btnPagarTurno;
	}

	public void setBtnCancelarTurno(JButton btnCancelarTurno)
	{
		this.btnCancelarTurno = btnCancelarTurno;
	}

/*	public void setCheckBoxUsarPuntos(JCheckBox checkBoxUsarPuntos)
	{
		this.checkBoxUsarPuntos = checkBoxUsarPuntos;
	}*/

	public JTextField getTxtTotalRestante()
	{
		return txtTotalRestante;
	}

	public JComboBox<Object> getComboBoxMediosDePago()
	{
		return comboBoxMediosDePago;
	}

	public void setTxtTotalRestante(JTextField txtTotalRestante)
	{
		this.txtTotalRestante = txtTotalRestante;
	}

	public JButton getBtnAgregarComentarios()
	{
		return btnAgregarComentarios;
	}

	public void setBtnAgregarComentarios(JButton btnAgregarComentarios)
	{
		this.btnAgregarComentarios = btnAgregarComentarios;
	}
	
	public void activarCampoComentarios() {
		if (!campoComentarioActivado) {
			this.txtComentario.setBackground(Color.WHITE);
			this.txtComentario.setText("");
			this.txtComentario.setEnabled(true);
			this.txtComentario.setEditable(true);
			
			this.campoComentarioActivado = true;
		} else {
			this.txtComentario.setBackground(Color.LIGHT_GRAY);
			this.txtComentario.setText("SIN COMENTARIOS.");
			this.txtComentario.setEnabled(false);
			this.txtComentario.setEditable(false);
			
			this.campoComentarioActivado = false;
		}
	}

	public int mensajeConfirmarPago() {
		return JOptionPane.showConfirmDialog(this, "¿Estás seguro?", "Confirmación de operación", JOptionPane.YES_NO_OPTION);
	}
	
	public void pagoExitoso(String montoARegistrar, String montoRestante) {
		JOptionPane.showMessageDialog(null, "¡Operación registrada con éxito!\nIngreso: $" + montoARegistrar + ".\nMonto restante: $" + montoRestante + ".", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void noTienePuntosSuficientes() {
		JOptionPane.showMessageDialog(null, "Error: el cliente no posee los puntos suficientes para abonar parcial o totalmente el turno.", "Confirmación de operación", JOptionPane.ERROR_MESSAGE);
	}
	
	public void errorNoSePuedeFiar() {
		JOptionPane.showMessageDialog(null, "Error: no es posible fiar un turno que posee pagos parciales.", "Confirmación de operación", JOptionPane.ERROR_MESSAGE);
	}
	
	public void errorMontoIngresado() {
		JOptionPane.showMessageDialog(null, "El monto ingresado es inválido. Intentá nuevamente.", "Confirmación de operación", JOptionPane.ERROR_MESSAGE);
	}
	
	public void ingresoCasualExitoso() {
		JOptionPane.showMessageDialog(null, "El ingreso se registró con éxito. Gracias.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void errorMontoSuperiorAlRestante() {
		JOptionPane.showMessageDialog(null, "El monto ingresado es superior al restante.", "Confirmación de operación", JOptionPane.ERROR_MESSAGE);
	}
	
	public void noImplementado() {
		JOptionPane.showMessageDialog(null, "Próximamente.", "Operación inválida", JOptionPane.ERROR_MESSAGE);
	}
	
	public void errorNoSeleccionoMedioDePago() {
		JOptionPane.showMessageDialog(null, "Error: no se ha seleccionado un medio de pago.", "Confirmación de operación", JOptionPane.ERROR_MESSAGE);
	}
	
	public void mensajePuntosDescontados(int puntosDescontados) {
		JOptionPane.showMessageDialog(null, "¡Se descontaron " + puntosDescontados + " puntos!", "Débito de puntaje", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void puntosAgregadosConExito(int puntosAcreditados) {
		JOptionPane.showMessageDialog(null, "¡Se acreditaron " + puntosAcreditados + " puntos por abonar el total del turno!", "Acreditación de puntaje", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public JTextField getTxtComentario()
	{
		return txtComentario;
	}

	public void mostrarServiciosComprados(List<String> nombreServicios) {
		StringBuilder nombres = new StringBuilder();
		
		for (int i=0; i < nombreServicios.size(); i++) {
			nombres.append(i+1 + ". " + nombreServicios.get(i) + ".\n");
		}
		
		JOptionPane.showMessageDialog(null, "Los servicios adquiridos son: \n" + nombres, "Servicios adquiridos", JOptionPane.INFORMATION_MESSAGE);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
	
	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}
	
	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			this.txtComentario.setBackground(Color.LIGHT_GRAY);
			this.txtComentario.setText("SIN COMENTARIOS.");
			this.txtComentario.setEnabled(false);
			this.campoComentarioActivado = false;
			
			this.ocultar();
		}
	}
}