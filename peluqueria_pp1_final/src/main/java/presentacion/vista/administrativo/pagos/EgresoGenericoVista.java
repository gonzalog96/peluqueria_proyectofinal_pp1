package presentacion.vista.administrativo.pagos;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;

public class EgresoGenericoVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private static EgresoGenericoVista INSTANCE;
	
	private JTextField txtFechaEgreso;
	private JTextField txtTotal;
	private JButton btnRegistrarEgreso;
	private JButton btnCancelarEgreso;
	private JLabel lblRegistrarEgreso;
	private JLabel lblCancelar;
	
	private JTextField txtConcepto;

	public static EgresoGenericoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EgresoGenericoVista();
			return new EgresoGenericoVista();
		} else
			return INSTANCE;
	}

	private EgresoGenericoVista() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 363, 338);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		controlDeCierre();
		
		JLabel lblTitulo = new JLabel("Registro de egresos INDIRECTOS");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setBounds(0, 0, 357, 26);
		contentPane.add(lblTitulo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 24, 337, 2);
		contentPane.add(separator);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setHorizontalAlignment(SwingConstants.CENTER);
		lblFecha.setForeground(Color.WHITE);
		lblFecha.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFecha.setBounds(10, 37, 86, 14);
		contentPane.add(lblFecha);
		
		txtFechaEgreso = new JTextField();
		txtFechaEgreso.setHorizontalAlignment(SwingConstants.CENTER);
		txtFechaEgreso.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtFechaEgreso.setEnabled(false);
		txtFechaEgreso.setEditable(false);
		txtFechaEgreso.setBounds(106, 37, 198, 20);
		contentPane.add(txtFechaEgreso);
		txtFechaEgreso.setColumns(10);
		
		JLabel lblMonto = new JLabel("Monto: ");
		lblMonto.setHorizontalAlignment(SwingConstants.CENTER);
		lblMonto.setForeground(Color.WHITE);
		lblMonto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMonto.setBounds(10, 152, 86, 14);
		contentPane.add(lblMonto);
		
		txtTotal = new JTextField();
		txtTotal.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotal.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtTotal.setColumns(10);
		txtTotal.setBounds(106, 147, 198, 26);
		contentPane.add(txtTotal);
		
		btnRegistrarEgreso = new JButton("");
		btnRegistrarEgreso.setIcon(new ImageIcon(EgresoGenericoVista.class.getResource("/presentacion/vista/img/guardarPago.png")));
		btnRegistrarEgreso.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegistrarEgreso.setBounds(66, 208, 86, 73);
		contentPane.add(btnRegistrarEgreso);
		
		btnCancelarEgreso = new JButton("");
		btnCancelarEgreso.setIcon(new ImageIcon(EgresoGenericoVista.class.getResource("/presentacion/vista/img/cancelarPago.png")));
		btnCancelarEgreso.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnCancelarEgreso.setBounds(195, 208, 98, 73);
		contentPane.add(btnCancelarEgreso);
		
		lblRegistrarEgreso = new JLabel("REGISTRAR EGRESO");
		lblRegistrarEgreso.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrarEgreso.setForeground(Color.WHITE);
		lblRegistrarEgreso.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRegistrarEgreso.setBounds(45, 292, 125, 14);
		contentPane.add(lblRegistrarEgreso);
		
		lblCancelar = new JLabel("CANCELAR");
		lblCancelar.setHorizontalAlignment(SwingConstants.CENTER);
		lblCancelar.setForeground(Color.WHITE);
		lblCancelar.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCancelar.setBounds(195, 292, 98, 14);
		contentPane.add(lblCancelar);
		
		JLabel lblConcepto = new JLabel("Concepto: ");
		lblConcepto.setHorizontalAlignment(SwingConstants.CENTER);
		lblConcepto.setForeground(Color.WHITE);
		lblConcepto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConcepto.setBounds(10, 93, 86, 14);
		contentPane.add(lblConcepto);
		
		txtConcepto = new JTextField();
		txtConcepto.setHorizontalAlignment(SwingConstants.CENTER);
		txtConcepto.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtConcepto.setColumns(10);
		txtConcepto.setBounds(106, 75, 198, 49);
		contentPane.add(txtConcepto);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 195, 337, 2);
		contentPane.add(separator_1);
		this.setVisible(false);
	}

	public JButton getBtnRegistrarEgreso()
	{
		return btnRegistrarEgreso;
	}

	public JButton getBtnCancelarEgreso()
	{
		return btnCancelarEgreso;
	}

	public JTextField getTxtFechaEgreso()
	{
		return txtFechaEgreso;
	}

	public JTextField getTxtTotal()
	{
		return txtTotal;
	}

	public JTextField getTxtConcepto()
	{
		return txtConcepto;
	}

	public int mensajeConfirmarEgreso() {
		return JOptionPane.showConfirmDialog(this, "¿Estás seguro?", "Confirmación de operación", JOptionPane.YES_NO_OPTION);
	}
	
	public void egresoExitoso() {
		JOptionPane.showMessageDialog(null, "Egreso registrado con éxito.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
		ocultar();
	}
	
	public void errorMontoNoNumerico() {
		JOptionPane.showMessageDialog(null, "Error: el monto ingresado NO es numérico o está vacío.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void errorConceptoVacio() {
		JOptionPane.showMessageDialog(null, "Error: no ingresaste ningún concepto.", "Confirmación de operación", JOptionPane.INFORMATION_MESSAGE);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		limpiar();
		this.setVisible(false);
	}
	
	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}
	
	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			limpiar();
			this.ocultar();
		}
	}
	
	private void limpiar() {
		this.txtFechaEgreso.setText("");
		this.txtConcepto.setText("");
		this.txtTotal.setText("");
	}
}