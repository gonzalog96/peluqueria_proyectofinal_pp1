package presentacion.vista.administrador.servicios;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.EstadoServicio;
import dto.ServicioDTO;
import dto.SucursalDTO;

import java.awt.Font;
import java.util.List;

public class AddServicioVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtDuracion;
	private JTextField txtPuntos;
	private JButton btnConfirmar;
	private static AddServicioVista INSTANCE;
	private JComboBox<EstadoServicio> comboEstado;

	public static AddServicioVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AddServicioVista();
			return new AddServicioVista();
		} else
			return INSTANCE;
	}

	private AddServicioVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 383, 288);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 369, 250);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblApellido = new JLabel("Precio");
		lblApellido.setBounds(22, 90, 113, 14);
		panel.add(lblApellido);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 62, 113, 14);
		panel.add(lblNombre);

		JLabel lblEmail = new JLabel("Puntos");
		lblEmail.setBounds(22, 146, 113, 14);
		panel.add(lblEmail);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 59, 226, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtPrecio = new JTextField();
		txtPrecio.setBounds(133, 87, 226, 20);
		panel.add(txtPrecio);
		txtPrecio.setColumns(10);
		txtDuracion = new JTextField();
		txtDuracion.setBounds(133, 115, 226, 20);
		panel.add(txtDuracion);
		txtDuracion.setColumns(10);
		txtPuntos = new JTextField();
		txtPuntos.setBounds(133, 143, 226, 20);
		panel.add(txtPuntos);
		txtPuntos.setColumns(10);
		
		comboEstado = new JComboBox<EstadoServicio>();
		comboEstado.setBounds(133, 171, 129, 20);
		panel.add(comboEstado);
		comboEstado.addItem(EstadoServicio.ACTIVO);
		comboEstado.addItem(EstadoServicio.INACTIVO);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(22, 171, 46, 14);
		panel.add(lblEstado);



		JLabel lblDni = new JLabel("Duracion");
		lblDni.setBounds(22, 118, 113, 14);
		panel.add(lblDni);


		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(133, 216, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblCrearServicio = new JLabel("Crear Servicio");
		lblCrearServicio.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblCrearServicio.setBounds(102, 28, 176, 23);
		panel.add(lblCrearServicio);


		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtPrecio.setText(null);
		this.txtDuracion.setText(null);
		this.txtPuntos.setText(null);
		this.dispose();
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}

	public JTextField getTxtDuracion() {
		return txtDuracion;
	}

	public void setTxtDuracion(JTextField txtDuracion) {
		this.txtDuracion = txtDuracion;
	}

	public JTextField getTxtPuntos() {
		return txtPuntos;
	}

	public void setTxtPuntos(JTextField txtPuntos) {
		this.txtPuntos = txtPuntos;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}

	public JComboBox<EstadoServicio> getComboEstado() {
		return comboEstado;
	}

	public void setComboEstado(JComboBox<EstadoServicio> comboEstado) {
		this.comboEstado = comboEstado;
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
}












