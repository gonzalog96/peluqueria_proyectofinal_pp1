package presentacion.vista.administrador.promociones;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import dto.EstadoPromocion;
import dto.EstadoServicio;
import dto.PromocionDTO;
import dto.ServicioDTO;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JTable;

public class AddPromocionVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtPrecio;
	private JTextField txtMultiplicacion;
	private JButton btnConfirmar;
	private static AddPromocionVista INSTANCE;
	private JComboBox<EstadoPromocion> comboEstado;
	private JTable tablaServicios;
	private DefaultTableModel modelInformacion;
	private String[] nombreColumnasTabla;
	private JScrollPane spTablaInformacion;
	private JTableHeader tableHeader;
	private JButton btnSubtractServicio;
	private JButton btnAddServicios;

	public static AddPromocionVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AddPromocionVista();
			return new AddPromocionVista();
		} else
			return INSTANCE;
	}

	private AddPromocionVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 383, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 369, 363);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblApellido = new JLabel("Precio");
		lblApellido.setBounds(22, 90, 113, 14);
		panel.add(lblApellido);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 62, 113, 14);
		panel.add(lblNombre);

		JLabel lblEmail = new JLabel("Multiplicacion");
		lblEmail.setBounds(22, 115, 113, 14);
		panel.add(lblEmail);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 59, 226, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtPrecio = new JTextField();
		txtPrecio.setBounds(133, 87, 226, 20);
		panel.add(txtPrecio);
		txtPrecio.setColumns(10);
		txtMultiplicacion = new JTextField();
		txtMultiplicacion.setBounds(133, 115, 226, 20);
		panel.add(txtMultiplicacion);
		txtMultiplicacion.setColumns(10);
		
		comboEstado = new JComboBox<EstadoPromocion>();
		comboEstado.setBounds(133, 140, 129, 20);
		panel.add(comboEstado);
		comboEstado.addItem(EstadoPromocion.ACTIVO);
		comboEstado.addItem(EstadoPromocion.INACTIVO);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(22, 140, 46, 14);
		panel.add(lblEstado);


		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(133, 329, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblCrearServicio = new JLabel("Crear promocion");
		lblCrearServicio.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblCrearServicio.setBounds(73, 28, 510, 23);
		panel.add(lblCrearServicio);
		
		btnAddServicios = new JButton("+");
		btnAddServicios.setBounds(10, 181, 79, 41);
		panel.add(btnAddServicios);
		
		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(133, 181, 226, 137);
		panel.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Servicio", "Estado"};
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla) {
			private static final long serialVersionUID = 9133156499672580767L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaServicios = new JTable(modelInformacion);
		tableHeader = tablaServicios.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaServicios.setBackground(new Color(64, 64, 64));
		tablaServicios.setForeground(Color.white);
		tablaServicios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaServicios.setRowHeight(30);
		tablaServicios.setShowGrid(false);
		

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaServicios);
		
		btnSubtractServicio = new JButton("-");
		btnSubtractServicio.setBounds(10, 233, 79, 41);
		panel.add(btnSubtractServicio);

		this.setVisible(false);
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtPrecio.setText(null);
		this.txtMultiplicacion.setText(null);
		this.dispose();
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
	
	public void llenarTabla(List<ServicioDTO> servicios) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (int i = 0; i < servicios.size(); i++) {
			ServicioDTO t = servicios.get(i);
			String nombre = t.getNombre().toString();
			float precio = t.getPrecio();
			int duracion = t.getDuracion();
			int puntos = t.getPuntos();
			EstadoServicio estado = t.getEstado();
			Object[] fila = { nombre, precio, duracion, puntos, estado};
			this.getModelInformacion().addRow(fila);
		}
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}

	public JTextField getTxtMultiplicacion() {
		return txtMultiplicacion;
	}

	public void setTxtMultiplicacion(JTextField txtMultiplicacion) {
		this.txtMultiplicacion = txtMultiplicacion;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}

	public JComboBox<EstadoPromocion> getComboEstado() {
		return comboEstado;
	}

	public void setComboEstado(JComboBox<EstadoPromocion> comboEstado) {
		this.comboEstado = comboEstado;
	}

	public JTable getTablaServicios() {
		return tablaServicios;
	}

	public void setTablaServicios(JTable tablaServicios) {
		this.tablaServicios = tablaServicios;
	}

	public JButton getBtnSubtractServicio() {
		return btnSubtractServicio;
	}

	public void setBtnSubtractServicio(JButton btnSubtractServicio) {
		this.btnSubtractServicio = btnSubtractServicio;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public JButton getBtnAddServicios() {
		return btnAddServicios;
	}

	public void setBtnAddServicios(JButton btnAddServicios) {
		this.btnAddServicios = btnAddServicios;
	}

	
	
	
	

	/////////////////////////////////////////////////////////////////////////////////////////////

	

}












