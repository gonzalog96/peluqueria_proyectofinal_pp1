package presentacion.vista.administrador.promociones;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.util.List;
import java.awt.Component;
import java.awt.Cursor;

import dto.EstadoServicio;
import dto.ServicioDTO;

import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

public class InfoPromocionVista extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel subPanelCentral;
	private JPanel panelSuperior_tabla;
	private JPanel subPanelSuperior;
	private static InfoPromocionVista INSTANCE;
	private JPanel panelInferior_tabla;
	private JPanel panelCentral_tabla;
	private JTable tablaServicios;
	private DefaultTableModel modelInformacion;
	private String[] nombreColumnasTabla;
	private JScrollPane spTablaInformacion;
	private JTableHeader tableHeader;
	private JLabel lblServicios;

	public static InfoPromocionVista getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InfoPromocionVista();
		return INSTANCE;
	}

	private InfoPromocionVista() {
		setPropiedades();

		setPanelPrincipal();
		setSubPanelSuperior();
		setSubPanelCentral();

		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 100);
		setMinimumSize(new Dimension(450, 300));
		getContentPane().setLayout(new BorderLayout(0, 0));
		// setExtendedState(JFrame.MAXIMIZED_BOTH);
		setTitle("Administrador");
		controlDeCierre();
	}



	private void setSubPanelCentral() {
		subPanelCentral = new JPanel();
		subPanelCentral.setBackground(Color.WHITE);
		panelPrincipal.add(subPanelCentral, BorderLayout.CENTER);
		subPanelCentral.setLayout(new BorderLayout(0, 0));

		panelSuperior_tabla = new JPanel();
		panelSuperior_tabla.setPreferredSize(new Dimension(10, 50));
		panelSuperior_tabla.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSuperior_tabla.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSuperior_tabla.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panelSuperior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelSuperior_tabla, BorderLayout.NORTH);
		panelSuperior_tabla.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		lblServicios = new JLabel();
		lblServicios.setFont(new Font("Arial Black", Font.BOLD, 21));
		panelSuperior_tabla.add(lblServicios);

		panelInferior_tabla = new JPanel();
		panelInferior_tabla.setBackground(Color.GRAY);
		subPanelCentral.add(panelInferior_tabla, BorderLayout.SOUTH);

		panelCentral_tabla = new JPanel();
		panelCentral_tabla.setBackground(Color.WHITE);
		subPanelCentral.add(panelCentral_tabla, BorderLayout.CENTER);
		panelCentral_tabla.setLayout(new BoxLayout(panelCentral_tabla, BoxLayout.Y_AXIS));

		spTablaInformacion = new JScrollPane();
		spTablaInformacion.setBackground(Color.WHITE);
		spTablaInformacion.setPreferredSize(new Dimension(0, 0));
		spTablaInformacion.setBorder(null);
		spTablaInformacion.setViewportBorder(null);
		spTablaInformacion.getViewport().setBackground(new Color(64, 64, 64));
		spTablaInformacion.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		spTablaInformacion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		spTablaInformacion.setBounds(0, 0, 976, 547);
		panelCentral_tabla.add(spTablaInformacion);

		nombreColumnasTabla = new String[] { "Servicio", "Duracion", "Estado"};
		modelInformacion = new DefaultTableModel(null, nombreColumnasTabla) {
			private static final long serialVersionUID = 9133156499672580767L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tablaServicios = new JTable(modelInformacion);
		tableHeader = tablaServicios.getTableHeader();
		tableHeader.setFont(new Font("Tahoma", Font.BOLD, 18));
		tableHeader.setBackground(new Color(255, 255, 255));
		tableHeader.setForeground(Color.black);

		tablaServicios.setBackground(new Color(64, 64, 64));
		tablaServicios.setForeground(Color.white);
		tablaServicios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tablaServicios.setRowHeight(30);
		tablaServicios.setShowGrid(false);
		

		// tablaTurnos.setDefaultRenderer(Object.class, new RenderDeTablaCustom());

		spTablaInformacion.setViewportView(tablaServicios);
	}


	private void setSubPanelSuperior() {
		subPanelSuperior = new JPanel();
		subPanelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(subPanelSuperior, BorderLayout.NORTH);
		subPanelSuperior.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 10));
	}

	private void setPanelPrincipal() {
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		panelPrincipal.setLayout(new BorderLayout());
		getContentPane().add(panelPrincipal, BorderLayout.CENTER);
	}


	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public void ocultarPanelPrincipal() {
		panelPrincipal.setVisible(false);
	}
	
	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				// guardar();
				close();
			}
		});
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Está seguro de cerrar la aplicación", "Salir del sistema",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	public void llenarTabla(List<ServicioDTO> servicios) {
		this.getModelInformacion().setRowCount(0); // Para vaciar la tabla
		this.getModelInformacion().setColumnCount(0);
		this.getModelInformacion().setColumnIdentifiers(this.getNombreColumnasTabla());

		for (int i = 0; i < servicios.size(); i++) {
			// { "Fecha", "Inicio", "fin", "Cliente", "Precio", "Puntaje", "Pagado",
			// "Estado"};
			ServicioDTO t = servicios.get(i);
			String nombre = t.getNombre().toString();
			int duracion = t.getDuracion();
			EstadoServicio estado = t.getEstado();
			Object[] fila = { nombre, duracion, estado};
			this.getModelInformacion().addRow(fila);
		}
	}

	public JPanel getPanelPrincipal() {
		return panelPrincipal;
	}

	public void setPanelPrincipal(JPanel panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}

	public JPanel getSubPanelCentral() {
		return subPanelCentral;
	}

	public void setSubPanelCentral(JPanel subPanelCentral) {
		this.subPanelCentral = subPanelCentral;
	}

	public JPanel getPanelSuperior_tabla() {
		return panelSuperior_tabla;
	}

	public void setPanelSuperior_tabla(JPanel panelSuperior_tabla) {
		this.panelSuperior_tabla = panelSuperior_tabla;
	}

	public JPanel getSubPanelSuperior() {
		return subPanelSuperior;
	}

	public void setSubPanelSuperior(JPanel subPanelSuperior) {
		this.subPanelSuperior = subPanelSuperior;
	}

	public JPanel getPanelInferior_tabla() {
		return panelInferior_tabla;
	}

	public void setPanelInferior_tabla(JPanel panelInferior_tabla) {
		this.panelInferior_tabla = panelInferior_tabla;
	}

	public JPanel getPanelCentral_tabla() {
		return panelCentral_tabla;
	}

	public void setPanelCentral_tabla(JPanel panelCentral_tabla) {
		this.panelCentral_tabla = panelCentral_tabla;
	}

	public JTable getTablaServicios() {
		return tablaServicios;
	}

	public void setTablaServicios(JTable tablaServicios) {
		this.tablaServicios = tablaServicios;
	}

	public DefaultTableModel getModelInformacion() {
		return modelInformacion;
	}

	public void setModelInformacion(DefaultTableModel modelInformacion) {
		this.modelInformacion = modelInformacion;
	}

	public String[] getNombreColumnasTabla() {
		return nombreColumnasTabla;
	}

	public void setNombreColumnasTabla(String[] nombreColumnasTabla) {
		this.nombreColumnasTabla = nombreColumnasTabla;
	}

	public JScrollPane getSpTablaInformacion() {
		return spTablaInformacion;
	}

	public void setSpTablaInformacion(JScrollPane spTablaInformacion) {
		this.spTablaInformacion = spTablaInformacion;
	}

	public JTableHeader getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(JTableHeader tableHeader) {
		this.tableHeader = tableHeader;
	}

	public JLabel getLblServicios() {
		return lblServicios;
	}

	public void setLblServicios(JLabel lblServicios) {
		this.lblServicios = lblServicios;
	}
	
	
	
	
}
