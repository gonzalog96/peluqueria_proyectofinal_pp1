package presentacion.vista.administrador.bbdd;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

public class ImportarBDVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static ImportarBDVista INSTANCE;
	private JButton btnCarpeta;
	private JButton btnImportar;
	private JTextField txtRuta;

	public static ImportarBDVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ImportarBDVista();
			return new ImportarBDVista();
		} else
			return INSTANCE;
	}

	private ImportarBDVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 617, 166);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 591, 132);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnCarpeta = new JButton("Carpeta");
		btnCarpeta.setBounds(475, 33, 89, 23);
		panel.add(btnCarpeta);
		
		btnImportar = new JButton("Importar");
		btnImportar.setBounds(36, 90, 528, 23);
		panel.add(btnImportar);
		
		txtRuta = new JTextField();
		txtRuta.setBounds(36, 34, 401, 20);
		panel.add(txtRuta);
		txtRuta.setColumns(10);


		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JButton getBtnCarpeta() {
		return btnCarpeta;
	}

	public void setBtnCarpeta(JButton btnCarpeta) {
		this.btnCarpeta = btnCarpeta;
	}

	public JButton getBtnImportar() {
		return btnImportar;
	}

	public void setBtnImportar(JButton btnExportar) {
		this.btnImportar = btnExportar;
	}

	public JTextField getTxtRuta() {
		return txtRuta;
	}

	public void setTxtRuta(JTextField txtRuta) {
		this.txtRuta = txtRuta;
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
}
