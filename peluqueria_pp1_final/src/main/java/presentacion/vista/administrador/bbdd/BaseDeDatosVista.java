package presentacion.vista.administrador.bbdd;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class BaseDeDatosVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static BaseDeDatosVista INSTANCE;
	private JButton btnExportar;
	private JButton btnImportar;

	public static BaseDeDatosVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new BaseDeDatosVista();
			return new BaseDeDatosVista();
		} else
			return INSTANCE;
	}

	private BaseDeDatosVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 383, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 369, 172);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnExportar = new JButton("Exportar");
		btnExportar.setBounds(10, 67, 349, 23);
		panel.add(btnExportar);
		
		btnImportar = new JButton("Importar");
		btnImportar.setBounds(10, 138, 349, 23);
		panel.add(btnImportar);

		this.setVisible(false);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public JButton getBtnExportar() {
		return btnExportar;
	}

	public void setBtnExportar(JButton btnExportar) {
		this.btnExportar = btnExportar;
	}

	public JButton getBtnImportar() {
		return btnImportar;
	}

	public void setBtnImportar(JButton btnImportar) {
		this.btnImportar = btnImportar;
	}
	
	
}
