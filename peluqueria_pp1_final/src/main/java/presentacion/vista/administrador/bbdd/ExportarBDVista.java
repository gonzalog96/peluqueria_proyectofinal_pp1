package presentacion.vista.administrador.bbdd;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

public class ExportarBDVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private static ExportarBDVista INSTANCE;
	private JButton btnCarpeta;
	private JButton btnExportar;
	private JTextField txtRuta;

	public static ExportarBDVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ExportarBDVista();
			return new ExportarBDVista();
		} else
			return INSTANCE;
	}

	private ExportarBDVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 617, 166);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 591, 132);
		contentPane.add(panel);
		panel.setLayout(null);

		btnCarpeta = new JButton("Carpeta");
		btnCarpeta.setBounds(475, 33, 89, 23);
		panel.add(btnCarpeta);
		
		btnExportar = new JButton("Exportar");
		btnExportar.setBounds(36, 90, 528, 23);
		panel.add(btnExportar);
		
		txtRuta = new JTextField();
		txtRuta.setBounds(36, 34, 411, 20);
		panel.add(txtRuta);
		txtRuta.setColumns(10);

		this.setVisible(false);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public JButton getBtnCarpeta() {
		return btnCarpeta;
	}

	public void setBtnCarpeta(JButton btnCarpeta) {
		this.btnCarpeta = btnCarpeta;
	}

	public JButton getBtnExportar() {
		return btnExportar;
	}

	public void setBtnExportar(JButton btnExportar) {
		this.btnExportar = btnExportar;
	}

	public JTextField getTxtRuta() {
		return txtRuta;
	}

	public void setTxtRuta(JTextField txtRuta) {
		this.txtRuta = txtRuta;
	}
	
	
	
	
}
