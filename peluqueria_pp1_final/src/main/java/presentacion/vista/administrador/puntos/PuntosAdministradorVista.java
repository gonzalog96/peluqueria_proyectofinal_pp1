package presentacion.vista.administrador.puntos;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;

public class PuntosAdministradorVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnGuardarCambios;
	private JButton btnCancelar;
	private static PuntosAdministradorVista INSTANCE;
	private JTextField txtPuntosActuales;
	private JTextField txtDineroActual;
	private JTextField txtPuntosNuevos;
	private JTextField txtDineroNuevo;
	private JCheckBox checkHabilitarPuntosNuevos;
	private JCheckBox checkHabilitarDineroNuevo;
	
	private boolean campoPuntosNuevosActivado = false;
	private boolean campoDineroNuevoActivado = false;

	public static PuntosAdministradorVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PuntosAdministradorVista();
			return new PuntosAdministradorVista();
		} else
			return INSTANCE;
	}

	private PuntosAdministradorVista() {
		super();
		setResizable(false);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 253);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 386, 224);
		contentPane.add(panel);
		panel.setLayout(null);


		btnGuardarCambios = new JButton("Confirmar");
		btnGuardarCambios.setBounds(95, 190, 89, 23);
		panel.add(btnGuardarCambios);
		
		JLabel lblConfigPuntos = new JLabel("Configuración de puntos y conversión");
		lblConfigPuntos.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfigPuntos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConfigPuntos.setBounds(0, 0, 376, 23);
		panel.add(lblConfigPuntos);
		
		JLabel lblPuntosParaConversin = new JLabel("Puntos para conversión actuales: ");
		lblPuntosParaConversin.setBounds(10, 45, 168, 14);
		panel.add(lblPuntosParaConversin);
		
		txtPuntosActuales = new JTextField();
		txtPuntosActuales.setHorizontalAlignment(SwingConstants.CENTER);
		txtPuntosActuales.setEnabled(false);
		txtPuntosActuales.setEditable(false);
		txtPuntosActuales.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtPuntosActuales.setBounds(173, 42, 203, 20);
		panel.add(txtPuntosActuales);
		txtPuntosActuales.setColumns(10);
		
		JLabel lblConversion = new JLabel("Dinero por conversión actual: ");
		lblConversion.setBounds(10, 74, 168, 14);
		panel.add(lblConversion);
		
		txtDineroActual = new JTextField();
		txtDineroActual.setHorizontalAlignment(SwingConstants.CENTER);
		txtDineroActual.setEnabled(false);
		txtDineroActual.setEditable(false);
		txtDineroActual.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtDineroActual.setColumns(10);
		txtDineroActual.setBounds(173, 70, 203, 20);
		panel.add(txtDineroActual);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(194, 190, 89, 23);
		panel.add(btnCancelar);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 99, 369, 2);
		panel.add(separator);
		
		JLabel lblPuntosNuevos = new JLabel("Puntos para conversión nuevos: ");
		lblPuntosNuevos.setBounds(10, 112, 168, 14);
		panel.add(lblPuntosNuevos);
		
		txtPuntosNuevos = new JTextField();
		txtPuntosNuevos.setEnabled(false);
		txtPuntosNuevos.setEditable(false);
		txtPuntosNuevos.setHorizontalAlignment(SwingConstants.CENTER);
		txtPuntosNuevos.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtPuntosNuevos.setColumns(10);
		txtPuntosNuevos.setBounds(173, 112, 135, 20);
		panel.add(txtPuntosNuevos);
		
		JLabel lblDineroNuevo = new JLabel("Dinero para conversión nuevo: ");
		lblDineroNuevo.setBounds(10, 143, 168, 14);
		panel.add(lblDineroNuevo);
		
		txtDineroNuevo = new JTextField();
		txtDineroNuevo.setEditable(false);
		txtDineroNuevo.setEnabled(false);
		txtDineroNuevo.setHorizontalAlignment(SwingConstants.CENTER);
		txtDineroNuevo.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtDineroNuevo.setColumns(10);
		txtDineroNuevo.setBounds(173, 143, 135, 20);
		panel.add(txtDineroNuevo);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 176, 369, 2);
		panel.add(separator_1);
		
		checkHabilitarPuntosNuevos = new JCheckBox("Habilitar");
		checkHabilitarPuntosNuevos.setBounds(314, 108, 65, 23);
		panel.add(checkHabilitarPuntosNuevos);
		
		checkHabilitarDineroNuevo = new JCheckBox("Habilitar");
		checkHabilitarDineroNuevo.setBounds(314, 139, 65, 23);
		panel.add(checkHabilitarDineroNuevo);
		
		this.setVisible(false);
	}
	
	public void completarDatos(String puntosActuales, String valorActual) {
		this.txtPuntosActuales.setText(puntosActuales);
		this.txtDineroActual.setText(valorActual);
		this.txtDineroNuevo.setText(valorActual);
		this.txtPuntosNuevos.setText(puntosActuales);
	}
	
	public void activarCampoPuntosNuevos() {
		if (!campoPuntosNuevosActivado) {
			this.txtPuntosNuevos.setEnabled(true);
			this.txtPuntosNuevos.setEditable(true);
			this.txtPuntosNuevos.setText(this.txtPuntosActuales.getText());
			
			this.campoPuntosNuevosActivado = true;
		} else {
			this.txtPuntosNuevos.setEnabled(false);
			this.txtPuntosNuevos.setEditable(false);
			
			this.campoPuntosNuevosActivado = false;
		}
	}
	
	public void activarCampoDineroNuevo() {
		if (!campoDineroNuevoActivado) {
			this.txtDineroNuevo.setEnabled(true);
			this.txtDineroNuevo.setEditable(true);
			this.txtDineroNuevo.setText(this.txtDineroActual.getText());
			
			this.campoDineroNuevoActivado = true;
		} else {
			this.txtDineroNuevo.setEnabled(false);
			this.txtDineroNuevo.setEditable(false);
			
			this.campoDineroNuevoActivado = false;
		}
	}
	
	public JCheckBox getCheckHabilitarPuntosNuevos()
	{
		return checkHabilitarPuntosNuevos;
	}

	public JCheckBox getCheckHabilitarDineroNuevo()
	{
		return checkHabilitarDineroNuevo;
	}

	public void limpiar() {
		this.txtDineroActual.setText("");
		this.txtDineroNuevo.setText("");
		this.txtPuntosActuales.setText("");
		this.txtPuntosNuevos.setText("");
	}

	public JButton getBtnGuardarCambios()
	{
		return btnGuardarCambios;
	}

	public JButton getBtnCancelar()
	{
		return btnCancelar;
	}

	public JTextField getTxtPuntosActuales()
	{
		return txtPuntosActuales;
	}

	public JTextField getTxtDineroActual()
	{
		return txtDineroActual;
	}

	public JTextField getTxtPuntosNuevos()
	{
		return txtPuntosNuevos;
	}

	public JTextField getTxtDineroNuevo()
	{
		return txtDineroNuevo;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
}