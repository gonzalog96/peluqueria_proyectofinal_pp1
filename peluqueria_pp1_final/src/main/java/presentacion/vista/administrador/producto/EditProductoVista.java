package presentacion.vista.administrador.producto;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.EstadoProducto;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class EditProductoVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtRubro;
	private JTextField txtPrecio;
	private JButton btnConfirmar;
	private static EditProductoVista INSTANCE;
	private JComboBox<EstadoProducto> comboEstado;

	public static EditProductoVista getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EditProductoVista();
			return new EditProductoVista();
		} else
			return INSTANCE;
	}

	private EditProductoVista() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 383, 229);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, -12, 369, 206);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblApellido = new JLabel("Rubro");
		lblApellido.setBounds(22, 90, 113, 14);
		panel.add(lblApellido);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 62, 113, 14);
		panel.add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.setBounds(133, 59, 226, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtRubro = new JTextField();
		txtRubro.setBounds(133, 87, 226, 20);
		panel.add(txtRubro);
		txtRubro.setColumns(10);
		txtPrecio = new JTextField();
		txtPrecio.setBounds(133, 115, 226, 20);
		panel.add(txtPrecio);
		txtPrecio.setColumns(10);


		JLabel lblDni = new JLabel("Precio");
		lblDni.setBounds(22, 118, 113, 14);
		panel.add(lblDni);


		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnConfirmar.setBounds(133, 177, 89, 23);
		panel.add(btnConfirmar);
		
		JLabel lblCrearServicio = new JLabel("Editar Producto");
		lblCrearServicio.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblCrearServicio.setBounds(102, 28, 267, 23);
		panel.add(lblCrearServicio);
		
		comboEstado = new JComboBox<EstadoProducto>();
		comboEstado.setBounds(133, 146, 129, 20);
		panel.add(comboEstado);
		comboEstado.addItem(EstadoProducto.ACTIVO);
		comboEstado.addItem(EstadoProducto.INACTIVO);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(22, 143, 46, 14);
		panel.add(lblEstado);


		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtRubro.setText(null);
		this.txtPrecio.setText(null);
		this.dispose();
	}

	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}


	/////////////////////////////////////////////////////////////////////////////////////////////
	public JTextField getTxtNombre() {
		return txtNombre;
	}
	
	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}
	
	public JTextField getTxtRubro() {
		return txtRubro;
	}
	
	public void setTxtRubro(JTextField txtRubro) {
		this.txtRubro = txtRubro;
	}
	
	public JTextField getTxtPrecio() {
		return txtPrecio;
	}
	
	public void setTxtPrecio(JTextField txtPrecio) {
		this.txtPrecio = txtPrecio;
	}
	
	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}
	
	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}
	
	public JComboBox<EstadoProducto> getComboEstado() {
		return comboEstado;
	}
	
	public void setComboEstado(JComboBox<EstadoProducto> comboEstado) {
		this.comboEstado = comboEstado;
	}
	
	
	
}












