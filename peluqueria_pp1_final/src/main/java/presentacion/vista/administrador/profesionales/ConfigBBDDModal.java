package presentacion.vista.administrador.profesionales;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.JPasswordField;

public class ConfigBBDDModal extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtDireccion;
	private JTextField txtPuerto;
	private JTextField txtDbname;
	private JTextField txtUser;
	private JTextField txtPass;
	private JPanel subPanelBotones;
	private JButton btnConfirmar;
	private JButton btnCancelar;
	private static ConfigBBDDModal INSTANCE;
	private JPanel panelSuperior;
	private JPanel panelIzquierdo;
	private JPanel panelDerecho;
	private JPanel panelCentral;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel label_8;
	private JLabel label_9;
	private JLabel label_10;
	private JLabel label_11;
	private JLabel label_12;
	private JLabel label_13;

	public static ConfigBBDDModal getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ConfigBBDDModal();
		return INSTANCE;
	}

	public ConfigBBDDModal() {
		super();
		setPropiedades();
		setPanelSuperior();
		setPanelIzquierdo();
		setPanelCentral();
		setPanelDerecho();
		setPanelBotones();
	}

	private void setPropiedades() {
		setAlwaysOnTop(true);
		setModal(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 545, 499);
		getContentPane().setLayout(new BorderLayout(0, 0));
	}

	private void setPanelSuperior() {
		panelSuperior = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelSuperior.getLayout();
		flowLayout.setVgap(10);
		panelSuperior.setBackground(Color.DARK_GRAY);
		getContentPane().add(panelSuperior, BorderLayout.NORTH);

		JLabel lblConfiguracinDeLa = new JLabel("Configuración de la base de datos");
		lblConfiguracinDeLa.setForeground(Color.WHITE);
		lblConfiguracinDeLa.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panelSuperior.add(lblConfiguracinDeLa);
	}

	private void setPanelIzquierdo() {
		panelIzquierdo = new JPanel();
		panelIzquierdo.setBackground(Color.DARK_GRAY);
		getContentPane().add(panelIzquierdo, BorderLayout.WEST);
	}

	private void setPanelCentral() {
		panelCentral = new JPanel();
		panelCentral.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(panelCentral, BorderLayout.CENTER);

		setCamposDeEntrada();
		
		ocultar();
	}

	private void setCamposDeEntrada() {
		panelCentral.setLayout(null);
		JLabel lblDireccion = new JLabel("Dirección");
		lblDireccion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDireccion.setBounds(30, 50, 94, 36);
		panelCentral.add(lblDireccion);

		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPuerto.setBounds(30, 99, 78, 39);
		panelCentral.add(lblPuerto);

		JLabel lblDbname = new JLabel("Nombre de BBDD");
		lblDbname.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDbname.setBounds(30, 151, 133, 36);
		panelCentral.add(lblDbname);

		label = new JLabel("");
		label.setBounds(283, 13, 0, 0);
		panelCentral.add(label);

		JLabel lblUser = new JLabel("Usuario");
		lblUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUser.setBounds(30, 200, 109, 36);
		panelCentral.add(lblUser);

		JLabel lblPass = new JLabel("Password");
		lblPass.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPass.setBounds(30, 249, 133, 36);
		panelCentral.add(lblPass);

		label_1 = new JLabel("");
		label_1.setBounds(409, 13, 0, 0);
		panelCentral.add(label_1);

		txtDireccion = new JTextField();
		txtDireccion.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtDireccion.setBounds(177, 50, 293, 39);
		panelCentral.add(txtDireccion);
		txtDireccion.setColumns(10);

		txtPuerto = new JTextField();
		txtPuerto.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPuerto.setBounds(177, 102, 293, 36);
		panelCentral.add(txtPuerto);
		txtPuerto.setColumns(10);

		label_2 = new JLabel("");
		label_2.setBounds(287, 37, 0, 0);
		panelCentral.add(label_2);

		label_3 = new JLabel("");
		label_3.setBounds(292, 37, 0, 0);
		panelCentral.add(label_3);

		txtDbname = new JTextField();
		txtDbname.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtDbname.setBounds(177, 151, 293, 36);
		panelCentral.add(txtDbname);
		txtDbname.setColumns(10);

		label_4 = new JLabel("");
		label_4.setBounds(418, 37, 0, 0);
		panelCentral.add(label_4);

		label_5 = new JLabel("");
		label_5.setBounds(423, 37, 0, 0);
		panelCentral.add(label_5);

		txtUser = new JTextField();
		txtUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtUser.setBounds(177, 200, 293, 36);
		panelCentral.add(txtUser);
		txtUser.setColumns(10);
		
		txtPass = new JTextField();
		txtPass.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPass.setColumns(10);
		txtPass.setBounds(177, 249, 293, 36);
		panelCentral.add(txtPass);

		label_6 = new JLabel("");
		label_6.setBounds(216, 64, 0, 0);
		panelCentral.add(label_6);

		label_7 = new JLabel("");
		label_7.setBounds(221, 64, 0, 0);
		panelCentral.add(label_7);

		label_8 = new JLabel("");
		label_8.setBounds(226, 64, 0, 0);
		panelCentral.add(label_8);

		label_9 = new JLabel("");
		label_9.setBounds(231, 64, 0, 0);
		panelCentral.add(label_9);

		label_10 = new JLabel("");
		label_10.setBounds(357, 64, 0, 0);
		panelCentral.add(label_10);

		label_11 = new JLabel("");
		label_11.setBounds(362, 64, 0, 0);
		panelCentral.add(label_11);

		label_12 = new JLabel("");
		label_12.setBounds(367, 64, 0, 0);
		panelCentral.add(label_12);

		label_13 = new JLabel("");
		label_13.setBounds(372, 64, 0, 0);
		panelCentral.add(label_13);
	}

	private void setPanelDerecho() {
		panelDerecho = new JPanel();
		panelDerecho.setBackground(Color.DARK_GRAY);
		getContentPane().add(panelDerecho, BorderLayout.EAST);

	}

	private void setPanelBotones() {
		subPanelBotones = new JPanel();
		FlowLayout flowLayout = (FlowLayout) subPanelBotones.getLayout();
		flowLayout.setVgap(20);
		flowLayout.setHgap(50);
		subPanelBotones.setBackground(Color.DARK_GRAY);
		getContentPane().add(subPanelBotones, BorderLayout.SOUTH);
		setBotones();
	}

	private void setBotones() {
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setPreferredSize(new Dimension(150, 35));
		btnConfirmar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		subPanelBotones.add(btnConfirmar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setPreferredSize(new Dimension(150, 35));
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		subPanelBotones.add(btnCancelar);
		contentPanel.setLayout(new FlowLayout());
	}

	public JTextField getTxtDireccion() {
		return txtDireccion;
	}

	public void setTxtDireccion(JTextField txtDireccion) {
		this.txtDireccion = txtDireccion;
	}

	public JTextField getTxtPuerto() {
		return txtPuerto;
	}

	public void setTxtPuerto(JTextField txtPuerto) {
		this.txtPuerto = txtPuerto;
	}

	public JTextField getTxtDbname() {
		return txtDbname;
	}

	public void setTxtDbname(JTextField txtDbname) {
		this.txtDbname = txtDbname;
	}

	public JTextField getTxtUser() {
		return txtUser;
	}

	public void setTxtUser(JTextField txtUser) {
		this.txtUser = txtUser;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public void setBtnConfirmar(JButton btnConfirmar) {
		this.btnConfirmar = btnConfirmar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JTextField getTxtPass() {
		return txtPass;
	}

	public void setTxtPass(JPasswordField txtPass) {
		this.txtPass = txtPass;
	}
	
	public void mostrar() {
		this.setVisible(true);
	}

	public void ocultar() {
		this.setVisible(false);
	}
}
