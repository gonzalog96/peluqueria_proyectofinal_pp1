package presentacion.reportes.controlador;

import java.util.List;
import dto.ClienteDTO;
import modelo.Peluqueria;
import presentacion.reportes.loader.ReporteClientes;
import presentacion.reportes.loader.ReporteEmpleados;
import presentacion.reportes.loader.ReporteIE;
import presentacion.reportes.loader.ReporteServicios;
import presentacion.reportes.objetosReporte.ObjetoReporte;
import presentacion.reportes.objetosReporte.ObjetoReporteIE;
import presentacion.reportes.vista.ReporteVista;

public class ReporteController {
	private static ReporteController INSTANCE;
	private Peluqueria peluqueria;
	private ReporteVista reporteVista;

	public static ReporteController getInstance(Peluqueria peluqueria) {
		if (INSTANCE == null)
			INSTANCE = new ReporteController(peluqueria);
		return INSTANCE;
	}

	private ReporteController(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
		this.reporteVista = ReporteVista.getInstance();
		escucharBotones();
	}

	private void escucharBotones() {
		this.reporteVista.getBtnReporteClientes().addActionListener(cl -> mostrarReporteClientes());
		this.reporteVista.getBtnReporteServicios().addActionListener(s -> mostrarReporteServicios());
		this.reporteVista.getBtnReporteEmpleados().addActionListener(e -> mostrarReporteEmpleados());
		this.reporteVista.getBtnReporteIngresoEgreso().addActionListener(ie -> mostrarReporteIngresoEgreso());
	}

	private void mostrarReporteClientes() {
		List<ClienteDTO> clientes = this.peluqueria.obtenerClientes();
		ReporteClientes reporte = new ReporteClientes(clientes);
		reporte.mostrar();
	}

	private void mostrarReporteServicios() {
		List<ObjetoReporte> servicios = ObjetoReporte.mapeo(this.peluqueria.turnosCerrados());
		ReporteServicios reporte = new ReporteServicios(servicios);
		reporte.mostrar();
	}

	private void mostrarReporteEmpleados() {
		List<ObjetoReporteIE> caja = ObjetoReporteIE.mapeoEmp(this.peluqueria.getListadoDeIngresos());
		caja.addAll(ObjetoReporteIE.mapeoEmp(this.peluqueria.getListadoDeEgresos()));
		ReporteEmpleados reporte = new ReporteEmpleados(caja);
		reporte.mostrar();
	}

	private void mostrarReporteIngresoEgreso() {
		List<ObjetoReporteIE> caja = ObjetoReporteIE.mapeoIng(this.peluqueria.getListadoDeIngresos());
		caja.addAll(ObjetoReporteIE.mapeoIng(this.peluqueria.getListadoDeEgresos()));
		ReporteIE reporte = new ReporteIE(caja);
		reporte.mostrar();
	}

	public void mostrarConfigReportesVista() {
		this.reporteVista.mostrar();
	}
}
