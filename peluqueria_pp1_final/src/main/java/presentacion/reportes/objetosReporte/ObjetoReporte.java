package presentacion.reportes.objetosReporte;

import java.util.ArrayList;
import java.util.List;

import dto.DetalleTurnoDTO;
import dto.CajaDTO;
import dto.TurnoDTO;
import dto.UsuarioDTO;

public class ObjetoReporte {
	private String group1;
	private String group2;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	public ObjetoReporte(String group1, String group2, String field1, String field2, String field3,
			String field4, String field5, String field6) {
		super();
		this.group1 = group1;
		this.group2 = group2;
		this.field1 = field1;
		this.field2 = field2;
		this.field3 = field3;
		this.field4 = field4;
		this.field5 = field5;
		this.field6 = field6;
	}
	public String getGroup1() {
		return group1;
	}
	public void setGroup1(String group1) {
		this.group1 = group1;
	}
	public String getGroup2() {
		return group2;
	}
	public void setGroup2(String group2) {
		this.group2 = group2;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	
	
	@Override
	public String toString() {
		return "ObjetoReporteProfesional [group1=" + group1 + ", group2=" + group2 + ", field1=" + field1 + ", field2="
				+ field2 + ", field3=" + field3 + ", field4=" + field4 + ", field5=" + field5 + ", field6=" + field6
				+ "]";
	}
	public static List<ObjetoReporte> mapeo(List<TurnoDTO> turnos){
		ArrayList<ObjetoReporte> ret = new ArrayList<ObjetoReporte>();
		for (TurnoDTO turno : turnos) {
			for (DetalleTurnoDTO detalle : turno.getDetalles()) {
				String nombreProf = detalle.getProfesional().getNombre() + " " + detalle.getProfesional().getApellido();
				String nombreServicio = detalle.getServicio().getNombre();
				String fecha = turno.getFecha().toString();
				String horaInicio = detalle.getHoraInicio().toString();
				String horaFin = detalle.getHoraFin().toString();
				String cliente = turno.getCliente().getNombre() + " " + turno.getCliente().getApellido();
				
				String idProf =Integer.toString(detalle.getProfesional().getId()) ;
				String idServicio =Integer.toString(detalle.getServicio().getId()) ;
				
				ObjetoReporte fila = new ObjetoReporte( idServicio, idProf, nombreServicio, nombreProf, fecha, horaInicio, horaFin, cliente);
				System.out.println(fila.toString());
				ret.add(fila);
			}
		}
		
		
		return ret;
	}
	public static List<ObjetoReporte> mapeoIng(List<CajaDTO> datos){
		ArrayList<ObjetoReporte> lista = new ArrayList<ObjetoReporte>();
		for (CajaDTO dato : datos) {
			String grupo1 = dato.getTipoMovimiento();
			String grupo2 = Integer.toString(dato.getIdSucursal());
			String field1 = dato.getTipoMovimiento();
			String field2 = Integer.toString(dato.getIdSucursal());
			String field3 = dato.getFecha().toString();
			String field4 = dato.getMonto().toString();
			String field5 = Integer.toString(dato.getIdEmpleado());
			String field6 = dato.getDetalle();
			ObjetoReporte fila = new ObjetoReporte(grupo1, grupo2, field1, field2, field3, field4, field5, field6);
			System.out.println(fila.toString());
			lista.add(fila);
		}
		return lista;
	}
}
