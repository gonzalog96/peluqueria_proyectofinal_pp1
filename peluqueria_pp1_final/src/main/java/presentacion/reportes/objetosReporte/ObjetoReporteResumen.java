package presentacion.reportes.objetosReporte;

public class ObjetoReporteResumen {
	private float monto;
	private String tipoMovimiento;
	private String id_sucursal;
	private String fechaInicio;
	private String fechaFin;
	public ObjetoReporteResumen(float monto, String tipoMovimiento, String id_sucursal, String fechaInicio,
			String fechaFin) {
		super();
		this.monto = monto;
		this.tipoMovimiento = tipoMovimiento;
		this.id_sucursal = id_sucursal;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}
	public float getMonto() {
		return monto;
	}
	public void setMonto(float monto) {
		this.monto = monto;
	}
	public String getTipoMovimiento() {
		return tipoMovimiento;
	}
	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}
	public String getId_sucursal() {
		return id_sucursal;
	}
	public void setId_sucursal(String id_sucursal) {
		this.id_sucursal = id_sucursal;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
}
