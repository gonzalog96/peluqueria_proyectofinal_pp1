package presentacion.reportes.vista;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.Font;

public class ReporteVista extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel panelPrincipal;
	private JPanel panelCentral;
	private JButton btnReporteClientes;
	private JButton btnReporteServicios;
	private JButton btnReporteEmpleados;
	private JButton btnReporteIngresoEgreso;
	private static ReporteVista INSTANCE;

	public static ReporteVista getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ReporteVista();
		return INSTANCE;
	}

	private ReporteVista() {
		setTitle("Reportes");
		setPropiedades();
		controlDeCierre();
		ocultar();
	}

	private void setPropiedades() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 391, 455);
		panelPrincipal = new JPanel();
		panelPrincipal.setBorder(null);
		setContentPane(panelPrincipal);
		panelPrincipal.setLayout(new BorderLayout(0, 0));
		setPanelInferior();
		setPanelSuperior();
		setPanelIzquierdo();
		setPanelDerecho();
		setPanelCentral();
	}

	private void setPanelCentral() {
		panelCentral = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelCentral.getLayout();
		flowLayout.setVgap(35);
		flowLayout.setHgap(50);
		panelCentral.setBackground(Color.LIGHT_GRAY);
		panelPrincipal.add(panelCentral, BorderLayout.CENTER);
		setBotones();
	}

	private void setBotones() {
		btnReporteClientes = new JButton("Clientes");
		btnReporteClientes.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReporteClientes.setPreferredSize(new Dimension(150, 50));
		panelCentral.add(btnReporteClientes);

		btnReporteServicios = new JButton("Servicios");
		btnReporteServicios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReporteServicios.setPreferredSize(new Dimension(150, 50));
		panelCentral.add(btnReporteServicios);

		btnReporteEmpleados = new JButton("Empleados");
		btnReporteEmpleados.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReporteEmpleados.setPreferredSize(new Dimension(150, 50));
		panelCentral.add(btnReporteEmpleados);

		btnReporteIngresoEgreso = new JButton("Ingreso | Egreso");
		btnReporteIngresoEgreso.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnReporteIngresoEgreso.setPreferredSize(new Dimension(150, 50));
		panelCentral.add(btnReporteIngresoEgreso);
	}

	private void setPanelDerecho() {
		JPanel panelDerecho = new JPanel();
		panelDerecho.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(panelDerecho, BorderLayout.EAST);
	}

	private void setPanelIzquierdo() {
		JPanel panelIzquierdo = new JPanel();
		panelIzquierdo.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(panelIzquierdo, BorderLayout.WEST);
	}

	private void setPanelSuperior() {
		JPanel panelSuperior = new JPanel();
		panelSuperior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(panelSuperior, BorderLayout.NORTH);
	}

	private void setPanelInferior() {
		JPanel panelInferior = new JPanel();
		panelInferior.setBackground(Color.DARK_GRAY);
		panelPrincipal.add(panelInferior, BorderLayout.SOUTH);
	}

	private void controlDeCierre() {
		// Manejo del cierre de ventana
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				close();
			}
		});
	}

	private void close() {
		if (JOptionPane.showConfirmDialog(this, "Se cerrará la ventana de reportes", "¿Desea continuar?",
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			dispose();
		}
	}

	public JButton getBtnReporteClientes() {
		return btnReporteClientes;
	}

	public void setBtnReporteClientes(JButton btnReporteClientes) {
		this.btnReporteClientes = btnReporteClientes;
	}

	public JButton getBtnReporteServicios() {
		return btnReporteServicios;
	}

	public void setBtnReporteServicios(JButton btnReporteServicios) {
		this.btnReporteServicios = btnReporteServicios;
	}

	public JButton getBtnReporteEmpleados() {
		return btnReporteEmpleados;
	}

	public void setBtnReporteEmpleados(JButton btnReporteEmpleados) {
		this.btnReporteEmpleados = btnReporteEmpleados;
	}

	public JButton getBtnReporteIngresoEgreso() {
		return btnReporteIngresoEgreso;
	}

	public void setBtnReporteIngresoEgreso(JButton btnReporteIngresoEgreso) {
		this.btnReporteIngresoEgreso = btnReporteIngresoEgreso;
	}

	public void ocultar() {
		this.setVisible(false);
	}

	public void mostrar() {
		this.setVisible(true);
	}
}
