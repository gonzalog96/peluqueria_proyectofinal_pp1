package presentacion.reportes.loader;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import presentacion.reportes.objetosReporte.ObjetoReporteIE;

public class ReporteEmpleados {
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint reporteLleno;
	// private Logger log = Logger.getLogger(ReporteClientes.class);

	// Recibe la lista de clientes para armar el reporte

	public ReporteEmpleados(List<ObjetoReporteIE> objReporte) {
			Map<String, Object> parametersMap = new HashMap<String, Object>();
			parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			
			Collections.sort(objReporte, new Comparator<ObjetoReporteIE>() {
				public int compare(ObjetoReporteIE obj1, ObjetoReporteIE obj2) {
				if(obj1.getGroup1().compareTo(obj2.getGroup2())==0) {
					if(obj1.getGroup2().compareTo(obj2.getGroup2())==0) {
						return obj1.getField3().compareTo(obj2.getField3());
					}
					else {
						return obj1.getGroup2().compareTo(obj2.getGroup2());
					}
				}
				else {
					return obj1.getGroup2().compareTo(obj2.getGroup2());
				}
				}
			});

		try {
			this.reporte = (JasperReport) JRLoader
					.loadObjectFromFile("reportes" + File.separator + "ReporteEmp.jasper");
			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap,
					new JRBeanCollectionDataSource(objReporte));
			// log.info("Se cargó correctamente el reporte");
			System.out.println("Se cargó correctamente el reporte");
		} catch (JRException ex) {
			// log.error("Ocurrió un error mientras se cargaba el archivo
			// ReporteAgenda.Jasper", ex);
			System.out.println("Ocurrió un error mientras se cargaba el archivo ReporteEmp.Jasper");
			System.out.println("El error es: " + ex);
		}
	}

	public void mostrar() {
		this.reporteViewer = new JasperViewer(this.reporteLleno, false);
		this.reporteViewer.setVisible(true);
	}
}