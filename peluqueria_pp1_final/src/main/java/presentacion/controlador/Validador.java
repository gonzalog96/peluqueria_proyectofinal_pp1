package presentacion.controlador;

import java.util.regex.Pattern;

public class Validador {
		
		public Validador () {
			super();
		}
		//validacion nombre
		private static final String STRING_REGEX = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";

		private static final Pattern STRING_PATTERN = Pattern.compile(STRING_REGEX);


		private static final String NUM_REGEX = "\\d+";

		private static final Pattern NUM_PATTERN = Pattern.compile(NUM_REGEX);

	
		private static final String EMAIL_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*" +
				"@" + "(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

		private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

		private static final String ALFANUM_REGEX = "^[a-zA-Z0-9]+"; 

		private static final Pattern ALFANUM_PATTERN = Pattern.compile(ALFANUM_REGEX);
		
		private static final String FLOAT_REGEX = "[0-9]*\\\\.?[0-9]+";
		
		private static final Pattern FLOAT_PATTERN = Pattern.compile(FLOAT_REGEX);
		
		private static final String HOUR_REGEX = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
		
		private static final Pattern HOUR_PATTERN = Pattern.compile(HOUR_REGEX);
	
		@SuppressWarnings("static-access")
		public boolean esStringValido(String nombre) {
			return STRING_PATTERN.matches(STRING_REGEX, nombre);
		}
		
		@SuppressWarnings("static-access")
		public boolean esEmailValido(String email) {
			return EMAIL_PATTERN.matches(EMAIL_REGEX, email);
		}
		
		@SuppressWarnings("static-access")
		public boolean esNumerico(String string) {
			return NUM_PATTERN.matches(NUM_REGEX, string);
		}
		
		@SuppressWarnings("static-access")
		public boolean esAlfanum(String string) {
			return ALFANUM_PATTERN.matches(ALFANUM_REGEX, string);
		}
		
		@SuppressWarnings("static-access")
		public boolean esFloat(String string) {
			return FLOAT_PATTERN.matches(FLOAT_REGEX, string);
		}
		
		@SuppressWarnings("static-access")
		public boolean esHora(String string) {
			return HOUR_PATTERN.matches(HOUR_REGEX, string);
		}
}
