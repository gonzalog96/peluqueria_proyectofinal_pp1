package presentacion.controlador;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import static java.time.temporal.ChronoUnit.MINUTES;

import dto.ClienteDTO;
import dto.EstadoCliente;
import dto.EstadoTurno;
import dto.TurnoDTO;
import email.Sender;
import modelo.Peluqueria;
import persistencia.propiedades.Propiedad;

public class Demonio {
	Peluqueria peluqueria;
	List<TurnoDTO> turnos;
	private Timer timer;
	public TimerTask task;
	public TimerTask task2;
	public Demonio(Peluqueria peluqueria,Timer timer) {
		this.peluqueria = peluqueria;
		
		
		System.out.println("hola1");
	    this.timer = timer;

	    task = new TimerTask() {
	    
	        @Override
	        public void run()
	        {
	        	
	            turnos = peluqueria.turnosDelDia(LocalDate.now());
	           
	            for (TurnoDTO turnoDTO : turnos) {
	            	
	            	analizarTurno(turnoDTO,LocalTime.now());
	            	peluqueria.actualizarTurno(turnoDTO);
	            }
	        }
	        };
	     
	        // Empezamos dentro de 10ms y luego lanzamos la tarea cada 60000ms = 1 minuto
	      
	    
	    
	    
	    

	    task2 = new TimerTask() {
	    
	        @Override
	        public void run()
	        {
	        	
	        	Sender mail = new Sender();
	        	int deudamax = Integer.valueOf(Propiedad.load("deuda","propiedades/config.properties"));
	        	List<ClienteDTO> lista = peluqueria.obtenerClientes();
	        	for (ClienteDTO cliente : lista) {
					if(analizarCliente(cliente)<deudamax) {
						System.out.println("cliente no moroso");
						if(cliente.getEstado().equals(EstadoCliente.MOROSO)) {
							cliente.setEstado(EstadoCliente.ACTIVO);
							peluqueria.actualizarCliente(cliente);
							mail.enviarEmail(cliente.getEmail(), "prueba", "sus deudas estan saldadas");
						}
						
					}
					else {
						if(cliente.getEstado().equals(EstadoCliente.ACTIVO)) {
							cliente.setEstado(EstadoCliente.MOROSO);
							peluqueria.actualizarCliente(cliente);
							mail.enviarEmail(cliente.getEmail(), "prueba", "estimado cliente venga a pagar");
							}
						}
					}
	        	}
	        };
	    
	   
		
	}
	
	
	public static boolean analizarTurno(TurnoDTO turno,LocalTime hora) {
		LocalTime tiempo = turno.getHora_inicio().toLocalTime();
		if(turno.getEstado_turno().equals(EstadoTurno.OCUPADO) || turno.getEstado_turno().equals(EstadoTurno.DEMORADO)) {
			if(tiempo.compareTo(hora)<0) {
				if(tiempo.until(hora, MINUTES)<15) {
					turno.setEstado_turno(EstadoTurno.DEMORADO);
					return true;
				}
				else {
					turno.setEstado_turno(EstadoTurno.AUSENTE);
					return true;
				}
			}
	
		}
		return false;
	
	}
	public float analizarCliente(ClienteDTO cliente) {
		float deuda = 0;
		List<TurnoDTO> turnos = this.peluqueria.turnosDeuda(cliente);
		for (TurnoDTO turno : turnos) {
			deuda = deuda + (turno.getPrecio()-turno.getMontoPagado());
		}
		return deuda;
	}
	public void runDemonio() {
		timer.schedule(task, 10, 60000);
		timer.schedule(task2, 10, 360000);
	}
	
}
