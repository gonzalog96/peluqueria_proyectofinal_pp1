package presentacion.controlador.contador;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import dto.SucursalDTO;
import modelo.Peluqueria;
import presentacion.reportes.loader.ReporteResumen;
import presentacion.reportes.objetosReporte.ObjetoReporteResumen;
import presentacion.vista.contador.ReporteResumenVista;

public class ReporteResumenController {
	private static ReporteResumenController INSTANCE;
	private Peluqueria peluqueria;
	private ReporteResumenVista reporteVista;
	private List<SucursalDTO> sucursales;

	public static ReporteResumenController getInstance(Peluqueria peluqueria) {
		if (INSTANCE == null)
			INSTANCE = new ReporteResumenController(peluqueria);
		return INSTANCE;
	}

	private ReporteResumenController(Peluqueria pel) {
		peluqueria = pel;
		reporteVista = ReporteResumenVista.getInstance();

		completarComboSucursal();
		acciones();
	}

	private void acciones() {
		reporteVista.getChckbxTodasLasSucursales().addActionListener(e -> System.out.println(validarCheck()));
		reporteVista.getBtnConfirmar().addActionListener(s -> generarReporte());
	}

	public void mostrarFiltro() {
		reporteVista.mostrar();
	}

	private void completarComboSucursal() {
		sucursales = peluqueria.obtenerSucursales();
		for (SucursalDTO s : sucursales) {
			reporteVista.getComboSucursal().addItem(s);
		}
	}

	private boolean validarCheck() {
		if (reporteVista.getChckbxTodasLasSucursales().isSelected()) {
			reporteVista.getComboSucursal().setEnabled(false);
			return true;
		} else {
			reporteVista.getComboSucursal().setEnabled(true);
			return false;
		}
	}
	
	private void generarReporte() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		int sucursal;
		String desde = parseString(reporteVista.getDateDesde().getDate());
		String hasta = parseString(reporteVista.getDateHasta().getDate());
		
		LocalDate localDesde = LocalDate.parse(desde, formatter);
		LocalDate localHasta = LocalDate.parse(hasta, formatter);
		
		if(validarCheck()) {
			sucursal = 0;
		}else {
			SucursalDTO sucursalObj = (SucursalDTO) reporteVista.getComboSucursal().getSelectedItem();
			sucursal = sucursalObj.getId();
		}
		
		List<ObjetoReporteResumen> datos = peluqueria.getDatosReporteResumen(localDesde, localHasta, sucursal);
		ReporteResumen reporte = new ReporteResumen(datos);
		reporte.mostrar();
		
	}
	
	private String parseString(Date date) {
		String fecha = "";
		if (date != null) {
			SimpleDateFormat formateador = new SimpleDateFormat("d/MM/yyyy");
			fecha = formateador.format(date);
		}
		return fecha;
	}


}














