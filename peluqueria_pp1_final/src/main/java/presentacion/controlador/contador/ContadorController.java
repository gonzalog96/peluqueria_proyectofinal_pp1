package presentacion.controlador.contador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dto.SucursalDTO;
import modelo.Peluqueria;
import presentacion.vista.contador.ContadorVista;

public class ContadorController {
	private static ContadorController INSTANCE;
	private ContadorVista contadorVista;
	private Peluqueria peluqueria;
	private ReporteResumenController reporteController;
	private Map<String, Integer> mapIdNombreSucursales;
	
	public static ContadorController getInstance(Peluqueria pelu) {
		if(INSTANCE == null) INSTANCE = new ContadorController(pelu);
		return INSTANCE;
	}
	
	private ContadorController(Peluqueria pelu) {
		this.contadorVista = ContadorVista.getInstance();
		this.peluqueria = pelu;
		reporteController = ReporteResumenController.getInstance(pelu);
		mapIdNombreSucursales = new HashMap<String, Integer>();
		acciones();
	}
	
	private void acciones() {
		contadorVista.getBtnIngresoEgreso().addActionListener(x -> mostrarFiltroReporte(x));
	}
	
	private void mostrarFiltroReporte(ActionEvent x) {
		reporteController.mostrarFiltro();		
	}
	
	public void mostrarContadorVista() {
		this.contadorVista.getLblFechaActual().setText(getFechaActual("dd/MM/yyyy"));
		cargarSucursales();
		escucharSucursalSeleccionada();
		
		this.contadorVista.mostrar();
	}
	
	private void escucharSucursalSeleccionada() {
		this.contadorVista.getComboBoxSucursales().addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		        cargarMovimientos((String) contadorVista.getComboBoxSucursales().getSelectedItem());
		        //cargarEgresos((String) contadorVista.getComboBoxSucursales().getSelectedItem());
		    }
		});
	}
	
	private void cargarSucursales() {
		List<SucursalDTO> sucursales = peluqueria.obtenerSucursales();
		
		this.contadorVista.getComboBoxSucursales().addItem("-");
		this.contadorVista.getComboBoxSucursales().addItem("Todas las sucursales");
		for (SucursalDTO s : sucursales) {
			this.contadorVista.getComboBoxSucursales().addItem(s.getNombre());
			this.mapIdNombreSucursales.put(s.getNombre(), s.getId());
		}
	}
	private void cargarMovimientos(String nombreSucursal) {
		/**
		 * "-": NO SE SELECCIONÓ NINGUNA SUCURSAL.
		 * "Todas las sucursales": OBTENER LOS DATOS GLOBALES.
		 * Caso contrario, se obtienen los datos de una sucursal LOCAL.
		 */
		if (!nombreSucursal.equals("-")) {
			if (nombreSucursal.equals("Todas las sucursales")) {
				// cargamos los datos de TODAS las sucursales!
				BigDecimal montoTotalIngresos = peluqueria.getSaldosDeSucursales("INGRESOS");
				this.contadorVista.getMontoIngresos().setText("$" + String.valueOf(montoTotalIngresos));
				
				BigDecimal montoTotalEgresos = peluqueria.getSaldosDeSucursales("EGRESOS");
				this.contadorVista.getMontoEgresos().setText("$" + String.valueOf(montoTotalEgresos));
				
				// cargamos los datos en la tabla de ingresos de TODAS LAS SUCURSALES!
				this.contadorVista.llenarTablaIngresos(peluqueria.getDetalleGlobalDeIngresos());
				this.contadorVista.llenarTablaEgresos(peluqueria.getDetalleGlobalDeEgresos());
				
			} else {
				// cargamos los datos de una sucursal en particular.
				BigDecimal montoTotalIngresos = peluqueria.getTotalesPorSucursalID(mapIdNombreSucursales.get(nombreSucursal), "INGRESOS");
				this.contadorVista.getMontoIngresos().setText("$" + String.valueOf(montoTotalIngresos));
				
				BigDecimal montoTotalEgresos = peluqueria.getTotalesPorSucursalID(mapIdNombreSucursales.get(nombreSucursal), "EGRESOS");
				this.contadorVista.getMontoEgresos().setText("$" + String.valueOf(montoTotalEgresos));
				
				int sucursalID = mapIdNombreSucursales.get(nombreSucursal);
				this.contadorVista.llenarTablaIngresos(peluqueria.getDetalleIngresosPorSucursalID(sucursalID));
				this.contadorVista.llenarTablaEgresos(peluqueria.getDetalleEgresosPorSucursalID(sucursalID));
			}
		} else {
			this.contadorVista.limpiarMovimientos();
		}
	}
	
	private String getFechaActual(String formato) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		String fecha = localDate.format(formatter);
		return fecha;
	}

	private LocalDate formatearFecha(String fechaRecibida) {
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		return LocalDate.parse(fechaRecibida, formateador);
		
	}
}