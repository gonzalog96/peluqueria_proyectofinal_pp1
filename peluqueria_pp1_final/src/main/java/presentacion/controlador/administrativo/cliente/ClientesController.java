package presentacion.controlador.administrativo.cliente;

import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.util.List;

import javax.swing.JOptionPane;

import dto.ClienteDTO;
import dto.EstadoCliente;
import modelo.Peluqueria;
import presentacion.controlador.administrativo.AdministrativoController;
import presentacion.vista.administrativo.cliente.ClientesVista;

public class ClientesController {
	private Peluqueria peluqueria;
	private static ClientesController INSTANCE;
	private ClientesVista clientesVista;
	private AdministrativoController adminController;
	private AddClientesController addClientesController;
	private EditClienteController editClienteController;
	private List<ClienteDTO> clientes;
	
	
	public static ClientesController getInstance(Peluqueria peluqueria, AdministrativoController admin) {
		if (INSTANCE == null)
			INSTANCE = new ClientesController(peluqueria, admin);
		return INSTANCE;
	}
	private ClientesController(Peluqueria pel, AdministrativoController admin) {
		peluqueria = pel;
		adminController = admin;
		clientesVista = ClientesVista.getInstance();
		addClientesController = AddClientesController.getInstance(peluqueria,this);
		editClienteController = EditClienteController.getInstance(pel, this);
		actualizarTablaServicios();
		acciones();
	}

	public void acciones() {
		actualizarTablaServicios();
		clientesVista.getBtnTurnos().addActionListener(q -> activateTurnosVista(q));
		clientesVista.getBtnCrearClientes().addActionListener(e -> activateAddClienteVista(e));
		clientesVista.getBtnCambiarEstadoCliente().addActionListener(s -> deleteCliente(s, seleccionar()));
		clientesVista.getBtnEditarCliente().addActionListener(x -> activateEditClienteVista(x));
	}

	public void activateTurnosVista(ActionEvent q) {
		clientesVista.ocultar();
		adminController.mostrarAdministrativoVista();
	}

	public void mostrarClientesVista() {
		this.clientesVista.mostrar();
	}

	public void actualizarTablaServicios() {
		clientes = peluqueria.obtenerClientes();
		clientesVista.llenarTabla(clientes);
	}

	private void activateAddClienteVista(ActionEvent e) {
		System.out.println("add vista ");
		addClientesController.mostrarAddClienteVista();
	}
	
	private int seleccionar() {
		return  clientesVista.getTablaClientes().getSelectedRow();
	}
	
	private void activateEditClienteVista(ActionEvent s) {
		int fila = seleccionar();
		if(fila >= 0 ) {
			editClienteController.mostrarEditClienteVista();
			editClienteController.setEditar(clientes.get(fila));
			completarCamposEditVista();
			
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente para editar.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	private void completarCamposEditVista() {
		String nombre = clientes.get(seleccionar()).getNombre();
		String apellido = clientes.get(seleccionar()).getApellido();
		String email = clientes.get(seleccionar()).getEmail();
		String telefono = clientes.get(seleccionar()).getTelefono();
		String dni = clientes.get(seleccionar()).getDni();
		EstadoCliente estado = clientes.get(seleccionar()).getEstado();
		
		editClienteController.getEditProfVista().getTxtNombre().setText(nombre);
		editClienteController.getEditProfVista().getTxtApellido().setText(apellido);
		editClienteController.getEditProfVista().getTxtEmail().setText(email);
		editClienteController.getEditProfVista().getTxtTelefono().setText(telefono);
		editClienteController.getEditProfVista().getTxtDni().setText(dni);
		editClienteController.getEditProfVista().getComboEstado().setSelectedItem(estado);
	}

	
	public boolean deleteCliente(ActionEvent s, int filaCliente) {
		try {
			int fila = filaCliente;
			int id = this.clientes.get(fila).getId();
			String nombre = this.clientes.get(fila).getNombre();
			String apellido = this.clientes.get(fila).getApellido();
			String email = this.clientes.get(fila).getEmail();
			String telefono = this.clientes.get(fila).getTelefono();
			String dni = this.clientes.get(fila).getDni();
			EstadoCliente estado = this.clientes.get(fila).getEstado();
			LocalDate ultima = this.clientes.get(fila).getUltimaVisita();
			int puntos = this.clientes.get(fila).getPuntos();

			if (estado.equals(EstadoCliente.ACTIVO))
				estado = EstadoCliente.INACTIVO;
			else
				estado = EstadoCliente.ACTIVO;

			ClienteDTO editar = new ClienteDTO(id, nombre, apellido, email, telefono, dni, estado, ultima, puntos);
			int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere cambiar el estado del cliente?",
					"Estado profesional", JOptionPane.YES_NO_OPTION);

			if (resp == 0) {
				peluqueria.actualizarCliente(editar);
				actualizarTabla();
				return true;
			}
			return false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente.", "ALERTA!",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}

	}
	public void actualizarTabla() {
		clientes = peluqueria.obtenerClientes();
		clientesVista.llenarTabla(clientes);
	}
	public Peluqueria getPeluqueria() {
		return peluqueria;
	}
	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}
	public ClientesVista getClientesVista() {
		return clientesVista;
	}
	public void setClientesVista(ClientesVista clientesVista) {
		this.clientesVista = clientesVista;
	}
	public AdministrativoController getAdminController() {
		return adminController;
	}
	public void setAdminController(AdministrativoController adminController) {
		this.adminController = adminController;
	}
	public AddClientesController getAddClientesController() {
		return addClientesController;
	}
	public void setAddClientesController(AddClientesController addClientesController) {
		this.addClientesController = addClientesController;
	}
	public EditClienteController getEditClienteController() {
		return editClienteController;
	}
	public void setEditClienteController(EditClienteController editClienteController) {
		this.editClienteController = editClienteController;
	}
	public List<ClienteDTO> getClientes() {
		return clientes;
	}
	public void setClientes(List<ClienteDTO> clientes) {
		this.clientes = clientes;
	}
	
	

}
