package presentacion.controlador.administrativo.cliente;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import dto.ClienteDTO;
import dto.EstadoCliente;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrativo.cliente.AddClienteVista;

public class AddClientesController {
	private static AddClientesController INSTANCE;
	private Peluqueria peluqueria;
	private AddClienteVista addClienteVista;
	private ClientesController clientesController;
	private Validador validador;

	public static AddClientesController getInstance(Peluqueria peluqueria, ClientesController cliente) {
		if (INSTANCE == null)
			INSTANCE = new AddClientesController(peluqueria, cliente);
		return INSTANCE;
	}

	private AddClientesController(Peluqueria pel, ClientesController cliente) {
		addClienteVista = AddClienteVista.getInstance();
		clientesController = cliente;
		peluqueria = pel;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		addClienteVista.getBtnAgregarProfesional().addActionListener(e -> crearCliente(e));
	}

	public void mostrarAddClienteVista() {
		addClienteVista.mostrar();
	}

	
	public void completarVistaEditar() {
		
	}

	public boolean crearCliente(ActionEvent e) {
		String nombre = addClienteVista.getTxtNombre().getText();
		String apellido = addClienteVista.getTxtApellido().getText();
		String email = addClienteVista.getTxtEmail().getText();
		String telefono = addClienteVista.getTxtTelefono().getText();
		String dni = addClienteVista.getTxtDni().getText();
		EstadoCliente estado = (EstadoCliente) addClienteVista.getComboEstado().getSelectedItem();

		ClienteDTO nuevo = new ClienteDTO(nombre, apellido, email, telefono, dni, estado);
		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Crear cliente", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, apellido, email, telefono, dni)) {
			peluqueria.agregarCliente(nuevo);
			clientesController.actualizarTabla();
			addClienteVista.ocultar();
			limpiarCampos();
			return true;
		}
		limpiarCampos();
		return false;
	}
	
	private boolean validarCampos(String nombre, String apellido, String email, String telefono, String dni) {
		boolean validado = true;
		String mensaje = "";

		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esStringValido(apellido)) {
			validado = validado && false;
			mensaje += "Apellido invalido\n";
		}
		if (!validador.esEmailValido(email)) {
			validado = validado && false;
			mensaje += "Email invalido\n";
		}
		if (!validador.esNumerico(telefono)) {
			validado = validado && false;
			mensaje += "Telefono invalido\n";
		}
		if (!validador.esNumerico(dni)) {
			validado = validado && false;
			mensaje += "DNI invalido\n";
		}

		if (!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
	private void limpiarCampos() {
		addClienteVista.getTxtApellido().setText(null);
		addClienteVista.getTxtDni().setText(null);
		addClienteVista.getTxtEmail().setText(null);
		addClienteVista.getTxtNombre().setText(null);
		addClienteVista.getTxtTelefono().setText(null);

	}

	
	///////////////////////////////////////////////////////

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public AddClienteVista getAddClienteVista() {
		return addClienteVista;
	}

	public void setAddClienteVista(AddClienteVista addClienteVista) {
		this.addClienteVista = addClienteVista;
	}
	
	

}
