package presentacion.controlador.administrativo.cliente;

import java.awt.event.ActionEvent;
import java.time.LocalDate;

import javax.swing.JOptionPane;

import dto.EstadoCliente;
import dto.ClienteDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.controlador.administrativo.cliente.ClientesController;
import presentacion.vista.administrativo.cliente.EditClienteVista;

public class EditClienteController {
	private static EditClienteController INSTANCE;
	private Peluqueria peluqueria;
	private EditClienteVista editClienteVista;
	private ClientesController clienteController;
	private ClienteDTO editar;
	private Validador validador;

	public static EditClienteController getInstance(Peluqueria peluqueria, ClientesController servicio) {
		if (INSTANCE == null)
			INSTANCE = new EditClienteController(peluqueria, servicio);
		return INSTANCE;
	}

	private EditClienteController(Peluqueria pel, ClientesController servicio) {
		editClienteVista = EditClienteVista.getInstance();
		clienteController = servicio;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		editClienteVista.getBtnConfirmar().addActionListener(z -> editarCliente(z));
	}

	public void mostrarEditClienteVista() {
		editClienteVista.mostrar();
	}

	private EditClienteController(Peluqueria pel) {
		editClienteVista = EditClienteVista.getInstance();
		peluqueria = pel;
		acciones();
	}

	public boolean editarCliente(ActionEvent e) {
		int id = editar.getId();

		String nombre = editClienteVista.getTxtNombre().getText();
		String apellido = editClienteVista.getTxtApellido().getText();
		String email = editClienteVista.getTxtEmail().getText();
		String telefono = editClienteVista.getTxtTelefono().getText();
		String dni = editClienteVista.getTxtDni().getText();
		LocalDate ultimaVisita = editar.getUltimaVisita();
		int puntos = editar.getPuntos();
		EstadoCliente estado = (EstadoCliente) editClienteVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Editar cliente",
				JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, apellido, email, telefono, dni)) {
			ClienteDTO editar = new ClienteDTO(id, nombre, apellido, email, telefono, dni, estado, ultimaVisita, puntos);
			peluqueria.actualizarCliente(editar);
			clienteController.actualizarTablaServicios();
			editClienteVista.ocultar();
			return true;
		}
		return false;
	}

	private boolean validarCampos(String nombre, String apellido, String email, String telefono, String dni) {
		boolean validado = true;
		String mensaje = "";

		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esStringValido(apellido)) {
			validado = validado && false;
			mensaje += "Apellido invalido\n";
		}
		if (!validador.esEmailValido(email)) {
			validado = validado && false;
			mensaje += "Email invalido\n";
		}
		if (!validador.esNumerico(telefono)) {
			validado = validado && false;
			mensaje += "Telefono invalido\n";
		}
		if (!validador.esNumerico(dni)) {
			validado = validado && false;
			mensaje += "DNI invalido\n";
		}

		if (!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}

	///////////////////////////////////////////////////////

	public EditClienteVista getEditProfVista() {
		return editClienteVista;
	}

	public void setAddEditProfVista(EditClienteVista addEditProfVista) {
		this.editClienteVista = addEditProfVista;
	}

	public ClienteDTO getEditar() {
		return editar;
	}

	public void setEditar(ClienteDTO edit) {
		this.editar = edit;
	}

}
