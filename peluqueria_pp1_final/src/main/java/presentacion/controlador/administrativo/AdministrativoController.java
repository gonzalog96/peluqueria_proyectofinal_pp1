package presentacion.controlador.administrativo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.JOptionPane;

import dto.EstadoCliente;
import dto.EstadoTurno;
import dto.SucursalDTO;
import dto.TurnoDTO;
import dto.UsuarioDTO;
import modelo.Peluqueria;
import persistencia.propiedades.Propiedad;
import presentacion.controlador.administrativo.avisos.AvisosController;
import presentacion.controlador.administrativo.cliente.ClientesController;
import presentacion.controlador.administrativo.pagos.PagoController;
import presentacion.controlador.administrativo.stock.StockController;
import presentacion.vista.administrativo.AdministrativoVista;

public class AdministrativoController {
	private static AdministrativoController INSTANCE;
	private AdministrativoVista administrativoVista;
	private AddTurnoController addTurnoController;
	private Peluqueria peluqueria;
	private UsuarioDTO usuario;
	private SucursalDTO sucursal;
	private List<TurnoDTO> turnos;
	private String fechaEnFiltro;
	private String horaEnFiltro;
	private boolean promocionesCheck;
	private boolean demoradosCheck;
	private boolean pagosPendientesCheck;
	private boolean turnosCanceladosCheck;
	private static Locale locale;
	private ResourceBundle bundle;
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE CLIENTES///////////////////////////////////////////
	private ClientesController clientesController;
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE CLIENTES///////////////////////////////////////////
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE STOCK///////////////////////////////////////////
	private StockController stockController;
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE STOCK///////////////////////////////////////////
	
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE AVISOS ///////////////////////////////////////////
	private AvisosController avisosController;
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE AVISOS ///////////////////////////////////////////
	
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE PAGOS ///////////////////////////////////////////
	private PagoController pagoController;
	///////////////////////////////////////// SE AGREGA EL CONTROLADOR DE PAGOS ///////////////////////////////////////////
	
	public static AdministrativoController getInstance(Peluqueria peluqueria, UsuarioDTO usuario) {
		if (INSTANCE == null)
			INSTANCE = new AdministrativoController(peluqueria, usuario);
		return INSTANCE;
/*		} else {
			return INSTANCE;
		}*/
	}

	private AdministrativoController(Peluqueria peluqueria, UsuarioDTO usuario) {
		this.peluqueria = peluqueria;
		this.usuario = usuario;
		this.sucursal = usuario.getSucursal();
		this.turnos = this.peluqueria.obtenerTurnos();
		this.administrativoVista = AdministrativoVista.getInstance();

		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE CLIENTES///////////////////////////////////////////
		this.clientesController = ClientesController.getInstance(peluqueria, this);
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE CLIENTES///////////////////////////////////////////

		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE STOCK///////////////////////////////////////////
		this.stockController = StockController.getInstance(peluqueria, this);
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE STOCK///////////////////////////////////////////
		
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE AVISOS ///////////////////////////////////////////
		this.avisosController = AvisosController.getInstance(peluqueria);
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE AVISOS ///////////////////////////////////////////
		
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE PAGOS ///////////////////////////////////////////
		this.pagoController = PagoController.getInstance(peluqueria, this);
		///////////////////////////////////////// SE INSTANCIA EL CONTROLADOR DE PAGOS ///////////////////////////////////////////

		escucharComponentes();
		setIdiomaInicial();
	}

	public void mostrarAdministrativoVista() {
		setCalendarioEnFechaActual();
		setLblUsuarioActual();
		setLblSucursalActual();
		refrescarTabla(filtrarTurnosPorFecha(this.peluqueria.obtenerTurnos(this.usuario.getSucursal().getId()),	getFechaActual("yyyy-MM-dd")));

		
		this.administrativoVista.mostrar();
	}
	
	private void setHorariosEnCombo() {
		this.administrativoVista.getComboBoxHoras().removeAllItems();
		for(String h : obtenerTodosLosHorarios()) {
			this.administrativoVista.getComboBoxHoras().addItem(h);
		}
	}

	private void setIdiomaInicial() {
		cargarIdiomas();
		locale = new Locale("es","ES");
		bundle = ResourceBundle.getBundle("i18n/strings_es_ES", locale);
		setIdiomasEnComponentes();
	}

	private void cargarIdiomas() {
		List<String> idiomas = cargarListaDeIdiomas();
		for(String s : idiomas) {
			this.administrativoVista.getBoxLang().addItem(s);
		}
	}

	private List<String> cargarListaDeIdiomas() {
		List<String> idiomas = new ArrayList<String>();
		idiomas.add("es_ES");
		idiomas.add("en_US");
		return idiomas;
	}
	
	private void setIdiomasEnComponentes() {
		this.administrativoVista.getLblFecha().setText(bundle.getString("lblFecha"));
		this.administrativoVista.getLblHora().setText(bundle.getString("lblHora"));
		this.administrativoVista.getLblFiltros().setText(bundle.getString("lblFiltrar"));
		this.administrativoVista.getLblUsuario().setText(bundle.getString("lblUsuario"));
		this.administrativoVista.getLblSucursal().setText(bundle.getString("lblSucursal"));
		this.administrativoVista.getBoxPromocion().setText(bundle.getString("checkPromocion"));
		this.administrativoVista.getBoxDemorado().setText(bundle.getString("checkDemorado"));
		this.administrativoVista.getBoxPagoPendiente().setText(bundle.getString("checkPendiente"));
		this.administrativoVista.getBoxCancelados().setText(bundle.getString("checkCancelado"));
		//this.administrativoVista.getBtnClientes().setText(bundle.getString("btnClientes"));
		//this.administrativoVista.getBtnAvisos().setText(bundle.getString("btnAvisos"));
		
		setLblUsuarioActual();
		setLblSucursalActual();
		setHorariosEnCombo();
		
	}

	private void cambiarIdioma() {
		for(ActionListener al : this.administrativoVista.getBtnSetIdioma().getActionListeners()) {
			this.administrativoVista.getBtnSetIdioma().removeActionListener(al);
		}
		this.administrativoVista.getBtnSetIdioma().addActionListener(l -> escucharIdioma());
		
	}

	private void escucharIdioma() {
		bundle = ResourceBundle.getBundle("i18n/strings_"+this.administrativoVista.getBoxLang().getSelectedItem(), locale);
		setIdiomasEnComponentes();
	}

//	private void setLblFechaActual(String fechaActual) {
//		this.administrativoVista.getLblFechaActual().setText("Fecha: " + fechaActual);
//	}

	private void setCalendarioEnFechaActual() {
		java.util.Date fechaParseada = null;
		try {
			fechaParseada = new SimpleDateFormat("dd/MM/yyyy").parse(getFechaActual("dd/MM/yyyy"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.administrativoVista.getDateChooser().setDate(fechaParseada);
	}

	private void setLblUsuarioActual() {
		this.administrativoVista.getLblUsuario().setText("Usuario: " + usuario.getUsuario());
	}

	private void setLblSucursalActual() {
		this.administrativoVista.getLblSucursal().setText("Sucursal: " + sucursal.getNombre());
	}

	void refrescarTabla(List<TurnoDTO> turnos) {
		this.administrativoVista.llenarTabla(turnos);
	}

	private void escucharComponentes() {
		escucharBotones();
		escucharFiltros();
	}

	private void escucharBotones() {
		escucharBotonRegistrarEgreso();
		escucharBotonPagar();
		escucharBotonCrear();// listo
		escucharBotonCancelar();// listo
		escucharBotonFiltro();// listo
		escucharBotonLimpiarFiltro();// listo
		cambiarIdioma();

		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON CLIENTES//////////////////////////////////////////////
		escucharBotonClientes();
		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON CLIENTES//////////////////////////////////////////////
		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON STOCK//////////////////////////////////////////////
		escucharBotonStock();
		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON STOCK//////////////////////////////////////////////
		
		//////////////////////////////////SE LLAMA A LA FUNCION ESCUCHAR BOTON AVISOS//////////////////////////////////////////////
		escucharBotonAvisos();
		//////////////////////////////////SE LLAMA A LA FUNCION ESCUCHAR BOTON AVISOS//////////////////////////////////////////////
		
		//////////////////////////////////SE LLAMA A LA FUNCION ESCUCHAR BOTON 	CERRAR TURNO///////////////////////////////////////
		escucharBotonCerrarTurno();
		//////////////////////////////////SE LLAMA A LA FUNCION ESCUCHAR BOTON 	CERRAR TURNO//////////////////////////////////////
		escucharBotonHelp();
	}

	private void escucharBotonRegistrarEgreso() {
		this.administrativoVista.getBotonEgreso().addActionListener(e -> activarVentanaEgresos());
	}
	
	private void escucharBotonPagar() {
		this.administrativoVista.getBotonPagarTurno().addActionListener(p -> pagarTurno(p));
	}

	////////////////////////////////// SE AGREGA ESCUCHAR BOTON CLIENTES//////////////////////////////////////////////
	private void escucharBotonClientes() {
		administrativoVista.getBtnClientes().addActionListener(q -> activateClientesVista(q));
	}
	////////////////////////////////// SE AGREGA ESCUCHAR BOTON CLIENTES//////////////////////////////////////////////

	////////////////////////////////// SE AGREGA MOSTRAR VENTANA CLIENTES//////////////////////////////////////////////
	private void activateClientesVista(ActionEvent q) {
		administrativoVista.ocultar();
		clientesController.mostrarClientesVista();
	}
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA CLIENTES//////////////////////////////////////////////

	//////////////////////////////////SE AGREGA ESCUCHAR BOTON STOCK//////////////////////////////////////////////
	private void escucharBotonStock() {
		administrativoVista.getBtnStock().addActionListener(x -> activateStockVista(x));
	}
	//////////////////////////////////SE AGREGA ESCUCHAR BOTON STOCK//////////////////////////////////////////////
	
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA STOCK//////////////////////////////////////////////
	private void activateStockVista(ActionEvent q) {
		administrativoVista.ocultar();
		stockController.mostrarStockVista();
	}
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA STOCK//////////////////////////////////////////////
	
	
	//////////////////////////////////SE AGREGA ESCUCHAR BOTON AVISOS//////////////////////////////////////////////
	private void escucharBotonAvisos() {
		this.administrativoVista.getBtnAvisos().addActionListener(av -> activateAvisosVista(av));
	}
	//////////////////////////////////SE AGREGA ESCUCHAR BOTON AVISOS//////////////////////////////////////////////
	private void escucharBotonHelp() {
		this.administrativoVista.getBtnHelp().addActionListener(av -> activarMenuAyuda(av));
	}
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA AVISOS//////////////////////////////////////////////
	private void activateAvisosVista(ActionEvent av) {
		this.avisosController.mostrarAvisosVista();
	}
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA AVISOS//////////////////////////////////////////////
	private void activarMenuAyuda(ActionEvent av) {
		this.menuAyuda();
	}
	//////////////////////////////////SE AGREGA MOSTRAR VENTANA CERRAR TURNO//////////////////////////////////////////////
	private void escucharBotonCerrarTurno() {
		this.administrativoVista.getBotonCerrarTurno().addActionListener(av -> activateCerrarTurnoVista(av));
	}
	
	public void actualizarGUI() {
		List<TurnoDTO> turnosDeLaSucursal = this.peluqueria.obtenerTurnos(this.usuario.getSucursal().getId());
		List<TurnoDTO> turnosEnTabla = filtrar(turnosDeLaSucursal);
		refrescarTabla(turnosEnTabla);
	}
	
	private void activateCerrarTurnoVista(ActionEvent av) {
		//se prepara la lista de turnosEnTabla
		int idSucursal = this.usuario.getSucursal().getId();
		List<TurnoDTO> turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
		//List<TurnoDTO> turnosEnTabla = filtrarTurnosPorFecha(turnosDeLaSucursal, escucharFiltroFecha());
		List<TurnoDTO> turnosEnTabla = filtrar(turnosDeLaSucursal);
		
		//Si está elegida alguna fila
		if (this.administrativoVista.getTablaTurnos().getSelectedRow() != -1) {
			int option = JOptionPane.showConfirmDialog(this.administrativoVista,
					"¿Está seguro que desea cerrar este turno?");
			if (option == 0) {
				
				//Se obtiene el id del turno
				int id = turnosEnTabla.get(this.administrativoVista.getTablaTurnos().getSelectedRow()).getId();
				//se procede a extraer los datos del turno seleccionado o a actualizar el estado como en este caso
				TurnoDTO turnoACerrar = this.peluqueria.getTurnoDesdeID(id);
				
				//condición para cerrar el turno, que no esté cancelado, o ya cerrado, además se advierte si el cliente es moroso
				if(!turnoACerrar.getEstado_turno().equals(EstadoTurno.CANCELADO)) {
					if(!turnoACerrar.getEstado_turno().equals(EstadoTurno.CERRADO)) {
						if(turnoACerrar.getCliente().getEstado().equals(EstadoCliente.MOROSO)) {
							int opcion = JOptionPane.showConfirmDialog(this.administrativoVista, "ATENCIÓN! Cliente moroso, procure cobrar todo el turno");
							if(opcion == JOptionPane.YES_OPTION) {
								turnoACerrar.setEstado_turno(EstadoTurno.CERRADO);
								this.peluqueria.actualizarTurno(turnoACerrar);
						
								//finalmente, se actualiza la vista
								turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
								//turnosEnTabla = filtrarTurnosPorFecha(turnosDeLaSucursal, escucharFiltroFecha());
								turnosEnTabla = filtrar(turnosDeLaSucursal);
								this.administrativoVista.llenarTabla(turnosEnTabla);
								
								// mensaje final de operacion.
								this.administrativoVista.turnoCerradoConExito();
							}
						}
						else {
							turnoACerrar.setEstado_turno(EstadoTurno.CERRADO);
							this.peluqueria.actualizarTurno(turnoACerrar);
					
							//finalmente, se actualiza la vista
							turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
							//turnosEnTabla = filtrarTurnosPorFecha(turnosDeLaSucursal, escucharFiltroFecha());
							turnosEnTabla = filtrar(turnosDeLaSucursal);
							this.administrativoVista.llenarTabla(turnosEnTabla);
							
							// mensaje final de operacion.
							this.administrativoVista.turnoCerradoConExito();
						}
					}else {
						JOptionPane.showMessageDialog(this.administrativoVista, "El turno ya se encuentra cerrado!");
					}
					
				}else {
					JOptionPane.showMessageDialog(this.administrativoVista, "El turno se encuentra cancelado!");
				}
			} 
		}
		else {
			JOptionPane.showMessageDialog(this.administrativoVista, "Debe seleccionar un turno de la tabla para poder cerrarlo.");
		}
	}
	
	// ********************* GONZA
	public AdministrativoVista getAdministrativoVista()
	{
		return administrativoVista;
	}
	
	private void activarVentanaEgresos() {
		this.pagoController.mostrarVentanaEgresos();
	}
	
	public void setFechaFiltro(LocalDate date) {
		this.administrativoVista.getDateChooser().setDate(convertToDateViaSqlDate(date));
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String fecha = date.format(formatter);
		this.fechaEnFiltro = fecha;

		this.administrativoVista.getBoxPromocion().setSelected(false);
		this.administrativoVista.getBoxCancelados().setSelected(false);
		this.administrativoVista.getBoxDemorado().setSelected(false);
		this.administrativoVista.getBoxPagoPendiente().setSelected(false);
	}
	
	public Date convertToDateViaSqlDate(LocalDate dateToConvert) {
	    return java.sql.Date.valueOf(dateToConvert);
	}

	private void pagarTurno(ActionEvent p) {
		// se prepara la lista de turnosEnTabla
		int idSucursal = this.usuario.getSucursal().getId();
		List<TurnoDTO> turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
		List<TurnoDTO> turnosEnTabla = filtrar(turnosDeLaSucursal);

		// Si está elegida alguna fila
		if (this.administrativoVista.getTablaTurnos().getSelectedRow() != -1) {
				//se obtiene el id del turno
				int id = turnosEnTabla.get(this.administrativoVista.getTablaTurnos().getSelectedRow()).getId();
				TurnoDTO turnoParticular = this.peluqueria.getTurnoDesdeID(id);
				
				// 1. validacion: el turno está cancelado o no está cerrado.
				if (!turnoCancelado(turnoParticular)) {
					// 2. validacion: el turno ya está pagado.
					if (!turnoEstaPago(turnoParticular)) {
						this.pagoController.cargarDatosYMostrarVentanaPagos(turnoParticular);
					} else {
						// el turno ya está pago!
						this.administrativoVista.errorTurnoPago();
					}
				} else {
					// el turno está cancelado!
					this.administrativoVista.errorTurnoCancelado();
				}
		} else {
			// en caso de NO seleccionar un turno, se procede al pago de un CLIENTE CASUAL!
			int opcion = this.administrativoVista.mensajeClienteCasual();
			if (opcion == 0) {
				TurnoDTO turnoParticular = null;
				
				// solucion temporal para los ingresos casuales.
				switch(this.sucursal.getId()) {
				case 1:
					turnoParticular = peluqueria.getTurnoDesdeID(9990);
					break;
				case 2:
					turnoParticular = peluqueria.getTurnoDesdeID(9991);
					break;
				case 3:
					turnoParticular = peluqueria.getTurnoDesdeID(9992);
					break;
				case 4:
					turnoParticular = peluqueria.getTurnoDesdeID(9993);
					break;
					
				default: 
					break;
				}
				this.pagoController.cargarDatosClienteCasual(turnoParticular);
			}
		}
	}
	
	private boolean turnoCancelado(TurnoDTO turnoParticular) {
		return turnoParticular.getEstado_turno().equals(EstadoTurno.CANCELADO);
	}
	
	private boolean turnoEstaPago(TurnoDTO turnoParticular) {
		boolean ret = false;
		
		float montoTotal = turnoParticular.getPrecio();
		float montoPagado = turnoParticular.getMontoPagado();
		
		if (Float.compare(montoTotal, montoPagado) == 0) {
			ret = true;
		}
		return ret;
	}
	//////////////////////////////////// **

	private void escucharBotonCrear() {
		for( ActionListener al : this.administrativoVista.getBotonCrearTurno().getActionListeners() ) {
			this.administrativoVista.getBotonCrearTurno().removeActionListener( al );
		 }
		this.administrativoVista.getBotonCrearTurno().addActionListener(cr -> crearTurno(cr));
	}

	private void crearTurno(ActionEvent cr) {
		this.addTurnoController = AddTurnoController.getInstance(peluqueria, this.usuario, this);
		this.addTurnoController.mostrarAddTurnoVista();
	}

	private void escucharBotonCancelar() {
		this.administrativoVista.getBotonCancelarTurno().addActionListener(c -> cancelarTurno(c));

	}

	private void cancelarTurno(ActionEvent c) {
		//se prepara la lista de turnosEnTabla
		int idSucursal = this.usuario.getSucursal().getId();
		List<TurnoDTO> turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
		//List<TurnoDTO> turnosEnTabla = filtrarTurnosPorFecha(turnosDeLaSucursal, escucharFiltroFecha());
		List<TurnoDTO> turnosEnTabla = filtrar(turnosDeLaSucursal);
		
		//Si está elegida alguna fila
		if (this.administrativoVista.getTablaTurnos().getSelectedRow() != -1) {
			int option = JOptionPane.showConfirmDialog(this.administrativoVista,
					"¿Está seguro que desea cancelar este turno?");
			if (option == 0) {
				//Se obtiene el id del turno
				int id = turnosEnTabla.get(this.administrativoVista.getTablaTurnos().getSelectedRow()).getId();
				TurnoDTO turnoACancelar = this.peluqueria.getTurnoDesdeID(id);
				if(!turnoACancelar.getEstado_turno().equals(EstadoTurno.CANCELADO)) {
					//se procede a extraer los datos del turno seleccionado o a actualizar el estado como en este caso
					this.peluqueria.cancelarTurno(id);
					
					//finalmente, se actualiza la vista
					turnosDeLaSucursal = this.peluqueria.obtenerTurnos(idSucursal);
					//turnosEnTabla = filtrarTurnosPorFecha(turnosDeLaSucursal, escucharFiltroFecha());
					turnosEnTabla = filtrar(turnosDeLaSucursal);
					
					this.administrativoVista.llenarTabla(turnosEnTabla);
					
					this.administrativoVista.turnoCanceladoConExito();
				}else {
					JOptionPane.showMessageDialog(this.administrativoVista, "El turno ya se encuentra cancelado!");
				}
				
			}

		} else {
			JOptionPane.showMessageDialog(this.administrativoVista, "No se ha seleccionado ningún turno de la tabla");
		}
	}

	private void escucharBotonFiltro() {
		this.administrativoVista.getBotonFiltrarTurnos().addActionListener(f -> filtrarTurnos(f));
	}

	private void filtrarTurnos(ActionEvent f) {
		escucharFiltros();
	}

	private void escucharFiltros() {
		this.fechaEnFiltro = escucharFiltroFecha();
		this.horaEnFiltro = escucharFiltroHora();
		this.promocionesCheck = escucharFiltroPromocion();
		this.demoradosCheck = escucharFiltroDemorados();
		this.pagosPendientesCheck = escucharFiltroPagosPendientes();
		this.turnosCanceladosCheck = escucharFiltroCancelados();

		if (this.fechaEnFiltro.isEmpty() && this.horaEnFiltro.equals("Todas") && !(promocionesCheck || demoradosCheck || pagosPendientesCheck || turnosCanceladosCheck)) {
			this.turnos = this.peluqueria.obtenerTurnos();
			this.administrativoVista.getDateChooser().setDate(new Date(System.currentTimeMillis()));
			this.fechaEnFiltro = getFechaActual("yyyy-MM-dd");
			
			refrescarTabla(filtrarTurnosPorFecha(this.turnos, getFechaActual("yyyy-MM-dd")));// con los filtros vacios todos los turnos del dia
			
		}
		else {
			System.out.println("\n\n\nAlguno de los filtros están ACTIVADOS!!!!");
			System.out.println(fechaEnFiltro+" "+horaEnFiltro);
			System.out.println("FECHA EN FILTRO: "+fechaEnFiltro);
			
			// Si uno ó varios filtros estan activos, se manda a filtrar
			List<TurnoDTO> turnosParaLaTabla = filtrar(this.peluqueria.obtenerTurnos(this.usuario.getSucursal().getId()));
			refrescarTabla(turnosParaLaTabla);
		}
	}
	
	private boolean ningunFiltroActivado() {
		return this.fechaEnFiltro.equals("") && this.horaEnFiltro.equals("Todas") && !(promocionesCheck || demoradosCheck || pagosPendientesCheck || turnosCanceladosCheck);
	}

	public String escucharFiltroFecha() {
		Date date = this.administrativoVista.getDateChooser().getDate();
		String fecha = "";
		if (date != null) {
			SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
			fecha = formateador.format(date);
		}
		return fecha;
	}
	
	public String formatearFechaDesdeDate(Date date) {
		String fecha = "";
		if (date != null) {
			SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
			fecha = formateador.format(date);
		}
		return fecha;
	}

	private String escucharFiltroHora() {
		Object itemSeleccionado = this.administrativoVista.getComboBoxHoras().getSelectedItem();
		String hora = "Todas";
		return (itemSeleccionado != null) ? hora = itemSeleccionado.toString() : hora;
	}

	private boolean escucharFiltroPromocion() {
		return this.administrativoVista.getBoxPromocion().isSelected();
	}

	private boolean escucharFiltroDemorados() {
		return this.administrativoVista.getBoxDemorado().isSelected();
	}

	private boolean escucharFiltroPagosPendientes() {
		return this.administrativoVista.getBoxPagoPendiente().isSelected();
	}
	
	private boolean escucharFiltroCancelados() {
		return this.administrativoVista.getBoxCancelados().isSelected();
	}

	public List<TurnoDTO> filtrar(List<TurnoDTO> turnosCompletos) {
		List<TurnoDTO> turnosFiltrados = new ArrayList<TurnoDTO>();

		// Filtro por fecha
		if (!this.fechaEnFiltro.equals("") || this.fechaEnFiltro != null) {
			turnosFiltrados = filtrarTurnosPorFecha(turnosCompletos, fechaEnFiltro);
		} else {
			turnosFiltrados = filtrarTurnosPorFecha(turnosCompletos, getFechaActual("yyyy-MM-dd"));
		}

		// Filtro por hora
		if (!this.horaEnFiltro.equals("Todas") && turnosFiltrados.size() != 0) {
			List<TurnoDTO> turnosABorrar = new ArrayList<TurnoDTO>();
			for (TurnoDTO t : turnosFiltrados) {
				if (!t.getHora_inicio().toString().substring(0, 5).equals(horaEnFiltro)) {
					turnosABorrar.add(t);
				}
			}
			for(TurnoDTO t : turnosABorrar) {
				turnosFiltrados.remove(t);
			}
		}

		// Filtro por promociones
		if (this.promocionesCheck && turnosFiltrados.size() != 0) {
			List<TurnoDTO> turnosABorrar = new ArrayList<TurnoDTO>();
			for (TurnoDTO t : turnosFiltrados) {
				if (t.getPromocion().getId() == 1) {// 1 indica una promoción nula
					turnosABorrar.add(t);
				}
			}
			for(TurnoDTO t : turnosABorrar) {
				turnosFiltrados.remove(t);
			}
		}

		// Filtro por turnos demorados
		if (this.demoradosCheck && turnosFiltrados.size() != 0) {
			List<TurnoDTO> turnosABorrar = new ArrayList<TurnoDTO>();
			for (TurnoDTO t : turnosFiltrados) {
				if (!t.getEstado_turno().equals(EstadoTurno.DEMORADO)) {
					turnosABorrar.add(t);
				}
			}
			for(TurnoDTO t : turnosABorrar) {
				turnosFiltrados.remove(t);
			}
		}

		// Filtro por pagos pendientes
		if (this.pagosPendientesCheck && turnosFiltrados.size() != 0) {
			List<TurnoDTO> turnosABorrar = new ArrayList<TurnoDTO>();
			for (TurnoDTO t : turnosFiltrados) {
				if (t.getMontoPagado() == t.getPrecio()) {
					turnosABorrar.add(t);
				}
			}
			turnosFiltrados.removeAll(turnosABorrar);
		}
		
		// Filtro por turnos cancelados
		if (this.turnosCanceladosCheck && turnosFiltrados.size() != 0) {
			List<TurnoDTO> turnosABorrar = new ArrayList<TurnoDTO>();
			for (TurnoDTO t : turnosFiltrados) {
				if (!t.getEstado_turno().equals(EstadoTurno.CANCELADO)) {
					turnosABorrar.add(t);
				}
			}
			for(TurnoDTO t : turnosABorrar) {
				turnosFiltrados.remove(t);
			};
		}

		return turnosFiltrados;
	}

	private List<String> obtenerTodosLosHorarios() {
		List<String> ret = new ArrayList<String>();
		ret.add(bundle.getString("todasLasHoras"));
		
		String horaAux_cadena = "";
		String horaInicial_cadena =  Propiedad.load("hora_inicio", "propiedades/config.properties");
		String horaLimite_cadena = Propiedad.load("hora_limite", "propiedades/config.properties");
		
		LocalTime horaInicial = LocalTime.parse(horaInicial_cadena);
		LocalTime horaLimite = LocalTime.parse(horaLimite_cadena);

		while(horaInicial.isBefore(horaLimite)) {
			horaAux_cadena = horaInicial.toString();
			ret.add(horaAux_cadena);
			horaInicial = horaInicial.plusMinutes(30);
		}
		
		return ret;
	}

	List<TurnoDTO> filtrarTurnosPorFecha(List<TurnoDTO> lista, String fecha) {
		List<TurnoDTO> ret = new ArrayList<TurnoDTO>();
		if (!fecha.equals("")) {
			for (TurnoDTO t : lista) {
				if (t.getFecha().toString().equals(fecha)) {
					ret.add(t);
				}
			}
		} else {
			ret = lista;
		}

		return ret;
	}

	private void escucharBotonLimpiarFiltro() {
		this.administrativoVista.getBotonLimpiarFiltros().addActionListener(l -> limpiarFiltros(l));
	}

	private void limpiarFiltros(ActionEvent l) {
		this.administrativoVista.getDateChooser().setDate(null);
		this.administrativoVista.getComboBoxHoras().setSelectedIndex(0);
		this.administrativoVista.getBoxPromocion().setSelected(false);
		this.administrativoVista.getBoxDemorado().setSelected(false);
		this.administrativoVista.getBoxPagoPendiente().setSelected(false);
		refrescarTabla(filtrarTurnosPorFecha(this.peluqueria.obtenerTurnos(), getFechaActual("yyyy-MM-dd")));
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	// ********************* GONZA
	public UsuarioDTO getUsuario()
	{
		return usuario;
	}
	
	public String getFechaActual(String formato) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		String fecha = localDate.format(formatter);
		return fecha;
	}
	// ***************************
	
	private void menuAyuda() {
		try {
			// Carga el fichero de ayuda
			File fichero = new File("help/help_set.hs");
			URL hsURL = fichero.toURI().toURL();

			// Crea el HelpSet y el HelpBroker
			HelpSet helpset = new HelpSet(getClass().getClassLoader(), hsURL);
			HelpBroker hb = helpset.createHelpBroker();

			// Pone ayuda a item de menu al pulsarlo y a F1 en ventana
			// principal y secundaria.
			hb.enableHelpOnButton(administrativoVista.getBtnHelp(), "aplicacion", helpset);
			hb.enableHelpKey(administrativoVista.getContentPane(), "ventana_principal",
					helpset);
			hb.enableHelpKey(administrativoVista.getContentPane(), "ventana_secundaria",
					helpset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setearFechaEnDateChooser(String fecha) {
		java.util.Date fechaParseada = null;
		try {
			fechaParseada = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.administrativoVista.getDateChooser().setDate(fechaParseada);

	}
}
