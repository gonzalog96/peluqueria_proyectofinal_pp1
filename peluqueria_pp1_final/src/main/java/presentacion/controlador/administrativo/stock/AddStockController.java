package presentacion.controlador.administrativo.stock;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import dto.EstadoPromocion;
import dto.ProductoDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrativo.stock.AddStockVista;

public class AddStockController {
	private static AddStockController INSTANCE;
	private Peluqueria peluqueria;
	private AddStockVista addStockVista;
	private StockController stockController;
	private List<ProductoDTO> productos;
	private Validador validador;

	public static AddStockController getInstance(Peluqueria peluqueria, StockController stock) {
		if (INSTANCE == null)
			INSTANCE = new AddStockController(peluqueria, stock);
		return INSTANCE;
	}

	private AddStockController(Peluqueria pel, StockController stock) {
		addStockVista = AddStockVista.getInstance();
		stockController = stock;
		peluqueria = pel;
		validador = new Validador();
		acciones();
	}

	public void mostrarAddStockVista() {
		addStockVista.mostrar();
	}

	private void acciones() {
		actualizarComboProductos();
		addStockVista.getBtnConfirmar().addActionListener(s -> insertarStock(s));
	}

	private void actualizarComboProductos() {
		productos = peluqueria.getProductos();
		for (ProductoDTO p : productos)
			addStockVista.getComboProducto().addItem(p);
	}

	private void insertarStock(ActionEvent s) {
		int idSucursal = stockController.getAdministrativo().getSucursal().getId();
		ProductoDTO producto = (ProductoDTO) addStockVista.getComboProducto().getSelectedItem();
		int nuevo = peluqueria.obtenerStock(producto, idSucursal);
		System.out.println("STOCK VIEJO "+ nuevo);
		String cantidad = addStockVista.getTxtCantidad().getText();
		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Agregar nuevo stock",
				JOptionPane.YES_NO_OPTION);
		if(resp == 0) {
			if(validador.esNumerico(cantidad)) {
				peluqueria.actualizarStock(producto, idSucursal, Integer.parseInt(cantidad) + nuevo);
				stockController.actualizarTablaProductos();
				addStockVista.ocultar();
			}else {
				JOptionPane.showMessageDialog(null, "Debe ingresar una cantidad valida.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

}
