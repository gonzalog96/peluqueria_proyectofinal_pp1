package presentacion.controlador.administrativo.stock;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import dto.CajaDTO;
import dto.StockDTO;
import dto.TurnoDTO;
import modelo.Peluqueria;
import presentacion.controlador.administrativo.AdministrativoController;
import presentacion.vista.administrativo.pagos.EgresoProductoVista;
import presentacion.vista.administrativo.stock.StockVista;

public class StockController {
	private Peluqueria peluqueria;
	private static StockController INSTANCE;
	private StockVista stockVista;
	private AddStockController addStockController;
	private AdministrativoController administrativo;
	private EgresoProductoVista pagarProductoVista;

	private List<StockDTO> stock;

	public static StockController getInstance(Peluqueria peluqueria, AdministrativoController admin) {// ) {
		if (INSTANCE == null)
			INSTANCE = new StockController(peluqueria, admin);
		return INSTANCE;
	}

	private StockController(Peluqueria pel, AdministrativoController admin) {// , AdministradorController admin) {
		peluqueria = pel;
		addStockController = AddStockController.getInstance(pel, this);
		stockVista = StockVista.getInstance();
		pagarProductoVista = EgresoProductoVista.getInstance();
		administrativo = admin;
		acciones();
	}

	private void acciones() {
		actualizarTablaProductos();
		stockVista.getBtnAddProducto().addActionListener(e -> activateAddStockVista(e));
		stockVista.getBtnEgreso().addActionListener(e -> ventanaEgresos(e));
		stockVista.getBtnProfesionales().addActionListener(k -> activateAdministrativoVista(k));
		
		escucharCambiosEnComboCantidad();
		
		pagarProductoVista.getBtnRegistrarEgresoProducto().addActionListener(re -> registrarEgreso(re));
		pagarProductoVista.getBtnCancelarEgresoProducto().addActionListener(ce -> cancelarEgreso(ce));
	}
	
	private void escucharCambiosEnComboCantidad() {
		this.pagarProductoVista.getComboBoxCantidadProducto().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					pagarProductoVista.getTxtTotalNeto().setText(calcularImporteTotal());
				}
			}
		});
	}
	
	private void activateAdministrativoVista(ActionEvent k) {
		administrativo.mostrarAdministrativoVista();
		stockVista.ocultar();
	}
	
	private void registrarEgreso(ActionEvent re) {
		
		int opcion = this.pagarProductoVista.mensajeConfirmarEgreso();
		if (opcion == 0) {
			// registramos el egreso en la caja.
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate fechaActual = LocalDate.parse(getFechaActual("yyyy-MM-dd"), formatter);

			int idSucursal = this.administrativo.getUsuario().getSucursal().getId();
			int idTurno = calcularIdTurno();
			CajaDTO nuevoEgreso = new CajaDTO(fechaActual, "ingresos", "S/D", idSucursal, idTurno, this.administrativo.getUsuario().getId(), "compra de " + getNombreProductoSeleccionado(), new BigDecimal(pagarProductoVista.getTxtTotalNeto().getText()));
			peluqueria.agregarPago(nuevoEgreso);
			
			// ajustamos el stock del producto en la tabla.
			List<StockDTO> stocks = peluqueria.obtenerStocks(idSucursal);
			StockDTO stockParticular = stocks.get(this.stockVista.getTablaProductos().getSelectedRow());
			
			int cantOriginal = obtenerStockDelProducto();
			int cantComprada = Integer.valueOf(pagarProductoVista.getComboBoxCantidadProducto().getSelectedItem().toString());
			int cantTotalStock = cantOriginal - cantComprada;
			
			peluqueria.actualizarStock(stockParticular.getProducto(), idSucursal, cantTotalStock);
			
			// actualizamos la tabla de productos.
			stock = peluqueria.obtenerStocks(idSucursal);
			stockVista.llenarTabla(stock);
			
			// mensaje final.
			pagarProductoVista.egresoExitoso();
		}
	}
	
	// fix temporal para mostrar adecuadamente los ingresos al contador.
	private int calcularIdTurno() {
		TurnoDTO turnoParticular = null;
		
		// solucion temporal para los ingresos casuales.
		switch(this.administrativo.getSucursal().getId()) {
			case 1:
				turnoParticular = peluqueria.getTurnoDesdeID(9990);
				break;
			case 2:
				turnoParticular = peluqueria.getTurnoDesdeID(9991);
				break;
			case 3:
				turnoParticular = peluqueria.getTurnoDesdeID(9992);
				break;
			case 4:
				turnoParticular = peluqueria.getTurnoDesdeID(9993);
				break;
				
			default: 
				break;
		}
			
		return turnoParticular.getId();
	}
	
	private void cancelarEgreso(ActionEvent re) {
		this.pagarProductoVista.ocultar();
	}
	
	private void ventanaEgresos(ActionEvent e) {
		if (stockVista.getTablaProductos().getSelectedRow() != -1) {
			if (obtenerStockDelProducto() > 0) {
				this.pagarProductoVista.getTxtFechaEgresoProducto().setText(getFechaActual("yyyy-MM-dd"));
				
				agregarCantidadAComboProductos();
				
				this.pagarProductoVista.getTxtProducto().setText(getNombreProductoSeleccionado());
				this.pagarProductoVista.getTxtTotalNeto().setText(calcularImporteTotal());
				this.pagarProductoVista.mostrar();
			} else {
				JOptionPane.showMessageDialog(null, "No hay stock de este producto.", "Registro de egresos de productos", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			JOptionPane.showMessageDialog(null, "No se ha seleccionado ningún producto de la tabla.", "Registro de egresos de productos", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void agregarCantidadAComboProductos() {
		int cantStock = obtenerStockDelProducto();
		
		//this.pagarProductoVista.getComboBoxCantidadProducto().addItem("-");
		for (int i=0; i < cantStock; i++) {
			this.pagarProductoVista.getComboBoxCantidadProducto().addItem(String.valueOf(i+1));
		}
	}
	
	private int obtenerStockDelProducto() {
		int fila = this.stockVista.getTablaProductos().getSelectedRow();
		JTable tabla = this.stockVista.getTablaProductos();
		
		int value = Integer.valueOf(tabla.getModel().getValueAt(fila, 4).toString());
		return value;
	}
	
	private String getNombreProductoSeleccionado() {
		int fila = this.stockVista.getTablaProductos().getSelectedRow();
		JTable tabla = this.stockVista.getTablaProductos();
		
		String value = tabla.getModel().getValueAt(fila, 0).toString();
		return value;
	}
	
	private String calcularImporteTotal() {
		BigDecimal precioUnitario = obtenerPrecioPorUnidad();
		BigDecimal cantidad = new BigDecimal(this.pagarProductoVista.getComboBoxCantidadProducto().getSelectedItem().toString());
	
		BigDecimal total = precioUnitario.multiply(cantidad);
		return total.toString();
	}
	
	private BigDecimal obtenerPrecioPorUnidad() {
		int fila = this.stockVista.getTablaProductos().getSelectedRow();
		JTable tabla = this.stockVista.getTablaProductos();
		
		String value = tabla.getModel().getValueAt(fila, 2).toString();
		return new BigDecimal(value);
	}
	
	private String getFechaActual(String formato) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		String fecha = localDate.format(formatter);
		return fecha;
	}
	
	//
	
	

	public void mostrarStockVista() {
		stockVista.mostrar();
	}

	public void actualizarTablaProductos() {
		int idSucursal = administrativo.getSucursal().getId();
		stock = peluqueria.obtenerStocks(idSucursal);
		stockVista.llenarTabla(stock);
	}

	private void activateAddStockVista(ActionEvent q) {
		addStockController.mostrarAddStockVista();
	}

	public AdministrativoController getAdministrativo() {
		return administrativo;
	}

	public void setAdministrativo(AdministrativoController administrativo) {
		this.administrativo = administrativo;
	}
	
	
}
