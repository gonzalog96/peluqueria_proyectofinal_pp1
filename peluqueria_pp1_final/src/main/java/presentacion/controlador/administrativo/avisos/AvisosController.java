package presentacion.controlador.administrativo.avisos;

import java.util.List;
import javax.swing.JOptionPane;
import dto.TurnoDTO;
import modelo.Peluqueria;
import presentacion.vista.administrativo.avisos.AvisosVista;

public class AvisosController {
	private static AvisosController INSTANCE;
	private AvisosVista avisosVista;
	private List<TurnoDTO> turnosInactivos;
	private Peluqueria peluqueria;
	private int controlAviso;

	public static AvisosController getInstance(Peluqueria peluqueria) {
		if (INSTANCE == null) {
			INSTANCE = new AvisosController(peluqueria);
			return new AvisosController(peluqueria);
		} else {
			return INSTANCE;
		}
	}

	private AvisosController(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
		this.avisosVista = AvisosVista.getInstance();
	}

	public void mostrarAvisosVista() {
		escucharBotonInformado();
		this.refrescarTabla();
		this.avisosVista.mostrar();
	}

	private void refrescarTabla() {
		this.turnosInactivos = this.peluqueria.traeravisos();
		this.avisosVista.llenarTabla(turnosInactivos);
	}

	private void escucharBotonInformado() {
		this.avisosVista.getBtnInformado().addActionListener(av -> confirmarAviso());
	}

	private void confirmarAviso() {
		int ordenSeleccionado = this.avisosVista.getTablaTurnos().getSelectedRow();
		if (ordenSeleccionado >= 0) {
			if (JOptionPane.showConfirmDialog(this.avisosVista, "El aviso se dará como informado, ¿Desea proceder?",
					"Informar avisado", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				int id = this.turnosInactivos.get(ordenSeleccionado).getId();
				this.peluqueria.updateAviso(id);
				this.refrescarTabla();
			}

		} else {
			JOptionPane.showMessageDialog(this.avisosVista, "Debe seleccionar el aviso para informar como cerrado");
		}
	}
}
