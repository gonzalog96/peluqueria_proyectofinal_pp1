package presentacion.controlador.administrativo.pagos;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JOptionPane;

import dto.CajaDTO;
import dto.TurnoDTO;
import modelo.Peluqueria;
import persistencia.propiedades.Propiedad;
import presentacion.controlador.administrativo.AdministrativoController;
import presentacion.vista.administrativo.pagos.EgresoGenericoVista;
import presentacion.vista.administrativo.pagos.PagarTurnoVista;

public class PagoController
{
	private static PagoController INSTANCE;
	private Peluqueria peluqueria;
	private PagarTurnoVista pagarTurnoVista;
	private EgresoGenericoVista egresoGenericoVista;
	private AdministrativoController administrativoController;
	
	// FIX TEMPORAL
	private TurnoDTO turnoTemporal;
	private int puntosADescontar = 0;

	public static PagoController getInstance(Peluqueria peluqueria, AdministrativoController adminController) {
		if (INSTANCE == null) 
			INSTANCE = new PagoController(peluqueria, adminController);
		return INSTANCE;
	}

	private PagoController(Peluqueria pel, AdministrativoController adminController) {
		peluqueria = pel;
		administrativoController = adminController;
		pagarTurnoVista = PagarTurnoVista.getInstance();
		egresoGenericoVista = EgresoGenericoVista.getInstance();
		
		escucharBotones();
		ocultar();
	}
	
	public void cargarDatosClienteCasual(TurnoDTO turnoParticular) {
		limpiar();
		
		this.turnoTemporal = turnoParticular;
		
		if (this.pagarTurnoVista.getComboBoxMediosDePago().getItemCount() == 0)
		{
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("-");
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("Efectivo");
			this.pagarTurnoVista.getComboBoxMediosDePago().setSelectedIndex(1);
			this.pagarTurnoVista.getComboBoxMediosDePago().setEnabled(false);
		} else {
			this.pagarTurnoVista.getComboBoxMediosDePago().setSelectedIndex(1);
		}
		
		this.pagarTurnoVista.getBtnVerDetalleDeServicios().setEnabled(false);
		this.pagarTurnoVista.getTxtFechaHoraTurno().setText(getFechaActual("dd/MM/yyyy"));
		this.pagarTurnoVista.getTxtNombreCliente().setText("Cliente CASUAL");
		this.pagarTurnoVista.getTxtTotalNeto().setText("-");
		this.pagarTurnoVista.getTxtTotalRestante().setText("-");
		
		this.pagarTurnoVista.mostrar();
	}

	public void cargarDatosYMostrarVentanaPagos(TurnoDTO turno) {
		limpiar();
		
		System.out.println("COMBO: " + this.pagarTurnoVista.getComboBoxMediosDePago().getItemCount());
		
		if (this.pagarTurnoVista.getComboBoxMediosDePago().getItemCount() == 0)
		{
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("-");
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("Efectivo");
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("Fiado");
			this.pagarTurnoVista.getComboBoxMediosDePago().addItem("Puntos");
			this.pagarTurnoVista.getComboBoxMediosDePago().setSelectedIndex(0);
		} else {
			this.pagarTurnoVista.getComboBoxMediosDePago().setSelectedIndex(0);
		}
		
		
		this.turnoTemporal = turno;
		
		this.pagarTurnoVista.getTxtFechaHoraTurno().setText(turno.getFecha().toString() + " | " + turno.getHora_inicio().toString());
		this.pagarTurnoVista.getTxtNombreCliente().setText(turno.getCliente().getNombre() + " " + turno.getCliente().getApellido());
		this.pagarTurnoVista.getTxtTotalNeto().setText(String.valueOf(turno.getPrecio()));
		this.pagarTurnoVista.getTxtTotalRestante().setText(String.valueOf(turno.getPrecio() - turno.getMontoPagado()));
		
		this.pagarTurnoVista.mostrar();
	}
	
	private void escucharBotones() {
		this.pagarTurnoVista.getBtnVerDetalleDeServicios().addActionListener(e -> verDetalleServicios(e));
		this.pagarTurnoVista.getBtnPagarTurno().addActionListener(p -> confirmarPago(p));
		this.pagarTurnoVista.getBtnCancelarTurno().addActionListener(ct -> cancelarPagoTurno(ct));
		this.pagarTurnoVista.getBtnAgregarComentarios().addActionListener(ac -> agregarComentario(ac));
		
		this.egresoGenericoVista.getBtnRegistrarEgreso().addActionListener(re -> registrarEgreso(re));
		this.egresoGenericoVista.getBtnCancelarEgreso().addActionListener(ce -> cancelarEgreso(ce));
		
		escucharComboMediosDePago();
	}
	
	private void escucharComboMediosDePago() {
		this.pagarTurnoVista.getComboBoxMediosDePago().addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	if (pagarTurnoVista.getComboBoxMediosDePago().getItemCount() > 0) {
		        	switch ((String) pagarTurnoVista.getComboBoxMediosDePago().getSelectedItem()) {
	        		case "Fiado": 
						BigDecimal montoOriginal = new BigDecimal(pagarTurnoVista.getTxtTotalNeto().getText());
						BigDecimal montoRestante = new BigDecimal(pagarTurnoVista.getTxtTotalRestante().getText());
						
						if (montoOriginal.compareTo(montoRestante) == 0) {
		        			pagarTurnoVista.actualizarDatosParaFiado();
						} else {
							pagarTurnoVista.limpiar();
							pagarTurnoVista.errorNoSePuedeFiar();
						}
	        		break;
	        		
	        		case "Efectivo":
	        		case "Débito":
	        		case "Crédito":
	        		break;
	        			
	        		case "Puntos":
	        			int puntosDelCliente = turnoTemporal.getCliente().getPuntos();
	        			String puntosNecesarios = Propiedad.load("cant_puntos", "propiedades/puntos.properties");
	        			String valorPorCantXDePuntos = Propiedad.load("dinero_por_cantidad", "propiedades/puntos.properties");
	        			
	        			if (puntosDelCliente >= Integer.valueOf(puntosNecesarios)) {
	        				actualizarDatosPagoPorPuntos(puntosDelCliente, valorPorCantXDePuntos, puntosNecesarios);
	        			} else {
	        				pagarTurnoVista.limpiar();
	        				pagarTurnoVista.noTienePuntosSuficientes();
	        			}
	        		break;
	        		
	        		default: 
	        			pagarTurnoVista.limpiar();
	        		break;
	        	}
	        	}
	        }
		});
	}
	
	private void actualizarDatosPagoPorPuntos(int puntosDelCliente, String valorPorCantXDePuntos, String puntosNecesarios) {
		BigDecimal montoRestante = new BigDecimal(this.pagarTurnoVista.getTxtTotalRestante().getText());
		
		// calculamos cuanto dinero puede aportar el cliente.
		int calculoDinero = puntosDelCliente * Integer.valueOf(valorPorCantXDePuntos);
		BigDecimal dineroParaAportar = new BigDecimal(calculoDinero);
		System.out.println("Dinero a aportar: " + dineroParaAportar.toString());
		
		// el monto total de los puntos es SUPERIOR al total restante.
		if (dineroParaAportar.compareTo(montoRestante) == 1) {
			
			// 1. calculamos los puntos necesarios para el monto restante.
			BigDecimal puntosNec = new BigDecimal(puntosNecesarios);
			BigDecimal valorPorPunto = new BigDecimal(valorPorCantXDePuntos);
			int puntosRequeridos = montoRestante.multiply(puntosNec).divide(valorPorPunto, 2, RoundingMode.HALF_UP).intValue();
			
			System.out.println("Puntos necesarios: " + puntosRequeridos);
			
			// 2. calculamos el monto a descontar.
			//BigDecimal montoADescontar = new BigDecimal((puntosRequeridos / Integer.valueOf(puntosNecesarios)) * Integer.valueOf(valorPorCantXDePuntos));
			
			
			// 3. seteamos el monto en el campo en particular y confirmamos la cant. de puntos a descontar una vez se confirme el pago.
			this.pagarTurnoVista.getTxtImporteARegistrar().setText(/*montoADescontar.toString()*/this.pagarTurnoVista.getTxtTotalRestante().getText());
			this.pagarTurnoVista.getTxtImporteARegistrar().setEnabled(false);
			this.pagarTurnoVista.getTxtImporteARegistrar().setEditable(false);
			this.puntosADescontar = puntosRequeridos + 1;
			
		}
		// el monto total de los puntos es IGUAL al total restante.
		else if (dineroParaAportar.compareTo(montoRestante) == 0) {
			// seteamos los montos!
			this.pagarTurnoVista.getTxtImporteARegistrar().setText(this.pagarTurnoVista.getTxtTotalRestante().getText());
			this.pagarTurnoVista.getTxtImporteARegistrar().setEnabled(false);
			this.pagarTurnoVista.getTxtImporteARegistrar().setEditable(false);
			this.puntosADescontar = puntosDelCliente;
		} 
		// el monto total de los puntos es MENOR al total restante.
		else {
			// 1. calculamos el monto al cual se llega con los puntos del cliente.
			BigDecimal montoAlcanzado = new BigDecimal(puntosDelCliente * Integer.valueOf(valorPorCantXDePuntos));
			
			// 2. seteamos los montos y puntos.
			this.pagarTurnoVista.getTxtImporteARegistrar().setText(montoAlcanzado.toString());
			this.pagarTurnoVista.getTxtImporteARegistrar().setEnabled(false);
			this.pagarTurnoVista.getTxtImporteARegistrar().setEditable(false);
			this.puntosADescontar = puntosDelCliente;
		}
		
		this.pagarTurnoVista.mensajePagoConPuntos();
	}
	
	public void mostrarVentanaEgresos() {
		this.egresoGenericoVista.getTxtFechaEgreso().setText(this.administrativoController.getFechaActual("yyyy-MM-dd"));
		this.egresoGenericoVista.mostrar();
	}
	
	private void registrarEgreso(ActionEvent re) {
		if (!this.egresoGenericoVista.getTxtConcepto().getText().isEmpty()) {
			if (isNumeric(this.egresoGenericoVista.getTxtTotal().getText())) {
				int opcion = this.egresoGenericoVista.mensajeConfirmarEgreso();
				if (opcion == 0) {
					// creamos el egreso y lo ingresamos en la caja.
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					LocalDate fechaActual = LocalDate.parse(this.administrativoController.getFechaActual("yyyy-MM-dd"), formatter);

					int idSucursal = this.administrativoController.getUsuario().getSucursal().getId();
					CajaDTO nuevoEgreso = new CajaDTO(fechaActual, "egresos", "S/D", idSucursal, -1, this.administrativoController.getUsuario().getId(), "egreso por " + this.egresoGenericoVista.getTxtConcepto().getText(), new BigDecimal(egresoGenericoVista.getTxtTotal().getText()));
					
					peluqueria.agregarPago(nuevoEgreso);
					
					// mensaje final.
					egresoGenericoVista.egresoExitoso();
				}
			} else {
				// el valor ingresado NO es numérico.
				this.egresoGenericoVista.errorMontoNoNumerico();
			}
		} else {
			// el concepto está vacío.
			this.egresoGenericoVista.errorConceptoVacio();
		}
	}
	
	private void cancelarEgreso(ActionEvent ce) {
		this.egresoGenericoVista.ocultar();
	}
	
	private void agregarComentario(ActionEvent ac) {
		this.pagarTurnoVista.activarCampoComentarios();
	}
	
	private String obtenerComentarios() {
		String comentarios = "SIN COMENTARIOS";
		if (this.pagarTurnoVista.getTxtComentario().getText().length() > 0) {
			comentarios = this.pagarTurnoVista.getTxtComentario().getText();
		}
		return comentarios;
	}
	
	private void verDetalleServicios(ActionEvent e) {
		this.pagarTurnoVista.mostrarServiciosComprados(peluqueria.getServiciosPorTurnoID(this.turnoTemporal.getId()));
	}
	
	private void confirmarPago(ActionEvent p) {
		// validaciones: MEDIO DE PAGO SELECCIONADO!
		if (this.pagarTurnoVista.getComboBoxMediosDePago().getSelectedIndex() != 0) {
			// validaciones: REGEX.
			if (isNumeric(this.pagarTurnoVista.getTxtImporteARegistrar().getText())) {
					// validaciones: pago casual.
					if (this.pagarTurnoVista.getTxtNombreCliente().getText().equals("Cliente CASUAL")) {
						BigDecimal montoARegistrar = new BigDecimal(this.pagarTurnoVista.getTxtImporteARegistrar().getText());
						
						int opcion = JOptionPane.showConfirmDialog(this.pagarTurnoVista, "¿Confirmar pago?");
						if(opcion == JOptionPane.YES_OPTION) {
							// registramos el pago en la caja.
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
							LocalDate fechaActual = LocalDate.parse(getFechaActual("yyyy-MM-dd"), formatter);
							
							int idEmpleado = this.administrativoController.getUsuario().getId();
							int idSucursal = this.administrativoController.getSucursal().getId();
							int idTurno = this.turnoTemporal.getId();
							String medioDePago = this.pagarTurnoVista.getComboBoxMediosDePago().getSelectedItem().toString();
							
							CajaDTO nuevoPago = new CajaDTO(fechaActual, "ingresos", medioDePago, idSucursal, idTurno, idEmpleado, "ingreso casual: " + obtenerComentarios(), montoARegistrar);
							peluqueria.agregarPago(nuevoPago);
							
							// mostramos la ventana de confirmacion de ingreso.
							this.pagarTurnoVista.ingresoCasualExitoso();
							
							// limpiamos los datos.
							limpiar();
						}
					} else {
						// validaciones: monto NO mayor al restante.
						BigDecimal montoARegistrar = new BigDecimal(this.pagarTurnoVista.getTxtImporteARegistrar().getText());
						BigDecimal montoRestante = new BigDecimal(this.pagarTurnoVista.getTxtTotalRestante().getText());
						
						if (montoARegistrar.compareTo(montoRestante) == -1 || montoARegistrar.compareTo(montoRestante) == 0) {
							int opcion = JOptionPane.showConfirmDialog(this.pagarTurnoVista, "¿Confirmar pago?");
							if(opcion == JOptionPane.YES_OPTION) {
								// registramos el pago en la caja.
								DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
								LocalDate fechaActual = LocalDate.parse(getFechaActual("yyyy-MM-dd"), formatter);
								
								int idSucursal = this.turnoTemporal.getSucursal().getId();
								int idEmpleado = this.administrativoController.getUsuario().getId();
								
								int idTurno = this.turnoTemporal.getId();
								
								String medioDePago = this.pagarTurnoVista.getComboBoxMediosDePago().getSelectedItem().toString();
								
								CajaDTO nuevoPago = new CajaDTO(fechaActual, "ingresos", medioDePago, idSucursal, idTurno, idEmpleado, obtenerComentarios(), montoARegistrar);
								peluqueria.agregarPago(nuevoPago);
								
								// actualizamos el valor abonado en el turno.
								peluqueria.actualizarValorPagadoEnTurno(idTurno, montoARegistrar);
								
								// de corresponder, descontamos los puntos por el tipo de pago seleccionado.
								if (this.pagarTurnoVista.getComboBoxMediosDePago().getSelectedItem().toString().equals("Puntos") && this.puntosADescontar > 0) {
									peluqueria.descontarPuntosPorCliente(this.turnoTemporal.getCliente().getId(), this.puntosADescontar);
									
									this.pagarTurnoVista.mensajePuntosDescontados(this.puntosADescontar);
								}
								
								// verificamos si el turno está completamente pago: si lo está, acreditamos los puntos al cliente.
								float montoTotal = this.turnoTemporal.getPrecio();
								float montoTotalResultante = this.peluqueria.getTurnoDesdeID(idTurno).getMontoPagado();
								
								if (Float.compare(montoTotal, montoTotalResultante) == 0) {
									// se pago la totalidad del turno! acreditar los puntos.
									int puntos = this.turnoTemporal.getPuntos();
									peluqueria.addPuntosSegunCliente(this.turnoTemporal.getCliente().getId(), puntos);
									
									this.pagarTurnoVista.puntosAgregadosConExito(puntos);
								}
								
								// mostramos la ventana de confirmacion de pago y acreditamos los puntos.
								this.pagarTurnoVista.pagoExitoso(String.valueOf(montoARegistrar), String.valueOf(montoRestante.subtract(montoARegistrar)));
								
								// actualizar la GUI.
								this.administrativoController.actualizarGUI();
								
								// limpiamos los datos.
								limpiar();
							}
						} else {
							this.pagarTurnoVista.errorMontoSuperiorAlRestante();
						}
					}
			} else {
				this.pagarTurnoVista.errorMontoIngresado();
			}
		} else {
			this.pagarTurnoVista.errorNoSeleccionoMedioDePago();
		}
	}
	
	public String getFechaActual(String formato) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		String fecha = localDate.format(formatter);
		return fecha;
	}
	
	public static boolean isNumeric(String strNum) {
	    try {
	        @SuppressWarnings("unused")
			BigDecimal b = new BigDecimal(strNum);
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false;
	    }
	    return true;
	}
	
	private void limpiar() {
		
		this.pagarTurnoVista.getComboBoxMediosDePago().removeAllItems();
		
		this.pagarTurnoVista.getComboBoxMediosDePago().setEnabled(true);
		this.pagarTurnoVista.getTxtComentario().setEnabled(true);
		this.pagarTurnoVista.getBtnVerDetalleDeServicios().setEnabled(true);
		
		this.pagarTurnoVista.getTxtImporteARegistrar().setEnabled(true);
		this.pagarTurnoVista.getTxtImporteARegistrar().setEditable(true);
		
		this.pagarTurnoVista.getTxtFechaHoraTurno().setText("");
		
		this.pagarTurnoVista.getTxtNombreCliente().setText("");
		this.pagarTurnoVista.getTxtTotalNeto().setText("");
		this.pagarTurnoVista.getTxtTotalRestante().setText("");
		this.pagarTurnoVista.getTxtImporteARegistrar().setText("");
		
		this.pagarTurnoVista.getTxtComentario().setBackground(Color.LIGHT_GRAY);
		this.pagarTurnoVista.getTxtComentario().setText("Clic en AGREGAR COMENTARIO.");
		this.pagarTurnoVista.getTxtComentario().setEnabled(false);
		
		this.pagarTurnoVista.setCampoComentarioActivado(false);
		
		this.puntosADescontar = 0;
		
		this.pagarTurnoVista.ocultar();
	}
	
	private void cancelarPagoTurno(ActionEvent ct) {
		this.pagarTurnoVista.getTxtNombreCliente().setText("");
		this.pagarTurnoVista.getTxtTotalNeto().setText("");
		this.pagarTurnoVista.getTxtTotalRestante().setText("");
		
		this.pagarTurnoVista.ocultar();
	}
	
	public void mostrar() {
		this.pagarTurnoVista.mostrar();
	}
	
	public void ocultar() {
		this.pagarTurnoVista.ocultar();
	}
}