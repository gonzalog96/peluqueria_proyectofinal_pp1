package presentacion.controlador.administrativo;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import dto.DetalleTurnoDTO;
import dto.ProfesionalDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import dto.TurnoDTO;
import dto.UsuarioDTO;
import modelo.Peluqueria;
import presentacion.vista.administrativo.AddTurnoVista;
import presentacion.vista.administrativo.PromocionVista;

public class PromocionController {
	private static PromocionController INSTANCE;
	private AddTurnoVista addTurnoVista;
	private Peluqueria peluqueria;
	private UsuarioDTO usuario;
	private PromocionDTO promocion;
	private PromocionVista promocionVista;
	private List<ProfesionalDTO> profesionalesEnSucursal;
	private List<TurnoDTO> turnos;
	private List<ServicioDTO> servicios;
	private String fechaElegida;
	private List<ProfesionalDTO> profesionales;
	private int cont;
	private String nombre1;
	private String nombre2;
	private String nombre3;
	private String nombre4;
	private String nombre5;
	private AddTurnoController addTurnoController;

	public static PromocionController getInstance(Peluqueria peluqueria, UsuarioDTO usuario, AddTurnoController addTurnoController) {
		if (INSTANCE == null) {
			INSTANCE = new PromocionController(peluqueria, usuario, addTurnoController);
			return new PromocionController(peluqueria, usuario, addTurnoController);
		} else {
			return INSTANCE;
		}
	}

	PromocionController(Peluqueria peluqueria, UsuarioDTO usuario, AddTurnoController addTurnoController) {
		this.peluqueria = peluqueria;
		this.usuario = usuario;
		this.promocionVista = PromocionVista.getInstance();
		this.turnos = this.peluqueria.obtenerTurnos();
		this.profesionales = this.peluqueria.obtenerProfesionales(this.usuario.getSucursal().getId());
		this.addTurnoController = addTurnoController;
	}

	public void mostrarPromocionVista(String fechaElegida, PromocionDTO promocion) {
		this.fechaElegida = fechaElegida;
		this.promocion = promocion;
		this.servicios = this.promocion.getServicios();
		setFormularioPromocion();
		this.promocionVista.mostrar();
	}

	private void setFormularioPromocion() {
		cargarInformacionDePromocion();
		escucharBotonesPromocionVista();

	}

	private void cargarInformacionDePromocion() {
		this.promocionVista.getTxtNombrePromocion().setText(this.promocion.getNombre());
		this.promocionVista.getTxtPrecioPromocion().setText(this.promocion.getPrecio().toString());
		this.promocionVista.getTxtFechaElegida().setText(this.fechaElegida);
		cargarServicios();
	}

	private void cargarServicios() {
		nombre1 = "";
		nombre2 = "";
		nombre3 = "";
		nombre4 = "";
		nombre5 = "";
		for(int i=0; i<servicios.size(); i++) {
			if(i==0) {
				nombre1=this.servicios.get(i).getNombre();
				cargarProfesionales(i);
			}
			if(i==1){
				nombre2=this.servicios.get(i).getNombre();
				cargarProfesionales(i);
			}
			if(i==2){
				nombre3=this.servicios.get(i).getNombre();
				cargarProfesionales(i);
			}
			if(i==3){
				nombre4=this.servicios.get(i).getNombre();
				cargarProfesionales(i);
			}
			if(i==4){
				nombre5=this.servicios.get(i).getNombre();
				cargarProfesionales(i);
			}
		}
		
		this.promocionVista.getTxtServicio1().setText(nombre1);
		this.promocionVista.getTxtServicio2().setText(nombre2);
		this.promocionVista.getTxtServicio3().setText(nombre3);
		this.promocionVista.getTxtServicio4().setText(nombre4);
		this.promocionVista.getTxtServicio5().setText(nombre5);
	}
	

	private void cargarProfesionales(int nroBox) {
		if(!nombre1.equals("")) {
			this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(nroBox)), nroBox);
		}
		getProfesionalesConHabilidad();
	}
	
	private List<ProfesionalDTO> getProfesionalesConHabilidad() {
		List<ProfesionalDTO> lista = new ArrayList<ProfesionalDTO>();
		for(ProfesionalDTO p : this.profesionales) {
			for(ServicioDTO s : this.servicios) {
				if(this.peluqueria.tieneHabilidad(p.getId(), s.getId())) {
					lista.add(p);
				}
			}
		}
		return lista;
	}

	private List<ProfesionalDTO> cargarBoxProfesional(ServicioDTO servicio) {
		List<ProfesionalDTO> lista = new ArrayList<ProfesionalDTO>();
		this.profesionalesEnSucursal = profesionalesEnSucursal();
		for (ProfesionalDTO p : profesionalesEnSucursal) {
			if (this.peluqueria.tieneHabilidad(p.getId(), servicio.getId()))
				lista.add(p);
		}
		return lista;

	}

	private List<ProfesionalDTO> profesionalesEnSucursal() {
		List<ProfesionalDTO> lista = new ArrayList<ProfesionalDTO>();
		if (profesionales != null) {
			if (profesionales.size() > 0) {
				for (ProfesionalDTO p : profesionales) {
					if (p.getSucursal().getId() == this.usuario.getSucursal().getId()) {
						lista.add(p);
						System.out.println("se agrega un profesional");
					}
				}
			}
		}
		return lista;
	}

	private List<String> refrescarHorariosDePromocion() {

		List<String> horarios = obtenerTodosLosHorarios();
		List<DetalleTurnoDTO> detalles = getDetallesDelProfesional();
		if (detalles != null) {
			for (DetalleTurnoDTO d : detalles) {
				String horarioOcupado = d.getHoraInicio().toString().substring(0, 5);
				if (horarios.contains(horarioOcupado)) {
					horarios.remove(horarioOcupado);
				}
			}
		}

		return horarios;

	}

	private List<String> obtenerTodosLosHorarios() {
		List<String> ret = new ArrayList<String>();

		for (int i = 8; i <= 20; i++) {
			if (i == 8 || i == 9) {
				ret.add("0" + i + ":00");
				ret.add("0" + i + ":30");
			} else {
				ret.add(i + ":00");
				ret.add(i + ":30");
			}
		}
		return ret;
	}

	private List<DetalleTurnoDTO> getDetallesDelProfesional() {
		List<DetalleTurnoDTO> detalles = new ArrayList<DetalleTurnoDTO>();

		int idProfesionalSeleccionado = this.addTurnoVista.getBoxProfesional().getSelectedIndex() + 1;
		if (idProfesionalSeleccionado > 0) {
			if (this.addTurnoVista.getBoxProfesional().getItemAt(idProfesionalSeleccionado - 1) != null) {
				if (!this.addTurnoVista.getBoxProfesional().getItemAt(idProfesionalSeleccionado - 1)
						.equals("Servicio sin profesionales")) {
					for (TurnoDTO t : this.turnos) {
						if (t.getFecha().toString().equals(this.fechaElegida)) {
							for (DetalleTurnoDTO d : t.getDetalles()) {
								if (d.getProfesional().getId() == idProfesionalSeleccionado) {
									detalles.add(d);
								}
							}
						}
					}
				} else {
					detalles = null;
				}
			}

		}

		return detalles;
	}

	private int cantidadDeHorasSeguidas(String horario, List<String> horas) {
		int ret = 1;

		// extracion de hora y minutos
		String cadena_hora = horario.substring(0, 2);
		int hora = Integer.parseInt(cadena_hora);
		String cadena_minutos = horario.substring(3, 5);
		int minutos = Integer.parseInt(cadena_minutos);

		// Se construye la nueva hora
		String horaSiguiente = "";
		int horaNueva = hora;
		int minutosNuevos = minutos + 30;

		if (minutosNuevos == 60) {
			horaNueva++;
			if (hora < 10) {
				horaSiguiente = "0" + Integer.toString(horaNueva) + ":00";
			} else {
				horaSiguiente = Integer.toString(horaNueva) + ":00";
			}
		} else {
			if (hora < 10) {
				horaSiguiente = "0" + cadena_hora + ":30";
			} else {
				horaSiguiente = cadena_hora + ":30";
			}
		}

		for (String h : horas) {
			if (h.equals(horaSiguiente)) {
				ret += cantidadDeHorasSeguidas(horaSiguiente, horas);
				System.out.println("hora pasada: " + horario + " - hora siguiente: " + horaSiguiente);
			}
		}
		return ret;
	}

	private void escucharBotonesPromocionVista() {
		escucharBotonCancelar();
		escucharBotonReservar();

	}

	private void escucharBotonCancelar() {
		for(ActionListener al : this.promocionVista.getBtnCancelar().getActionListeners()) {
			this.promocionVista.getBtnCancelar().removeActionListener(al);
		}
		this.promocionVista.getBtnCancelar().addActionListener(x -> cancelar());
	}

	private void cancelar() {
		int opcion = JOptionPane.showConfirmDialog(null, "¿Estás seguro que quieres cancelar?");
		if (opcion == 0) {
			this.promocionVista.ocultar();
			this.addTurnoController.mostrarAddTurnoVista();
		}
	}
	
	
	private void escucharBotonReservar() {
		for(ActionListener al : this.promocionVista.getBtnReservar().getActionListeners()) {
			this.promocionVista.getBtnReservar().removeActionListener(al);
		}
		this.promocionVista.getBtnReservar().addActionListener(x -> reservar());
	}
	
	private void reservar() {
		int opcion = JOptionPane.showConfirmDialog(null, "Se procederá a reservar el turno con promoción");
		if (opcion == 0) {
			this.promocionVista.ocultar();
			this.addTurnoController.mostrarAddTurnoVista();
		}
	}
	

//	private boolean cargarInformacionDePromocion() {
//		boolean ret = false;
//		if (obtenerServicios(this.promocion)) {
//			ret = true;
//			this.promocionVista.getTxtNombrePromocion().setText(this.promocion.getNombre());
//			this.promocionVista.getTxtPrecioPromocion().setText("$" + this.promocion.getPrecio().toString());
//		}
//
//		return ret;
//	}
//
//	private boolean obtenerServicios(PromocionDTO promocion) {
//		boolean ret = false;
//		List<ServicioDTO> servicios = promocion.getServicios();
//
//		if (servicios.size() > 0) {
//			for (int i = 0; i < servicios.size(); i++) {
//				switch (i) {
//				case 0:
//					this.promocionVista.getTxtServicio1().setText(servicios.get(i).getNombre());
//					this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(i)), i);
//					System.out.println("entra en el case: " + i);
//					break;
//				case 1:
//					this.promocionVista.getTxtServicio2().setText(servicios.get(i).getNombre());
//					this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(i)), i);
//					System.out.println("entra en el case: " + i);
//					break;
//				case 2:
//					this.promocionVista.getTxtServicio3().setText(servicios.get(i).getNombre());
//					this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(i)), i);
//					System.out.println("entra en el case: " + i);
//					break;
//				case 3:
//					this.promocionVista.getTxtServicio4().setText(servicios.get(i).getNombre());
//					this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(i)), i);
//					System.out.println("entra en el case: " + i);
//					break;
//				case 4:
//					this.promocionVista.getTxtServicio5().setText(servicios.get(i).getNombre());
//					this.promocionVista.llenarComboProfesional(cargarBoxProfesional(servicios.get(i)), i);
//					System.out.println("entra en el case: " + i);
//					break;
//				}
//			}
//
//			System.out.println("");
//			System.out.println("======================================================================");
//			System.out.println("======================================================================");
//			System.out.println("");
//
//			ret = true;
//		}
//		return ret;
//	}
}
