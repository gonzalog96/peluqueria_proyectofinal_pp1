package presentacion.controlador.administrativo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import dto.ClienteDTO;
import dto.DetalleTurnoDTO;
import dto.EstadoTurno;
import dto.ProfesionalDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import dto.TurnoDTO;
import dto.UsuarioDTO;
import email.Sender;
import modelo.Peluqueria;
import persistencia.propiedades.Propiedad;
import presentacion.vista.administrativo.AddTurnoVista;

public class AddTurnoController {
	private static AddTurnoController INSTANCE;
	private AddTurnoVista addTurnoVista;
	private Peluqueria peluqueria;
	private List<ClienteDTO> clientes;
	private List<PromocionDTO> promociones;
	private List<ServicioDTO> servicios;
	private List<ProfesionalDTO> profesionales;
	private List<ProfesionalDTO> profesionalesEnSucursal;
	private List<ProfesionalDTO> profesionalesParaServicio;
	private UsuarioDTO usuario;
	private int controlFijarDatos;
	private String fechaElegida;
	private java.sql.Time horaInicio;
	private java.sql.Time horaInicioDetalle;
	private java.sql.Time horaFin;
	private ClienteDTO clienteElegido;
	private ServicioDTO servicioElegido;
	private ProfesionalDTO profesional;
	private float precioTotal;
	private LocalDate fechaLocalDate;
	private LocalTime horaInicioLocalTime;
	private List<DetalleTurnoDTO> listaDetalles;
	private int duracionDelTurno;
	private List<String> horariosDelProfesional;
	private AdministrativoController administrativoController;
	private PromocionDTO promocionElegida;
	private DetalleTurnoDTO detalle;
	private String horaSolicitada;

	public static AddTurnoController getInstance(Peluqueria peluqueria, UsuarioDTO usuario,
			AdministrativoController administrativoController) {
		if (INSTANCE == null) {
			INSTANCE = new AddTurnoController(peluqueria, usuario, administrativoController);
			return new AddTurnoController(peluqueria, usuario, administrativoController);
		} else {
			return INSTANCE;
		}
	}

	private AddTurnoController(Peluqueria peluqueria, UsuarioDTO usuario,
			AdministrativoController administrativoController) {
		this.peluqueria = peluqueria;
		this.usuario = usuario;
		this.administrativoController = administrativoController;

	}

	public void mostrarAddTurnoVista() {
		this.addTurnoVista = AddTurnoVista.getInstance();
		this.listaDetalles = new ArrayList<DetalleTurnoDTO>();
		setFormularioAddTurno();

		this.addTurnoVista.mostrar();
	}

	private void setFormularioAddTurno() {
		obtenerListasCompletas();
		llenarCombos();
		
		desactivarBoxServicios();
		desactivarBtnSeleccionarServicios();
		desactivarBoxProfesional();
		desactivarBtnVerHorarios();
		desactivarBoxHorarios();
		desactivarBtnAgregar();
		setearFechaPorDefecto();
		escucharFormulario();
	}

	private void obtenerListasCompletas() {
		clientes = this.peluqueria.obtenerClientes();
		promociones = this.peluqueria.getPromociones();
		servicios = this.peluqueria.obtenerServicios();
		profesionales = this.peluqueria.obtenerProfesionales();
		filtrarListasPorSucursal();
	}

	private void filtrarListasPorSucursal() {
		this.profesionalesEnSucursal = profesionalesEnSucursal();
	}

	private List<ProfesionalDTO> profesionalesEnSucursal() {
		profesionales = this.peluqueria.obtenerProfesionales();
		List<ProfesionalDTO> lista = new ArrayList<ProfesionalDTO>();
		if (profesionales != null) {
			if (profesionales.size() != 0) {
				for (ProfesionalDTO p : profesionales) {
					if (p.getSucursal().getId() == this.usuario.getSucursal().getId()) {
						lista.add(p);
					}
				}
			}
		}
		return lista;
	}

	private void llenarCombos() {
		clientes = this.peluqueria.obtenerClientes();
		promociones = this.peluqueria.getPromociones();
		servicios = this.peluqueria.obtenerServicios();
		this.addTurnoVista.llenarComboCliente(clientes); // listo
		this.addTurnoVista.llenarComboPromocion(promociones); // listo
		//this.addTurnoVista.llenarComboServicio(servicios);// listo
		//this.addTurnoVista.getBoxHorario().setEnabled(false);
		//refrescarProfesionales();
		//promocionElegida = null;

		escucharCambioEnBoxPromocion();
		escucharBtnSeleccionarPromocion();// Si se elige una promoción, se cargan los servicios de la misma
		escucharCambioEnBoxServicio();// Llena el combo de profesionales según el servicio elegido
		escucharBtnSeleccionarServicio();
		escucharCambioEnBoxProfesionales();
		escucharaBtnBuscarHorarios();// llena el combo de horarios según la fecha y el profesional elegido
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	private void escucharBtnSeleccionarPromocion() {
		for(ActionListener al : this.addTurnoVista.getBtnSeleccionarPromocion().getActionListeners()) {
			this.addTurnoVista.getBtnSeleccionarPromocion().removeActionListener(al);
		}
		this.addTurnoVista.getBtnSeleccionarPromocion().addActionListener(sp -> seleccionarPromocion());	
	}
	
	private void escucharCambioEnBoxPromocion() {
				
		this.addTurnoVista.getBoxPromocion().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
						desactivarBoxServicios();
						desactivarBtnSeleccionarServicios();
						desactivarBoxProfesional();
						desactivarBtnVerHorarios();
						desactivarBoxHorarios();
						desactivarBtnAgregar();
				}
			}
		});
	}

	private void seleccionarPromocion() {
		if (conPromocion()) {
			setPromocionElegida();
			cargarServiciosDePromocion();
			activarBoxServicios();
			activarBtnSeleccionarServicios();
			desactivarBoxProfesional();
			desactivarBoxHorarios();
			desactivarBtnAgregar();
		} else {
			setPromocionElegida();
			cargarServiciosCompletos();
			activarBoxServicios();
			activarBtnSeleccionarServicios();
			desactivarBoxProfesional();
			desactivarBoxHorarios();
			desactivarBtnAgregar();
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	protected void desactivarBtnFijarFecha() {
		this.addTurnoVista.getBtnFijarFecha().setEnabled(false);
	}
	
	protected void activarBtnFijarFecha() {
		this.addTurnoVista.getBtnFijarFecha().setEnabled(true);
	}

	protected void desactivarBoxServicios() {
		this.addTurnoVista.getBoxServicio().setEnabled(false);
	}
	
	protected void activarBoxServicios() {
		this.addTurnoVista.getBoxServicio().setEnabled(true);
	}
	
	protected void desactivarBtnSeleccionarServicios() {
		this.addTurnoVista.getBtnSeleccionarServicio().setEnabled(false);
	}
	
	protected void activarBtnSeleccionarServicios() {
		this.addTurnoVista.getBtnSeleccionarServicio().setEnabled(true);
	}
	
	protected void desactivarBoxProfesional() {
		this.addTurnoVista.getBoxProfesional().setEnabled(false);
	}
	
	protected void activarBoxProfesional() {
		this.addTurnoVista.getBoxProfesional().setEnabled(true);
	}
	
	protected void desactivarBoxHorarios() {
		this.addTurnoVista.getBoxHorario().setEnabled(false);
	}
	protected void activarBoxHorarios() {
		this.addTurnoVista.getBoxHorario().setEnabled(true);
	}
	
	protected void desactivarBtnAgregar() {
		this.addTurnoVista.getBtnAgregarServicio().setEnabled(false);
	}
	
	private void desactivarBtnVerHorarios() {
		this.addTurnoVista.getBtnBuscarHorarios().setEnabled(false);
	}
	
	private void activarBtnVerHorarios() {
		this.addTurnoVista.getBtnBuscarHorarios().setEnabled(true);
	}

	
	protected void activarBtnAgregar() {
		this.addTurnoVista.getBtnAgregarServicio().setEnabled(true);
	}

	protected void cargarServiciosCompletos() {
		servicios = this.peluqueria.obtenerServicios();
		this.addTurnoVista.llenarComboServicio(servicios);
	}
	
	protected void setPromocionElegida() {
		this.promociones = this.peluqueria.getPromociones();
		int idPromocion = this.promociones.get( this.addTurnoVista.getBoxPromocion().getSelectedIndex() ).getId();
		for(PromocionDTO p :  this.peluqueria.getPromociones()) {
			if(p.getId() == idPromocion) this.promocionElegida = p;
		}
	}

	protected void cargarServiciosDePromocion() {
		int id_promocion = this.addTurnoVista.getBoxPromocion().getSelectedIndex();
		if (id_promocion >= 0) {
			PromocionDTO promocion = this.peluqueria.getPromociones().get(id_promocion);
			servicios = promocion.getServicios();
			this.addTurnoVista.llenarComboServicio(servicios);
		}
	}

	private boolean conPromocion() {
		int sinPromocion = this.promociones.get(0).getId();
		int seleccionado = this.addTurnoVista.getBoxPromocion().getSelectedIndex() + 1;
		return seleccionado != sinPromocion;// Con promocion
	}

	private void escucharCambioEnBoxServicio() {
		
		this.addTurnoVista.getBoxServicio().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					desactivarBoxProfesional();
					desactivarBoxHorarios();
					desactivarBtnAgregar();
				}
			}
		});
	}
	
	private void escucharBtnSeleccionarServicio() {
		for(ActionListener al : this.addTurnoVista.getBtnSeleccionarServicio().getActionListeners()) {
			this.addTurnoVista.getBtnSeleccionarServicio().removeActionListener(al);
		}
		this.addTurnoVista.getBtnSeleccionarServicio().addActionListener(bh -> seleccionarServicio());
	}

	private void seleccionarServicio() {
		refrescarProfesionales();
		activarBoxProfesional();
		if(listaDetalles.size() == 0) {
			activarBtnVerHorarios();
		}else {
			activarBtnAgregar();
		}
		
		setServicioElegido();
	}

	private void setServicioElegido() {
		int ordenDeServicioEnLista = this.addTurnoVista.getBoxServicio().getSelectedIndex();
		int idServicio = this.servicios.get(ordenDeServicioEnLista).getId();
		this.servicioElegido = this.peluqueria.getServicioDesdeID(idServicio);
		
		boolean ServicioYaElegido = false;
		if(listaDetalles.size() > 0) {
			for(DetalleTurnoDTO d : listaDetalles ) {
				if(d.getServicio().equals(servicioElegido)) {
					ServicioYaElegido = true;
				}
			}
		}
		
		if(ServicioYaElegido) JOptionPane.showMessageDialog(this.addTurnoVista, "El servicio ya fue elegido!");
		else ServicioYaElegido = false;
	}

	private void escucharCambioEnBoxProfesionales() {
		this.addTurnoVista.getBoxProfesional().addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					desactivarBoxHorarios();
					if(listaDetalles.size() > 0) activarBtnAgregar();
					else desactivarBtnAgregar();
				}
			}
		});
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void refrescarProfesionales() {
		// cada vez que llama se cargan las listas de nuevo
		this.profesionales = peluqueria.obtenerProfesionales();
		this.profesionalesEnSucursal = profesionalesEnSucursal();

		this.profesionalesParaServicio = new ArrayList<ProfesionalDTO>();
		this.profesionalesParaServicio = getProfesionalesParaElServicio(this.profesionalesEnSucursal);

		this.addTurnoVista.llenarComboProfesional(this.profesionalesParaServicio);
	}

	private List<ProfesionalDTO> getProfesionalesParaElServicio(List<ProfesionalDTO> profesionalesEnSucursal) {

		List<ProfesionalDTO> lista = new ArrayList<ProfesionalDTO>();
		int idServicio = this.addTurnoVista.getBoxServicio().getSelectedIndex() + 1;

		for (ProfesionalDTO p : profesionalesEnSucursal) {
			if (this.peluqueria.tieneHabilidad(p.getId(), idServicio)) {
				lista.add(p);
			}
		}
		return lista;
	}

	private void escucharaBtnBuscarHorarios() {
		this.addTurnoVista.getBtnBuscarHorarios().addActionListener(bh -> refrescarHorarios());
	}

	private void refrescarHorarios() {
		this.addTurnoVista.getBoxHorario().setEnabled(true);
		activarBtnAgregar();
		horariosDelProfesional = obtenerTodosLosHorarios();
		List<DetalleTurnoDTO> detalles = getDetallesDelProfesional();
		if (detalles != null) {
			for (DetalleTurnoDTO d : detalles) {
				String horarioInicioOcupado = d.getHoraInicio().toString().substring(0, 5);
				if (horariosDelProfesional.contains(horarioInicioOcupado)) {
					horariosDelProfesional.remove(horarioInicioOcupado);
				}
			}
		} else {
			horariosDelProfesional = null;
		}

		this.addTurnoVista.llenarComboHorario(horariosDelProfesional);

	}

	//////////////////////////////////////////////////////////////////////////////////////////////

	private List<String> obtenerTodosLosHorarios() {
		List<String> ret = new ArrayList<String>();
		String horaAux_cadena = "";
		String horaInicial_cadena = "";
		String horaLimite_cadena = Propiedad.load("hora_limite", "propiedades/config.properties");
		LocalTime horaLimite = LocalTime.parse(horaLimite_cadena);

		if (this.addTurnoVista.getTxtHoraInicio().getText() != null) {
			horaInicial_cadena = this.addTurnoVista.getTxtHoraInicio().getText();
		}

		if (horaInicial_cadena.equals("")) {
			horaInicial_cadena = Propiedad.load("hora_inicio", "propiedades/config.properties");
		}

		LocalTime horaInicial = LocalTime.parse(horaInicial_cadena);

		while (horaInicial.isBefore(horaLimite)) {
			horaAux_cadena = horaInicial.toString();
			ret.add(horaAux_cadena);
			horaInicial = horaInicial.plusMinutes(30);
		}

		return ret;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////

	private List<DetalleTurnoDTO> getDetallesDelProfesional() {
		List<DetalleTurnoDTO> detalles = new ArrayList<DetalleTurnoDTO>();
		List<TurnoDTO> turnosDeLaSucursal = this.peluqueria.obtenerTurnos(this.usuario.getSucursal().getId());

		int ordenDelProfesionalEnCombo = this.addTurnoVista.getBoxProfesional().getSelectedIndex();

		if (ordenDelProfesionalEnCombo >= 0) {

			int idProfesional = this.profesionalesParaServicio.get(ordenDelProfesionalEnCombo).getId();

			if (idProfesional > 0) {
				if (!this.addTurnoVista.getBoxProfesional().getItemAt(ordenDelProfesionalEnCombo)
						.equals("Servicio sin profesionales")) {
					for (TurnoDTO t : turnosDeLaSucursal) {
						if (t.getFecha().toString().equals(fechaElegida())) {
							for (DetalleTurnoDTO d : t.getDetalles()) {
								if (d.getProfesional().getId() == idProfesional) {
									detalles.add(d);
								}
							}
						}
					}
				} else {
					detalles = null;
				}
			}
		}

		return detalles;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private String fechaElegida() {
		String fecha = getFechaEnDateChooser();
		if (fecha.equals("")) {
			fecha = getFechaActual("yyyy-MM-dd");
		} else {
			fecha = getFechaEnDateChooser();
		}
		return fecha;
	}

	private String getFechaActual(String formato) {
		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		String fecha = localDate.format(formatter);
		return fecha;
	}

	private String getFechaEnDateChooser() {
		Date date = this.addTurnoVista.getDateChooser().getDate();
		String fecha = "";
		if (date != null) {
			SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
			fecha = formateador.format(date);
		}
		return fecha;
	}

	private void setearFechaPorDefecto() {
		this.addTurnoVista.getTxtFechaElegida().setText(getFechaActual("dd/MM/yyyy"));
		java.util.Date fechaParseada = null;
		try {
			fechaParseada = new SimpleDateFormat("dd/MM/yyyy").parse(getFechaActual("dd/MM/yyyy"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.addTurnoVista.getDateChooser().setDate(fechaParseada);

	}

	private void escucharFormulario() {
		escucharBotonFijarFecha();
		escucharBotonAgregar();
		escucharBotonReiniciar();
	}

	private void escucharBotonFijarFecha() {
		this.addTurnoVista.getBtnFijarFecha().addActionListener(f -> fijarFecha(f));
	}

	private void fijarFecha(ActionEvent f) {
		desactivarBoxHorarios();
		Date date = this.addTurnoVista.getDateChooser().getDate();
		String fecha = "";
		if (date != null) {
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
			fecha = formateador.format(date);
		} else {
			JOptionPane.showMessageDialog(this.addTurnoVista, "Debe fijar una fecha para el turno");
		}
		this.addTurnoVista.getTxtFechaElegida().setText(fecha);
	}

	private void escucharBotonAgregar() {
		for (ActionListener al : this.addTurnoVista.getBtnAgregarServicio().getActionListeners()) {
			this.addTurnoVista.getBtnAgregarServicio().removeActionListener(al);
		}
		this.addTurnoVista.getBtnAgregarServicio().addActionListener(a -> analizarFormulario());
	}

	private void analizarFormulario() {
		
		boolean ServicioYaElegido = servicioRepetido();
		
		if(ServicioYaElegido) JOptionPane.showMessageDialog(this.addTurnoVista, "El servicio ya fue elegido!");
		else {
			if(this.addTurnoVista.getBoxProfesional().getSelectedItem().toString().equals("Servicio sin profesionales")) {
				JOptionPane.showMessageDialog(this.addTurnoVista, "No hay profesionales disponibles para ese servicio!");
			}else {
				ServicioYaElegido = false;
				horaSolicitada = ""; //Es la hora que se solicita, se usa para comprobar la disponibilidad del profesional
				
				setServicioElegido();
				desactivarBoxHorarios();
				desactivarBtnVerHorarios();
				desactivarBtnAgregar();
				desactivarBtnFijarFecha();
				
				detalle = null;

				// Se recuperan los datos del profesional y servicio elegido
				recuperarDatos();

				fijarPrecioDeLaPromocion(promocionElegida);

				// Se fijan los datos base de un turno: fecha, cliente, hora inicio
				fijarDatosBaseDelTurno();
				
				// se guarda la hora inicio del detalle/servicio para el turno de promoción y otros
				if (listaDetalles.size() == 0) {
					horaSolicitada = this.addTurnoVista.getBoxHorario().getSelectedItem().toString();
					
				} else {
					horaSolicitada = this.addTurnoVista.getTxtHoraFin().getText();
				}
				
				// Se actualiza la duración del turno
				actualizarDuracionDelTurno();
				
				// Si la lista de detalles ya tiene al menos un detalle, vemos si el profesional
						// está libre en la hora que continua
				if(profesionalDisponible(horaSolicitada)) {
					
					// Se verifica que la hora de fin no exceda la hora límite laboral seteada en
					// properties
					String horaLimite_cadena = Propiedad.load("hora_limite", "propiedades/config.properties");
					LocalTime horaLimite = LocalTime.parse(horaLimite_cadena);

					LocalTime horaFinDelServicio = this.horaFin.toLocalTime();
					

					// Si el fin del servicio no supera la hora limite se inserta el nuevo detalle
					if (horaFinDelServicio.isBefore(horaLimite) || horaFinDelServicio.equals(horaLimite)) {
							
						// Se crea el detalle con los datos recuperados
						this.horaInicioDetalle = java.sql.Time.valueOf(LocalTime.parse(horaSolicitada));
						detalle = new DetalleTurnoDTO(this.profesional.getId(), this.horaInicioDetalle, this.horaFin,
								this.profesional, this.servicioElegido);

						// Se agregar el detalle creado a la lista de detalles
						this.listaDetalles.add(detalle);
						
						// Se actualiza el precio total del turno normal en el caso de que no sea con promocion
						if(!conPromocion()) calcularTotal();
						
						// Si ya se agregó un detalle ya no se permite selección libre de horas
						desactivarBoxHorarios();
						desactivarBtnVerHorarios();

						// Se refresca la tabla de servicios agregados
						this.addTurnoVista.AgregarServicioALaTabla(servicioElegido, profesional);

						this.addTurnoVista.getTxtHoraFin().setText(this.horaFin.toString().substring(0, 5));
					} else {
						JOptionPane.showMessageDialog(addTurnoVista,
								"Se excedió la hora límite laboral!\nNo se puede agregar este servicio");
					}
				}else {
					JOptionPane.showMessageDialog(addTurnoVista,
					"El profesional no se encuentra disponible en el horario "
					+this.addTurnoVista.getTxtHoraFin().getText()+" para hacer el servicio");
				}
			}
		}
			

		escucharBotonConfirmar();
	}

	private boolean servicioRepetido() {
		boolean ServicioYaElegido = false;
		if(listaDetalles.size() > 0) {
			for(DetalleTurnoDTO d : listaDetalles ) {
				if(d.getServicio().equals(servicioElegido)) {
					ServicioYaElegido = true;
				}
			}
		}
		return ServicioYaElegido;
	}

	private boolean profesionalDisponible(String horaInicio) {
		for(String h : horariosDelProfesional) {
			if (h.equals(horaInicio)) return true;
		}
		return false;
	}

	private void fijarPrecioDeLaPromocion(PromocionDTO promocion) {
		String precio = promocion.getPrecio().toString();
		this.addTurnoVista.getTxtTotal().setText(precio);
	}

	
	private void calcularTotal() {
		this.precioTotal += this.servicioElegido.getPrecio();
		this.addTurnoVista.getTxtTotal().setText(Float.toString(this.precioTotal));
	}

	private void fijarDatosBaseDelTurno() {
		// control de turno: Una vez que se agrega un servicio, se inhabilitan fecha,
		// cliente, horario.
		// Porque el turno pertenece al mismo cliente, en la misma fecha, en el mismo
		// horario de inicio
		if (controlFijarDatos == 0) {
			controlFijarDatos++;

			fijarFechaDelTurno();
			fijarClienteDelTurno();

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			this.fechaLocalDate = LocalDate.parse(fechaElegida, formatter);

			this.addTurnoVista.getBoxCliente().setEnabled(false);
			this.addTurnoVista.getDateChooser().setEnabled(false);
			this.addTurnoVista.getBoxPromocion().setEnabled(false);
			this.addTurnoVista.getBoxHorario().setEnabled(false);
		}
	}

	private void recuperarDatos() {
		// guardan en variables de instancia lo que recuperan
		recuperarServicio();
		recuperarProfesional();
	}

	private void actualizarDuracionDelTurno() {
		if(profesionalDisponible(horaSolicitada)) {
			// Se guarda la hora de inicio
			this.horaInicioLocalTime = recuperarHora();
			this.horaInicio = java.sql.Time.valueOf(this.horaInicioLocalTime);
			this.addTurnoVista.getTxtHoraInicio().setText(horaInicioLocalTime.toString());

			// Se obtiene la duracion del servicio agregado y se lo suma a la duracion del
			// turno
			
			System.out.println("\nduracion del turno antes: "+duracionDelTurno);
			this.duracionDelTurno += this.servicioElegido.getDuracion();
			System.out.println("duracion del turno después: "+duracionDelTurno+"\n");
			// Se actualiza la proxima hora inicio del detalle
			this.horaInicioDetalle = this.horaFin;

			// Se actualiza la hora del fin del turno
			this.horaInicioLocalTime = this.horaInicioLocalTime.plusMinutes(this.duracionDelTurno);
			this.horaFin = java.sql.Time.valueOf(this.horaInicioLocalTime);
		}
	}

	private void recuperarProfesional() {
		int idProfesionalElegido = this.addTurnoVista.getBoxProfesional().getSelectedIndex();
		this.profesional = null;
		List<Integer> ids = new ArrayList<Integer>();// guarda todos los ids

		for (ProfesionalDTO p : profesionalesParaServicio) {
			ids.add(p.getId());
		}

		this.profesional = this.peluqueria.getProfesionalDesdeID(ids.get(idProfesionalElegido));
	}

	private LocalTime recuperarHora() {
		LocalTime hora = LocalTime.parse(this.addTurnoVista.getBoxHorario().getSelectedItem().toString());
		return hora;
	}

	private void recuperarServicio() {
		int ordenServicioSeleccionado = this.addTurnoVista.getBoxServicio().getSelectedIndex();
		int id = servicios.get(ordenServicioSeleccionado).getId();
		this.servicioElegido = this.peluqueria.getServicioDesdeID(id);
	}

	private void fijarClienteDelTurno() {
		this.clienteElegido = this.peluqueria
				.getClienteDesdeID(this.addTurnoVista.getBoxCliente().getSelectedIndex() + 1);
	}

	private void fijarFechaDelTurno() {
		this.fechaElegida = getFechaEnDateChooser();
		if (fechaElegida.equals(""))
			fechaElegida = getFechaActual("yyyy-MM-dd");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		this.fechaLocalDate = LocalDate.parse(fechaElegida, formatter);
	}

	private void escucharBotonReiniciar() {
		for (ActionListener al : this.addTurnoVista.getBtnReiniciar().getActionListeners()) {
			this.addTurnoVista.getBtnReiniciar().removeActionListener(al);
		}
		this.addTurnoVista.getBtnReiniciar().addActionListener(r -> reiniciar());

	}

	protected void reiniciar() {
		int opcion = JOptionPane.showConfirmDialog(addTurnoVista, "Se reinciará el armado del turno, ¿Desea Proceder?");
		if (opcion == JOptionPane.YES_OPTION) {
			obtenerListasCompletas();
			llenarCombos();
			
			this.addTurnoVista.getBoxCliente().setEnabled(true);
			this.addTurnoVista.getDateChooser().setEnabled(true);
			this.addTurnoVista.getBoxPromocion().setEnabled(true);
			setearFechaPorDefecto();
			activarBtnFijarFecha();
			desactivarBoxServicios();
			desactivarBtnSeleccionarServicios();
			desactivarBoxProfesional();
			desactivarBtnVerHorarios();
			desactivarBoxHorarios();
			desactivarBtnAgregar();
			
			this.addTurnoVista.vaciarTabla();
			this.addTurnoVista.getTxtHoraInicio().setText("");
			this.addTurnoVista.getTxtHoraFin().setText("");
			this.addTurnoVista.getTxtTotal().setText("");
			this.listaDetalles = new ArrayList<DetalleTurnoDTO>();
			this.precioTotal = 0;
			controlFijarDatos = 0;
			this.duracionDelTurno = 0;
		}
	}

	private void escucharBotonConfirmar() {
		for (ActionListener al : this.addTurnoVista.getBtnAgregarTurno().getActionListeners()) {
			this.addTurnoVista.getBtnAgregarTurno().removeActionListener(al);
		}
		this.addTurnoVista.getBtnAgregarTurno().addActionListener(v -> guardar());
	}

	List<TurnoDTO> filtrarPorFecha(List<TurnoDTO> lista, String fecha) {
		List<TurnoDTO> ret = new ArrayList<TurnoDTO>();
		if (!fecha.equals("")) {
			for (TurnoDTO t : lista) {
				if (t.getFecha().toString().equals(fecha)) {
					ret.add(t);
				}
			}
		} else {
			ret = lista;
		}

		return ret;
	}

	private void guardar() {
		if(conPromocion()) {
			this.precioTotal = Integer.parseInt(this.addTurnoVista.getTxtTotal().getText());
		}else {
			promocionElegida = this.peluqueria.getPromociones().get(0);//Se elige sin promoción
		}
		int opcion = JOptionPane.showConfirmDialog(this.addTurnoVista, "¿Desea confirmar el turno?");
		if (opcion == JOptionPane.YES_OPTION) {
			
			TurnoDTO turno = new TurnoDTO(0, this.fechaLocalDate, this.horaInicio, this.precioTotal, 0,
					calcularPuntos(this.precioTotal), this.clienteElegido, this.listaDetalles,
					this.usuario.getSucursal(), EstadoTurno.OCUPADO, promocionElegida);

			this.peluqueria.agregarTurno(turno);
			

			this.administrativoController.setFechaFiltro(this.fechaLocalDate);
			this.administrativoController.refrescarTabla(this.administrativoController.filtrar(this.peluqueria.obtenerTurnos(this.usuario.getSucursal().getId())));

			enviarEmailAlCliente(this.clienteElegido);
			
			// Se redirecciona a la vista principal en el día del turno agregado
			List<TurnoDTO> turnosAMostrar = this.peluqueria.obtenerTurnosDia(this.usuario.getSucursal().getId(), this.fechaLocalDate);
			
			this.administrativoController.refrescarTabla(turnosAMostrar);
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			String fecha = this.fechaLocalDate.format(formatter);
			this.administrativoController.setearFechaEnDateChooser(fecha);
			
			this.addTurnoVista.dispose();
		}
	}

	private void enviarEmailAlCliente(ClienteDTO cliente) {
		Sender sender = new Sender();
		sender.enviarEmail("lumusika@gmail.com", 
							"Registro de turno en Peluquería Hair and Head", 
							"Hair and Head\n\nHola estimado "+cliente.getNombre()+"! Usted ha registrado un turno en la peluquería "
							+this.usuario.getSucursal().getNombre()+" con dirección en "
							+this.usuario.getSucursal().getCalle()+" "+this.usuario.getSucursal().getAltura()
							+", para la fecha "+this.fechaLocalDate.toString()+" en el horario de "+this.horaInicio+"hs."
							+"\nEl valor del turno es $"+this.precioTotal
							+"\nEs posible pagar utilizando sus puntos, Usted cuenta con "+this.clienteElegido.getPuntos()+" puntos.\n"
							+"Les deseamos una excelente experiencia con nuestros servicios. Muchas gracias!"
							+"\n\nPeluquerías Hair and Head"
						);
	}

	private int calcularPuntos(float precioDelTurno) {
		int precio = (int)precioDelTurno;
		int valorDelPunto = Integer.parseInt(Propiedad.load("dinero_por_cantidad", "propiedades/puntos.properties"));
		return (precio == 0) ? 0 : precio / valorDelPunto;
	}
}