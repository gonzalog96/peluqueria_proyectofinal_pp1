package presentacion.controlador.administrador.producto;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import dto.EstadoProducto;
import dto.ProductoDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.producto.AddProductoVista;

public class AddProductoController {
	private static AddProductoController INSTANCE;
	private Peluqueria peluqueria;
	private AddProductoVista addProductoVista;
	private ProductoController productoController;
	private Validador validador;

	public static AddProductoController getInstance(Peluqueria peluqueria, ProductoController producto) {
		if (INSTANCE == null)
			INSTANCE = new AddProductoController(peluqueria, producto);
		return INSTANCE;
	}

	private AddProductoController(Peluqueria pel, ProductoController producto) {
		addProductoVista = AddProductoVista.getInstance();
		productoController = producto;
		peluqueria = pel;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		addProductoVista.getBtnConfirmar().addActionListener(z -> crearProducto(z));
	}

	public void mostrarAddServicioVista() {
		addProductoVista.mostrar();
	}

	public void crearProducto(ActionEvent e) {
		String nombre = addProductoVista.getTxtNombre().getText();
		String rubro = addProductoVista.getTxtRubro().getText();
		String precio = addProductoVista.getTxtPrecio().getText();
		EstadoProducto estado = (EstadoProducto) addProductoVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Crear producto", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, rubro)) {
			ProductoDTO nuevo = new ProductoDTO(0, nombre, rubro, Float.parseFloat(precio), estado);
			peluqueria.insertProducto(nuevo);
			productoController.actualizarTablaProductos();
			addProductoVista.ocultar();
		}
		limpiarCampos();
	}

	public boolean validarCampos(String nombre, String precio, String rubro) {
		boolean validado = true;
		String mensaje = "";
		
		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esStringValido(rubro)) {
			validado = validado && false;
			mensaje += "Rubro invalido\n";
		}
		
		if(!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
	
	private void limpiarCampos() {
		addProductoVista.getTxtRubro().setText(null);
		addProductoVista.getTxtNombre().setText(null);
		addProductoVista.getTxtPrecio().setText(null);
	}
	
	///////////////////////////////////////////////////////

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public AddProductoVista getAddProductoVista() {
		return addProductoVista;
	}

	public void setAddProductoVista(AddProductoVista addProductoVista) {
		this.addProductoVista = addProductoVista;
	}

	public ProductoController getProductoController() {
		return productoController;
	}

	public void setProductoController(ProductoController productoController) {
		this.productoController = productoController;
	}

	public Validador getValidador() {
		return validador;
	}

	public void setValidador(Validador validador) {
		this.validador = validador;
	}
	
	

}

