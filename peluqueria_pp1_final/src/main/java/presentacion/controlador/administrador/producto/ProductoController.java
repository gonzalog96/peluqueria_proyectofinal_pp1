package presentacion.controlador.administrador.producto;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoProducto;
import dto.ProductoDTO;
import modelo.Peluqueria;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.controlador.administrador.servicios.ServiciosController;
import presentacion.vista.administrador.producto.ProductosVista;

public class ProductoController {
	private Peluqueria peluqueria;
	private static ProductoController INSTANCE;
	private ProductosVista productosVista;
	private AdministradorController adminController;
	private ServiciosController servicioController;
	private AddProductoController addProductoCotroller;
	private EditProductoController editProductoController;
	private List<ProductoDTO> productos;

	public static ProductoController getInstance(Peluqueria peluqueria, AdministradorController admin) {
		if (INSTANCE == null)
			INSTANCE = new ProductoController(peluqueria, admin);
		return INSTANCE;
	}

	private ProductoController(Peluqueria pel, AdministradorController admin) {
		peluqueria = pel;
		adminController = admin;
		productosVista = ProductosVista.getInstance();
		servicioController = ServiciosController.getInstance(pel, admin);
		addProductoCotroller = AddProductoController.getInstance(pel, this);
		editProductoController = EditProductoController.getInstance(pel, this);
		actualizarTablaProductos();
		acciones();
	}

	public void acciones() {
		actualizarTablaProductos();
		productosVista.getBtnProfesionales().addActionListener(q -> activateProfesionalVista(q));
		productosVista.getBtnServicios().addActionListener(l -> activateServiciosVista(l));
		productosVista.getBtnAddProducto().addActionListener(s -> activateAddProductoVista(s));
		productosVista.getBtnEditProducto().addActionListener(w -> activateEditProductoVista(w));
		productosVista.getBtnRemoveProducto().addActionListener(x -> deleteProducto(x, seleccionar()));
	}

	private void activateProfesionalVista(ActionEvent q) {
		productosVista.ocultar();
		adminController.mostrarAdministradorVista();
	}
	
	private void activateServiciosVista(ActionEvent w) {
		productosVista.ocultar();
		servicioController.mostrarServiciosVista();
	}
	
	private void activateAddProductoVista(ActionEvent s) {
		addProductoCotroller.mostrarAddServicioVista();
	}
	
	public int seleccionar() {
		return  productosVista.getTablaProductos().getSelectedRow();
	}
	
	public void mostrarProductosVista() {
		this.productosVista.mostrar();
	}
	

	public void actualizarTablaProductos() {
		productos = peluqueria.getProductos();
		productosVista.llenarTabla(productos);
	}
	
	private void activateEditProductoVista(ActionEvent s) {
		int fila = seleccionar();
		if(fila >= 0 ) {
			editProductoController.mostrarEditServicioVista();
			editProductoController.setEditar(productos.get(fila));
			completarCamposEditVista();
			
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un servcio para editar.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	private void completarCamposEditVista() {
		String nombre = productos.get(seleccionar()).getNombre();
		String rubro= productos.get(seleccionar()).getRubro();
		float precio = productos.get(seleccionar()).getPrecio();
		EstadoProducto estado = productos.get(seleccionar()).getEstado();
		
		editProductoController.getEditProfVista().getTxtNombre().setText(nombre);
		editProductoController.getEditProfVista().getTxtPrecio().setText(Float.toString(precio));
		editProductoController.getEditProfVista().getTxtRubro().setText(rubro);
		editProductoController.getEditProfVista().getComboEstado().setSelectedItem(estado);
	}
	
	public boolean deleteProducto(ActionEvent s, int filaSeleccionada) {
		try {
			int fila = filaSeleccionada;
			int id = this.productos.get(fila).getId();
			String nombre = this.productos.get(fila).getNombre();
			float precio = this.productos.get(fila).getPrecio();
			String rubro = this.productos.get(fila).getRubro();
			EstadoProducto estado = this.productos.get(fila).getEstado();

			if (estado.equals(EstadoProducto.ACTIVO))
				estado = EstadoProducto.INACTIVO;
			else
				estado = EstadoProducto.ACTIVO;

			ProductoDTO editar = new ProductoDTO(id, nombre, rubro, precio, estado);
			int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere cambiar el estado del producto?",
					"Estado producto", JOptionPane.YES_NO_OPTION);

			if (resp == 0) {
				peluqueria.updateProducto(editar);
				actualizarTablaProductos();
				return true;
			}else {
				return false;
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un producto.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			return false;
		}

	}

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public ProductosVista getProductosVista() {
		return productosVista;
	}

	public void setProductosVista(ProductosVista productosVista) {
		this.productosVista = productosVista;
	}

	public AdministradorController getAdminController() {
		return adminController;
	}

	public void setAdminController(AdministradorController adminController) {
		this.adminController = adminController;
	}

	public ServiciosController getServicioController() {
		return servicioController;
	}

	public void setServicioController(ServiciosController servicioController) {
		this.servicioController = servicioController;
	}

	public AddProductoController getAddProductoCotroller() {
		return addProductoCotroller;
	}

	public void setAddProductoCotroller(AddProductoController addProductoCotroller) {
		this.addProductoCotroller = addProductoCotroller;
	}

	public EditProductoController getEditProductoController() {
		return editProductoController;
	}

	public void setEditProductoController(EditProductoController editProductoController) {
		this.editProductoController = editProductoController;
	}

	public List<ProductoDTO> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoDTO> productos) {
		this.productos = productos;
	}
	
	
	
	

}
