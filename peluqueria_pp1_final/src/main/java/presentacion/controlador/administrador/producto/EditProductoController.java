package presentacion.controlador.administrador.producto;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import dto.EstadoProducto;
import dto.ProductoDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.producto.EditProductoVista;

public class EditProductoController {
	private static EditProductoController INSTANCE;
	private Peluqueria peluqueria;
	private EditProductoVista editProductoVista;
	private ProductoController productoController;
	private ProductoDTO editar;
	private Validador validador;

	public static EditProductoController getInstance(Peluqueria peluqueria, ProductoController servicio) {
		if (INSTANCE == null)
			INSTANCE = new EditProductoController(peluqueria, servicio);
		return INSTANCE;
	}

	private EditProductoController(Peluqueria pel, ProductoController servicio) {
		editProductoVista = EditProductoVista.getInstance();
		productoController = servicio;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		editProductoVista.getBtnConfirmar().addActionListener(z -> editarProducto(z));
	}

	public void mostrarEditServicioVista() {
		editProductoVista.mostrar();
	}

	public boolean editarProducto(ActionEvent e) {
		int id = editar.getId();

		String nombre = editProductoVista.getTxtNombre().getText();
		String rubro = editProductoVista.getTxtRubro().getText();
		String precio = editProductoVista.getTxtPrecio().getText();
		EstadoProducto estado = (EstadoProducto) editProductoVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Editar Producto",
				JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, rubro)) {
			ProductoDTO editar = new ProductoDTO(id, nombre, rubro, Float.parseFloat(precio), estado);
			peluqueria.updateProducto(editar);
			productoController.actualizarTablaProductos();
			editProductoVista.ocultar();
			return true;
		}
		return false;
	}


	public  boolean validarCampos(String nombre, String precio, String rubro) {
		boolean validado = true;
		String mensaje = "";
		
		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esStringValido(rubro)) {
			validado = validado && false;
			mensaje += "Rubro invalido\n";
		}
		
		if(!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	///////////////////////////////////////////////////////

	public EditProductoVista getEditProfVista() {
		return editProductoVista;
	}

	public void setAddEditProfVista(EditProductoVista addEditProfVista) {
		this.editProductoVista = addEditProfVista;
	}

	public ProductoDTO getEditar() {
		return editar;
	}

	public void setEditar(ProductoDTO edit) {
		this.editar = edit;
	}

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public EditProductoVista getEditProductoVista() {
		return editProductoVista;
	}

	public void setEditProductoVista(EditProductoVista editProductoVista) {
		this.editProductoVista = editProductoVista;
	}

	public ProductoController getProductoController() {
		return productoController;
	}

	public void setProductoController(ProductoController productoController) {
		this.productoController = productoController;
	}

	public Validador getValidador() {
		return validador;
	}

	public void setValidador(Validador validador) {
		this.validador = validador;
	}
	
	

}
