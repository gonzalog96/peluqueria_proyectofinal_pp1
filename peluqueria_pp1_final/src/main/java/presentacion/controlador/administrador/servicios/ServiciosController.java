package presentacion.controlador.administrador.servicios;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoServicio;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.vista.administrador.servicios.ServiciosVista;

public class ServiciosController {
	private Peluqueria peluqueria;
	private static ServiciosController INSTANCE;
	private ServiciosVista serviciosVista;
	private AdministradorController adminController;
	private AddServicioController addServicioController;
	private EditServicioController editServicioController;
	private List<ServicioDTO> servicios;

	public static ServiciosController getInstance(Peluqueria peluqueria, AdministradorController admin) {
		if (INSTANCE == null)
			INSTANCE = new ServiciosController(peluqueria, admin);
		return INSTANCE;
	}

	private ServiciosController(Peluqueria pel, AdministradorController admin) {
		peluqueria = pel;
		adminController = admin;
		serviciosVista = ServiciosVista.getInstance();
		addServicioController = AddServicioController.getInstance(pel, this);
		editServicioController = EditServicioController.getInstance(pel, this);
		actualizarTablaServicios();
		acciones();
	}

	public void acciones() {
		actualizarTablaServicios();
		serviciosVista.getBtnAddServicio().addActionListener(l -> activateAddServicioVista(l));
		serviciosVista.getBtnProfesionales().addActionListener(q -> activateProfesionalVista(q));
		serviciosVista.getBtnRemoveServicio().addActionListener(s -> deleteServ(s, seleccionar()));
		serviciosVista.getBtnEditServicio().addActionListener(r -> activateEditServicioVista(r, seleccionar()));
	}

	private void activateProfesionalVista(ActionEvent q) {
		serviciosVista.ocultar();
		adminController.mostrarAdministradorVista();
	}

	public boolean activateEditServicioVista(ActionEvent s, int filaSeleccionada) {
		int fila = filaSeleccionada;
		if(fila >= 0 ) {
			editServicioController.mostrarEditServicioVista();
			editServicioController.setEditar(servicios.get(fila));
			System.out.println(servicios.get(fila));
			completarCamposEditVista();
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un servcio para editar.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
	
	private int seleccionar() {
		return  serviciosVista.getTablaServicios().getSelectedRow();
	}
	
	public void mostrarServiciosVista() {
		this.serviciosVista.mostrar();
	}
	
	private void completarCamposEditVista() {
		String nombre = servicios.get(seleccionar()).getNombre();
		float precio = servicios.get(seleccionar()).getPrecio();
		int duracion = servicios.get(seleccionar()).getDuracion();
		int puntos = servicios.get(seleccionar()).getPuntos();
		EstadoServicio estado = servicios.get(seleccionar()).getEstado();
		
		editServicioController.getEditProfVista().getTxtNombre().setText(nombre);
		editServicioController.getEditProfVista().getTxtPrecio().setText(Float.toString(precio));
		editServicioController.getEditProfVista().getTxtDuracion().setText(Integer.toString(duracion));
		editServicioController.getEditProfVista().getTxtPuntos().setText(Integer.toString(puntos));
		editServicioController.getEditProfVista().getComboEstado().setSelectedItem(estado);
	}
	
	public boolean deleteServ(ActionEvent s, int filaSeleccionada) {
		try {
			int fila = filaSeleccionada;
			int id = this.servicios.get(fila).getId();
			String nombre = this.servicios.get(fila).getNombre();
			float precio = this.servicios.get(fila).getPrecio();
			int duracion = this.servicios.get(fila).getDuracion();
			int puntos = this.servicios.get(fila).getPuntos();
			EstadoServicio estado = this.servicios.get(fila).getEstado();

			if (estado.equals(EstadoServicio.ACTIVO)) {
				estado = EstadoServicio.INACTIVO;
			}
			else
				estado = EstadoServicio.ACTIVO;

			ServicioDTO editar = new ServicioDTO(id, nombre, precio, duracion, puntos, estado);
			int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere cambiar el estado del profesional?",
					"Estado profesional", JOptionPane.YES_NO_OPTION);

			if (resp == 0) {
				if(estado.equals(EstadoServicio.INACTIVO)) {
					peluqueria.cancelarServicio(editar);
				}
				peluqueria.actualizarServicio(editar);
				actualizarTablaServicios();
				return true;
			}
			return false;

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un profesional.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}

	public void actualizarTablaServicios() {
		servicios = peluqueria.obtenerServicios();
		serviciosVista.llenarTabla(servicios);
	}

	private void activateAddServicioVista(ActionEvent e) {
		addServicioController.mostrarAddServicioVista();
	}

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public ServiciosVista getServiciosVista() {
		return serviciosVista;
	}

	public void setServiciosVista(ServiciosVista serviciosVista) {
		this.serviciosVista = serviciosVista;
	}

	public AdministradorController getAdminController() {
		return adminController;
	}

	public void setAdminController(AdministradorController adminController) {
		this.adminController = adminController;
	}

	public AddServicioController getAddServicioController() {
		return addServicioController;
	}

	public void setAddServicioController(AddServicioController addServicioController) {
		this.addServicioController = addServicioController;
	}

	public EditServicioController getEditServicioController() {
		return editServicioController;
	}

	public void setEditServicioController(EditServicioController editServicioController) {
		this.editServicioController = editServicioController;
	}

	public List<ServicioDTO> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServicioDTO> servicios) {
		this.servicios = servicios;
	}
	
}
