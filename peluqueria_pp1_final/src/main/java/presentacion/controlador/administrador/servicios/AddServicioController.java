package presentacion.controlador.administrador.servicios;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import dto.EstadoServicio;
import dto.ProfesionalDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.servicios.AddServicioVista;

public class AddServicioController {
	private static AddServicioController INSTANCE;
	private Peluqueria peluqueria;
	private AddServicioVista addServicioVista;
	private ServiciosController serviciosController;
	private ProfesionalDTO editar;
	private Validador validador;

	public static AddServicioController getInstance(Peluqueria peluqueria, ServiciosController servicio) {
		if (INSTANCE == null)
			INSTANCE = new AddServicioController(peluqueria, servicio);
		return INSTANCE;
	}

	private AddServicioController(Peluqueria pel, ServiciosController servicio) {
		addServicioVista = AddServicioVista.getInstance();
		serviciosController = servicio;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		addServicioVista.getBtnConfirmar().addActionListener(z -> crearServicio(z));
	}

	public void mostrarAddServicioVista() {
		addServicioVista.mostrar();
	}

	private AddServicioController(Peluqueria pel) {
		addServicioVista = AddServicioVista.getInstance();
		peluqueria = pel;
		acciones();
	}

	public boolean crearServicio(ActionEvent e) {
		String nombre = addServicioVista.getTxtNombre().getText();
		String precio = addServicioVista.getTxtPrecio().getText();
		String duracion = addServicioVista.getTxtDuracion().getText();
		String puntos = addServicioVista.getTxtPuntos().getText();
		EstadoServicio estado = (EstadoServicio) addServicioVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Crear servicio", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, duracion, puntos)) {
			ServicioDTO nuevo = new ServicioDTO(0, nombre, Float.parseFloat(precio), Integer.parseInt(duracion), Integer.parseInt(puntos), estado);
			peluqueria.agregarServicio(nuevo);
			serviciosController.actualizarTablaServicios();
			addServicioVista.ocultar();
			limpiarCampos();
			return true;
		}
		limpiarCampos();
		return false;
	}

	private boolean validarCampos(String nombre, String precio, String duracion, String puntos) {
		boolean validado = true;
		String mensaje = "";
		
		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esNumerico(duracion)) {
			validado = validado && false;
			mensaje += "Duracion invalido\n";
		}
		if (!validador.esNumerico(puntos)) {
			validado = validado && false;
			mensaje += "Punto invalido\n";
		}
		
		if(!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
	
	private void limpiarCampos() {
		addServicioVista.getTxtDuracion().setText(null);
		addServicioVista.getTxtNombre().setText(null);
		addServicioVista.getTxtPrecio().setText(null);
		addServicioVista.getTxtPuntos().setText(null);
	}
	
	///////////////////////////////////////////////////////

	public AddServicioVista getAddEditProfVista() {
		return addServicioVista;
	}

	public void setAddEditProfVista(AddServicioVista addEditProfVista) {
		this.addServicioVista = addEditProfVista;
	}

	public ProfesionalDTO getEditar() {
		return editar;
	}

	public void setEditar(ProfesionalDTO editar) {
		this.editar = editar;
	}

}
