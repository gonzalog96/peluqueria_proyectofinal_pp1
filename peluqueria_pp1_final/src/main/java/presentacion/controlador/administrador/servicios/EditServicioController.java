package presentacion.controlador.administrador.servicios;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import dto.EstadoServicio;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.servicios.EditServicioVista;

public class EditServicioController {
	private static EditServicioController INSTANCE;
	private Peluqueria peluqueria;
	private EditServicioVista editServicioVista;
	private ServiciosController serviciosController;
	private ServicioDTO editar;
	private Validador validador;

	public static EditServicioController getInstance(Peluqueria peluqueria, ServiciosController servicio) {
		if (INSTANCE == null)
			INSTANCE = new EditServicioController(peluqueria, servicio);
		return INSTANCE;
	}

	private EditServicioController(Peluqueria pel, ServiciosController servicio) {
		editServicioVista = EditServicioVista.getInstance();
		serviciosController = servicio;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
	}

	private void acciones() {
		editServicioVista.getBtnConfirmar().addActionListener(z -> editarServicio(z));
	}

	public void mostrarEditServicioVista() {
		editServicioVista.mostrar();
	}

	public boolean editarServicio(ActionEvent e) {
		int id = editar.getId();

		String nombre = editServicioVista.getTxtNombre().getText();
		String precio = editServicioVista.getTxtPrecio().getText();
		String duracion = editServicioVista.getTxtDuracion().getText();
		String puntos = editServicioVista.getTxtPuntos().getText();
		EstadoServicio estado = (EstadoServicio) editServicioVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Editar servicio",
				JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, duracion, puntos)) {
			ServicioDTO editar = new ServicioDTO(id, nombre, Float.parseFloat(precio), Integer.parseInt(duracion),
					Integer.parseInt(puntos), estado);
			peluqueria.actualizarServicio(editar);
			serviciosController.actualizarTablaServicios();
			editServicioVista.ocultar();
			return true;
		}
		return false;
	}

	private boolean validarCampos(String nombre, String precio, String duracion, String puntos) {
		boolean validado = true;
		String mensaje = "";

		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esNumerico(duracion)) {
			validado = validado && false;
			mensaje += "Duracion invalido\n";
		}
		if (!validador.esNumerico(puntos)) {
			validado = validado && false;
			mensaje += "Punto invalido\n";
		}

		if (!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}

	///////////////////////////////////////////////////////

	public EditServicioVista getEditProfVista() {
		return editServicioVista;
	}

	public void setAddEditProfVista(EditServicioVista addEditProfVista) {
		this.editServicioVista = addEditProfVista;
	}

	public ServicioDTO getEditar() {
		return editar;
	}

	public void setEditar(ServicioDTO edit) {
		this.editar = edit;
	}

}
