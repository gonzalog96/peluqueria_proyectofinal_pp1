package presentacion.controlador.administrador.promociones;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoPromocion;
import dto.PromocionDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.promociones.EditPromocionVista;

public class EditPromocionController {
	private static EditPromocionController INSTANCE;
	private Peluqueria peluqueria;
	private EditPromocionVista editPromocionVista;
	private PromocionesController promocionController;
	private PromocionDTO editar;
	private Validador validador;
	private List<ServicioDTO> serviciosPromocion;
	private List<ServicioDTO> serviciosEnTabla;
	private int cantServicios;

	public static EditPromocionController getInstance(Peluqueria peluqueria, PromocionesController promocion) {
		if (INSTANCE == null)
			INSTANCE = new EditPromocionController(peluqueria, promocion);
		return INSTANCE;
	}

	private EditPromocionController(Peluqueria pel, PromocionesController promocion) {
		editPromocionVista = EditPromocionVista.getInstance();
		promocionController = promocion;
		serviciosPromocion = new ArrayList<ServicioDTO>();
		peluqueria = pel;
		editar = null;
		cantServicios = 2;
		validador = new Validador();
		acciones();
	}
	
	private void acciones() {
		editPromocionVista.getBtnConfirmar().addActionListener(e -> editarPromocion(e));
	}
	
	public void mostrarEditPromocionVista() {
		editPromocionVista.mostrar();
	}

	private void editarPromocion(ActionEvent e) {
		int id = editar.getId();

		String nombre = editPromocionVista.getTxtNombre().getText();
		String precio = editPromocionVista.getTxtPrecio().getText();
		String multiplicacion = editPromocionVista.getTxtMultiplicacion().getText();
		EstadoPromocion estado = (EstadoPromocion) editPromocionVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Editar servicio",
				JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, multiplicacion)) {
			PromocionDTO editar = new PromocionDTO(id, nombre, estado, new BigDecimal(precio), Integer.parseInt(multiplicacion), serviciosPromocion);
			peluqueria.actualizarPromocion(editar);
			promocionController.actualizarTablaPromociones();
			editPromocionVista.ocultar();
		}
	}
	
	private boolean validarCampos(String nombre, String precio, String multiplicacion) {
		boolean validado = true;
		String mensaje = "";

		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esNumerico(multiplicacion)) {
			validado = validado && false;
			mensaje += "Multiplicacion invalida\n";
		}

		if (!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public PromocionDTO getEditar() {
		return editar;
	}

	public void setEditar(PromocionDTO editar) {
		this.editar = editar;
	}

	public List<ServicioDTO> getServiciosPromocion() {
		return serviciosPromocion;
	}

	public void setServiciosPromocion(ArrayList<ServicioDTO> serviciosPromocion) {
		this.serviciosPromocion = serviciosPromocion;
	}

	public EditPromocionVista getEditPromocionVista() {
		return editPromocionVista;
	}

	public void setEditPromocionVista(EditPromocionVista editPromocionVista) {
		this.editPromocionVista = editPromocionVista;
	}
	
	

	

}
