package presentacion.controlador.administrador.promociones;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoPromocion;
import dto.PromocionDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.administrador.profesional.AdministradorController;
import presentacion.vista.administrador.promociones.InfoPromocionVista;
import presentacion.vista.administrador.promociones.PromocionesVista;

public class PromocionesController {
	private Peluqueria peluqueria;
	private static PromocionesController INSTANCE;
	private PromocionesVista promocionesVista;
	private InfoPromocionVista infoPromo;
	private AdministradorController adminController;
	private AddPromocionController addPromocionController;
	private EditPromocionController editPromocionController;
	private List<PromocionDTO> promociones;

	public static PromocionesController getInstance(Peluqueria peluqueria, AdministradorController admin) {
		if (INSTANCE == null)
			INSTANCE = new PromocionesController(peluqueria, admin);
		return INSTANCE;
	}

	private PromocionesController(Peluqueria pel, AdministradorController admin) {
		peluqueria = pel;
		adminController = admin;
		promocionesVista = PromocionesVista.getInstance();
		infoPromo = InfoPromocionVista.getInstance();
		addPromocionController = AddPromocionController.getInstance(pel, this);
		editPromocionController = EditPromocionController.getInstance(pel, this);
		acciones();
		actualizarTablaPromociones();
	}

	private void acciones() {
		promocionesVista.getBtnInfo().addActionListener(x -> activateInforDePromocion(x,seleccionar()));
		promocionesVista.getBtnAddPromocion().addActionListener(s -> activateAddPromocionVista(s));
		promocionesVista.getBtnRemovePromocion().addActionListener(r -> deleteProm(r, seleccionar()));
		promocionesVista.getBtnEditPromocion().addActionListener(k -> activateEditPromocionVista(k, seleccionar()));
		promocionesVista.getBtnProfesionales().addActionListener(x -> activateProfesionalesVista(x));
	}

	public void activateProfesionalesVista(ActionEvent x) {
		adminController.mostrarAdministradorVista();
		promocionesVista.ocultar();
	}

	public void mostrarPromocionesVista() {
		promocionesVista.mostrar();
	}

	public void actualizarTablaPromociones() {
		promociones = peluqueria.getPromociones();
		promocionesVista.llenarTabla(promociones);
	}

	public boolean activateInforDePromocion(ActionEvent x, int filaSeleccionada) {
		int fila = filaSeleccionada;
		if (fila >= 0) {
			PromocionDTO promo = promociones.get(fila);
			infoPromo.llenarTabla(promo.getServicios());
			infoPromo.getLblServicios().setText("Servicios | Multiplicación: " + promo.getMultiplicacion());
			infoPromo.mostrar();
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una promocion para ver su informacion.", "ALERTA!",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}

	private void activateAddPromocionVista(ActionEvent s) {
		addPromocionController.mostrarAddPromocionVista();
	}

	private int seleccionar() {
		return promocionesVista.getTablaPromociones().getSelectedRow();
	}

	public boolean activateEditPromocionVista(ActionEvent s, int filaSeleccionada) {
		int fila = filaSeleccionada;
		if (fila >= 0) {
			editPromocionController.mostrarEditPromocionVista();
			editPromocionController.setEditar(promociones.get(fila));
			completarCamposEditVista();
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una promocion para editar.", "ALERTA!",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
	
	public boolean deleteProm(ActionEvent s, int filaSeleccionada) {
		try {
			int fila = filaSeleccionada;
			int id = this.promociones.get(fila).getId();
			String nombre = this.promociones.get(fila).getNombre();
			BigDecimal precio = this.promociones.get(fila).getPrecio();
			int multiplicacion = this.promociones.get(fila).getMultiplicacion();
			EstadoPromocion estado = this.promociones.get(fila).getEstado();
			List<ServicioDTO> servicios = promociones.get(fila).getServicios();

			if (estado.equals(EstadoPromocion.ACTIVO))
				estado = EstadoPromocion.INACTIVO;
			else
				estado = EstadoPromocion.ACTIVO;

			PromocionDTO editar = new PromocionDTO(id, nombre, estado, precio, multiplicacion, servicios);
			int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere cambiar el estado de la promocion?",
					"Estado profesional", JOptionPane.YES_NO_OPTION);
			System.out.println(editar.toString());
			if (resp == 0) {
				peluqueria.actualizarPromocion(editar);
				actualizarTablaPromociones();
				return true;
			}
			return false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un profesional.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			System.out.println(e);
			return false;
		}
	}

	private void completarCamposEditVista() {
		String nombre = promociones.get(seleccionar()).getNombre();
		BigDecimal precio = promociones.get(seleccionar()).getPrecio();
		int multiplicacion = promociones.get(seleccionar()).getMultiplicacion();
		EstadoPromocion estado = promociones.get(seleccionar()).getEstado();
		List<ServicioDTO> servicios = promociones.get(seleccionar()).getServicios();

		editPromocionController.getEditPromocionVista().getTxtNombre().setText(nombre);
		editPromocionController.getEditPromocionVista().getTxtPrecio().setText(precio.toString());
		editPromocionController.getEditPromocionVista().getTxtMultiplicacion()
				.setText(Integer.toString(multiplicacion));
		editPromocionController.getEditPromocionVista().getComboEstado().setSelectedItem(estado);
		editPromocionController.getEditPromocionVista().llenarTabla(servicios);
	}

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public PromocionesVista getPromocionesVista() {
		return promocionesVista;
	}

	public void setPromocionesVista(PromocionesVista promocionesVista) {
		this.promocionesVista = promocionesVista;
	}

	public InfoPromocionVista getInfoPromo() {
		return infoPromo;
	}

	public void setInfoPromo(InfoPromocionVista infoPromo) {
		this.infoPromo = infoPromo;
	}

	public AdministradorController getAdminController() {
		return adminController;
	}

	public void setAdminController(AdministradorController adminController) {
		this.adminController = adminController;
	}

	public AddPromocionController getAddPromocionController() {
		return addPromocionController;
	}

	public void setAddPromocionController(AddPromocionController addPromocionController) {
		this.addPromocionController = addPromocionController;
	}

	public EditPromocionController getEditPromocionController() {
		return editPromocionController;
	}

	public void setEditPromocionController(EditPromocionController editPromocionController) {
		this.editPromocionController = editPromocionController;
	}

	public List<PromocionDTO> getPromociones() {
		return promociones;
	}

	public void setPromociones(List<PromocionDTO> promociones) {
		this.promociones = promociones;
	}

	
}
