package presentacion.controlador.administrador.promociones;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import dto.EstadoPromocion;
import dto.PromocionDTO;
import dto.ServicioDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.promociones.AddPromocionVista;

public class AddPromocionController {
	private static AddPromocionController INSTANCE;
	private Peluqueria peluqueria;
	private AddPromocionVista addPromocionVista;
	private PromocionesController promocionController;
	private Validador validador;
	private ArrayList<ServicioDTO> serviciosPromocion;
	private List<ServicioDTO> serviciosEnTabla;
	private int cantServicios;

	public static AddPromocionController getInstance(Peluqueria peluqueria, PromocionesController promocion) {
		if (INSTANCE == null)
			INSTANCE = new AddPromocionController(peluqueria, promocion);
		return INSTANCE;
	}

	private AddPromocionController(Peluqueria pel, PromocionesController promocion) {
		addPromocionVista = AddPromocionVista.getInstance();
		promocionController = promocion;
		serviciosPromocion = new ArrayList<ServicioDTO>();
		peluqueria = pel;
		cantServicios = 2;
		validador = new Validador();
		acciones();
	}

	public void mostrarAddPromocionVista() {
		addPromocionVista.mostrar();
	}

	private void acciones() {
		addPromocionVista.getBtnConfirmar().addActionListener(q -> crearPromocion(q));
		addPromocionVista.getBtnAddServicios().addActionListener(x -> seleccionarServicio(x));
		addPromocionVista.getBtnSubtractServicio().addActionListener(s -> eliminarServicio(s));
	}

	private void crearPromocion(ActionEvent q) {
		String nombre = addPromocionVista.getTxtNombre().getText();
		String precio = addPromocionVista.getTxtPrecio().getText();
		String multiplicacion = addPromocionVista.getTxtMultiplicacion().getText();
		EstadoPromocion estado = (EstadoPromocion) addPromocionVista.getComboEstado().getSelectedItem();

		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Crear promocion", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nombre, precio, multiplicacion)) {
			PromocionDTO nueva = new PromocionDTO(0, nombre, estado, new BigDecimal(precio),
					Integer.parseInt(multiplicacion), serviciosPromocion);
			peluqueria.agregarPromocion(nueva);
			promocionController.actualizarTablaPromociones();
			addPromocionVista.ocultar();
			serviciosPromocion.clear();
		}
		limpiarCampos();
	}

	private void limpiarCampos() {
		addPromocionVista.getTxtNombre().setText(null);
		addPromocionVista.getTxtMultiplicacion().setText(null);
		addPromocionVista.getTxtPrecio().setText(null);
		for (int i = 0; i < addPromocionVista.getTablaServicios().getRowCount()+1; i++) {
			addPromocionVista.getModelInformacion().removeRow(i);
		}
	}

	private void seleccionarServicio(ActionEvent s) {
		try {
			if (serviciosPromocion.size() <= cantServicios) {
				serviciosEnTabla = peluqueria.obtenerServicios();
				ServicioDTO[] array = serviciosEnTabla.toArray(new ServicioDTO[serviciosEnTabla.size()]);

				Icon icon = new Icon() {

					@Override
					public void paintIcon(Component c, Graphics g, int x, int y) {
						// TODO Auto-generated method stub
					}

					@Override
					public int getIconWidth() {
						// TODO Auto-generated method stub
						return 0;
					}

					@Override
					public int getIconHeight() {
						// TODO Auto-generated method stub
						return 0;
					}
				};
				ServicioDTO resp = (ServicioDTO) JOptionPane.showInputDialog(null, "Seleccione un servicio", "Servicios",
						JOptionPane.DEFAULT_OPTION, icon, array, array[0]);
				
				if(!estaEnArray(resp)) {
					serviciosPromocion.add(resp);
					System.out.println("Servicios para la promo: " + serviciosPromocion.toString());
					addPromocionVista.llenarTabla(serviciosPromocion);
				}else {
					JOptionPane.showMessageDialog(null, "El servicio "+ resp.getNombre() +" ya se agrego a la promocion", "ALERTA!", JOptionPane.WARNING_MESSAGE);
				}
				
			} else {
				JOptionPane.showMessageDialog(null, "La promocion ya tiene 3 servicios", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			}
			
			System.out.println(serviciosPromocion.toString());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void eliminarServicio(ActionEvent x) {
		try {
			int fila = addPromocionVista.getTablaServicios().getSelectedRow();
			serviciosPromocion.remove(fila);
			addPromocionVista.llenarTabla(serviciosPromocion);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Seleccione un servicio.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		
	}
	
	private boolean estaEnArray(ServicioDTO servicio) {
		for (ServicioDTO s : serviciosPromocion) {
			if (s.equals(servicio))
				return true;
		}
		return false;
	}

	private boolean validarCampos(String nombre, String precio, String multiplicacion) {
		boolean validado = true;
		String mensaje = "";

		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esNumerico(precio)) {
			validado = validado && false;
			mensaje += "Precio invalido\n";
		}
		if (!validador.esNumerico(multiplicacion)) {
			validado = validado && false;
			mensaje += "Multiplicacion invalida\n";
		}

		if (!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}

}
