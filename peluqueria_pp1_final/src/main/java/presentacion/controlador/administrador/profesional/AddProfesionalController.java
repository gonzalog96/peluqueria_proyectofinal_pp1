package presentacion.controlador.administrador.profesional;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoProfesional;
import dto.ProfesionalDTO;
import dto.SucursalDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.profesionales.AddProfesionalVista;

public class AddProfesionalController {
	private static AddProfesionalController INSTANCE;
	private Peluqueria peluqueria;
	private AddProfesionalVista addEditProfVista;
	private AdministradorController adminController;
	private List<SucursalDTO> sucursales;
	private ProfesionalDTO editar;
	private Validador validador;

	public static AddProfesionalController getInstance(Peluqueria peluqueria, AdministradorController admin) {
		if (INSTANCE == null)
			INSTANCE = new AddProfesionalController(peluqueria, admin);
		return INSTANCE;
	}

	private AddProfesionalController(Peluqueria pel, AdministradorController admin) {
		addEditProfVista = AddProfesionalVista.getInstance();
		adminController = admin;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
		actualizarComboSucursal();
	}

	private void acciones() {
		addEditProfVista.getBtnAgregarProfesional().addActionListener(e -> crearProfesional(e));
	}

	public void mostrarAddEditProfesionalVista() {
		addEditProfVista.mostrar();
	}

	public void actualizarComboSucursal() {
		sucursales = peluqueria.obtenerSucursales();
		addEditProfVista.actualizarComboSucursal(sucursales);
	}

	public boolean crearProfesional(ActionEvent e) {
//		actualizarComboSucursal();
		String nombre = addEditProfVista.getTxtNombre().getText();
		String apellido = addEditProfVista.getTxtApellido().getText();
		String email = addEditProfVista.getTxtEmail().getText();
		String telefono = addEditProfVista.getTxtTelefono().getText();
		String dni = addEditProfVista.getTxtDni().getText();
		EstadoProfesional estado = (EstadoProfesional) addEditProfVista.getComboEstado().getSelectedItem();
		SucursalDTO sucursal = (SucursalDTO) addEditProfVista.getComboSucursal().getSelectedItem();
		System.out.println(sucursal.toString());
		
		
		ProfesionalDTO nuevo = new ProfesionalDTO(nombre, apellido, email, telefono, dni, sucursal, estado);
		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Crear profesional", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(nuevo)) {
			peluqueria.agregarProfesional(nuevo);
			adminController.actualizarTablaProfesionales();
			addEditProfVista.ocultar();
			limpiarCampos();
			return true;
		}
		limpiarCampos();
		return false;
	}
	
	private boolean validarCampos(ProfesionalDTO prof) {
		String nombre = prof.getNombre();
		String apellido = prof.getApellido();
		String email = prof.getEmail();
		String telefono = prof.getTelefono();
		String dni = prof.getDni();
		boolean validado = true;
		String mensaje = "";
		
		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esStringValido(apellido)) {
			validado = validado && false;
			mensaje += "Apellido invalido\n";
		}
		if (!validador.esEmailValido(email)) {
			validado = validado && false;
			mensaje += "Email invalido\n";
		}
		if (!validador.esNumerico(telefono)) {
			validado = validado && false;
			mensaje += "Telefono invalido\n";
		}
		if (!validador.esNumerico(dni)) {
			validado = validado && false;
			mensaje += "DNI invalido\n";
		}
		
		if(!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
	private void limpiarCampos() {
		addEditProfVista.getTxtApellido().setText(null);
		addEditProfVista.getTxtDni().setText(null);
		addEditProfVista.getTxtEmail().setText(null);
		addEditProfVista.getTxtNombre().setText(null);
		addEditProfVista.getTxtTelefono().setText(null);
	}

	
	///////////////////////////////////////////////////////

	public AddProfesionalVista getAddProfVista() {
		return addEditProfVista;
	}

	public void setAddProfVista(AddProfesionalVista addEditProfVista) {
		this.addEditProfVista = addEditProfVista;
	}

	public ProfesionalDTO getEditar() {
		return editar;
	}

	public void setEditar(ProfesionalDTO editar) {
		this.editar = editar;
	}

	
	
	
}
