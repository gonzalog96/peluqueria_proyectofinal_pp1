package presentacion.controlador.administrador.profesional;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JOptionPane;

import dto.EstadoProfesional;
import dto.ProfesionalDTO;
import dto.SucursalDTO;
import modelo.Peluqueria;
import presentacion.controlador.Validador;
import presentacion.vista.administrador.profesionales.EditProfesionalVista;

public class EditProfesionalController {
	private static EditProfesionalController INSTANCE;
	private Peluqueria peluqueria;
	private EditProfesionalVista editProfVista;
	private AdministradorController adminController;
	private List<SucursalDTO> sucursales;
	private ProfesionalDTO editar;
	private Validador validador;

	public static EditProfesionalController getInstance(Peluqueria peluqueria, AdministradorController admin) {
		if (INSTANCE == null)
			INSTANCE = new EditProfesionalController(peluqueria, admin);
		return INSTANCE;
	}

	private EditProfesionalController(Peluqueria pel, AdministradorController admin) {
		editProfVista = EditProfesionalVista.getInstance();
		adminController = admin;
		peluqueria = pel;
		editar = null;
		validador = new Validador();
		acciones();
		actualizarComboSucursal();
	}

	private void acciones() {
		editProfVista.getBtnConfirmar().addActionListener(e -> editarProfesional(e));
	}

	public void mostrarEditProfesionalVista() {
		editProfVista.mostrar();
	}


	public void actualizarComboSucursal() {
		sucursales = peluqueria.obtenerSucursales();
		editProfVista.actualizarComboSucursal(sucursales);
	}

	public boolean editarProfesional(ActionEvent e) {
		int id = editar.getId();
		String nombre = editar.getNombre();
		String apellido = editar.getApellido();
		String email = editar.getEmail();
		String telefono = editar.getTelefono();
		String dni = editar.getDni();
		EstadoProfesional estado = editar.getEstado();
		SucursalDTO sucursal = editar.getSucursal();
		
		nombre = editProfVista.getTxtNombre().getText();
		apellido = editProfVista.getTxtApellido().getText();
		email = editProfVista.getTxtEmail().getText();
		telefono = editProfVista.getTxtTelefono().getText();
		dni = editProfVista.getTxtDni().getText();
		estado = (EstadoProfesional) editProfVista.getComboEstado().getSelectedItem();
		sucursal = (SucursalDTO) editProfVista.getComboSucursal().getSelectedItem();
		
		ProfesionalDTO edit = new ProfesionalDTO(id, nombre, apellido, email, telefono, dni, sucursal, estado);
		int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro?", "Editar profesional", JOptionPane.YES_NO_OPTION);
		if (resp == 0 && validarCampos(edit)) {
			peluqueria.actualizarProfesional(edit);
			adminController.actualizarTablaProfesionales();
			editProfVista.ocultar();
			return true;
		}
		return false;
	}
	
	public boolean validarCampos(ProfesionalDTO prof) {
		String nombre = prof.getNombre();
		String apellido = prof.getApellido();
		String email = prof.getEmail();
		String telefono = prof.getTelefono();
		String dni = prof.getDni();
		boolean validado = true;
		String mensaje = "";
		
		if (!validador.esStringValido(nombre)) {
			validado = validado && false;
			mensaje += "Nombre invalido\n";
		}
		if (!validador.esStringValido(apellido)) {
			validado = validado && false;
			mensaje += "Apellido invalido\n";
		}
		if (!validador.esEmailValido(email)) {
			validado = validado && false;
			mensaje += "Email invalido\n";
		}
		if (!validador.esNumerico(telefono)) {
			validado = validado && false;
			mensaje += "Telefono invalido\n";
		}
		if (!validador.esNumerico(dni)) {
			validado = validado && false;
			mensaje += "DNI invalido\n";
		}
		
		if(!validado) {
			JOptionPane.showMessageDialog(null, mensaje, "ALERTA!", JOptionPane.WARNING_MESSAGE);
		}
		return validado;
	}
	
//	private void limpiarCampos() {
//		editProfVista.getTxtApellido().setText(null);
//		editProfVista.getTxtDni().setText(null);
//		editProfVista.getTxtEmail().setText(null);
//		editProfVista.getTxtNombre().setText(null);
//		editProfVista.getTxtTelefono().setText(null);
//	}

	
	///////////////////////////////////////////////////////

	public EditProfesionalVista getEditProfVista() {
		return editProfVista;
	}

	public void setEditProfVista(EditProfesionalVista addEditProfVista) {
		this.editProfVista = addEditProfVista;
	}

	public ProfesionalDTO getEditar() {
		return editar;
	}

	public void setEditar(ProfesionalDTO editar) {
		this.editar = editar;
	}

	
	
	
}
