package presentacion.controlador.administrador.profesional;

import java.awt.Component;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import dto.EstadoProfesional;
import dto.ProfesionalDTO;
import dto.ServicioDTO;
import dto.SucursalDTO;
import modelo.Peluqueria;
import persistencia.conexion.Conexion;
import persistencia.propiedades.Propiedad;
import presentacion.controlador.Validador;
import presentacion.controlador.administrador.bbdd.BaseDeDatosController;
import presentacion.controlador.administrador.producto.ProductoController;
import presentacion.controlador.administrador.promociones.PromocionesController;
import presentacion.controlador.administrador.servicios.ServiciosController;
import presentacion.reportes.controlador.ReporteController;
import presentacion.vista.administrador.profesionales.AdministradorVista;
import presentacion.vista.administrador.profesionales.ConfigBBDDModal;
import presentacion.vista.administrador.puntos.PuntosAdministradorVista;

public class AdministradorController {
	private Peluqueria peluqueria;
	private static AdministradorController INSTANCE;
	private AdministradorVista administradorVista;
	private BaseDeDatosController bbddController;
	private ServiciosController serviciosController;
	private ProductoController productoController;
	private AddProfesionalController addProfController;
	private EditProfesionalController editProfController;
	private PromocionesController promocionesController;
	private List<ProfesionalDTO> profesionalesEnTabla;
	private List<ServicioDTO> servicios;
	
	// vista temporal para modificar puntos y dinero.
	private PuntosAdministradorVista puntosAdministradorVista;
	/////////////////////////////////////////////////

	///////////////////////////////////////// SE AGREGA CONTROLADOR PARA CONFIGURAR
	///////////////////////////////////////// REPORTES
	///////////////////////////////////////// ///////////////////////////////////////////
	private ReporteController configReporteController;
	///////////////////////////////////////// SE AGREGA LA CONTROLADOR PARA
	///////////////////////////////////////// CONFIGURAR REPORTES
	///////////////////////////////////////// ///////////////////////////////////////////

	///////////////////////////////////////// SE AGREGA VISTA PARA CONFIGURAR LA
	///////////////////////////////////////// BBDD
	///////////////////////////////////////// ///////////////////////////////////////////
	private ConfigBBDDModal configBBDDModal;
	///////////////////////////////////////// SE AGREGA VISTA PARA CONFIGURAR LA
	///////////////////////////////////////// BBDD
	///////////////////////////////////////// ///////////////////////////////////////////
	private String direccion_ingresada;
	private String dbname_ingresado;
	private String puerto_ingresado;
	private String user_ingresado;
	private String pass_ingresado;
	private String direccionConfigProperties;

	public static AdministradorController getInstance(Peluqueria peluqueria) {
		if (INSTANCE == null)
			INSTANCE = new AdministradorController(peluqueria);
		return INSTANCE;
	}

	private AdministradorController(Peluqueria pel) {
		peluqueria = pel;
		instanciar();
		actualizarTablaProfesionales();
		acciones();
	}

	private void instanciar() {
		administradorVista = AdministradorVista.getInstance();
		serviciosController = ServiciosController.getInstance(peluqueria, this);
		productoController = ProductoController.getInstance(peluqueria, this);
		addProfController = AddProfesionalController.getInstance(peluqueria, this);
		editProfController = EditProfesionalController.getInstance(peluqueria, this);
		promocionesController = PromocionesController.getInstance(peluqueria, this);
		bbddController = BaseDeDatosController.getInstance();
		
		// vista temporal para modificar puntos y dinero.
		puntosAdministradorVista = PuntosAdministradorVista.getInstance();
		/////////////////////////////////////////////////

		/////////////////////////////////// SE INSTANCIA EL CONTROLADOR PARA CONFIGURAR
		/////////////////////////////////// REPORTES //////////////////////
		configReporteController = ReporteController.getInstance(peluqueria);
		/////////////////////////////////// SE INSTANCIA EL CONTROLADOR PARA CONFIGURAR
		/////////////////////////////////// REPORTES //////////////////////

		///////////////////////////////////////// SE INSTANCIA VISTA PARA CONFIGURAR LA
		///////////////////////////////////////// BBDD
		///////////////////////////////////////// ///////////////////////////////////////////
		configBBDDModal = ConfigBBDDModal.getInstance();
		direccionConfigProperties = "propiedades/config.properties";
		///////////////////////////////////////// SE INSTANCIA VISTA PARA CONFIGURAR LA
		///////////////////////////////////////// BBDD
		///////////////////////////////////////// ///////////////////////////////////////////
	}

	private void acciones() {
		actualizarTablaProfesionales();
		administradorVista.getBtnAddProfesional().addActionListener(e -> activateAddProfesionalVista(e));
		administradorVista.getBtnEditProfesional().addActionListener(p -> activateEditProfesionalVista(p));
		administradorVista.getBtnAddServicioProfesional().addActionListener(s -> agregarServicioAprofesional(s, seleccionar()));
		administradorVista.getBtnRemoveProfesional().addActionListener(x -> deleteProf(x, seleccionar()));
		administradorVista.getBtnServicios().addActionListener(t -> activateServiciosVista(t));
		administradorVista.getBtnBaseDeDatos().addActionListener(z -> activateBBDDVista(z));
		administradorVista.getBtnProductos().addActionListener(zx -> activateProductosVista(zx));
		administradorVista.getBtnPromociones().addActionListener(zw -> activatePromocionesVista());

		////////////////////////////////// SE LLAMAN A LAS FUNCIONES ESCUCHAR DE LOS
		////////////////////////////////// BOTONES NUEVOS//////////////////////
		administradorVista.getBtnConfigurarBbdd().addActionListener(bd -> activateConfigBbddVista());
		administradorVista.getBtnHoraLaboralLmite().addActionListener(hl -> activateHoraLimiteVista());
		
		
		administradorVista.getBtnValorDelPunto().addActionListener(vp -> activateValorPuntoVista());
		puntosAdministradorVista.getBtnGuardarCambios().addActionListener(gc -> guardarCambiosEnPuntos());
		puntosAdministradorVista.getBtnCancelar().addActionListener(c -> cancelarCambiosEnPuntos());
		puntosAdministradorVista.getCheckHabilitarDineroNuevo().addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	puntosAdministradorVista.activarCampoDineroNuevo();
	        }
		});
		puntosAdministradorVista.getCheckHabilitarPuntosNuevos().addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	        	puntosAdministradorVista.activarCampoPuntosNuevos();
	        }
		});
		
		administradorVista.getBtnDeudaMaxima().addActionListener(dm -> activateDeudaMaximaVista());
		////////////////////////////////// SE LLAMAN A LAS FUNCIONES ESCUCHAR DE LOS
		////////////////////////////////// BOTONES NUEVOS//////////////////////

		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON
		////////////////////////////////// REPORTES////////
		escucharBotonReportes();
		////////////////////////////////// SE LLAMA A LA FUNCION ESCUCHAR BOTON
		////////////////////////////////// REPORTES////////
		
		this.configBBDDModal.getBtnConfirmar().addActionListener(y -> confirmarCambioConfigBbdd());
		this.configBBDDModal.getBtnCancelar().addActionListener(n -> cancelarCambioConfigBbdd());
	}

	////////////////////////////////// SE AGREGA ESCUCHAR BOTON
	////////////////////////////////// REPORTES////////////////////////
	private void escucharBotonReportes() {
		this.administradorVista.getBtnReporteClientes().addActionListener(r -> activateReportesVista(r));

	}
	////////////////////////////////// SE AGREGA ESCUCHAR BOTON
	////////////////////////////////// REPORTES////////////////////////

	////////////////////////////////// SE AGREGA MOSTRAR VENTANA
	////////////////////////////////// REPORTES///////////////////////
	private void activateReportesVista(ActionEvent r) {
		this.configReporteController.mostrarConfigReportesVista();
	}
	////////////////////////////////// SE AGREGA MOSTRAR VENTANA
	////////////////////////////////// REPORTES///////////////////////

	public void activateServiciosVista(ActionEvent x) {
		administradorVista.ocultar();
		serviciosController.mostrarServiciosVista();
	}

	public boolean agregarServicioAprofesional(ActionEvent s, int filaSeleccionada) {
		try {
			int fila = filaSeleccionada;
			ProfesionalDTO prof = this.profesionalesEnTabla.get(fila);
			servicios = peluqueria.obtenerServicios();
			ServicioDTO[] array = servicios.toArray(new ServicioDTO[servicios.size()]);

			Icon icon = new Icon() {

				@Override
				public void paintIcon(Component c, Graphics g, int x, int y) {
					// TODO Auto-generated method stub
				}

				@Override
				public int getIconWidth() {
					// TODO Auto-generated method stub
					return 0;
				}

				@Override
				public int getIconHeight() {
					// TODO Auto-generated method stub
					return 0;
				}
			};
			ServicioDTO resp = (ServicioDTO) JOptionPane.showInputDialog(null, "Seleccione un servicio", "Servicios",
					JOptionPane.DEFAULT_OPTION, icon, array, array[0]);

			peluqueria.atarProfesionalServicio(resp, prof);
			return true;
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un profesional para eliminar.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			return false;
		}
	}
	

	public void activateProductosVista(ActionEvent xz) {
		administradorVista.ocultar();
		productoController.mostrarProductosVista();
	}

	
	public void activateBBDDVista(ActionEvent z) {
		bbddController.mostrarBDVista();
	}
	
	public void activatePromocionesVista() {
		administradorVista.ocultar();
		promocionesController.mostrarPromocionesVista();
	}

	public void activateValorPuntoVista() {
		String cant_puntos = Propiedad.load("cant_puntos", "propiedades/puntos.properties");
		String dinero_por_cantidad = Propiedad.load("dinero_por_cantidad", "propiedades/puntos.properties");
		
		// abrimos la ventana de los puntos.
		this.puntosAdministradorVista.completarDatos(cant_puntos, dinero_por_cantidad);
		this.puntosAdministradorVista.mostrar();
	}
	
	private void guardarCambiosEnPuntos() {
		Validador vale = new Validador();
		if (!puntosAdministradorVista.getTxtDineroNuevo().getText().isEmpty() && !puntosAdministradorVista.getTxtPuntosNuevos().getText().isEmpty() && vale.esNumerico(puntosAdministradorVista.getTxtDineroNuevo().getText()) && vale.esNumerico(puntosAdministradorVista.getTxtPuntosNuevos().getText())) {
			int opcion = JOptionPane.showConfirmDialog(this.puntosAdministradorVista, "¿Confirma la modificación de datos?");
			if(opcion == JOptionPane.YES_OPTION) {
				String puntaje = puntosAdministradorVista.getTxtPuntosNuevos().getText();
				String dinero = puntosAdministradorVista.getTxtDineroNuevo().getText();
				
				Propiedad.setPropertyValue("cant_puntos", puntaje, "propiedades/puntos.properties");
				Propiedad.setPropertyValue("dinero_por_cantidad", dinero, "propiedades/puntos.properties");

				puntosAdministradorVista.limpiar();
				puntosAdministradorVista.ocultar();
				
				JOptionPane.showMessageDialog(this.administradorVista, "Cambios guardados con éxito.");
			}
		} else {
			JOptionPane.showMessageDialog(this.administradorVista, "Error en el ingreso de datos: intentar nuevamente.");
			this.puntosAdministradorVista.getTxtDineroNuevo().setText(this.puntosAdministradorVista.getTxtDineroActual().getText());
			this.puntosAdministradorVista.getTxtPuntosNuevos().setText(this.puntosAdministradorVista.getTxtPuntosActuales().getText());
		}
	}
	
	private void cancelarCambiosEnPuntos() {
		this.puntosAdministradorVista.limpiar();
		this.puntosAdministradorVista.ocultar();
	}

	private void activateHoraLimiteVista() {
		String horaLimite = Propiedad.load("hora_limite", direccionConfigProperties);
		String valorIngresado_cadena = JOptionPane.showInputDialog(
				"La hora límite actual es: " + horaLimite + "\nIngrese la nueva hora límite");

		Validador vale = new Validador();
		if (valorIngresado_cadena != null) {
			if (vale.esHora(valorIngresado_cadena)) {
				Propiedad.setPropertyValue("hora_limite", valorIngresado_cadena, direccionConfigProperties);
			}else {
				JOptionPane.showMessageDialog(this.administradorVista, "El dato ingresado no es válido!");
			}
		}
	}

	private void activateConfigBbddVista() {
		this.configBBDDModal.mostrar();
	}

	private void confirmarCambioConfigBbdd() {
		if(conexionOK()) {
			JOptionPane.showMessageDialog(this.configBBDDModal, "Se ha guardado la nueva configuración!", "CONEXIÓN EXITOSA", JOptionPane.INFORMATION_MESSAGE);
			setearConfiguracionNueva();
			this.configBBDDModal.ocultar();
		}else {
			JOptionPane.showMessageDialog(this.configBBDDModal, "Se ha descartado la configuración ingresada!", "ERROR DE CONEXIÓN", JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean conexionOK() {
		direccion_ingresada = configBBDDModal.getTxtDireccion().getText();
		dbname_ingresado = configBBDDModal.getTxtDbname().getText();
		puerto_ingresado = configBBDDModal.getTxtPuerto().getText();
		user_ingresado = configBBDDModal.getTxtUser().getText();
		pass_ingresado = configBBDDModal.getTxtPass().getText();
		
		Conexion conexion = Conexion.getConexion();
		conexion.cerrarConexion();
		
		return conexion.dbExists(direccion_ingresada, dbname_ingresado, puerto_ingresado, user_ingresado, pass_ingresado);
			
	}

	private void setearConfiguracionNueva() {
		// Seteo del archivo properties
		Propiedad.setPropertyValue("direccion", direccion_ingresada, direccionConfigProperties);
		Propiedad.setPropertyValue("dbname", dbname_ingresado, direccionConfigProperties);
		Propiedad.setPropertyValue("puerto", puerto_ingresado, direccionConfigProperties);
		Propiedad.setPropertyValue("user", user_ingresado, direccionConfigProperties);
		Propiedad.setPropertyValue("pass", pass_ingresado, direccionConfigProperties);
	}

	private void cancelarCambioConfigBbdd() {
		this.configBBDDModal.ocultar();
	}

	private void activateDeudaMaximaVista() {
		String deudaActual = Propiedad.load("deuda", direccionConfigProperties);
		String valorIngresado_cadena = JOptionPane.showInputDialog(
				"Deuda máxima permitida actual: " + deudaActual + "\nIngrese el nuevo valor para la deuda máxima");

		Validador vale = new Validador();
		if (valorIngresado_cadena != null) {
			if (vale.esNumerico(valorIngresado_cadena)) {
				Propiedad.setPropertyValue("deuda", valorIngresado_cadena, direccionConfigProperties);
			}
		}
	}


	public boolean deleteProf(ActionEvent s, int filaSeleccionada) {
		try {
			int fila = filaSeleccionada;
			int id = this.profesionalesEnTabla.get(fila).getId();
			String nombre = this.profesionalesEnTabla.get(fila).getNombre();
			String apellido = this.profesionalesEnTabla.get(fila).getApellido();
			String telefono = this.profesionalesEnTabla.get(fila).getTelefono();
			String email = this.profesionalesEnTabla.get(fila).getEmail();
			String dni = this.profesionalesEnTabla.get(fila).getDni();
			SucursalDTO sucursal = this.profesionalesEnTabla.get(fila).getSucursal();
			EstadoProfesional estado = this.profesionalesEnTabla.get(fila).getEstado();

			if (estado.equals(EstadoProfesional.ACTIVO))
				estado = EstadoProfesional.INACTIVO;
			else
				estado = EstadoProfesional.ACTIVO;

			ProfesionalDTO editar = new ProfesionalDTO(id, nombre, apellido, email, telefono, dni, sucursal, estado);
			int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere cambiar el estado del profesional?",
					"Estado profesional", JOptionPane.YES_NO_OPTION);

			if (resp == 0) {
				peluqueria.actualizarProfesional(editar);
				actualizarTablaProfesionales();
				return true;
			}
			return false;

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un profesional.", "ALERTA!", JOptionPane.WARNING_MESSAGE);
			return false;
		}

	}

	public void activateEditProfesionalVista(ActionEvent p) {
		int fila = seleccionar();
		if (fila >= 0) {
			editProfController.mostrarEditProfesionalVista();
			editProfController.setEditar(profesionalesEnTabla.get(fila));
			completarCamposEditVista();

		} else {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un profesional para editar.", "ALERTA!",
					JOptionPane.WARNING_MESSAGE);
		}
	}
	
	
	public int seleccionar() {
		return  administradorVista.getTablaProfesionales().getSelectedRow();
	}
	

	public void activateAddProfesionalVista(ActionEvent e) {
		addProfController.getAddProfVista().getLblAddEditProfesional().setText("Crear Profesional");
		addProfController.mostrarAddEditProfesionalVista();
	}

	private void completarCamposEditVista() {
		String nombre = profesionalesEnTabla.get(seleccionar()).getNombre();
		String apellido = profesionalesEnTabla.get(seleccionar()).getApellido();
		String dni = profesionalesEnTabla.get(seleccionar()).getDni();
		String email = profesionalesEnTabla.get(seleccionar()).getEmail();
		String telefono = profesionalesEnTabla.get(seleccionar()).getTelefono();
		EstadoProfesional estado = profesionalesEnTabla.get(seleccionar()).getEstado();
		SucursalDTO sucursal = profesionalesEnTabla.get(seleccionar()).getSucursal();

		editProfController.getEditProfVista().getTxtNombre().setText(nombre);
		editProfController.getEditProfVista().getTxtApellido().setText(apellido);
		editProfController.getEditProfVista().getTxtDni().setText(dni);
		editProfController.getEditProfVista().getTxtEmail().setText(email);
		editProfController.getEditProfVista().getTxtTelefono().setText(telefono);
		editProfController.getEditProfVista().getComboEstado().setSelectedItem(estado);
		editProfController.getEditProfVista().getComboSucursal().setSelectedItem(sucursal);
	}

	public void actualizarTablaProfesionales() {
		profesionalesEnTabla = peluqueria.obtenerProfesionales();
		administradorVista.llenarTabla(profesionalesEnTabla);
	}

	public void mostrarAdministradorVista() {
		this.administradorVista.mostrar();
	}

	public AdministradorVista getAdministradorVista() {
		return administradorVista;
	}

	public void setAdministradorVista(AdministradorVista administradorVista) {
		this.administradorVista = administradorVista;
	}

	public List<ProfesionalDTO> getProfesionalesEnTabla() {
		return profesionalesEnTabla;
	}

	public void setProfesionalesEnTabla(List<ProfesionalDTO> profesionalesEnTabla) {
		this.profesionalesEnTabla = profesionalesEnTabla;
	}

	public Peluqueria getPeluqueria() {
		return peluqueria;
	}

	public void setPeluqueria(Peluqueria peluqueria) {
		this.peluqueria = peluqueria;
	}

	public BaseDeDatosController getBbddController() {
		return bbddController;
	}

	public void setBbddController(BaseDeDatosController bbddController) {
		this.bbddController = bbddController;
	}

	public ServiciosController getServiciosController() {
		return serviciosController;
	}

	public void setServiciosController(ServiciosController serviciosController) {
		this.serviciosController = serviciosController;
	}

	public ProductoController getProductoController() {
		return productoController;
	}

	public void setProductoController(ProductoController productoController) {
		this.productoController = productoController;
	}

	public AddProfesionalController getAddProfController() {
		return addProfController;
	}

	public void setAddProfController(AddProfesionalController addProfController) {
		this.addProfController = addProfController;
	}

	public EditProfesionalController getEditProfController() {
		return editProfController;
	}

	public void setEditProfController(EditProfesionalController editProfController) {
		this.editProfController = editProfController;
	}

	public PromocionesController getPromocionesController() {
		return promocionesController;
	}

	public void setPromocionesController(PromocionesController promocionesController) {
		this.promocionesController = promocionesController;
	}

	public List<ServicioDTO> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServicioDTO> servicios) {
		this.servicios = servicios;
	}
	
	
	
	
}
