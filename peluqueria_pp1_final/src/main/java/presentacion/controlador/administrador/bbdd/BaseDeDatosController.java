package presentacion.controlador.administrador.bbdd;

import java.awt.event.ActionEvent;

import presentacion.vista.administrador.bbdd.BaseDeDatosVista;

public class BaseDeDatosController {
	private static BaseDeDatosController INSTANCE;
	private ExportBDController exportarController;
	private ImportBDController importarController;
	private BaseDeDatosVista bbddVista;

	public static BaseDeDatosController getInstance() {
		if (INSTANCE == null)
			INSTANCE = new BaseDeDatosController();
		return INSTANCE;
	}

	private BaseDeDatosController() {
		exportarController = ExportBDController.getInstance();
		importarController = ImportBDController.getInstance();
		bbddVista = BaseDeDatosVista.getInstance();
		acciones();
	}

	private void acciones() {
		bbddVista.getBtnExportar().addActionListener(s -> activateExportVista(s));
		bbddVista.getBtnImportar().addActionListener(q -> activateImportVista(q));
	}

	public void mostrarBDVista() {
		bbddVista.mostrar();;
	}
	
	public void activateExportVista(ActionEvent s) {
		bbddVista.ocultar();
		exportarController.mostrarExportVista();
	}
	
	public void activateImportVista(ActionEvent q) {
		bbddVista.ocultar();
		importarController.mostrarImportVista();
	}
}
