package presentacion.controlador.administrador.bbdd;

import java.awt.event.ActionEvent;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import presentacion.vista.administrador.bbdd.ExportarBDVista;

public class ExportBDController {
	private static ExportBDController INSTANCE;
	private ExportarBDVista exportVista;
	private String ruta;

	public static ExportBDController getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ExportBDController();
		return INSTANCE;
	}

	private ExportBDController() {
		exportVista = ExportarBDVista.getInstance();
		ruta = "";
		acciones();
	}

	private void acciones() {
		exportVista.getBtnCarpeta().addActionListener(e -> directorio(e));
		exportVista.getBtnExportar().addActionListener(l -> backup(l));
	}

	public void mostrarExportVista() {
		exportVista.mostrar();
	}
	
	private void directorio(ActionEvent e) {
		JFileChooser ch = new JFileChooser();
		ch.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int se = ch.showSaveDialog(null);
		if (se == JFileChooser.APPROVE_OPTION) {
			ruta = ch.getSelectedFile().getPath();
			exportVista.getTxtRuta().setText(ruta);
		}
	}
	
	private void exportar(ActionEvent e) {
//		ruta = exportVista.getTxtRuta().getText();
//		String nombre = "\\backup.sql";
//		String backup = "";
//		
//		if (ruta.length() != 0) {
//			try {
//				backup = "mysqldump -u root -proot peluqueria>"+ruta+nombre;
//				Runtime rm = Runtime.getRuntime();
//				rm.exec(backup);
//				JOptionPane.showMessageDialog(null, "Se exporto correctamente!");
//				
//			} catch (Exception ex) {
//				JOptionPane.showMessageDialog(null, "No se exporto!");
//				System.out.println("EX "+ ex);
//			}
//		}
	}
	
	private static void backup(ActionEvent e) {
		   try {
		      Process p = Runtime
		            .getRuntime()
		            .exec("mysqldump -u root -ppassword database");

		      InputStream is = p.getInputStream();
		      FileOutputStream fos = new FileOutputStream("backup_pruebas.sql");
		      byte[] buffer = new byte[1000];

		      int leido = is.read(buffer);
		      while (leido > 0) {
		         fos.write(buffer, 0, leido);
		         leido = is.read(buffer);
		      }

		      fos.close();

		   } catch (Exception e1) {
		      e1.printStackTrace();
		   }
		}

}





















