package presentacion.controlador.administrador.bbdd;

import java.awt.event.ActionEvent;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import presentacion.vista.administrador.bbdd.ImportarBDVista;

public class ImportBDController {
	private static ImportBDController INSTANCE;
	private ImportarBDVista importVista;
	private String ruta;

	public static ImportBDController getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ImportBDController();
		return INSTANCE;
	}

	private ImportBDController() {
		importVista = ImportarBDVista.getInstance();
		ruta = "";
		acciones();
	}

	private void acciones() {
		importVista.getBtnCarpeta().addActionListener(x -> directorio(x));
		importVista.getBtnImportar().addActionListener(e -> importar(e));
	}

	public void mostrarImportVista() {
		importVista.mostrar();
	}
	
	private void directorio(ActionEvent e) {
		JFileChooser ch = new JFileChooser();
		FileNameExtensionFilter fill = new FileNameExtensionFilter("SQL", "sql");
		ch.setFileFilter(fill);
		int se = ch.showOpenDialog(null);
		
		if (se == JFileChooser.APPROVE_OPTION) {
			ruta = ch.getSelectedFile().getPath();
			importVista.getTxtRuta().setText(ruta);
		}
	}
	
	private void importar(ActionEvent e) {
		ruta = importVista.getTxtRuta().getText();
		String backup = "";
		
		if (ruta.length() != 0) {
			try {
				backup = "cmd /c mysqldump -h localhost -u root -proot peluqueria>"+ruta;
				Runtime rm = Runtime.getRuntime();
				rm.exec(backup);
				JOptionPane.showMessageDialog(null, "Se exporto correctamente!");
				
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "No se exporto!");
				System.out.println("EX "+ ex);
			}
		}
		
	}
	
}

























