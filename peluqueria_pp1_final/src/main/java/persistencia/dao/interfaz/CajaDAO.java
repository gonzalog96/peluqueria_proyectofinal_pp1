package persistencia.dao.interfaz;

import java.math.BigDecimal;
import java.util.List;

import dto.CajaDTO;
import dto.EgresoCaja;
import dto.IngresoCaja;

public interface CajaDAO {
	public boolean insert(CajaDTO pago);
//	public boolean delete(int idPago);
	public int getUltimoPagoID();
	
	public BigDecimal getTotalesPorSucursalID(int sucursalID, String tipoDeMovimiento);
	public BigDecimal getSaldosDeSucursales(String tipoDeMovimiento);
	
	public List<IngresoCaja> getDetalleIngresosPorSucursalID(int sucursalID);
	public List<IngresoCaja> getDetalleGlobalDeIngresos();
	
	public List<EgresoCaja> getDetalleEgresosPorSucursalID(int sucursalID);
	public List<EgresoCaja> getDetalleGlobalDeEgresos();
	public boolean actualizarValorPagadoEnTurno(int idTurno, BigDecimal montoARegistrar);
	public boolean acreditarPuntosPorCliente(int idCliente, int puntos);
	public int obtenerPuntosActuales(int idCliente);
	public List<String> getServiciosPorTurnoID(int idTurno);
	public List<CajaDTO> getListadoDeIngresos();
	public List<CajaDTO> getListadoDeEgresos();
	public boolean descontarPuntosPorCliente(int idCliente, int puntos);
}