package persistencia.dao.interfaz;

import java.util.List;

import dto.ClienteDTO;
import dto.ProductoDTO;
import dto.StockDTO;

public interface ProductoDAO {

	boolean insert(ProductoDTO producto);

	List<ProductoDTO> readAll();
	
	boolean update(ProductoDTO producto);

	boolean updateStock(ProductoDTO producto, int id_sucursal, int cantidad);

	int obtenerStock(ProductoDTO producto, int id_sucursal);

	List<StockDTO> readAllStock();

	List<StockDTO> readAllStock(int id_sucursal);
}
