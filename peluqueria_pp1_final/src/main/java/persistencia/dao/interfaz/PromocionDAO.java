package persistencia.dao.interfaz;

import java.util.List;

import dto.PromocionDTO;

public interface PromocionDAO {

	public boolean insert(PromocionDTO promocion);

	public List<PromocionDTO> readall();

	public PromocionDTO getPromocion(int id);

	boolean update(PromocionDTO promo);

	int ultimoID();

	boolean insertX(int id_servicio, int id_promocion);
}