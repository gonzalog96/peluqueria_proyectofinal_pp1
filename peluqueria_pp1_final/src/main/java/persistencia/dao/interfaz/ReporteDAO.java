package persistencia.dao.interfaz;

import java.time.LocalDate;
import java.util.List;

import presentacion.reportes.objetosReporte.ObjetoReporteResumen;

public interface ReporteDAO {
	
	public List<ObjetoReporteResumen> ReporteResumenIE(LocalDate desde,LocalDate hasta,int id_sucursal);
	
	

}
