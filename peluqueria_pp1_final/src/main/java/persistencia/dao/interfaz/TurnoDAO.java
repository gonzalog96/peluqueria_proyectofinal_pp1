package persistencia.dao.interfaz;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import dto.ClienteDTO;
import dto.ProfesionalDTO;
import dto.TurnoDTO;

public interface TurnoDAO {
	public boolean insert(TurnoDTO profesional);

	public boolean delete(TurnoDTO profesional_a_eliminar);

	public boolean update(TurnoDTO profesional_a_editar);

	public boolean used(TurnoDTO profesional_a_verificar);

	public List<TurnoDTO> readAll();

	List<TurnoDTO> readAll(int id_sucursal);

	public ClienteDTO getClienteDesdeTurno(int id_cliente);

	public ProfesionalDTO getProfesionalDesdeTurno(int id_profesional);

	public TurnoDTO obtenerDesdeID(int id_turno);

	public boolean cancelarTurno(int id_turno);

	public List<TurnoDTO> getTurnosDelDia(int id_sucursal, LocalDate fecha);

	List<TurnoDTO> readAll(int id_sucursal, LocalDate fecha_inicio);

	List<TurnoDTO> readAll(LocalDate fecha_inicio);

	public boolean cancelarTurnoAviso(int id);

	ArrayList<TurnoDTO> getAvisos();

	boolean updateAviso(int id);

	List<TurnoDTO> getTurnosDelDia(LocalDate fecha);

	List<TurnoDTO> getTurnosDeuda(ClienteDTO cliente);

	List<TurnoDTO> getCerrados();
	
}
