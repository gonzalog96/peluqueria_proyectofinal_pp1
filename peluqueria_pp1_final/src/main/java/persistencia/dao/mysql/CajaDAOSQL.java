package persistencia.dao.mysql;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import dto.CajaDTO;
import dto.EgresoCaja;
import dto.IngresoCaja;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.CajaDAO;

public class CajaDAOSQL implements CajaDAO
{
	private static final String insert = "INSERT INTO caja(fecha, tipo_movimiento, medio_de_pago, id_sucursal, id_usuario, id_turno, detalle, monto) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String update_pago_del_turno = "UPDATE turno SET monto_pagado = monto_pagado + ? WHERE id_turno = ?";
	
	private static final String get_last_pago = "SELECT MAX(id_caja) FROM caja";
	private static final String get_totales_caja_segun_tipo_movimiento = "SELECT SUM(monto) AS montoTotal FROM caja WHERE id_sucursal = ? AND tipo_movimiento = ?";
	private static final String get_totales_sucursal_segun_tipo_movimiento = "SELECT SUM(monto) AS montoTotal FROM caja WHERE tipo_movimiento = ?";
	private static final String acreditar_puntos_segun_cliente = "UPDATE cliente SET puntos = puntos + ? WHERE id_cliente = ?";
	private static final String debitar_puntos_segun_cliente = "UPDATE cliente SET puntos = puntos - ? WHERE id_cliente = ?";
	private static final String obtener_puntos_segun_cliente = "SELECT puntos FROM cliente WHERE id_cliente = ?";
	
	private static final String get_listado_ingresos = "SELECT * FROM caja WHERE tipo_movimiento = 'ingresos'";
	private static final String get_listado_egresos = "SELECT * FROM caja WHERE tipo_movimiento = 'egresos'";
	
	private static final String obtener_servicios_por_turnoID = "SELECT sv.nombre AS nombre_servicio"
			+ " FROM turno t"
			+ " INNER JOIN detalle_turno dt ON t.id_turno = dt.id_turno"
			+ " INNER JOIN servicio sv ON dt.id_servicio = sv.id_servicio"
			+ " WHERE t.id_turno = ?";
	
	private static final String get_detalle_ingresos_por_sucursalId = "SELECT c.fecha AS fecha_ingreso,"
			+ " m.nombre AS nombre_sucursal, "
			+ " u.nombre AS nombre_empleado, "
			+ " u.apellido AS apellido_empleado, "
			+ "c2.nombre AS nombre_cliente, "
			+ "c2.apellido AS apellido_cliente, "
			+ "c.monto AS importe "
			+ "FROM caja c "
			+ "INNER JOIN sucursal m ON c.id_sucursal = m.id_sucursal "
			+ "INNER JOIN turno t ON c.id_turno = t.id_turno "
			+ "INNER JOIN cliente c2 ON t.id_cliente = c2.id_cliente "
			+ "INNER JOIN usuario u ON c.id_usuario = u.id_usuario "
			+ "WHERE c.tipo_movimiento = 'ingresos' AND m.id_sucursal = ?";
	
	private static final String get_detalle_ingresos_globales = "SELECT c.fecha AS fecha_ingreso,"
			+ " m.nombre AS nombre_sucursal, "
			+ "u.nombre AS nombre_empleado, "
			+ "u.apellido AS apellido_empleado, "
			+ "c2.nombre AS nombre_cliente, "
			+ "c2.apellido AS apellido_cliente, "
			+ "c.monto AS importe "
			+ "FROM caja c "
			+ "INNER JOIN sucursal m ON c.id_sucursal = m.id_sucursal "
			+ "INNER JOIN turno t ON c.id_turno = t.id_turno "
			+ "INNER JOIN cliente c2 ON t.id_cliente = c2.id_cliente "
			+ "INNER JOIN usuario u ON c.id_usuario = u.id_usuario "
			+ "WHERE c.tipo_movimiento = 'ingresos'";
	
	private static final String get_detalle_egresos_por_sucursalId = "SELECT c.fecha AS fecha_egreso, "
			+ "m.nombre as nombre_sucursal, "
			+ "u.nombre AS nombre_empleado, "
			+ "u.apellido AS apellido_empleado, "
			+ "c.monto AS importe, "
			+ "c.detalle AS concepto FROM caja c "
			+ "INNER JOIN sucursal m on c.id_sucursal = m.id_sucursal "
			+ "INNER JOIN usuario u on c.id_usuario = u.id_usuario "
			+ "WHERE c.tipo_movimiento = 'egresos' AND c.id_sucursal = ?";
	
	private static final String get_detalle_egresos_globales = "SELECT c.fecha AS fecha_egreso, "
			+ "m.nombre as nombre_sucursal, "
			+ "u.nombre AS nombre_empleado, "
			+ "u.apellido AS apellido_empleado, "
			+ "c.monto AS importe, "
			+ "c.detalle AS concepto FROM caja c "
			+ "INNER JOIN sucursal m on c.id_sucursal = m.id_sucursal "
			+ "INNER JOIN usuario u on c.id_usuario = u.id_usuario "
			+ "WHERE c.tipo_movimiento = 'egresos'";
	
	@Override
	public boolean insert(CajaDTO pago)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setDate(1, Date.valueOf(pago.getFecha()));
			statement.setString(2, pago.getTipoMovimiento());
			statement.setString(3, pago.getMedioDePago());
			statement.setInt(4, pago.getIdSucursal());
			statement.setInt(5, pago.getIdEmpleado());
			statement.setInt(6, pago.getIdTurno());
			statement.setString(7, pago.getDetalle());
			statement.setBigDecimal(8, pago.getMonto());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isInsertExitoso;
	}
	
/*	@Override
	public boolean delete(int idPago)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isDeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, idPago);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isDeleteExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isDeleteExitoso;
	}*/
	
	@Override
	public int getUltimoPagoID()
	{
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		int id = 0;
		try {
			statement = conexion.prepareStatement(get_last_pago);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				id = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return id;
	}
	
/*	@Override
	public boolean updateMontoEnTurno(int id_turno, BigDecimal monto)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(update_pago_del_turno);
			statement.setBigDecimal(1, monto);
			statement.setInt(2, id_turno);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isUpdateExitoso;
	}

	@Override
	public boolean updateEstadoTurnoSiEstaPago(int id_turno)
	{
		return false;
	}

	@Override
	public List<CajaDTO> getListaPagosPorID(int id_pago_seleccionado)
	{
		List<CajaDTO> listaDePagos = new ArrayList<CajaDTO>();
		
		Conexion conexion = Conexion.getConexion();
		PreparedStatement statement;
		try {
			statement = conexion.getSQLConexion().prepareStatement(get_lista_pagos_por_id);
			statement.setInt(1, id_pago_seleccionado);
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()){
				listaDePagos.add(new CajaDTO(resultSet.getInt(1), resultSet.getDate(2).toLocalDate(), resultSet.getString(3), resultSet.getInt(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getBigDecimal(7)));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listaDePagos;
	}*/

	@Override
	public BigDecimal getTotalesPorSucursalID(int sucursalID, String tipoDeMovimiento)
	{
		BigDecimal montoTotal = BigDecimal.ZERO;
		
		Conexion conexion = Conexion.getConexion();
		PreparedStatement statement;
		try {
			statement = conexion.getSQLConexion().prepareStatement(get_totales_caja_segun_tipo_movimiento);
			statement.setInt(1, sucursalID);
			
			if (tipoDeMovimiento.equals("INGRESOS")) {
				statement.setString(2, "ingresos");
			} else if (tipoDeMovimiento.equals("EGRESOS")){
				statement.setString(2, "egresos");
			}
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()){
				if (resultSet.getString("montoTotal") != null) {
					montoTotal = new BigDecimal(resultSet.getString("montoTotal"));
				}
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return montoTotal;
	}

	@Override
	public BigDecimal getSaldosDeSucursales(String tipoDeMovimiento)
	{
		BigDecimal montoTotal = BigDecimal.ZERO;
		
		Conexion conexion = Conexion.getConexion();
		PreparedStatement statement;
		try {
			statement = conexion.getSQLConexion().prepareStatement(get_totales_sucursal_segun_tipo_movimiento);
			
			if (tipoDeMovimiento.equals("INGRESOS")) {
				statement.setString(1, "ingresos");
			} else if (tipoDeMovimiento.equals("EGRESOS")){
				statement.setString(1, "egresos");
			}
			
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()){
				if (resultSet.getString("montoTotal") != null) {
					montoTotal = new BigDecimal(resultSet.getString("montoTotal"));
				}
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return montoTotal;
	}
	
	@Override
	public List<IngresoCaja> getDetalleIngresosPorSucursalID(int sucursalID)
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<IngresoCaja> listadoDeIngresos = new ArrayList<IngresoCaja>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_detalle_ingresos_por_sucursalId);
			statement.setInt(1, sucursalID);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha_ingreso") != null) {
						listadoDeIngresos.add(new IngresoCaja(resultSet.getString("fecha_ingreso"), resultSet.getString("nombre_sucursal"), resultSet.getString("nombre_empleado"), resultSet.getString("apellido_empleado"), resultSet.getString("nombre_cliente"), resultSet.getString("apellido_cliente"), resultSet.getString("importe")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los ingresos de la BBDD");
		}
		return listadoDeIngresos;
	}

	@Override
	public List<IngresoCaja> getDetalleGlobalDeIngresos()
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<IngresoCaja> listadoDeIngresos = new ArrayList<IngresoCaja>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_detalle_ingresos_globales);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha_ingreso") != null) {
						listadoDeIngresos.add(new IngresoCaja(resultSet.getString("fecha_ingreso"), resultSet.getString("nombre_sucursal"), resultSet.getString("nombre_empleado"), resultSet.getString("apellido_empleado"), resultSet.getString("nombre_cliente"), resultSet.getString("apellido_cliente"), resultSet.getString("importe")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los ingresos de la BBDD");
		}
		return listadoDeIngresos;
	}

	@Override
	public List<EgresoCaja> getDetalleEgresosPorSucursalID(int sucursalID)
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<EgresoCaja> listadoDeEgresos = new ArrayList<EgresoCaja>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_detalle_egresos_por_sucursalId);
			statement.setInt(1, sucursalID);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha_egreso") != null) {
						listadoDeEgresos.add(new EgresoCaja(resultSet.getString("fecha_egreso"), resultSet.getString("nombre_sucursal"), resultSet.getString("nombre_empleado"), resultSet.getString("apellido_empleado"), resultSet.getString("importe"), resultSet.getString("concepto")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los ingresos de la BBDD");
		}
		return listadoDeEgresos;
	}

	@Override
	public List<EgresoCaja> getDetalleGlobalDeEgresos()
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<EgresoCaja> listadoDeEgresos = new ArrayList<EgresoCaja>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_detalle_egresos_globales);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha_egreso") != null) {
						listadoDeEgresos.add(new EgresoCaja(resultSet.getString("fecha_egreso"), resultSet.getString("nombre_sucursal"), resultSet.getString("nombre_empleado"), resultSet.getString("apellido_empleado"), resultSet.getString("importe"), resultSet.getString("concepto")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los ingresos de la BBDD");
		}
		return listadoDeEgresos;
	}

	@Override
	public boolean actualizarValorPagadoEnTurno(int idTurno, BigDecimal montoARegistrar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(update_pago_del_turno);
			statement.setBigDecimal(1, montoARegistrar);
			statement.setInt(2, idTurno);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isUpdateExitoso;
	}

	@Override
	public boolean acreditarPuntosPorCliente(int idCliente, int puntos)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(acreditar_puntos_segun_cliente);
			statement.setInt(1, puntos);
			statement.setInt(2, idCliente);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isUpdateExitoso;
	}

	@Override
	public int obtenerPuntosActuales(int idCliente)
	{
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		int puntos = 0;
		try {
			statement = conexion.prepareStatement(obtener_puntos_segun_cliente);
			statement.setInt(1, idCliente);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				puntos = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return puntos;
	}

	@Override
	public List<String> getServiciosPorTurnoID(int idTurno)
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		
		ArrayList<String> listadoDeServicios = new ArrayList<String>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(obtener_servicios_por_turnoID);
			statement.setInt(1, idTurno);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("nombre_servicio") != null) {
						listadoDeServicios.add(resultSet.getString("nombre_servicio"));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los servicios de la BBDD");
		}
		
		// quitamos los servicios repetidos antes de devolver la lista.
		LinkedHashSet<String> hashSet = new LinkedHashSet<>(listadoDeServicios);
		listadoDeServicios = new ArrayList<String>(hashSet);
		
		return listadoDeServicios;
	}

	@Override
	public List<CajaDTO> getListadoDeIngresos()
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<CajaDTO> listadoDeIngresos = new ArrayList<CajaDTO>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_listado_ingresos);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha") != null) {
						listadoDeIngresos.add(new CajaDTO(resultSet.getInt("id_caja"), resultSet.getDate("fecha").toLocalDate(), resultSet.getString("tipo_movimiento"), resultSet.getString("medio_de_pago"), resultSet.getInt("id_sucursal"), resultSet.getInt("id_usuario"), resultSet.getInt("id_turno"), resultSet.getString("detalle"), resultSet.getBigDecimal("monto")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los ingresos de la BBDD");
		}
		return listadoDeIngresos;
	}

	@Override
	public List<CajaDTO> getListadoDeEgresos()
	{
		java.sql.PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<CajaDTO> listadoDeEgresos = new ArrayList<CajaDTO>();
		try {
			Conexion conexion = Conexion.getConexion();
			statement = conexion.getSQLConexion().prepareStatement(get_listado_egresos);
			
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
					if (resultSet.getString("fecha") != null) {
						listadoDeEgresos.add(new CajaDTO(resultSet.getInt("id_caja"), resultSet.getDate("fecha").toLocalDate(), resultSet.getString("tipo_movimiento"), resultSet.getString("medio_de_pago"), resultSet.getInt("id_sucursal"), resultSet.getInt("id_usuario"), resultSet.getInt("id_turno"), resultSet.getString("detalle"), resultSet.getBigDecimal("monto")));
					}
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al recuperar los egresos de la BBDD");
		}
		return listadoDeEgresos;
	}

	@Override
	public boolean descontarPuntosPorCliente(int idCliente, int puntos)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(debitar_puntos_segun_cliente);
			statement.setInt(1, puntos);
			statement.setInt(2, idCliente);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		} catch (SQLException e) {e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) { e1.printStackTrace(); }
		}
		return isUpdateExitoso;
	}
}