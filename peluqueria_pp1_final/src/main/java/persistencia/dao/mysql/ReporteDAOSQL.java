package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ReporteDAO;
import presentacion.reportes.objetosReporte.ObjetoReporteResumen;

public class ReporteDAOSQL implements ReporteDAO {
	
	private static final String sumatoriaSucu = "select sum(monto) from caja where id_sucursal = ? and fecha >= ? and fecha <= ?  and tipo_movimiento = ?;";
	private static final String sumatoria = "select sum(monto) from caja where  fecha >= ? and fecha <= ?  and tipo_movimiento = ?;";
	@Override
	public List<ObjetoReporteResumen> ReporteResumenIE(LocalDate desde, LocalDate hasta, int id_sucursal) {
		
		ObjetoReporteResumen ingresos;
		ObjetoReporteResumen egresos;
		
		List<ObjetoReporteResumen> lista = new ArrayList<ObjetoReporteResumen>();
		if(id_sucursal ==0) {
			float monto = sumatoriaTotal(desde,hasta,"ingresos");
			ingresos = new ObjetoReporteResumen(monto,"ingresos" ,"todas las sucursales", desde.toString(), hasta.toString());
			monto = sumatoriaTotal(desde,hasta,"egresos");
			egresos = new ObjetoReporteResumen(monto,"egresos" ,"todas las sucursales", desde.toString(), hasta.toString());
		}
		else {
			float monto = sumatoriaSucu(desde,hasta,id_sucursal,"ingresos");
			ingresos = new ObjetoReporteResumen(monto,"ingresos" ,Integer.toString(id_sucursal), desde.toString(), hasta.toString());
			monto = sumatoriaSucu(desde,hasta,id_sucursal,"egresos");
			egresos = new ObjetoReporteResumen(monto,"egresos" ,Integer.toString(id_sucursal), desde.toString(), hasta.toString());
		}
		lista.add(ingresos);
		lista.add(egresos);
		return lista;
	}
	
	private float sumatoriaSucu(LocalDate desde, LocalDate hasta, int id_sucursal,String tipoMovimiento) {
		float monto = 0;
		Conexion conexion = Conexion.getConexion();
		PreparedStatement statement;
		try {
			statement = conexion.getSQLConexion().prepareStatement(sumatoriaSucu);
			statement.setInt(1, id_sucursal);
			statement.setString(2, desde.toString());
			statement.setString(3, hasta.toString());
			statement.setString(4, tipoMovimiento);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				monto = resultSet.getFloat(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return monto;
	}
	private float sumatoriaTotal(LocalDate desde, LocalDate hasta,String tipoMovimiento) {
		float monto = 0;
		Conexion conexion = Conexion.getConexion();
		PreparedStatement statement;
		try {
			statement = conexion.getSQLConexion().prepareStatement(sumatoria);
			
			statement.setString(1, desde.toString());
			statement.setString(2, hasta.toString());
			statement.setString(3, tipoMovimiento);
			
			
			
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				monto = resultSet.getFloat(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return monto;
	}

}
