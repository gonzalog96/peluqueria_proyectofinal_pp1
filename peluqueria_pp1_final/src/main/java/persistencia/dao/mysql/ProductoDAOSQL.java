package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import dto.ClienteDTO;
import dto.EstadoCliente;
import dto.EstadoProducto;
import dto.ProductoDTO;
import dto.StockDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ClienteDAO;
import persistencia.dao.interfaz.ProductoDAO;

public class ProductoDAOSQL implements ProductoDAO {
	private static final String insert = "INSERT into producto(nombre,rubro,precio,estado) values(?,?,?,?);";
	private static final String readall = "select * from producto;";
	private static final String update = "update producto set nombre = ?, rubro = ?, precio = ?, estado = ? where id_producto = ?;";

	private static final String insertStock = "insert into stock(id_producto,id_sucursal,fecha_modificacion,cantidad) values(?,?,?,?);";
	private static final String selectStock = "select * from stock where id_producto = ? and id_sucursal = ?;";
	private static final String deleteStock = "delete from stock where id_producto = ? and id_sucursal = ?;";

	private static final String ultimoId = "SELECT MAX(id_producto) FROM producto;";

	private static final String readallStock = "SELECT * from producto,stock where producto.id_producto = stock.id_producto;";

	private static final String readallStockSucursal = "SELECT * from producto,stock where producto.id_producto = stock.id_producto and id_sucursal = ?;";

	@Override
	public boolean insert(ProductoDTO producto) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setString(1, producto.getNombre());
			statement.setString(2, producto.getRubro());
			statement.setFloat(3, producto.getPrecio());
			statement.setString(4, producto.getEstado().toString());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;

	}

	@Override
	public List<ProductoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<ProductoDTO> productos = new ArrayList<ProductoDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				productos.add(getProductoDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return productos;
	}

	@Override
	public List<StockDTO> readAllStock() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<StockDTO> stocks = new ArrayList<StockDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readallStock);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				stocks.add(getStockDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stocks;
	}

	@Override
	public List<StockDTO> readAllStock(int id_sucursal) {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<StockDTO> stocks = new ArrayList<StockDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readallStockSucursal);

			statement.setInt(1, id_sucursal);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				stocks.add(getStockDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stocks;
	}

	private StockDTO getStockDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id_producto");
		String nombre = resultSet.getString("nombre");
		String rubro = resultSet.getString("rubro");
		float precio = resultSet.getFloat("precio");
		EstadoProducto estado = EstadoProducto.valueOf(resultSet.getString("estado"));

		ProductoDTO prod = new ProductoDTO(id, nombre, rubro, precio, estado);

		int id_sucursal = resultSet.getInt("id_sucursal");
		int cantidad = resultSet.getInt("cantidad");
		LocalDate fecha = LocalDate.parse(resultSet.getString("fecha_modificacion"));

		return new StockDTO(prod, id_sucursal, cantidad, fecha);
	}

	private ProductoDTO getProductoDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id_producto");
		String nombre = resultSet.getString("nombre");
		String rubro = resultSet.getString("rubro");
		float precio = resultSet.getFloat("precio");
		EstadoProducto estado = EstadoProducto.valueOf(resultSet.getString("estado"));

		return new ProductoDTO(id, nombre, rubro, precio, estado);

	}

	@Override
	public boolean update(ProductoDTO producto) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isupdateExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setString(1, producto.getNombre());
			statement.setString(2, producto.getRubro());
			statement.setFloat(3, producto.getPrecio());
			statement.setString(4, producto.getEstado().toString());
			statement.setInt(5, producto.getId());

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isupdateExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isupdateExitoso;
	}

	public boolean insertStock(ProductoDTO producto, int id_sucursal, int cantidad) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insertStock);
			statement.setInt(1, producto.getId());
			statement.setInt(2, id_sucursal);
			statement.setString(3, LocalDate.now().toString());
			statement.setInt(4, cantidad);

			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;
	}

	public boolean deleteStock(ProductoDTO producto, int id_sucursal) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(deleteStock);
			statement.setInt(1, producto.getId());
			statement.setInt(2, id_sucursal);
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isInsertExitoso;
	}

	@Override
	public boolean updateStock(ProductoDTO producto, int id_sucursal, int cantidad) {
		boolean ret = deleteStock(producto, id_sucursal);
		ret = insertStock(producto, id_sucursal, cantidad);
		return ret;
	}

	@Override
	public int obtenerStock(ProductoDTO producto, int id_sucursal) {
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		int id = 0;
		try {
			statement = conexion.prepareStatement(selectStock);
			statement.setInt(1, producto.getId());
			statement.setInt(2, id_sucursal);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				id = resultSet.getInt(3);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return id;
	}

	public int getUltimoId() {
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		int id = 0;
		try {
			statement = conexion.prepareStatement(ultimoId);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				id = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return id;
	}
}
