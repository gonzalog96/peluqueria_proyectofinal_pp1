package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import persistencia.propiedades.Propiedad;

public class Conexion {
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);
	private String direccion;
	private String puerto;
	private String dbname;
	private String user;
	private String pass;

	private Conexion() throws Exception {
		try {
			this.direccion = Propiedad.load("direccion","propiedades/config.properties");
			this.puerto = Propiedad.load("puerto","propiedades/config.properties");
			this.dbname = Propiedad.load("dbname","propiedades/config.properties");
			this.user = Propiedad.load("user","propiedades/config.properties");
			this.pass = Propiedad.load("pass","propiedades/config.properties");
			
			System.out.println(direccion);
			System.out.println(puerto);
			System.out.println(dbname);
			System.out.println(user);
			System.out.println(pass);
//			
//			this.direccion = "localhost";
//			this.puerto = "3306";
//			this.dbname = "peluqueria";
//			this.user = "root";
//			this.pass = "root";

			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://" + direccion + ":" + puerto + "/" + dbname,
					user, pass);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
		} catch (Exception e) {
			log.error("Conexión fallida", e);
		}
	}

	public static Conexion getConexion() {
		if (instancia == null) {
			try {
				instancia = new Conexion();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instancia;
	}

	public Connection getSQLConexion() {
		return this.connection;
	}

	public void cerrarConexion() {
		try {
			this.connection.close();
			log.info("Conexion cerrada");
		} catch (SQLException e) {
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getPuerto() {
		return puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	//////////////////////AGREGADO PARA PROBAR SI UNA BBDD EXISTE/////////////////////////////////
	public boolean dbExists(String direccion, String db, String puerto, String user, String pass) {
        boolean exist = false;
        try {
            Connection conn = null;
            Statement st = null;
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + direccion + ":" + puerto + "/" + dbname,
					user, pass);
            st = conn.createStatement();
            String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + db + "'";
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                //JOptionPane.showMessageDialog(main, "La base de datos existe.");
                exist = true;
            }
        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(main, "La base de datos no existe.");
            exist = false;
        }
        return exist;
    }
	/////////////////////////////////////////////////////////////////////////////////////////////////
}
