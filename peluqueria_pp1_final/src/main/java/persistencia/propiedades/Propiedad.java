package persistencia.propiedades;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

public class Propiedad {
	public static Properties config = new Properties();
	public static InputStream configInput = null;
	public static OutputStream configOutput = null;

	public static String load(String propiedad, String direccion) {
		String valor = null;
		try {
			configInput = new FileInputStream(direccion);
			config.load(configInput);
			valor = config.getProperty(propiedad);

		} catch (Exception e) {
			System.out.println("La propiedad y/o la dirección no existe");
		}
		return valor;
	}

	public static boolean setPropertyValue(String property, String value, String direccion) {
		boolean exito = false;
		try {
			configOutput = new FileOutputStream(direccion);
			config.setProperty(property, value);
			config.store(new FileWriter(direccion), "Actualiza el archivo properties indicado en direccion");
			exito = true;
		} catch (Exception e) {
			exito = false;
			System.out.println("Error al guardar la configuración!");
		}
		
		return exito;
	}
}
