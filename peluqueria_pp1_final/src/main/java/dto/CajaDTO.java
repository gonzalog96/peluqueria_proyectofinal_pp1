package dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CajaDTO
{
	private int id;
	private LocalDate fecha;
	private String tipoMovimiento;
	private String medioDePago;
	
	private int idSucursal;
	private int idTurno;
	private int idEmpleado;
	
	private String detalle;
	private BigDecimal monto;
	
	public CajaDTO(int id, LocalDate fecha, String tipoMovimiento, String medioDePago, int idSucursal, int idTurno, int idEmpleado, String detalle, BigDecimal monto)
	{
		this.id = id;
		this.fecha = fecha;
		this.tipoMovimiento = tipoMovimiento;
		this.medioDePago = medioDePago;
		this.idSucursal = idSucursal;
		this.idTurno = idTurno;
		this.idEmpleado = idEmpleado;
		this.detalle = detalle;
		this.monto = monto;
	}
	
	public CajaDTO(LocalDate fecha, String tipoMovimiento, String medioDePago, int idSucursal, int idTurno, int idEmpleado, String detalle, BigDecimal monto)
	{
		this.fecha = fecha;
		this.tipoMovimiento = tipoMovimiento;
		this.medioDePago = medioDePago;
		this.idSucursal = idSucursal;
		this.idTurno = idTurno;
		this.idEmpleado = idEmpleado;
		this.detalle = detalle;
		this.monto = monto;
	}

	public int getId()
	{
		return id;
	}
	public LocalDate getFecha()
	{
		return fecha;
	}
	public String getTipoMovimiento()
	{
		return tipoMovimiento;
	}
	public String getMedioDePago()
	{
		return medioDePago;
	}
	public void setMedioDePago(String medioDePago)
	{
		this.medioDePago = medioDePago;
	}

	public int getIdSucursal()
	{
		return idSucursal;
	}
	public int getIdTurno()
	{
		return idTurno;
	}
	
	public String getDetalle()
	{
		return detalle;
	}
	public BigDecimal getMonto()
	{
		return monto;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public void setFecha(LocalDate fecha)
	{
		this.fecha = fecha;
	}
	public void setTipoMovimiento(String tipoMovimiento)
	{
		this.tipoMovimiento = tipoMovimiento;
	}
	public void setIdSucursal(int idSucursal)
	{
		this.idSucursal = idSucursal;
	}
	public void setIdTurno(int idTurno)
	{
		this.idTurno = idTurno;
	}
	public int getIdEmpleado()
	{
		return idEmpleado;
	}
	public void setIdEmpleado(int idUsuario)
	{
		this.idEmpleado = idUsuario;
	}
	public void setDetalle(String detalle)
	{
		this.detalle = detalle;
	}
	public void setMonto(BigDecimal monto)
	{
		this.monto = monto;
	}
	public String getString()
	{
		return medioDePago;
	}
	public void setString(String medioDePago)
	{
		this.medioDePago = medioDePago;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((detalle == null) ? 0 : detalle.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + id;
		result = prime * result + idEmpleado;
		result = prime * result + idSucursal;
		result = prime * result + idTurno;
		result = prime * result + ((medioDePago == null) ? 0 : medioDePago.hashCode());
		result = prime * result + ((monto == null) ? 0 : monto.hashCode());
		result = prime * result + ((tipoMovimiento == null) ? 0 : tipoMovimiento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CajaDTO other = (CajaDTO) obj;
		if (detalle == null)
		{
			if (other.detalle != null)
				return false;
		} else if (!detalle.equals(other.detalle))
			return false;
		if (fecha == null)
		{
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (id != other.id)
			return false;
		if (idEmpleado != other.idEmpleado)
			return false;
		if (idSucursal != other.idSucursal)
			return false;
		if (idTurno != other.idTurno)
			return false;
		if (medioDePago == null)
		{
			if (other.medioDePago != null)
				return false;
		} else if (!medioDePago.equals(other.medioDePago))
			return false;
		if (monto == null)
		{
			if (other.monto != null)
				return false;
		} else if (!monto.equals(other.monto))
			return false;
		if (tipoMovimiento == null)
		{
			if (other.tipoMovimiento != null)
				return false;
		} else if (!tipoMovimiento.equals(other.tipoMovimiento))
			return false;
		return true;
	}
}