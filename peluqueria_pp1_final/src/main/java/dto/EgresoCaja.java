package dto;

public class EgresoCaja
{
	private int id;
	private String fecha_egreso;
	private String nombreSucursal;
	private String nombreEmpleado;
	private String apellidoEmpleado;
	private String importe;
	private String concepto;
	
	public EgresoCaja(int id, String fecha_egreso, String nombreSucursal, String nombreEmpleado, String apellidoEmpleado, String importe, String concepto)
	{
		this.id = id;
		this.fecha_egreso = fecha_egreso;
		this.nombreSucursal = nombreSucursal;
		this.nombreEmpleado = nombreEmpleado;
		this.apellidoEmpleado = apellidoEmpleado;
		this.importe = importe;
		this.concepto = concepto;
	}
	
	public EgresoCaja(String fecha_egreso, String nombreSucursal, String nombreEmpleado, String apellidoEmpleado, String importe, String concepto)
	{
		this.fecha_egreso = fecha_egreso;
		this.nombreSucursal = nombreSucursal;
		this.nombreEmpleado = nombreEmpleado;
		this.apellidoEmpleado = apellidoEmpleado;
		this.importe = importe;
		this.concepto = concepto;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getFecha_egreso()
	{
		return fecha_egreso;
	}

	public void setFecha_egreso(String fecha_egreso)
	{
		this.fecha_egreso = fecha_egreso;
	}

	public String getNombreSucursal()
	{
		return nombreSucursal;
	}

	public String getNombreEmpleado()
	{
		return nombreEmpleado;
	}
	
	public String getApellidoEmpleado()
	{
		return apellidoEmpleado;
	}

	public String getImporte()
	{
		return importe;
	}

	public String getConcepto()
	{
		return concepto;
	}

	public void setNombreSucursal(String nombreSucursal)
	{
		this.nombreSucursal = nombreSucursal;
	}

	public void setImporte(String importe)
	{
		this.importe = importe;
	}

	public void setConcepto(String concepto)
	{
		this.concepto = concepto;
	}
	
	public String getNombreApellidoEmpleado() {
		return this.nombreEmpleado + " " + this.apellidoEmpleado;
	}
}