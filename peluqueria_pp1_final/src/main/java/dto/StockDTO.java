package dto;

import java.time.LocalDate;


public class StockDTO {
	private ProductoDTO producto;
	private int idSucursal;
	private int cantidad;
	private LocalDate fechaUltimaModificacion;
	
	
	
	public StockDTO(ProductoDTO producto, int idSucursal, int cantidad, LocalDate fechaUltimaModificacion) {
		super();
		this.producto = producto;
		this.idSucursal = idSucursal;
		this.cantidad = cantidad;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}



	public ProductoDTO getProducto() {
		return producto;
	}



	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}



	public int getIdSucursal() {
		return idSucursal;
	}



	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}



	public int getCantidad() {
		return cantidad;
	}



	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}



	public LocalDate getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}



	public void setFechaUltimaModificacion(LocalDate fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cantidad;
		result = prime * result + ((fechaUltimaModificacion == null) ? 0 : fechaUltimaModificacion.hashCode());
		result = prime * result + idSucursal;
		result = prime * result + ((producto == null) ? 0 : producto.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockDTO other = (StockDTO) obj;
		if (cantidad != other.cantidad)
			return false;
		if (fechaUltimaModificacion == null) {
			if (other.fechaUltimaModificacion != null)
				return false;
		} else if (!fechaUltimaModificacion.equals(other.fechaUltimaModificacion))
			return false;
		if (idSucursal != other.idSucursal)
			return false;
		if (producto == null) {
			if (other.producto != null)
				return false;
		} else if (!producto.equals(other.producto))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "StockDTO [producto=" + producto + ", idSucursal=" + idSucursal + ", cantidad=" + cantidad
				+ ", fechaUltimaModificacion=" + fechaUltimaModificacion + "]";
	}
	
	
	
	
	

}
