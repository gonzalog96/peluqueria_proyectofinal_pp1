package dto;

import java.time.LocalDate;

public class ClienteDTO {

	private int id;
	private String nombre;
	private String apellido;
	private String email;
	private String telefono;
	private String dni;
	private EstadoCliente estado;
	private LocalDate ultimaVisita;
	private int puntos;

	public ClienteDTO(int id, String nombre, String apellido, String email, String telefono, String dni,
			EstadoCliente estado, LocalDate ultimaVisita,int puntos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.dni = dni;
		this.estado = estado;
		this.ultimaVisita = ultimaVisita;
		this.puntos = puntos;
	}

	public ClienteDTO(String nombre, String apellido, String email, String telefono, String dni, EstadoCliente estado) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.dni = dni;
		this.estado = estado;
		this.puntos = 0;
	}
	public ClienteDTO(String nombre, String apellido, String email, String telefono, String dni, EstadoCliente estado, LocalDate ultimaVisita) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.dni = dni;
		this.estado = estado;
		this.ultimaVisita = ultimaVisita;
		this.puntos = 0;
	}
	public ClienteDTO(int id,String nombre, String apellido, String email, String telefono, String dni, EstadoCliente estado, LocalDate ultimaVisita) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.telefono = telefono;
		this.dni = dni;
		this.estado = estado;
		this.ultimaVisita = ultimaVisita;
		this.puntos = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public EstadoCliente getEstado() {
		return estado;
	}

	public void setEstado(EstadoCliente estado) {
		this.estado = estado;
	}

	public LocalDate getUltimaVisita() {
		return ultimaVisita;
	}

	public void setUltimaVisita(LocalDate ultimaVisita) {
		this.ultimaVisita = ultimaVisita;
	}
	

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}
	
	

	@Override
	public String toString() {
		return "ClienteDTO [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email
				+ ", telefono=" + telefono + ", dni=" + dni + ", estado=" + estado + ", ultimaVisita=" + ultimaVisita
				+ ", puntos=" + puntos + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((estado == null) ? 0 : estado.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		result = prime * result + ((ultimaVisita == null) ? 0 : ultimaVisita.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteDTO other = (ClienteDTO) obj;
		if (apellido == null) {
			if (other.apellido != null)
				return false;
		} else if (!apellido.equals(other.apellido))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (estado != other.estado)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		if (ultimaVisita == null) {
			if (other.ultimaVisita != null)
				return false;
		} else if (!ultimaVisita.equals(other.ultimaVisita))
			return false;
		return true;
	}
	
}
