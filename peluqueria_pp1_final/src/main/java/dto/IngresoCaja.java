package dto;

public class IngresoCaja
{
	private int id;
	private String fechaIngreso;
	private String nombreSucursal;
	private String nombreEmpleado;
	private String apellidoEmpleado;
	private String nombreCliente;
	private String apellidoCliente;
	private String importeTotal;
	
	public IngresoCaja(int id, String fechaIngreso, String nombreSucursal, String nombreEmpleado, String apellidoEmpleado, String nombreCliente, String apellidoCliente, String importeTotal)
	{
		this.id = id;
		this.fechaIngreso = fechaIngreso;
		this.nombreSucursal = nombreSucursal;
		this.nombreEmpleado = nombreEmpleado;
		this.apellidoEmpleado = apellidoEmpleado;
		this.nombreCliente = nombreCliente;
		this.apellidoCliente = apellidoCliente;
		this.importeTotal = importeTotal;
	}
	
	public IngresoCaja(String fechaIngreso, String nombreSucursal, String nombreEmpleado, String apellidoEmpleado, String nombreCliente, String apellidoCliente, String importeTotal)
	{
		this.fechaIngreso = fechaIngreso;
		this.nombreSucursal = nombreSucursal;
		this.nombreEmpleado = nombreEmpleado;
		this.apellidoEmpleado = apellidoEmpleado;
		this.nombreCliente = nombreCliente;
		this.apellidoCliente = apellidoCliente;
		this.importeTotal = importeTotal;
	}
	
	public int getId()
	{
		return id;
	}
	public String getFechaIngreso()
	{
		return fechaIngreso;
	}
	public String getNombreSucursal()
	{
		return nombreSucursal;
	}
	public String getNombreEmpleado()
	{
		return nombreEmpleado;
	}
	public String getNombreCliente()
	{
		return nombreCliente;
	}
	public String getApellidoCliente()
	{
		return apellidoCliente;
	}
	public String getApellidoEmpleado()
	{
		return apellidoEmpleado;
	}

	public String getImporteTotal()
	{
		return importeTotal;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public void setFechaIngreso(String fechaIngreso)
	{
		this.fechaIngreso = fechaIngreso;
	}
	public void setNombreSucursal(String nombreSucursal)
	{
		this.nombreSucursal = nombreSucursal;
	}
	public void setNombreCliente(String nombreCliente)
	{
		this.nombreCliente = nombreCliente;
	}
	public void setApellidoCliente(String apellidoCliente)
	{
		this.apellidoCliente = apellidoCliente;
	}
	public void setImporteTotal(String importeTotal)
	{
		this.importeTotal = importeTotal;
	}
	
	public String getNombreApellido() {
		return this.nombreCliente + " " + this.apellidoCliente;
	}
	public String getNombreApellidoEmpleado() {
		return this.nombreEmpleado + " " + this.apellidoEmpleado;
	}
}