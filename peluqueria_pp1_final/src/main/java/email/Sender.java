package email;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Sender {
	String remitente;
	String password;
	Properties props;
	Session session;
	
	public Sender() {
		remitente = "proyectoprofesional19@gmail.com";
		password = "ungs2019";
		props = System.getProperties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.user", remitente);
	    props.put("mail.smtp.clave", password);
	    props.put("mail.smtp.auth", "true");  
	    props.put("mail.smtp.starttls.enable", "true"); 
	    props.put("mail.smtp.port", "587");
	    session = Session.getDefaultInstance(props);
	}
	
	public void enviarEmail(String destinatario, String asunto, String cuerpo) {
	    MimeMessage message = new MimeMessage(session);

	    try {
	        message.setFrom(new InternetAddress(remitente));
	        message.addRecipients(Message.RecipientType.TO, destinatario);
	        message.setSubject(asunto);
	        message.setText(cuerpo);
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com", remitente, password);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();
	    }
	}
	
	public void enviarEmailMasivo(List<String> destinatarios, String asunto, String cuerpo) {
		  MimeMessage message = new MimeMessage(session);

		    try {
		        message.setFrom(new InternetAddress(remitente));
		        for (String s : destinatarios) {
		        	message.addRecipients(Message.RecipientType.TO, s);
		        }
		        message.setSubject(asunto);
		        message.setText(cuerpo);
		        Transport transport = session.getTransport("smtp");
		        transport.connect("smtp.gmail.com", remitente, password);
		        transport.sendMessage(message, message.getAllRecipients());
		        transport.close();
		        System.out.println("enviado con exito");
		    }
		    catch (MessagingException me) {
		        me.printStackTrace();
		    }
	}

}
