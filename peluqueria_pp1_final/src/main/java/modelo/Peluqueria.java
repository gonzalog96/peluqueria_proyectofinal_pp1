package modelo;

import java.time.LocalDate;
import java.math.BigDecimal;
import java.util.List;

import dto.CajaDTO;
import dto.ClienteDTO;
import dto.EgresoCaja;
import dto.EstadoPromocion;
import dto.IngresoCaja;
import dto.ProductoDTO;
import dto.ProfesionalDTO;
import dto.PromocionDTO;
import dto.ServicioDTO;
import dto.StockDTO;
import dto.SucursalDTO;
import dto.TurnoDTO;
import dto.UsuarioDTO;
import persistencia.dao.interfaz.CajaDAO;
import persistencia.dao.interfaz.ClienteDAO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.ProductoDAO;
import persistencia.dao.interfaz.ProfesionalDAO;
import persistencia.dao.interfaz.PromocionDAO;
import persistencia.dao.interfaz.ReporteDAO;
import persistencia.dao.interfaz.ServicioDAO;
import persistencia.dao.interfaz.SucursalDAO;
import persistencia.dao.interfaz.TurnoDAO;
import persistencia.dao.interfaz.UsuarioDAO;
import presentacion.reportes.objetosReporte.ObjetoReporteResumen;

public class Peluqueria {
	private ProfesionalDAO profesional;
	private ClienteDAO cliente;
	private TurnoDAO turno;
	private ServicioDAO servicio;
	private SucursalDAO sucursal;
	private UsuarioDAO usuario;
	private PromocionDAO promocion;
	private CajaDAO pago;
	private ProductoDAO producto;
	private ReporteDAO reporte;

	public Peluqueria(DAOAbstractFactory metodo_persistencia) {
		this.turno = metodo_persistencia.createTurnoDAO();
		this.profesional = metodo_persistencia.createProfesionalDAO();
		this.cliente = metodo_persistencia.createClienteDAO();
		this.servicio = metodo_persistencia.createServicioDAO();
		this.sucursal = metodo_persistencia.createSucursalDAO();
		this.usuario = metodo_persistencia.createUsuarioDAO();
		this.promocion = metodo_persistencia.createPromocionDAO();
		this.pago = metodo_persistencia.createCajaDAO();
		this.producto = metodo_persistencia.createProductoDAO();
		this.reporte = metodo_persistencia.createReporteDAO();
	}

	///////////////////////////////////////////////////////////////////

	public void agregarTurno(TurnoDTO nuevoTurno) {
		this.turno.insert(nuevoTurno);
	}

	public void borrarTurno(TurnoDTO turno_a_eliminar) {
		this.turno.delete(turno_a_eliminar);
	}
	public List<TurnoDTO> obtenerTurnosDia(int id_sucursal,LocalDate fecha) {
		return this.turno.getTurnosDelDia(id_sucursal, fecha);
	}

	public boolean actualizarTurno(TurnoDTO turno_a_actualizar) {
		return this.turno.update(turno_a_actualizar);
	}

	public List<TurnoDTO> obtenerTurnos() {
		List<TurnoDTO> lista = null;
		try {
			lista = this.turno.readAll();
		} catch (Exception e) {
			throw new RuntimeException("Error al buscar turnos en la clase Peluqueria");
		}
		return lista;
	}

	public List<TurnoDTO> obtenerTurnos(int id_sucursal) {
		List<TurnoDTO> lista = null;
		try {
			lista = this.turno.readAll(id_sucursal);
		} catch (Exception e) {
			throw new RuntimeException("Error al buscar turnos de la sucursal dada en la clase Peluqueria");
		}
		return lista;
	}
	public List<TurnoDTO> obtenerTurnosFuturo(int id_sucursal, LocalDate fecha){
		return this.turno.readAll(id_sucursal, fecha);
	}

	public TurnoDTO getTurnoDesdeID(int id_turno) {
		return this.turno.obtenerDesdeID(id_turno);
	}

	public boolean cancelarTurno(int id_turno) {
		return this.turno.cancelarTurno(id_turno);
	}
	public List<TurnoDTO> turnosDelDia(LocalDate fecha){
		 
		List<TurnoDTO> turnos = this.turno.getTurnosDelDia(fecha);
		
		System.out.println("-----"+turnos.size());
		
		return turnos;
	
	}
	
	public List<TurnoDTO> turnosCerrados(){
		return this.turno.getCerrados();
	}
	
	public List<TurnoDTO> traeravisos(){
		return this.turno.getAvisos();
	}
	public void updateAviso(int id) {
		this.turno.updateAviso(id);
	}
	
	///////////////////////////////////////////////////////////////////

	public void agregarProfesional(ProfesionalDTO nuevoProfesional) {
		this.profesional.insert(nuevoProfesional);
	}

	public void borrarProfesional(ProfesionalDTO profesional_a_eliminar) {
		this.profesional.delete(profesional_a_eliminar);
	}

	public void actualizarProfesional(ProfesionalDTO profesional_a_actualizar) {
		this.profesional.update(profesional_a_actualizar);
	}

	public List<ProfesionalDTO> obtenerProfesionales() {
		List<ProfesionalDTO> lista = null;
		try {
			lista = this.profesional.readAll();
		} catch (Exception e) {
			throw new RuntimeException("Error al obtener los profesionales en la clase Peluqueria");
		}
		return lista;
	}
	public List<ProfesionalDTO> obtenerProfesionales(int id_sucursal){
		List<ProfesionalDTO> lista = null;
		try {
			lista = this.profesional.readAll(id_sucursal);
		} catch (Exception e) {
			throw new RuntimeException("Error al obtener los profesionales en la clase Peluqueria");
		}
		return lista;
	}
	public ProfesionalDTO getProfesionalDesdeID(int id_profesional_seleccionado) {
		return this.profesional.obtenerDesdeID(id_profesional_seleccionado);
	}

	public ProfesionalDTO buscarProfesional(String cadenaProfesional) {
		return this.profesional.find(cadenaProfesional);
	}

	public ProfesionalDTO buscarProfesional(String cadenaProfesional, int id_sucursal) {
		return this.profesional.find(cadenaProfesional, id_sucursal);
	}

	public ProfesionalDTO buscarProfesional(String cadenaProfesional, int id_sucursal, int id_habilidad) {
		return this.profesional.find(cadenaProfesional, id_sucursal, id_habilidad);
	}
	public boolean atarProfesionalServicio(ServicioDTO servicio,ProfesionalDTO profesional) {
		return this.profesional.atarProfesionaServicio(servicio, profesional);
	}

	public boolean tieneHabilidad(int id_profesional, int id_servicio) {
		return this.profesional.tieneHabilidad(id_profesional, id_servicio);
	}

	///////////////////////////////////////////////////////////////////

	public void agregarCliente(ClienteDTO nuevoCliente) {
		this.cliente.insert(nuevoCliente);
	}

	public void borrarCliente(ClienteDTO clienteAborrar) {
		this.cliente.delete(clienteAborrar);
	}

	public void actualizarCliente(ClienteDTO clienteAactualizar) {
		this.cliente.update(clienteAactualizar);
	}

	public List<ClienteDTO> obtenerClientes() {
		List<ClienteDTO> lista = null;
		try {
			lista = this.cliente.readAll();
		} catch (Exception e) {
			throw new RuntimeException("Error al obtener los clientes de la clase Peluquería");
		}
		return lista;
	}

	public ClienteDTO getClienteDesdeID(int id_cliente_seleccionado) {
		return this.cliente.obtenerDesdeID(id_cliente_seleccionado);
	}

	public List<TurnoDTO> turnosDeuda(ClienteDTO cliente){
		return this.turno.getTurnosDeuda(cliente);
	}
	
	////////////////////////////////////////////////////////////////////

	public void agregarServicio(ServicioDTO nuevoServicio) {
		this.servicio.insert(nuevoServicio);
	}

	public void borrarServicio(ServicioDTO servicioAeliminar) {
		this.servicio.delete(servicioAeliminar);
	}

	public void actualizarServicio(ServicioDTO servicioAeditar) {
		this.servicio.update(servicioAeditar);
	}

	public ClienteDTO buscarCliente(String cadenaCliente) {
		return this.cliente.find(cadenaCliente);
	}

	///////////////////////////////////////////////////////////////////

	public List<ServicioDTO> obtenerServicios() {
		List<ServicioDTO> lista = null;
		try {
			lista = this.servicio.readAll();
		} catch (Exception e) {
			throw new RuntimeException("Error al obtener los servicios de la clase Peluquería");
		}
		return lista;
	}

	public ServicioDTO getServicioDesdeID(int id_servicio_seleccionado) {
		return this.servicio.obtenerDesdeID(id_servicio_seleccionado);
	}

	public ServicioDTO buscarServicio(String cadenaServicio) {
		return this.servicio.find(cadenaServicio);
	}
	
	public List<ServicioDTO> obtenerServiciosDeProfesional(int id_profesional){
		return this.servicio.obtenerDesdeProfesional(id_profesional);
	}
	
	public void cancelarServicio(ServicioDTO servicio) {
		List<TurnoDTO> turnos = this.turno.readAll(LocalDate.now());
		for (TurnoDTO turnoDTO : turnos) {
			if(turnoDTO.tieneServicio(servicio)) {
				System.out.println("hola");
				this.turno.cancelarTurnoAviso(turnoDTO.getId());
			}
		}
		List<PromocionDTO> promos = this.promocion.readall();
		for (PromocionDTO promocionDTO : promos) {
			if(promocionDTO.tiene(servicio)) {
				promocionDTO.setEstado(EstadoPromocion.INACTIVO);
				promocionDTO.setEstado(EstadoPromocion.INACTIVO);
				promocion.update(promocionDTO);
			}
		}
	}

	///////////////////////////////////////////////////////////////////

	public void agregarSucursal(SucursalDTO servicio) {
		this.sucursal.insert(servicio);
	}

	public void borrarSucursal(SucursalDTO servicio) {
		this.sucursal.delete(servicio);
	}

	public void actualizarSucursal(SucursalDTO servicio) {
		this.sucursal.update(servicio);
	}

	public List<SucursalDTO> obtenerSucursales() {
		List<SucursalDTO> lista = null;
		try {
			lista = this.sucursal.readAll();
		} catch (Exception e) {
			throw new RuntimeException("Error al obtener los servicios de la clase Peluquería");
		}
		return lista;
	}

	public SucursalDTO getSucursalDesdeID(int id_sucursal_seleccionado) {
		return this.sucursal.obtenerDesdeID(id_sucursal_seleccionado);
	}

	public SucursalDTO buscarSucursal(String cadenaSucursal) {
		return this.sucursal.find(cadenaSucursal);
	}

	public ServicioDTO buscarServicio(String cadenaServicio, int id_profesional) {
		return this.servicio.find(cadenaServicio, id_profesional);
	}

	/////////////////////////////////////////////////////////////////////

	public UsuarioDTO getUsuario(String usuario, String pass) {
		return this.usuario.getUser(usuario, pass);

	}

	////////////////////////////////////////////////////////////////////////

	public List<PromocionDTO> getPromociones() {
		return promocion.readall();
	}
	
	public int ultimaPromocion() {
		return this.promocion.ultimoID();
	}
	
	public boolean agregarPromocion(PromocionDTO promocion) {
		return this.promocion.insert(promocion);
	}
	
	public boolean actualizarPromocion(PromocionDTO promocion) {
		return this.promocion.update(promocion);
	}
	
	public boolean atarServicioPromocion(int id_servicio, int id_promocion) {
		return this.promocion.insertX(id_servicio, id_promocion);
	}
	////////////////////////////////////////////////////////////////////////////
	public void agregarPago(CajaDTO pago) {
		this.pago.insert(pago);
	}
	
	public BigDecimal getTotalesPorSucursalID(int sucursalID, String tipoDeMovimiento) {
		return this.pago.getTotalesPorSucursalID(sucursalID, tipoDeMovimiento);
	}
	
	public BigDecimal getSaldosDeSucursales(String tipoDeMovimiento) {
		return this.pago.getSaldosDeSucursales(tipoDeMovimiento);
	}
	
	public List<IngresoCaja> getDetalleIngresosPorSucursalID(int sucursalID) {
		return this.pago.getDetalleIngresosPorSucursalID(sucursalID);
	}
	
	public List<IngresoCaja> getDetalleGlobalDeIngresos() {
		return this.pago.getDetalleGlobalDeIngresos();
	}
	
	public List<EgresoCaja> getDetalleEgresosPorSucursalID(int sucursalID) {
		return this.pago.getDetalleEgresosPorSucursalID(sucursalID);
	}
	
	public List<EgresoCaja> getDetalleGlobalDeEgresos() {
		return this.pago.getDetalleGlobalDeEgresos();
	}
	
	///////////////////////////////////////////////////////////////////////////////
	
	public List<ProductoDTO> getProductos(){
		return this.producto.readAll();
	}
	public boolean updateProducto(ProductoDTO producto) {
		return this.producto.update(producto);
	}
	public boolean insertProducto(ProductoDTO producto) {
		return this.producto.insert(producto);
	}
	public int obtenerStock(ProductoDTO producto,int id_sucursal) {
		return this.producto.obtenerStock(producto, id_sucursal);
	}
	public boolean actualizarStock(ProductoDTO producto,int id_sucursal,int cantidad) {
		return this.producto.updateStock(producto, id_sucursal, cantidad);
	}
	public List<StockDTO> obtenerStocks(int id_sucursal){
		return this.producto.readAllStock(id_sucursal);
	}
	public List<StockDTO> obtenerStocks(){
		return this.producto.readAllStock();
	}
	
	///////////////////////////////////////////////////////////////////////////////
	// GONZA // 
	public void actualizarValorPagadoEnTurno(int idTurno, BigDecimal montoARegistrar) {
		this.pago.actualizarValorPagadoEnTurno(idTurno, montoARegistrar);
	}
	
	public void addPuntosSegunCliente(int idCliente, int puntos) {
		this.pago.acreditarPuntosPorCliente(idCliente, puntos);
	}
	
	public List<String> getServiciosPorTurnoID(int idTurno) {
		return this.pago.getServiciosPorTurnoID(idTurno);
	}
	
	public List<CajaDTO> getListadoDeIngresos() {
		return this.pago.getListadoDeIngresos();
	}
	
	public List<CajaDTO> getListadoDeEgresos() {
		return this.pago.getListadoDeEgresos();
	}
	
	public void descontarPuntosPorCliente(int idCliente, int puntos) {
		this.pago.descontarPuntosPorCliente(idCliente, puntos);
	}
	///////////////////////////////////////////////////////////////////////////////

	public List<ObjetoReporteResumen> getDatosReporteResumen(LocalDate desde, LocalDate hasta, int id_sucursal){
		return this.reporte.ReporteResumenIE(desde, hasta, id_sucursal);
	}

}
